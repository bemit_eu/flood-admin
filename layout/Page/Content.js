import React from 'react';
import {createUseStyles} from '../../lib/theme';
import StyleGrid from '../../lib/StyleGrid';
import {LoadingLangScreen} from "../../component/LoadingLangScreen";

const useStyles = createUseStyles({
    wrapper: {
        display: 'block',
        position: 'relative',
        margin: '0 auto auto auto',
        padding: '0  6px',
        width: '100%',
        [StyleGrid.mdUp]: {
            width: '960px',
            padding: 0,
        },
        [StyleGrid.lgUp]: {
            width: '1200px'
        }
    }
});

const Content = (props) => {
    const {loading_lang} = props;
    const classes = useStyles();

    return (
        <React.Fragment>
            <LoadingLangScreen loading={loading_lang}/>
            <div className={classes.wrapper}>
                {props.children}
            </div>
        </React.Fragment>
    );
};

export default (Content);
