import React from 'react';
import {
    withRouter
} from 'react-router-dom';
import {withNamespaces} from 'react-i18next';
import LoginForm from '@bemit/flood-admin/component/User/LoginForm'
import {i18n} from "@bemit/flood-admin/lib/i18n";

class Auth extends React.Component {
    render() {
        const {t, history} = this.props;

        return (
            <React.Fragment>
                <h1 style={{textAlign: 'center'}}>{t('title')}</h1>
                <LoginForm onLoggedIn={() => {
                    history.push('/' + i18n.languages[0] + '/dashboard');
                }}/>
            </React.Fragment>
        );
    }
}

export default withNamespaces('page--auth')(withRouter(Auth));
