import React from 'react';
import {
    Route
} from 'react-router-dom';

import Header from './Header';
import Sidebar from './ControlPanel/Sidebar';
import Content from './ControlPanel/Content';

import {withStyles, withTheme} from '../lib/theme';
import RouteCascaded from '../lib/RouteCascaded';
import StyleGrid from "../lib/StyleGrid";
import {i18nEvents} from "../lib/i18n";
import {NavMain} from "../component/NavMain";
import {SidebarsProvider} from "../component/Sidebars";

const styles = {
    center: {
        display: 'flex',
        height: '100%',
        width: '100%',
        flexGrow: 2,
        flexShrink: 1,
        flexDirection: 'column',
        overflowY: 'auto',
        overflowX: 'hidden',
        [StyleGrid.smUp]: {
            overflow: 'auto',
            flexDirection: 'row',
        },
    },
    centerInner: {
        display: 'flex',
        height: '100%',
        width: '200vw',
        flexGrow: 2,
        flexShrink: 1,
        flexDirection: 'row',
        overflow: 'auto',
        scrollBehavior: 'smooth',
        marginTop: 0,
        transition: 'transform 0.2s ease-out',
        [StyleGrid.smx]: {
            transform: 'translateX(0)',
        },
        [`@media (min-width: 560px)`]: {
            width: '150vw',
        },
        [StyleGrid.smUp]: {
            flexDirection: 'row',
        },
    },
    centerInnerActive: {
        transform: 'translateX(calc(-100vw))',
        [`@media (min-width: 560px)`]: {
            transform: 'translateX(calc(-50vw))',
        },
        [StyleGrid.smUp]: {
            transform: 'none',
        }
    },
    sidebarWrapper: {
        display: 'flex',
        height: '100%',
        width: '50%',
        flexShrink: 0,
        flexDirection: 'row',
        overflow: 'auto',
        [`@media (min-width: 560px)`]: {
            width: '33.5%',
            '& > *:first-child': {
                marginLeft: 'auto'
            }
        },
        [StyleGrid.smUp]: {
            width: 'auto',
            flexShrink: 1,
            marginTop: 0,
            flexDirection: 'row',
        },
    },
    sidebarToggle: {
        height: '100%',
        width: '10px',
        margin: '0 -5px',
        fontSize: 0,
        textIndent: '-999em',
        lineHeight: 0,
        flexShrink: 0,
        zIndex: 1,
        transition: 'background 0.3s linear',
        [StyleGrid.smUp]: {
            display: 'none',
        }
    }
};

const MainRouteItem = (props) => (
    <React.Fragment>
        {props.route.sidebar ? <div className={props.classes.sidebarWrapper}>
            <RouteCascaded {...props.route} component={props.route.sidebar}
                           childProps={{toggleSidebar: props.toggleSidebar}}/>
        </div> : null}

        {props.route.sidebar ?
            <button style={{
                background: props.theme.name === 'dark' ?
                    props.mobileWouldSwitch ? '#1d2b1d' : '#111115' :
                    props.mobileWouldSwitch ? '#aabda2' : '#b8b8b8'
            }}
                    id='control-panel--sidebar--toggle'
                    className={props.classes.sidebarToggle}
                    onClick={props.toggleSidebar}
            >open sidebars
            </button> : null}

        <Content loading_lang={props.loading_lang} route={props.route} sidebarExists={!!props.route.sidebar} sidebarOpen={!props.mobileContentActive}/>
    </React.Fragment>
);

class FactoryControlPanel extends React.PureComponent {
    state = {
        loading_lang: false,
        mobileContentActive: true,
        mobileWouldSwitch: false,
    };

    constructor(props) {
        super(props);

        this.centerInner = null;
        this.dragStart = false;
        this.lastLeft = false;
        this.lastPosX = false;

        this.evt_id = {
            languageChanging: null,
            languageChanged: null
        };
    }

    componentWillUnmount() {
        i18nEvents.off('languageChanging', this.evt_id.languageChanging);
        i18nEvents.off('languageChanged', this.evt_id.languageChanged);
    }

    componentDidMount() {
        this.evt_id.languageChanging = i18nEvents.on('languageChanging', (() => {
            this.setState({loading_lang: true});
        }));
        this.evt_id.languageChanged = i18nEvents.on('languageChanged', (() => {
            this.setState({loading_lang: false});
        }));

        this.centerInner.addEventListener('mousedown', (e) => {
            if(1 === e.which || 2 === e.which ||
                1 === e.key || 2 === e.key ||
                1 === e.keyIdentifier || 2 === e.keyIdentifier ||
                1 === e.keyCode || 2 === e.keyCode) {
                this.startDrag(e.pageX, e);
            }
        });
        this.centerInner.addEventListener('mousemove', (e) => {
            this.dragging(e.pageX, e);
        });
        this.centerInner.addEventListener('mouseup', this.endDrag.bind(this));
        this.centerInner.addEventListener('mouseleave', this.endDrag.bind(this));

        this.centerInner.addEventListener('touchstart', (e) => {
            this.startDrag(e.touches[0].pageX, e);
        });
        this.centerInner.addEventListener('touchmove', (e) => {
            this.dragging(e.touches[0].pageX, e);
        });
        this.centerInner.addEventListener('touchend', this.endDrag.bind(this));
        this.centerInner.addEventListener('touchcancel', this.endDrag.bind(this));
    }

    startDrag(pageX, e) {
        const toggle = this.centerInner.querySelector('#control-panel--sidebar--toggle');
        if(!toggle) {
            return;
        }

        let diff = false;
        if(!this.state.mobileContentActive) {
            diff = (pageX - toggle.getBoundingClientRect().x - 5) * -1;
        } else {
            diff = pageX - toggle.getBoundingClientRect().x - 5;
        }
        if(
            (window.innerWidth > 380 && (30 > diff && diff > -10)) ||
            (25 > diff && diff > -10)
        ) {
            e.preventDefault();
            document.body.style.cursor = 'grabbing';
            this.centerInner.style.transition = 'none';
            const content = this.centerInner.querySelector('#control-panel--content');
            if(content) {
                content.style.pointerEvents = 'none';
            }
            this.lastLeft = 0;
            this.dragStart = diff;
            this.lastPosX = diff;
            if(!this.state.mobileContentActive) {
                this.lastLeft = this.centerInner.parentNode.clientWidth - diff;
                this.dragStart = this.centerInner.parentNode.clientWidth - diff;
                this.lastPosX = this.centerInner.parentNode.clientWidth - diff;
            }
        }
    }

    dragging = (pageX, e) => {
        if(this.dragStart) {
            e.preventDefault();

            let dragRatioMove = ((this.centerInner.parentNode.clientWidth > '559' && !this.state.mobileContentActive ? 30 : 100) - (((this.lastLeft + ((this.dragStart - pageX) * -1.25)) / this.centerInner.parentNode.clientWidth) * 100)) /
                (this.centerInner.parentNode.clientWidth > '559' ? 3 : 2);
            let dragRatio = ((((this.lastLeft + ((this.dragStart - pageX))) / this.centerInner.parentNode.clientWidth) * 100));
            if(!this.state.mobileContentActive) {
                dragRatio = (this.centerInner.parentNode.clientWidth > '559' ? 30 : 100) - dragRatio;
            }
            if(dragRatio > 25 || dragRatio < -25) {
                this.setState({mobileWouldSwitch: true});
            } else {
                this.setState({mobileWouldSwitch: false});
            }
            this.centerInner.style.transform = 'translateX(-' + (dragRatioMove) + '%)';
            this.lastPosX = pageX;
        }
    };

    endDrag() {
        if(this.dragStart) {
            this.centerInner.style.transition = null;
            if(this.lastPosX) {
                let dragRatio = ((((this.lastLeft + ((this.dragStart - this.lastPosX))) / this.centerInner.parentNode.clientWidth) * 100));
                if(!this.state.mobileContentActive) {
                    dragRatio = 100 - dragRatio;
                }

                if(dragRatio > 25 || dragRatio < -25) {
                    this.toggleSidebar();
                } else {
                    this.centerInner.style.transform = null;
                }
            } else {
                this.centerInner.style.transform = null;
            }

            this.lastPosX = false;
            this.dragStart = false;
            document.body.style.cursor = 'auto';
            const content = this.centerInner.querySelector('#control-panel--content');
            if(content) {
                content.style.pointerEvents = null;
            }
            this.setState({mobileWouldSwitch: false});
        }
    }

    toggleSidebar = () => {
        this.setState({mobileContentActive: !this.state.mobileContentActive}, () => this.centerInner.style.transform = null);
    };

    render() {
        const {
            theme, classes,
            routes
        } = this.props;

        return (
            <React.Fragment>
                <Header style={{background: theme.bgNav}}/>

                <div className={classes.center}>
                    <SidebarsProvider>
                        <Sidebar center={(p) => <NavMain {...p} toggleSidebar={this.toggleSidebar}/>} collapseMobile={true} textToggle={true}/>

                        <div className={classes.centerInner + ' ' + (this.state.mobileContentActive ? classes.centerInnerActive : '')}
                             ref={(r) => {
                                 this.centerInner = r
                             }}>
                            {routes.map((route, i) => (
                                <Route
                                    key={i}
                                    path={route.path}
                                    render={props => (
                                        route.provider ?
                                            <route.provider><MainRouteItem theme={theme} classes={classes}
                                                                           route={route}
                                                                           mobileWouldSwitch={this.state.mobileWouldSwitch}
                                                                           mobileContentActive={this.state.mobileContentActive}
                                                                           toggleSidebar={this.toggleSidebar}
                                                                           loading_lang={this.state.loading_lang}/>
                                            </route.provider> :
                                            <MainRouteItem theme={theme} classes={classes}
                                                           route={route}
                                                           mobileWouldSwitch={this.state.mobileWouldSwitch}
                                                           mobileContentActive={this.state.mobileContentActive}
                                                           toggleSidebar={this.toggleSidebar}
                                                           loading_lang={this.state.loading_lang}/>
                                    )}
                                />
                            ))}
                        </div>
                    </SidebarsProvider>
                </div>
            </React.Fragment>
        );
    }
}

export default withTheme(withStyles(styles)(FactoryControlPanel));
