import React from 'react';
import {
    Route,
    Switch,
    Link
} from 'react-router-dom';
import {MdInvertColors, MdFormatSize, MdArrowDropDown} from 'react-icons/md';
import {withNamespaces} from 'react-i18next';
import posed from 'react-pose';

import {withTheme, withStyles, themeSwitcher, switchMagnification, addMagnificationConsumer, rmMagnificationConsumer, getMagnificationLvl} from '../lib/theme';

import PushState from '../component/Pusher/PushState';
import LocaleSwitch from '../component/LocaleSwitch';
import AccountNav from '../component/User/AccountNav';
import {withBackend} from "../feature/BackendConnect";
import {HookSwitchSelect} from "../component/Hook/HookSwitch";
import {withAuth} from "../feature/Auth";
import {withBranding} from "../component/Branding";

const styles = {
    wrapper: {
        display: 'flex',
        flexDirection: 'row',
        alignContent: 'center',
        justifyContent: 'center',
        width: '100%',
        paddingRight: '6px',
        marginLeft: 'auto',
        marginRight: 'auto'
    },
    item: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        height: '48px',
        padding: '0 6px'
    },
    logoWrapper: {
        display: 'flex',
        flexDirection: 'row'
    },
    sloganWrapper: {
        display: 'flex',
        flexDirection: 'row'
    },
    hookSwitch: {
        display: 'flex',
        flexDirection: 'row',
        overflow: 'hidden'
    },
    logo: {
        margin: 'auto 0 auto 0',
        height: '36px',
        width: '36px'
    },
    slogan: {
        margin: 'auto 0 auto  6px',
    },
    sloganHideSmall: {
        //margin: 'auto 0 auto  6px',
        [`@media (max-width: 560px)`]: {
            display: 'none'
        }
    },
    sloganHideSmaller: {
        [`@media (max-width: 340px)`]: {
            display: 'none'
        }
    },
    toStart: {
        marginRight: 'auto'
    },
    toEnd: {
        marginLeft: 'auto'
    }
};

const DropDownWrapper = posed.div({
    hidden: {
        height: 0,
    },
    visible: {
        height: '100%',
    }
});

class Header extends React.PureComponent {
    state = {
        magnificationLvl: 0,
        openHookSwitch: false
    };

    componentWillUnmount() {
        rmMagnificationConsumer('header');
    }

    componentDidMount() {
        const node = document.querySelector('html');

        this.setState({
            magnificationLvl: getMagnificationLvl(node.style.fontSize)
        });
        addMagnificationConsumer('header', (lvl) => {
            this.setState({
                magnificationLvl: lvl
            });
        });
    }

    switchFontSize = () => {
        this.setState({
            magnificationLvl: switchMagnification()
        });
    };

    render() {
        const {
            t, theme, classes,
            moreButtons: MoreButtons, branding,
            hooks_qty, hook_active,
            loggedIn
        } = this.props;

        let Logo = false;
        if(branding && branding.logo && branding.logo.normal) {
            Logo = branding.logo.normal;
        }
        let LogoWhite = false;
        if(branding && branding.logo && branding.logo.white) {
            LogoWhite = branding.logo.white;
        }

        const LogoStyled = (props) => ('dark' === theme.name && LogoWhite ? <LogoWhite className={props.className}/> : <Logo className={props.className}/>);

        const highMagnification = this.state.magnificationLvl >= 2;

        return (
            <Route path="/:lng" render={(props) => {
                return (
                    <div className={classes.wrapper} style={this.props.style}>
                        <div className={classes.toStart}>
                            {Logo ? (
                                loggedIn ?
                                    <div className={classes.item} style={{flexDirection: 'row'}}>
                                        <Link to={'/' + props.match.params.lng + '/dashboard'} className={classes.logoWrapper}>
                                            <LogoStyled className={classes.logo}/>
                                        </Link>
                                        {
                                            hooks_qty < 2 ?
                                                highMagnification ? null :
                                                    <div className={classes.sloganWrapper}>
                                                        <span className={classes.slogan + ' ' + classes.sloganHideSmaller}>{t('header.slogan')}</span>
                                                    </div> :
                                                <React.Fragment>
                                                    <div className={classes.sloganWrapper}>
                                                        <button className={classes.slogan} style={{color: theme.textColor}} onClick={() => this.setState({openHookSwitch: !this.state.openHookSwitch})}>
                                                            {highMagnification ? null :
                                                                <span className={classes.sloganHideSmall}>{t('header.slogan')}</span>}
                                                            <MdArrowDropDown size={'1.1em'}
                                                                             style={{
                                                                                 verticalAlign: 'text-top',
                                                                                 transition: '0.3s linear transform',
                                                                                 transform: 'rotate(' + (this.state.openHookSwitch ? '-180deg' : 0) + ')'
                                                                             }}/>
                                                        </button>
                                                    </div>
                                                    <div style={{margin: '0', display: 'flex', height: '100%', flexDirection: 'column'}}>
                                                        <DropDownWrapper className={classes.hookSwitch}
                                                                         style={{
                                                                             margin: this.state.openHookSwitch ? 'auto 0' : '0'
                                                                         }}
                                                                         withParent={false}
                                                                         pose={this.state.openHookSwitch ? 'visible' : 'hidden'}
                                                        >
                                                            <div style={{
                                                                margin: this.state.openHookSwitch ? 'auto 0' : '0'
                                                            }}>
                                                                <HookSwitchSelect onSwitched={() => this.setState({openHookSwitch: false})}/>
                                                            </div>
                                                        </DropDownWrapper>
                                                        <DropDownWrapper className={classes.hookSwitch}
                                                                         style={{
                                                                             margin: this.state.openHookSwitch ? '0' : 'auto 0'
                                                                         }}
                                                                         withParent={false}
                                                                         pose={this.state.openHookSwitch ? 'hidden' : 'visible'}
                                                        >
                                                            <button style={{
                                                                color: theme.textColor,
                                                                margin: 'auto 0'
                                                            }} onClick={() => this.setState({openHookSwitch: !this.state.openHookSwitch})}>
                                                                {hook_active.label}
                                                            </button>
                                                        </DropDownWrapper>
                                                    </div>
                                                </React.Fragment>
                                        }
                                    </div> :
                                    <div className={classes.item + ' ' + classes.logoWrapper}>
                                        <LogoStyled className={classes.logo}/>{highMagnification ? null : <span className={classes.slogan}>{t('header.slogan')}</span>}
                                    </div>
                            ) : null}
                        </div>

                        {MoreButtons ? <MoreButtons className={classes.item}/> : null}

                        {loggedIn ?
                            <PushState className={classes.item}/>
                            : null}

                        <Switch>
                            {/* exclude link to auth when on auth, show dashboard link on auth page when logged in */}
                            {/* todo: find a better way */}

                            <Route path="/:lng/auth" render={() => (
                                null
                            )}/>

                            <Route path="/*" render={() => (
                                <AccountNav className={classes.item}/>
                            )}/>
                        </Switch>

                        <button onClick={this.switchFontSize} className={classes.item} style={{position: 'relative'}}>
                            <span style={{position: 'absolute', right: '-2px', top: theme.gutter.base, fontSize: '0.875rem', color: theme.textColor}}>
                                {this.state.magnificationLvl}</span>
                            <MdFormatSize color={theme.textColor} size={'1.5em'}/>
                        </button>
                        <button onClick={themeSwitcher.exec} className={classes.item}><MdInvertColors color={theme.textColor} size={'1.5em'}/></button>

                        <LocaleSwitch className={classes.item} match={props.match} history={props.history}/>
                    </div>)
            }}/>
        );
    }
}

export default withNamespaces('common')(withAuth(withBackend(withBranding(withStyles(styles)(withTheme(Header))))));
