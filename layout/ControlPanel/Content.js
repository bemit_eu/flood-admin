import React from 'react';
import {withStyles} from '../../lib/theme';
import Footer from '../Footer';
import StyleGrid from '../../lib/StyleGrid';
import {LoadingLangScreen} from "../../component/LoadingLangScreen";
import RouteCascaded from "../../lib/RouteCascaded";

const styles = {
    wrapper: {
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
        flexGrow: 2,
        flexShrink: 0,
        width: '50%',
        height: '100%',
        margin: '0 auto auto 0',
        padding: '12px 9px 6px 9px',
        [`@media (min-width: 380px)`]: {
            padding: '12px 15px 12px 15px',
            width: '20%',
            flexGrow: 4,
            flexShrink: 1,
        },
        [StyleGrid.smUp]: {
            padding: '12px 18px 6px 18px',
            width: '20%',
            flexGrow: 4,
            flexShrink: 1,
        },
    },
    wrapperNoSidebar: {
        marginLeft: '50%',
        [`@media (min-width: 560px)`]: {
            marginLeft: '33.5%',
        },
        [StyleGrid.smUp]: {
            marginLeft: 'auto',
        },
    },
    wrapperNoSidebarOpen: {
        marginRight: '50%',
        marginLeft: 0,
        [`@media (min-width: 560px)`]: {
            marginRight: '33.5%',
        },
        [StyleGrid.smUp]: {
            marginRight: 'auto',
        },
    },
    innerWrapper: {
        marginBottom: 3 * 6 + 'px'
    },
    logo: {
        height: '48px',
        width: '48px'
    }
};

class Content extends React.PureComponent {
    state = {
        styleInnerWrapper: {}
    };

    changeStyleInnerWrapper = (style) => {
        this.setState({
            styleInnerWrapper: style
        });
    };

    render() {
        const {
            classes,
            sidebarOpen, sidebarExists, route
        } = this.props;

        return (
            <div className={classes.wrapper + ' ' + (sidebarExists ? '' : sidebarOpen ? classes.wrapperNoSidebarOpen : classes.wrapperNoSidebar)} id='control-panel--content'>
                <div className={classes.innerWrapper} style={this.state.styleInnerWrapper}>
                    <LoadingLangScreen loading={this.props.loading_lang}/>
                    {route.content ?
                        <RouteCascaded {...route} exact={true} component={route.content} childComponent='content' childProps={{
                            changeStyleInnerWrapper: this.changeStyleInnerWrapper
                        }}/> :
                        null}
                </div>
                <Footer/>
            </div>
        );
    }
}

export default withStyles(styles)(Content);
