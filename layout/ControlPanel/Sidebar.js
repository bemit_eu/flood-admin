import React from 'react';
import {withTheme, withStyles} from '../../lib/theme';
import StyleGrid from '../../lib/StyleGrid';
import {MdEject} from 'react-icons/md';
import {withSidebars} from "../../component/Sidebars";

const styles = theme => ({
    wrapper: {
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
        flexShrink: 0,
        marginLeft: '0',
        padding: '6px',
        '&:nth-child(1)': {
            background: theme.bgNav1,
        },
        '&:nth-child(2)': {
            background: theme.bgNav2,
        },
        '&:nth-child(3)': {
            background: theme.bgNav3,
            borderLeft: '1px solid ' + theme.bgNav,
        },
        '&.collapse-mobile': {
            background: theme.bgNav,
            padding: 0,
            height: 'auto',
            flexDirection: 'row',
            [StyleGrid.smUp]: {
                flexDirection: 'column',
                padding: '6px',
            },
        },
        [StyleGrid.smUp]: {
            overflow: 'auto',
            width: 'auto',
            padding: '6px',
        },
        '& ul': {
            display: 'flex',
            flexDirection: 'column',
            overflow: 'auto',
            height: '100%',
            margin: 0,
        },
        '&.collapse-mobile ul': {
            flexDirection: 'row',
            marginRight: '-6px',
            [StyleGrid.smUp]: {
                flexDirection: 'column',
                marginRight: '0',
            },
        },
        '& li': {
            listStyleType: 'none',
            display: 'block',
        },
        '& a': {}
    },
    logo: {
        height: '48px',
        width: '48px'
    },
    notxtToggle: {
        display: 'inline-block',
        width: '1.875em',
        flexGrow: 2,
        textAlign: 'left',
        margin: 0,
        padding: 3,
        [StyleGrid.smUp]: {
            margin: theme.gutter.base * 2 + 'px 0 0 0',
            padding: 0,
        },
    },
    notxtToggleIcon: {
        [StyleGrid.jss.smUp]: {},
    }
});

class Sidebar extends React.PureComponent {

    render() {
        const {
            theme, classes,
            top: Top,
            center: CenterContent,
            collapseMobile, textToggle,
            noText, toggleNoText
        } = this.props;

        return (
            <div className={classes.wrapper + ' ' + (collapseMobile ? 'collapse-mobile' : '')}>
                {Top ? <Top notxt={noText}/> : null}

                <CenterContent notxt={noText}/>

                {textToggle ?
                    <button className={classes.notxtToggle}
                            style={{marginLeft: noText ? '12px' : '8px'}}
                            onClick={toggleNoText}>
                        <MdEject color={theme.textColor}
                                 size={noText ? '1.25em' : '1em'}
                                 style={{
                                     transform: 'rotate(' + (noText ? '' : '-') + '90deg)',
                                     transition: '0.3s linear transform'
                                 }}/>
                    </button>
                    : null}
            </div>
        );
    }
}

export default withSidebars(withTheme(withStyles(styles)(Sidebar)));
