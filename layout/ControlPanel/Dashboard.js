import React from 'react';
import {withNamespaces} from 'react-i18next';
import {MdAdd} from "react-icons/md";
import {useTheme} from '../../lib/theme';

const Dashboard = (props) => {
    const {
        t,
    } = props;
    const theme = useTheme();

    return (
        <React.Fragment>
            <h1>{t('title')}</h1>
            <button><MdAdd color={theme.linkColor}/></button>
        </React.Fragment>
    );
};

export default withNamespaces('page--dashboard')(Dashboard);
