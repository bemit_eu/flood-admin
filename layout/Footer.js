import React from 'react';
import {
    Route,
    Link
} from 'react-router-dom';
import {withStyles, withTheme} from '../lib/theme';
import {withNamespaces} from "react-i18next";
import StyleGrid from '../lib/StyleGrid';
import {withBranding} from "../component/Branding";

const styles = {
    wrapper: {
        fontSize: '14px',
        lineHeight: '18px',
        display: 'flex',
        marginTop: 'auto'
    },
    wrapperInner: {
        display: 'flex',
        flexWrap: 'wrap',
        marginLeft: 'auto',
        marginRight: 'auto',
        '& > p, & > a': {
            padding: '3px',
            textAlign: 'center',
            margin: 'auto'
        }
    },
    copyright: {
        display: 'block',
        //width: '100%',
        marginLeft: 'auto',
        marginRight: 'auto',
        [StyleGrid.smUp]: {
            width: 'auto'
        }
    },
    link: {}
};

class Footer extends React.PureComponent {
    render() {
        const {theme, classes} = this.props;

        return (
            <div className={classes.wrapper} style={{borderTop: '1px solid ' + theme.textColorLight}}>
                <div className={classes.wrapperInner}>
                    <p className={classes.copyright}>
                        © 2019 <a href='https://bemit.eu' target='_blank' rel='noopener noreferrer' className={classes.link}>bemit</a>
                        <small> UG (haftungsbeschränkt)</small>
                    </p>

                    <Route path="/:lng" render={(props) => (
                        <Link to={'/' + props.match.params.lng + '/info'}>Info</Link>
                    )}/>
                </div>
            </div>
        );
    }
}

export default withNamespaces('common')(withBranding(withStyles(styles)(withTheme(Footer))));
