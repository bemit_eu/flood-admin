import React from 'react';
import {
    Switch,
    BrowserRouter,
    Route,
    Redirect
} from 'react-router-dom';
import Loadable from "react-loadable";
import {jss, JssProvider} from 'react-jss';

import {I18nProvider} from './lib/i18n';

import GlobalStyle from './style/GlobalStyle';

import {ThemeProvider, initialStyle, initialTheme, initMagnification, changeStyle, themeSwitcher, useTheme} from './lib/theme';

import './style/font.scss';

import {addApi} from './lib/ApiRequest';

import FactoryPage from './layout/FactoryPage';
import FactoryControlPanel from './layout/FactoryControlPanel';

import {BackendProvider} from './feature/BackendConnect';
import {AuthProvider} from "./feature/Auth";
import {BrandingProvider} from "./component/Branding";
import {RoutesStoreProvider, withRoutesStore} from "./component/RoutesStore";
import {FeatureManagerProvider} from "./component/FeatureManager";
import {AuthPermissionProvider} from "./feature/AuthPermission";
import {ApiControlProvider} from "./component/ApiControl";
import {ErrorControlProvider} from "./component/ErrorControl";
import {MessagesContainer as ErrorMessageContainer} from "./component/ErrorMessages/MessagesContainer";
import {PusherProvider} from "./component/Pusher/Pusher";
import {StoreMediaProvider} from "./feature/Media/Stores/Media";
import {StoreUploadProvider} from "./feature/Media/Stores/Upload";
import Loading from "./component/Loading";
import {setupComponents} from "./SetupComponents";

const LoginForm = Loadable({
    loader: () => import('./component/User/LoginForm'),
    loading: (props) => <Loading {...props} name='LoginForm'/>,
});

const localeConfig = {
    allLanguages: [
        "en",
        "de"
    ],
    defaultLanguage: "en",
    expirationTime: process.env.NODE_ENV === 'production' ? 2 * 24 * 60 * 60 * 1000 : 100,
    debug: false
};

setupComponents();

const BaseRoute = (props) => {
    const {
        routes_page, routes_control_panel,
        isLoginVisible, hideLogin,
    } = props;
    const theme = useTheme();

    /**
     * Where no backendConnect or authPermission loading must be displayed before children are displayed
     * @type {string[]}
     */
    const except = ['auth', 'info'];

    return (
        <React.Fragment>
            <Switch>
                <Route exact path="/:lng" render={(props) => (
                    false ? (
                        <Redirect to={'/' + props.match.params.lng + '/dashboard'}/>
                    ) : (
                        <Redirect to={'/' + props.match.params.lng + '/auth'}/>
                    )
                )}/>

                <Route path="/:lng" render={(props) => (
                    <BackendProvider except={except}>
                        <AuthProvider>
                            <AuthPermissionProvider except={except}>

                                {isLoginVisible ? <div style={{
                                    position: 'fixed',
                                    display: 'flex',
                                    flexDirection: 'column',
                                    alignItems: 'center',
                                    top: 0, right: 0, bottom: 0, left: 0,
                                    zIndex: 900,
                                    paddingTop: '24px',
                                    background: theme.name === 'dark' ? 'rgba(20, 20, 20, 0.6)' : 'rgba(220, 220, 220, 0.6)',
                                }}>
                                    <div style={{
                                        background: theme.bgPage,
                                        padding: '30px'
                                    }}>
                                        <LoginForm onLoggedIn={() => {
                                            hideLogin();
                                        }}/>
                                    </div>
                                </div> : null}

                                <PusherProvider>
                                    <StoreMediaProvider>
                                        <StoreUploadProvider>
                                            <Route path={routes_page.main} render={props => (
                                                <FactoryPage routes={routes_page.routes}/>
                                            )}/>

                                            <Route path={routes_control_panel.main} render={props => (
                                                <FactoryControlPanel routes={routes_control_panel.routes}/>
                                            )}/>

                                            {props.children}
                                        </StoreUploadProvider>
                                    </StoreMediaProvider>
                                </PusherProvider>
                            </AuthPermissionProvider>
                        </AuthProvider>
                    </BackendProvider>
                )}/>
            </Switch>
        </React.Fragment>
    );
};

const Routing = withRoutesStore(BaseRoute);

/**
 * Bootstrap a React Application with a lot of default functionality
 */
class BaseApp extends React.Component {

    state = {
        theme: initialTheme,
        style: initialStyle,
        onLoggedIn: [],
        loading: false,
        isLoginVisible: false,
    };

    constructor(props) {
        super(props);
        themeSwitcher.provide(this.changeStyle);

        for(let api_name in this.props.api) {
            if(this.props.api.hasOwnProperty(api_name)) {
                addApi(api_name, this.props.api[api_name]);
            }
        }
    }

    componentDidMount() {
        initMagnification();
    }

    changeStyle = () => {
        let new_style = changeStyle(this.state.theme);

        if(new_style) {
            this.setState(new_style);
        } else {
            // todo: when current theme transition doesn't exist currently fails silently for user - only log within changeStyle
        }
    };

    showLogin = (redo = false) => {
        if(!this.state.isLoginVisible) {
            this.setState({isLoginVisible: true});
        }

        if(redo) {
            const onLoggedIn = [...this.state.onLoggedIn];
            onLoggedIn.push(redo);
            this.setState({onLoggedIn});
        }
    };

    hideLogin = () => {
        if(this.state.isLoginVisible) {
            this.state.onLoggedIn.forEach((fn) => {
                fn();
            });
            this.setState({
                isLoginVisible: false,
                onLoggedIn: []
            });
        }
    };

    render() {
        const {
            defaultLanguage, locales,
            routesComponents,
            branding,
            features,
        } = this.props;

        const {style} = this.state;

        return (
            <JssProvider jss={jss}>
                <ThemeProvider theme={style}>
                    <React.Fragment>
                        <GlobalStyle/>

                        <ErrorControlProvider>
                            <BrandingProvider branding={branding}>
                                <BrowserRouter basename="/">
                                    <I18nProvider locales={locales} config={localeConfig} defaultLanguage={defaultLanguage}>
                                        <FeatureManagerProvider features={features}>
                                            <ApiControlProvider showLogin={this.showLogin}>
                                                <RoutesStoreProvider routesComponents={routesComponents}>
                                                    <Routing
                                                        isLoginVisible={this.state.isLoginVisible}
                                                        hideLogin={this.hideLogin}
                                                    >
                                                        {this.props.children}
                                                    </Routing>
                                                </RoutesStoreProvider>

                                                <ErrorMessageContainer/>
                                            </ApiControlProvider>
                                        </FeatureManagerProvider>
                                    </I18nProvider>
                                </BrowserRouter>
                            </BrandingProvider>
                        </ErrorControlProvider>
                    </React.Fragment>
                </ThemeProvider>
            </JssProvider>
        );
    }
}

export default BaseApp;
