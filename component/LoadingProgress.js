import React from 'react';

import {useTheme} from '../lib/theme';
import Loading from "./Loading";

/**
 * Component that show a loading spinner and optional label, when the props indicate a progress, progress is defined as:
 * - false: nothing done
 * - progress: started progress, is doing progress
 * - true: finished progress, maybe directly going to false again or after a timeout
 * - error: something failed during the execution
 *
 * Progresses: add multiple as array like: `<LoadingProgress progresses={[props.saving, props.loading]}/>`
 *
 * @param props with indices `progress` or `progresses`, progress is for a single loading/saving
 * @return {*}
 * @constructor
 */
const LoadingProgress = (props) => {
    const {progress, label} = props;
    const theme = useTheme();

    let progresses = props.progresses;
    if(progress) {
        progresses = [progress];
    }
    if(!Array.isArray(progresses)) {
        return null;
    }

    let loading = false;
    let error = false;
    progresses.forEach((progress) => {
        if('progress' === progress || 'error' === progress) {
            loading = true;
        }
        if('error' === progress) {
            error = true;
        }
    });

    return (
        <React.Fragment>
            {(loading ?
                <Loading style={{
                    wrapper: {margin: 0},
                    item: (error ? {borderColor: theme.errorColor} : {}),
                }}>{
                    loading && label ?
                        label :
                        ''
                }</Loading> :
                null)}
        </React.Fragment>
    )
};

export {LoadingProgress};
