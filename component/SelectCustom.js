import React from "react";
import {useTheme, withTheme} from '../lib/theme';

const OptionCustom = (p) => {
    const theme = useTheme();

    return <button
        type={'button'}
        onClick={p.onClick}
        style={{
            margin: '2px 0', color: theme.textColor,
            padding: '2px 3px',
            fontSize: p.fontSize ? p.fontSize : '0.85rem',
            fontWeight: p.fontWeight ? p.fontWeight : 'normal',
            background: p.active ? theme.btnActive : 'transparent',
        }}
    >{p.children}</button>
};

class SelectCustomBase extends React.PureComponent {
    render() {
        const {
            options, theme,
        } = this.props;
        let style = {};
        if(this.props.style) {
            style = this.props.style;
        }

        return <div style={{
            display: 'flex', flexDirection: 'column',
            padding: 3,
            border: '1px solid ' + theme.textColorLight,
            ...style
        }}>
            {this.props.children}
            {options.map((id) => <this.props.option key={id} id={id}/>)}
        </div>;
    }
}

const SelectCustom = withTheme(SelectCustomBase);

export {SelectCustom, OptionCustom};
