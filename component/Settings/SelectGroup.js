import React from "react";
import {Select} from "../Form";

class SelectGroup extends React.PureComponent {
    render() {
        const {
            formId,
            id,
            label,
            value,
            onChange,
            options,
            option: Opt,
            style, className,
        } = this.props;

        return <div className={className} style={style}>
            <label htmlFor={formId + '-' + id} style={{
                margin: '0 0 1px 0',
                padding: '3px 0 2px 0',
                fontSize: '0.85rem',
                display: 'block',
            }}>{label}</label>
            <Select id={formId + '-' + id}
                    value={value}
                    onChange={onChange}
                    style={{
                        padding: '4px 6px',
                        fontSize: '0.85rem',
                    }}
            >
                {options.map((op, i) => (
                    <option key={op} value={op}>{Opt(op, i)}</option>
                ))}
            </Select>
        </div>;
    }
}

const SettingsSelectGroup = SelectGroup;

export {SettingsSelectGroup};
