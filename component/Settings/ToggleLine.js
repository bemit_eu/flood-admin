import React from "react";
import {withTheme} from '../../lib/theme';
import {ButtonRaw} from "../Form";
import {ToggleOnOff} from "../../asset/icon/ToggleOnOff";

class ToggleLine extends React.PureComponent {
    render() {
        const {
            theme,
            formId,
            id,
            label,
            onClick,
            active,
            size,
        } = this.props;

        return <div style={{margin: '12px 0', display: 'flex', borderBottom: '1px solid ' + theme.textColorLight}}>
            <label htmlFor={formId + '-' + id} style={{cursor: 'pointer', minWidth: '25%', padding: '6px 0', margin: 'auto 4px auto 0'}}>{label}</label>
            <ButtonRaw
                id={formId + '-' + id}
                onClick={onClick}
            ><ToggleOnOff active={active} size={size || '2em'}/></ButtonRaw>
        </div>;
    }
}

const SettingsToggleLine = withTheme(ToggleLine);

export {SettingsToggleLine};
