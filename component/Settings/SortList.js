import React from "react";
import {MdDragHandle} from "react-icons/md";
import {isObject} from "@formanta/js";
import {useTheme, withTheme} from '../../lib/theme';
import {SortableHandle, SortableContainer, SortableElement} from 'react-sortable-hoc';


class DragHandleBase extends React.Component {
    timeoutid = false;

    componentWillUnmount() {
        if(this.timeoutid) {
            clearTimeout(this.timeoutid);
            this.timeoutid = false;
        }
    }

    render() {
        const {theme} = this.props;
        const style = {
            cursor: 'inherit',
            boxSizing: 'content-box',
            //marginTop: small ? '4px' : theme.gutter.base * -2 + 'px',
            margin: 'auto 0 auto ' + (theme.gutter.base * -2) + 'px',
            padding: '6px'
        };

        return <button style={style}
                       onMouseEnter={() => {
                           document.body.style.cursor = 'grab';
                       }}
                       onMouseLeave={() => {
                           if('grabbing' !== document.body.style.cursor) {
                               document.body.style.removeProperty('cursor');
                               if(this.timeoutid) {
                                   clearTimeout(this.timeoutid);
                               }
                           }
                       }}
                       onClick={() => {
                           this.timeoutid = setTimeout(() => {
                               document.body.style.cursor = 'grabbing';
                           }, 105);
                       }}
                       onMouseUp={() => {
                           document.body.style.removeProperty('cursor')
                           if(this.timeoutid) {
                               clearTimeout(this.timeoutid);
                           }
                       }}
        ><MdDragHandle size={'1.125em'} color={theme.textColor}/></button>
    }
}

const DragHandle = SortableHandle(withTheme(DragHandleBase));

const SortItemNo = props => <p style={{display: (props.small ? 'inline-block' : 'block'), wordBreak: 'break-all', fontStyle: 'italic', margin: (props.small ? '2px 4px 2px 0' : '12px 0')}}>{props.index + 1}.</p>;

const SortItem = SortableElement((props) => {
    const theme = useTheme();
    const Content = props.content;

    return <div style={{
        display: 'flex',
        flexShrink: 0,
        width: '100%',
        padding: '0 ' + theme.gutter.base + 'px ' + theme.gutter.base * 2 + 'px 3px',
    }}>
        <div style={{
            display: 'flex',
            flexDirection: props.small ? 'row' : 'column',
            alignItems: props.small ? 'center' : 'normal',
            width: '100%',
            background: theme.bgPage,
            transition: theme.transition + ' background',
            padding: theme.gutter.base * (props.small ? 1 : 2) + 'px',
            border: '1px solid ' + theme.textColorLight,
        }}>
            <div style={{
                display: props.small ? 'flex' : 'block',
                margin: 'auto 0',
                alignItems: props.small ? 'center' : 'auto'
            }}>
                <DragHandle small={props.small}/>
                {props.small ? <SortItemNo small={props.small} index={props.sortIndex}/> : null}
            </div>

            {props.small ? null : <SortItemNo small={props.small} index={props.sortIndex}/>}

            <div style={{display: (props.small ? 'flex' : 'block'), flexGrow: props.small ? 2 : 0, margin: props.small ? '0' : 0}}>
                {Content ? <Content {...props}/> : 'err. no comp for sortable'}
            </div>
        </div>
    </div>
});

const SortContainer = SortableContainer((props) => {
    return <div style={{
        display: 'flex',
        flexDirection: 'x' === props.viewAxis || 'xy' === props.viewAxis ? 'row' : 'column',
        flexWrap: 'xy' === props.viewAxis ? 'wrap' : 'nowrap',
        overflowX: 'x' === props.viewAxis ? 'scroll' : 'no-scroll',
        margin: '0 -3px'
    }}>{props.children}</div>;
});

class SortList extends React.PureComponent {

    onDragStart = () => {
        document.body.style.cursor = 'grabbing';
    };

    onDragEnd = ({oldIndex, newIndex}) => {
        let list = [...this.props.items];
        if(!this.props.update) {
            console.warn('No update supplied for Settings/SortList.');
            return;
        }
        if(!Array.isArray(list)) {
            console.warn('List is no array in Settings/SortList.');
            return;
        }
        if(!isObject(list[oldIndex])) {
            console.warn('Item to move is no object in Settings/SortList.');
            return;
        }
        const movingItem = {...list[oldIndex]};
        list.splice(oldIndex, 1);
        list.splice(newIndex, 0, movingItem);

        document.body.style.removeProperty('cursor');

        this.props.update(list);
    };

    render() {
        const {
            width, gutter,
            small,
            items, content,
        } = this.props;

        return <div style={{margin: gutter || '6px 0', width: width || 'auto'}}>
            <SortContainer
                updateBeforeSortStart={this.onDragStart}
                onSortEnd={this.onDragEnd}
                useWindowAsScrollContainer={true}
                axis={'y'}
                viewAxis={'y'}
                pressDelay={25}
                useDragHandle={true}
            >
                {items.map((item, i) => item.id ?
                    <SortItem
                        key={item.id}
                        index={i}
                        sortIndex={i}
                        small={small}
                        data={item}
                        content={content}
                    />
                    : null)}
            </SortContainer>
        </div>;
    }
}

const SettingsSortList = SortList;

export {SettingsSortList};
