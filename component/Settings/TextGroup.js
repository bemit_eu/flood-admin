import React from "react";
import {InputText} from "../Form";

class TextGroup extends React.PureComponent {
    render() {
        const {
            formId,
            id,
            label,
            onChange,
            value,
            width, gutter,
            labelGutter,
            inpGutter,
        } = this.props;

        return <div style={{margin: gutter || '6px 0', width: width || 'auto'}}>
            <label htmlFor={formId + '-' + id} style={{
                margin: labelGutter || '0 0 1px 0',
                padding: '3px 0 2px 0',
                fontSize: '0.85rem',
                display: 'block',
            }}>{label}</label>
            <InputText id={formId + '-' + id}
                       value={value}
                       style={{
                           padding: inpGutter || '4px 6px',
                           fontSize: '0.85rem',
                       }}
                       onChange={onChange}
            />
        </div>;
    }
}

const SettingsTextGroup = TextGroup;

export {SettingsTextGroup};
