import React from 'react';
import {MdChevronRight, MdOpenInNew} from 'react-icons/md';
import NewWindow from 'react-new-window';
import {ButtonRaw} from '../Form';
import {DropDownAuto} from '../../lib/DropDown';
import {withTheme} from '../../lib/theme';

class SettingsGroupBase extends React.PureComponent {
    state = {
        open: false,
        poppedOut: false,
    };

    render() {
        const {
            label, theme,
            popout, keepOpen, renderOnlyWhenOpen,
        } = this.props;

        return <div style={{margin: '6px 0 12px 0'}}>
            <div style={{display: 'flex'}}>
                <ButtonRaw
                    style={{
                        color: theme.textColor, background: theme.bgSpecial,
                        display: 'flex', width: '90%', flexGrow: 2,
                        textAlign: 'left', fontFamily: theme.monospace,
                        padding: '6px 3px'
                    }}
                    disabled={keepOpen}
                    active={this.state.open} onClick={() => this.setState({open: !this.state.open})}>
                    {keepOpen ? null : <MdChevronRight style={{
                        transform: 'rotate(' + (this.state.open ? '-90deg' : '90deg') + ')',
                        transition: 'transform 0.25s linear',
                        marginRight: 3
                    }}/>}
                    {label}
                </ButtonRaw>
                {popout ?
                    <ButtonRaw
                        style={{
                            color: theme.textColor, background: theme.bgSpecial,
                            display: 'flex',
                            flexShrink: 0,
                            padding: '6px 3px'
                        }}
                        active={this.state.poppedOut} onClick={() => this.setState({poppedOut: !this.state.poppedOut})}>
                        <MdOpenInNew color={this.state.poppedOut ? theme.linkColor : theme.textColor}/>
                    </ButtonRaw>
                    : null}
            </div>

            <DropDownAuto open={keepOpen || this.state.open} duration={250} style={{marginLeft: this.state.open ? 12 : 0, transition: '0.25s margin-left ease-out'}}>
                {!renderOnlyWhenOpen || (renderOnlyWhenOpen && (this.state.open || keepOpen)) ? this.props.children : null}
            </DropDownAuto>

            {this.state.poppedOut ? <NewWindow
                name={'Diagram Data Raw'}
                title={'Data Raw'}
                onUnload={(e) => this.setState({poppedOut: false})}
            >
                <div style={{padding: 6}}>
                    <SettingsGroup {...this.props} popout={false} keepOpen={true}/>
                </div>
            </NewWindow> : null}
        </div>
    }
}

const SettingsGroup = withTheme(SettingsGroupBase);

export {SettingsGroup};
