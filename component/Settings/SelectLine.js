import React from "react";
import {Select} from "../Form";

class SelectLine extends React.PureComponent {
    render() {
        const {
            formId,
            id,
            label,
            value,
            onChange,
            options,
            option: Opt,
        } = this.props;

        return <div style={{marginBottom: '12px', display: 'flex'}}>
            <label htmlFor={formId + '-' + id} style={{margin: 'auto 6px auto 0'}}>{label}</label>
            <Select id={formId + '-' + id}
                    value={value}
                    onChange={onChange}
            >
                {options.map((op, i) => (
                    <option key={op} value={op}>{Opt(op, i)}</option>
                ))}
            </Select>
        </div>;
    }
}

const SettingsSelectLine = SelectLine;

export {SettingsSelectLine};
