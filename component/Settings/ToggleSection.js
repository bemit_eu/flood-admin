import React from "react";
import {withStyles} from '../../lib/theme';
import {ButtonRaw} from "../Form";
import {ToggleOnOff} from "../../asset/icon/ToggleOnOff";

const styles = theme => ({
    wrapper: {
        border: '1px solid ' + theme.textColorLight,
        margin: '6px 0', padding: '0 9px 9px 9px',
        display: 'flex', flexDirection: 'column',
        '& > *:nth-child(2)': {
            marginTop: 0,
            paddingTop: 0,
        }
    }
});

class ToggleSection extends React.PureComponent {
    render() {
        const {
            classes,
            formId,
            id,
            label,
            onClick,
            active,
            icon: Icon,
            size,
        } = this.props;

        return <div className={classes.wrapper}>
            <div style={{display: 'flex', margin: 0, cursor: 'pointer',}}>
                <label style={{display: 'flex', flexGrow: 2, cursor: 'pointer',}} htmlFor={formId + id + '-togglesection'}>
                    {Icon ? Icon : null}
                    <span style={{margin: 'auto 6px', flexGrow: 4}}>{label}</span>
                </label>
                <ButtonRaw
                    id={formId + id + '-togglesection'}
                    style={{flexShrink: 0}}
                    onClick={onClick}
                ><ToggleOnOff active={active} size={size}/></ButtonRaw>
            </div>
            {this.props.children}
        </div>;
    }
}

const SettingsToggleSection = withStyles(styles)(ToggleSection);

export {SettingsToggleSection};
