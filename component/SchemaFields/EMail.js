// Lib
import React from 'react';

import String from "../Schema/field/String";

export default (props) => {
    return (
        <String
            type={'email'}
            {...props}
        />
    );
};
