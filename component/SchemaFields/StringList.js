// Lib
import React, {Component} from 'react';
import {MdAdd} from 'react-icons/md';
import {withStyles, withTheme} from '../../lib/theme';

import StringListItem from "./StringListItem";
import {withSchemaEditorStore} from "../Schema/EditorStore";

const styles = {
    wrapper: {
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'column'
    },
    toggle: {
        margin: '0 auto'
    }
};

class StringList extends Component {

    createItem = () => {
        const {
            name_abs, store, onChange,
        } = this.props;

        let value = store.get(name_abs) || [];
        if(!Array.isArray(value)) {
            value = [];
        }
        value.push('');
        onChange(store.set(name_abs, value));
    };

    deleteItem(key) {
        let list = [...this.state.list];
        if(list.hasOwnProperty(key)) {
            this.contentList.deleteItem(key);
            // todo: refine and make a true deletion that will delete from the array but also it should rely on key's
            setTimeout(() => {
                list[key] = '';
                this.setState({list: list});
            }, 250);
        }
    }

    render() {
        const {
            theme, classes, name_abs,
            store, onChange,
        } = this.props;

        let values = store.get(name_abs) || [];
        if(!Array.isArray(values)) {
            values = [];
        }

        return (
            <div className={classes.wrapper}>
                {values.map((value, i) => (
                    <StringListItem
                        key={i}
                        value={value}
                        onChange={(value) => onChange(store.set(name_abs + '.' + i, value))}
                        deleteItem={() => onChange(store.rm(name_abs + '.' + i))}
                        open={true}
                    />
                ))}
                <button className={classes.toggle} onClick={this.createItem}><MdAdd size={'1em'} color={theme.textColor}/></button>
            </div>
        );
    };
}

export default withStyles(styles)(withTheme(withSchemaEditorStore(StringList)));
