import React from 'react';

import FileFolderBase from './FileFolderBase';

class File extends React.Component {
    render() {
        return (
            <div>
                <FileFolderBase
                    selectFile={true}
                    selectDir={false}
                    multiple={false}
                    {...this.props}
                />
            </div>
        );
    }
}

export default File;
