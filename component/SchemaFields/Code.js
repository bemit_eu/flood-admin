// Lib
import React from 'react';
import {isString, isObject} from '@formanta/js';

import AceEditor from 'react-ace';

import 'brace/mode/javascript';
import 'brace/mode/json';
import 'brace/mode/xml';
import 'brace/mode/sass';
import 'brace/mode/markdown';
import 'brace/mode/mysql';
import 'brace/mode/html';
import 'brace/mode/css';
import 'brace/theme/github';
import 'brace/theme/monokai';

import {Select} from '../Form';
import {useTheme} from "../../lib/theme";
import {withSchemaEditorStore} from "../Schema/EditorStore";

const modes = [
    'javascript',
    'json',
    'xml',
    'sass',
    'markdown',
    'mysql',
    'html',
    'css',
].sort();

const AceEdit = (props) => {
    const {selectedMode, code, id, name, handleUpdate} = props;
    const theme = useTheme();

    return <AceEditor
        mode={selectedMode}
        theme={theme.name === 'dark' ? 'monokai' : 'github'}
        value={code}
        onChange={handleUpdate}
        name={id + name}
        editorProps={{$blockScrolling: true}}
        showGutter={true}
        showPrintMargin={false}
        highlightActiveLine={true}
        style={{width: '100%'}}
        setOptions={{
            wrap: true,
            enableMultiselect: true,
            useWorker: true,
            showLineNumbers: true,
            tabSize: 2,
        }}
    />;
};

class CodeBase extends React.PureComponent {
    getModes = () => {
        const {currentSchema} = this.props;
        return modes.filter((v) => {
            if(currentSchema && currentSchema.option && currentSchema.option.limit) {
                return (-1 !== currentSchema.option.limit.indexOf(v));
            }
            return true;
        });
    };

    handleUpdate = (raw) => {
        const {
            name_abs,
            store, onChange,
        } = this.props;

        onChange(store.set(name_abs + '.code', raw));
        const selectedMode = store.get(name_abs + '.selectedMode');
        if(!isString(selectedMode)) {
            onChange(store.set(name_abs + '.selectedMode', this.getModes()[0]));
        }
    };

    render() {
        const {
            lblId,
            name, name_abs, currentSchema,
            store, onChange,
        } = this.props;

        let value = store.get(name_abs) || {};

        if(!isObject(value)) {
            value = {};
        }

        const mod_filtered = this.getModes();

        return (
            <div style={{width: '100%'}}>
                <Select value={value.selectedMode}
                        onChange={(e) => onChange(store.set(name_abs + '.selectedMode', e.target.value))}
                        style={{
                            maxWidth: '150px',
                        }}
                >
                    {0 < mod_filtered.length ?
                        mod_filtered.map((mode, i) => (
                            <option key={i} value={mode}
                            >{mode}</option>
                        )) : null
                    }
                </Select>

                <AceEdit
                    selectedMode={value.selectedMode}
                    code={value.code || value.raw}
                    handleUpdate={this.handleUpdate}
                    id={lblId}
                    name={name}
                />
                {currentSchema && currentSchema.option && currentSchema.option.show_count && value.code ?
                    <small>{value.code.length}</small>
                    : null}
            </div>
        );
    };
}

const Code = withSchemaEditorStore(CodeBase);

export default Code;
