// Lib
import React from 'react';
import Editor from '../../component/EditorLoader';
import {withSchemaEditorStore} from "../Schema/EditorStore";

const TextRich = withSchemaEditorStore((props) => {
    const {
        name_abs,
        store, onChange,
        onlyInline,
    } = props;

    const value = store.get(name_abs);

    return (
        'string' === typeof value || 'undefined' === typeof value ? <Editor
            html={value || ''}
            onlyInline={onlyInline}
            name_abs={name_abs}
            onChange={(html) => onChange(store.set(name_abs, html))}
        /> : null
    );
});

export default TextRich;
