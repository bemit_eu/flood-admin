// Lib
import React from 'react';
import posed from 'react-pose';

import {InputText} from '../Form';

import ButtonInteractiveDelete from '../ButtonInteractiveDelete';

import {withStyles} from '../../lib/theme';

const styles = {
    wrapper: {
        display: 'flex',
        overflow: 'hidden',
    },
    item: {
        margin: '0 3px 0 0',
    },
    toggle: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'row'
    }
};

const Wrapper = posed.div({
    hidden: {
        height: 0,
        paddingBottom: 0,
    },
    visible: {
        height: 'auto',
        paddingBottom: '3px',
    }
});

class StringListItem extends React.Component {

    state = {
        open: false
    };

    render() {
        const {classes, value, onChange, deleteItem, open} = this.props;

        return (
            <Wrapper className={classes.wrapper} withParent={false} pose={this.state.open || open ? 'visible' : 'hidden'}>
                <InputText
                    className={classes.item}
                    value={value}
                    onChange={(e) => onChange(e.target.value)}
                    type='text'
                />

                <ButtonInteractiveDelete onClick={deleteItem}/>
            </Wrapper>
        );
    }
}

export default withStyles(styles)(StringListItem);
