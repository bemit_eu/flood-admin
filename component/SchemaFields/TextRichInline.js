// Lib
import React from 'react';
import TextRich from './TextRich';

export default (props) => <TextRich {...props} onlyInline={true}/>;
