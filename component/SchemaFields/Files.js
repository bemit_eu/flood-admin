import React from 'react';

import FileFolderBase from './FileFolderBase';

class Files extends React.Component {
    render() {
        return (
            <div>
                <FileFolderBase
                    selectFile={true}
                    selectDir={false}
                    multiple={true}
                    {...this.props}
                />
            </div>
        );
    }
}

export default Files;
