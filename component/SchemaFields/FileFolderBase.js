import React from 'react';
import {SortableHandle, SortableContainer, SortableElement} from 'react-sortable-hoc';
import {MdDragHandle} from 'react-icons/md';
import {isObject} from '@formanta/js';
import posed from 'react-pose';

import {useTheme, withTheme, withStyles} from '../../lib/theme';
import {ButtonSmall} from "../Form";
import {SchemaEditor} from "../Schema/EditorLoader";
import ExplorerSelect from '../FileExplorer/ExplorerSelect';
import ButtonInteractiveDelete from "../ButtonInteractiveDelete";
import StyleGrid from "../../lib/StyleGrid";
import {escapeDot, withSchemaEditorStore} from "../Schema/EditorStore";

const styles = {
    wrapper: {},
    wrapperSelect: {
        overflow: 'hidden'
    },
    wrapperItem: {
        width: '100%',
        flexGrow: '2',
        [StyleGrid.mdUp]: {
            width: '45%',
        },
        '&.single': {
            width: '100%',
        }
    }
};

const Wrapper = posed.div({
    hidden: {
        height: 0,
        marginBottom: 0,
    },
    visible: {
        height: 'auto',
        marginBottom: '6px',
    }
});

class DragHandleBase extends React.Component {
    timeoutid = false;

    componentWillUnmount() {
        if(this.timeoutid) {
            clearTimeout(this.timeoutid);
            this.timeoutid = false;
        }
    }

    render() {
        const {theme} = this.props;
        const style = {
            cursor: 'inherit',
            boxSizing: 'content-box',
            marginTop: theme.gutter.base * -2 + 'px',
            marginLeft: theme.gutter.base * -2 + 'px',
            padding: '6px',
            height: 'max-content',
            flexShrink: 0,
            flexGrow: 0,
        };

        return <button style={style}
                       onMouseEnter={() => {
                           document.body.style.cursor = 'grab';
                       }}
                       onMouseLeave={() => {
                           if('grabbing' !== document.body.style.cursor) {
                               document.body.style.removeProperty('cursor')
                               if(this.timeoutid) {
                                   clearTimeout(this.timeoutid);
                               }
                           }
                       }}
                       onClick={() => {
                           this.timeoutid = setTimeout(() => {
                               document.body.style.cursor = 'grabbing';
                           }, 105);
                       }}
                       onMouseUp={() => {
                           document.body.style.removeProperty('cursor')
                           if(this.timeoutid) {
                               clearTimeout(this.timeoutid);
                           }
                       }}
        ><MdDragHandle size={'1.125em'} color={theme.textColor}/></button>
    }
}

const DragHandle = SortableHandle(withTheme(DragHandleBase));

const SortItem = SortableElement(withSchemaEditorStore((props) => {
    const theme = useTheme();
    const buttonStyle = {
        marginLeft: 'auto',
        marginTop: theme.gutter.base * -2 + 'px',
        marginRight: theme.gutter.base * -2 + 'px',
        padding: '6px',
        height: 'max-content',
        flexShrink: 0,
        flexGrow: 0,
    };

    return <div
        className={props.className + (props.multiple ? '' : ' single')}
        style={{
            display: 'flex',
            flexWrap: 'wrap',
            background: theme.bgPage,
            transition: theme.transition + ' background, ' + theme.transition + ' border',
            border: '1px solid ' + theme.textColorLight,
            padding: '6px',
            margin: '6px',
        }}
    >
        {props.multiple ? <DragHandle/> : null}

        <ButtonInteractiveDelete onClick={() => props.delete(props.name)} size={'1.125em'} style={buttonStyle}/>

        <p style={{width: '100%', wordBreak: 'break-all'}}>{props.name}</p>

        <div style={{width: '100%'}}>
            <SchemaEditor
                t={props.t}
                scope={props.name_abs + '.data.' + escapeDot(props.name)}
                parentStore={props.store}
                onChange={props.onChange}
                schema={props.schema}
                tScope={'file-folder'}
            />
        </div>
    </div>
}));

const SortContainer = SortableContainer((props) => {
    return <div style={{
        display: 'flex',
        flexWrap: 'wrap',
        margin: '0 -6px',
    }}>{props.children}</div>;
});

class FileFolderBase extends React.PureComponent {

    state = {
        openExplorer: false,
        prevCursor: false,
    };

    componentWillUnmount() {
        this.setState({
            openExplorer: false,
            prevCursor: false,
        });
    }

    deleteItem = (name) => {
        const {onChange, store, name_abs} = this.props;
        let {data, file} = this.getCurrentData();

        if(data.hasOwnProperty(name)) {
            delete data[name];
        }

        if(-1 !== file.indexOf(name)) {
            file.splice(file.indexOf(name), 1);
        }

        onChange(store.set(name_abs, {
            data,
            file
        }));
    };

    /**
     *
     * @return {{file: *, data: *}}
     */
    getCurrentData = () => {
        const {store, name_abs} = this.props;

        let {data, file} = store.get(name_abs) || {};

        if(!isObject(data)) {
            data = {};
        }
        if(!Array.isArray(file)) {
            file = [];
        }

        return {
            data,
            file
        }
    };

    addSelected = (selected) => {
        const {
            name_abs,
            store, onChange,
        } = this.props;

        let {data, file} = this.getCurrentData();

        const {multiple} = this.props;
        if(!multiple) {
            data = {};
            file = [];
        }

        if(selected.file) {
            Object.keys(selected.file).forEach((file_name) => {
                if(!data.hasOwnProperty(file_name)) {
                    data[file_name] = {
                        data: {}
                    };

                    file.push(file_name);
                }
            });
        }

        onChange(store.set(name_abs, {
            data,
            file
        }));
    };

    onDragStart = () => {
        document.body.style.cursor = 'grabbing';
    };

    onDragEnd = ({oldIndex, newIndex}) => {
        const {
            name_abs,
            store, onChange,
        } = this.props;
        let {data, file} = this.getCurrentData();

        const movingFile = file[oldIndex];
        file.splice(oldIndex, 1);
        file.splice(newIndex, 0, movingFile);

        document.body.style.removeProperty('cursor');

        onChange(store.set(name_abs, {
            data,
            file
        }));

        this.setState({
            prevCursor: false,
        });
    };

    render() {
        const {
            t,
            classes,
            selectFile, selectDir, multiple, name_abs, currentSchema,
        } = this.props;

        let {file} = this.getCurrentData();

        // todo: only file atm
        let replaceSingle = (!multiple && file.length);

        return (
            <div className={classes.wrapper}>
                <ButtonSmall
                    onClick={() => {
                        this.setState({openExplorer: !this.state.openExplorer})
                    }}
                    style={{marginBottom: '6px'}}
                >{this.state.openExplorer ? t('file-folder.close-explorer') :
                    selectDir && selectFile ? multiple ? t('file-folder.add-items') : (replaceSingle ? t('file-folder.replace-item') : t('file-folder.add-item')) :
                        selectFile ? multiple ? t('file-folder.add-files') : (replaceSingle ? t('file-folder.replace-file') : t('file-folder.add-file')) :
                            selectDir ? multiple ? t('file-folder.add-dirs') : (replaceSingle ? t('file-folder.replace-dir') : t('file-folder.add-dir')) :
                                t('file-folder.open-explorer')
                }</ButtonSmall>

                <Wrapper className={classes.wrapperSelect} withParent={false} pose={this.state.openExplorer ? 'visible' : 'hidden'}>
                    <ExplorerSelect selectFile={selectFile}
                                    choosenFiles={file}
                                    selectDir={selectDir}
                                    multiple={multiple}
                                    save={this.addSelected}/>
                </Wrapper>

                <SortContainer updateBeforeSortStart={this.onDragStart}
                               onSortEnd={this.onDragEnd}
                               useWindowAsScrollContainer={true}
                               axis={'xy'}
                               pressDelay={100}
                               multiple={multiple}
                               useDragHandle={true}>
                    {file.map((elem, i) => (
                        <SortItem key={elem + i}
                                  index={i}
                                  sortIndex={i}
                                  name={elem}
                                  name_abs={name_abs}
                                  multiple={multiple}
                                  className={classes.wrapperItem}
                                  delete={this.deleteItem}
                                  schema={{
                                      type: 'object',
                                      properties: currentSchema.properties
                                  }}
                        />
                    ))}
                </SortContainer>
            </div>
        );
    }
}

export default withStyles(styles)(withTheme(withSchemaEditorStore(FileFolderBase)));
