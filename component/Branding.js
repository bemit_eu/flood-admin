import React from 'react';
import {withContextConsumer} from "@formanta/react";

const BrandingContext = React.createContext({});

class BrandingProvider extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
        if(props.branding) {
            this.state.branding = props.branding;
        }

        this.state.changeBranding = (data) => {
            this.setState({branding: data});
        };
    }

    render() {
        return (
            <BrandingContext.Provider value={this.state}>
                {this.props.children}
            </BrandingContext.Provider>
        );
    }
}

const withBranding = withContextConsumer(BrandingContext.Consumer);

export {BrandingProvider, withBranding};