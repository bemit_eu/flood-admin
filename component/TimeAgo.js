import React from 'react';
import {withNamespaces} from 'react-i18next';
import {withTheme, withStyles} from '../lib/theme';

const styles = {
    wrapper: {
        position: 'relative',
        '&:hover .tooltip': {
            opacity: 1
        }
    },
    tooltip: {
        position: 'absolute',
        zIndex: 1,
        opacity: 0,
        top: '100%',
        right: '50%',
        transform: 'translateX(50%)',
        pointerEvents: 'none',
        transition: '0.25s linear opacity'
    }
};

class TimeAgo extends React.Component {

    state = {
        time: 0
    };

    currentType = false;
    intervalId = false;

    componentWillUnmount() {
        clearInterval(this.intervalId);
    }

    componentDidMount() {
        this.setState({time: this.timeSince()});
    }

    componentDidUpdate(prevProps) {
        if(
            prevProps.stop !== this.props.stop ||
            prevProps.time !== this.props.time
        ) {
            clearInterval(this.intervalId);
            if(!this.props.stop) {
                // do run stop, when stop is not true
                this.setState({time: this.timeSince()});
            }
        }
    }

    createInterval(interval) {
        if(this.intervalId) {
            clearInterval(this.intervalId);
        }
        this.intervalId = setInterval(() => {
            this.setState({time: this.timeSince()});
        }, interval);
    }

    timeSince() {
        const {time} = this.props;

        let seconds = Math.floor((new Date() - time) / 1000);

        // years
        let duration = Math.floor(seconds / 31536000);
        if(duration > 1) {
            if('y' !== this.currentType) {
                this.currentType = 'y';
            }
            return duration;
        }

        // months
        duration = Math.floor(seconds / 2592000);
        if(duration > 1) {
            if('m' !== this.currentType) {
                this.currentType = 'm';
                this.createInterval(2592000 * 1000);
            }
            return duration;
        }

        // days
        duration = Math.floor(seconds / 86400);
        if(duration > 1) {
            if('d' !== this.currentType) {
                this.currentType = 'd';
                this.createInterval(86400 * 1000);
            }
            return duration;
        }

        // hours
        duration = Math.floor(seconds / 3600);
        if(duration > 1) {
            if('h' !== this.currentType) {
                this.currentType = 'h';
                this.createInterval(3600 * 1000);
            }
            return duration;
        }

        // minutes
        duration = Math.floor(seconds / 60);
        if(duration > 1) {
            if('min' !== this.currentType) {
                this.currentType = 'min';
                this.createInterval(60 * 1000);
            }
            return duration;
        }

        // seconds
        if('s' !== this.currentType) {
            this.currentType = 's';
            this.createInterval(1000);
        }
        return Math.floor(seconds);
    }

    render() {
        const {
            theme, classes,
            time, t,
        } = this.props;

        return (
            <time className={classes.wrapper} dateTime={time.toISOString()}>
                {(this.state.time < 4 && 's' === this.currentType) ?
                    t('time.ago.just') :
                    t('time.ago.' + this.currentType + (this.state.time > 1 ? '-pl' : ''), {time: this.state.time})}
                <span className={classes.tooltip + ' tooltip'} style={{background: theme.bgPage}}>{time.toLocaleString()}</span>
            </time>
        );
    }
}


export default withNamespaces('common')(withStyles(styles)(withTheme(TimeAgo)));
