import React from 'react';
import {withContextConsumer} from "@formanta/react";

const SidebarsContext = React.createContext({});

class SidebarsProvider extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            noText: this.isNoText(),
            toggleNoText: this.toggleNoText
        };
    }

    isNoText = () => {
        let noText = window.localStorage.getItem('sidebars-notext');
        if(null !== noText && '' !== noText.trim()) {
            // type conversion to boolean
            // eslint-disable-next-line
            return (noText == 'true');
        }
        return false;
    };

    toggleNoText = () => {
        window.localStorage.setItem('sidebars-notext', (!this.state.noText).toString());
        this.setState({
            noText: !this.state.noText
        });
    };

    render() {
        return (
            <SidebarsContext.Provider value={this.state}>
                {this.props.children}
            </SidebarsContext.Provider>
        );
    }
}

const withSidebars = withContextConsumer(SidebarsContext.Consumer);

export {SidebarsProvider, withSidebars};