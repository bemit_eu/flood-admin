import React from 'react';
import {Button} from "../Form";
import {withNamespaces} from "react-i18next";

class PasswordReset extends React.Component {
    render() {
        const {t} = this.props;
        return (
            <div style={this.props.style}>
                <Button>{t('reset.open')}</Button>
            </div>
        )
    }
}

export default withNamespaces(['page--auth'])(PasswordReset);
