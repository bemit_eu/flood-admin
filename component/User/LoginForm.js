import React from 'react';
import {withNamespaces} from "react-i18next";
import {withStyles} from '../../lib/theme';
import {withAuth} from "../../feature/Auth";
import {Button} from '../../component/Form';
//import PasswordReset from './PasswordReset';
import {bindForm} from "../FormApi";
import StyleGrid from "../../lib/StyleGrid";
import Loading from '../../component/LoadingSmall';
import {withApiControl} from "../ApiControl";

const styles = {
    wrapper: {
        margin: '0 auto',
        [StyleGrid.smUp]: {
            width: '280px'
        }
    },
    btn: {
        textAlign: 'center',
        marginTop: '12px'
    },
    reset: {
        marginTop: '12px'
    }
};

class Login extends React.Component {

    constructor(props) {
        super(props);

        // binding internal sender through `connect`
        props.connect.sender(this.sender.bind(this));
    }

    sender(evt, data) {
        const {authVerify, onLoggedIn} = this.props;
        // modify submit values before sending to server
        return authVerify(data, onLoggedIn);
    }

    render() {
        const {
            t, classes,
            FormApi, InputGroupText, process, errSend,
            loggedIn, logout
        } = this.props;

        return (
            <React.Fragment>
                {loggedIn ? (
                    'sended' === process ?
                        /* Just logged in */
                        // redir now handled from AuthProvider
                        null :
                        /* Logged in but not after a state-change */
                        <p style={{display: 'none'}}>{t('login.already')}<br/><Button onClick={logout}>Logout</Button></p>
                ) : null}
                <div className={classes.wrapper}>
                    <FormApi>
                        <h3>{t('login.name')}</h3>

                        <InputGroupText style={{wrapper: {margin: '12px 0'}}} name='username' lbl={t('username')} required/>
                        <InputGroupText style={{wrapper: {margin: '12px 0'}}} name='password' type='password' lbl={t('pass')} required/>
                        {errSend.backendErr && 'connection-error' === errSend.backendErr ?
                            <p>{t('process.' + errSend.backendErr)}</p> :
                            process && 'received' !== process ?
                                <React.Fragment>
                                    {'sending+error' !== process ?
                                        <Loading
                                            center={true}
                                        /> : null}
                                    <p>{t('process.' + process)}</p>
                                </React.Fragment>
                                : null}
                        {process && 'sending' !== process ?
                            <Button className={classes.btn}>{t('login.submit')}</Button>
                            : null}
                    </FormApi>
                    {/*<PasswordReset style={styling(theme).reset}/>*/}
                </div>
            </React.Fragment>
        );
    }
}

export default withNamespaces(['page--auth'])(withAuth(withApiControl(bindForm(withStyles(styles)(Login)))));
