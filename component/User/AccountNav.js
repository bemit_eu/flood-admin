import React from 'react';
import {MdAccountCircle} from 'react-icons/md';
import {
    NavLink as Link,
    Route,
    withRouter
} from 'react-router-dom';

import {withTheme, withStyles} from '../../lib/theme';
import {i18n} from "../../lib/i18n";
import {withNamespaces} from "react-i18next";
import {withAuth} from "../../feature/Auth";

const styles = theme => ({
    wrapper: {
        position: 'relative'
    },
    nav: {
        position: 'absolute',
        zIndex: 100,
        background: theme.accent.blue,
        transform: 'translateX(-50%)',
        left: '50%',
        top: 'calc(100% - ' + theme.gutter.base * 2 + 'px)',
        margin: '0',
        padding: '0',
        '& li': {
            listStyleType: 'none'
        },
        '& a, & button': {
            display: 'block',
            // todo: add theme color
            padding: theme.gutter.base * 2+ 'px 12px',
            margin: '0 0 2px 0',
            color: '#fff',
        }
    },
    toggle: {},
});

class AccountNav extends React.Component {
    state = {
        open: false,
    };

    logout(evt) {
        evt.persist();
        this.props.history.push('/' + i18n.languages[0] + '/auth');
        this.props.logout();
    }

    static redirect = (props) => {

    };

    render() {
        const {
            t,
            theme, classes,
            loggedIn
        } = this.props;

        return (
            <div className={classes.wrapper}>
                {
                    loggedIn ? (
                        <React.Fragment>
                            <button onClick={() => this.setState({open: !this.state.open})} className={this.props.className + ' ' + classes.toggle}><MdAccountCircle color={theme.textColor} size={'1.5em'}/></button>
                            {this.state.open ?
                                <ul className={classes.nav}>
                                    {/*<Route path="/:lng" render={props => <li><Link to={'/' + props.match.params.lng + '/account/setting'}>{t('header.account.setting')}</Link></li>}/>*/}
                                    <li>
                                        <button onClick={this.logout.bind(this)} color
                                        >{t('header.account.logout')}</button>
                                    </li>
                                </ul> : null}
                        </React.Fragment>
                    ) : (
                        <React.Fragment>
                            <Route path="/:lng" render={props => <Link to={'/' + props.match.params.lng + '/auth'} className={this.props.className}><MdAccountCircle color={theme.textColor} size={'1.5em'}/></Link>}/>
                        </React.Fragment>
                    )}
            </div>
        )
    };
}

export default withNamespaces('common')(withAuth(withRouter(withStyles(styles)(withTheme(AccountNav)))));
