import React from 'react';
import {withNamespaces} from "react-i18next";
import {MdAutorenew, MdFormatAlignLeft, MdSearch, MdSort} from "react-icons/md";
import {selectByDot, sortString, isString} from "@formanta/js";

import {withStyles, withTheme} from '../../lib/theme';
import {InputText, ButtonRaw, ButtonSmall, Select} from "../../component/Form";
import Loading from "../../component/Loading";

const styling = {
    wrapper: {
        border: 0,
        width: '100%',
        borderCollapse: 'collapse'
    },
    header: {},
    item: {},
    cellHead: {
        lineHeight: 0,
        opacity: 0,
        padding: '0 3px',
    }
};

class DataGridBase extends React.PureComponent {

    state = {
        // list as of current page and perPage
        currentList: [],
        // list as of props.list or the props.list after searching for somethign
        list: [],
        // current page
        currentPage: 0,
        // entries per page
        perPage: 50,
        // possible entries per page setting, 0 = all
        perPages: [0, 10, 25, 50, 150],
        // which searches are open
        openSearches: {},
        // search terms
        searchTerms: {},
        // currently only one sort term
        sortTerm: false,
        // if something is searched currently
        searchActive: false,
        firstSearch: true
    };

    /**
     * timeout id's of all search inputs (only search when after a few ms nothing has been entered)
     * @type {{}}
     */
    searchTimeouts = {};

    /**
     * @type {null|HTMLElement}
     */
    headFixed = null;
    /**
     * @type {null|HTMLElement}
     */
    headFixedDummy = null;
    /**
     * @type {null|HTMLElement}
     */
    headActual = null;
    /**
     * @type {null|HTMLElement}
     */
    body = null;

    animation = {
        running: false,
        fps: 1000 / 5,
        then: 0,
    };

    constructor(props) {
        super(props);

        this.props.headers.forEach(header => {
            if(header.search) {
                // eslint-disable-next-line
                this.state.searchTerms[header.search] = '';
            }
        });
    }

    componentDidMount() {
        this.setState({list: this.props.list || []});

        this.animation.running = true;
        this.animation.then = window.performance.now();
        this.updateHeaderSizing(window.performance.now());
    }

    componentWillUnmount() {
        this.animation.running = false;
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevProps.list !== this.props.list) {
            this.setState({list: this.props.list || []}, () => this.doSearch());
        }

        if(prevState.list !== this.state.list) {
            this.switchPage();
        }

        if(prevProps.active !== this.props.active && this.props.active) {
            this.goToFirstActive(this.props.active);
        }
    }

    //
    // Animation/FixedHeader
    //

    updateHeaderSizing = (now) => {
        if(!this.animation.running) {
            return;
        }

        window.requestAnimationFrame(this.updateHeaderSizing);

        if(!this.headFixed || !this.headFixedDummy || !this.headActual) {
            return;
        }

        let elapsed = now - this.animation.then;

        if(elapsed > this.animation.fps) {
            this.animation.then = now - (elapsed % this.animation.fps);

            // set width of fixed header to the same as the the inside of the scrollable container
            this.headFixed.style.width = this.headActual.parentNode.parentNode.offsetWidth + 'px';
            // set margin with the difference of offsetWidth and clientWidth to have the actual size of any y-scrollbar existing
            this.headFixedDummy.style.width = this.headActual.parentNode.parentNode.offsetWidth - this.headActual.parentNode.parentNode.clientWidth + 'px';

            // position scroll on each render to catch any pos/size changes
            this.handleScroll(this.headActual.parentNode.parentNode);

            [].forEach.call(this.headActual.querySelector('tr').children, (elem, i) => {
                // set each element in the fixed header to the size of the actual header
                if(this.headFixed.children[i].style.width !== elem.offsetWidth + 'px') {
                    this.headFixed.children[i].style.width = elem.offsetWidth + 'px';
                }
            });
        }
    };

    /**
     * @todo still got 1-2px scroll-too-far at about 1300px content with 900px width scroll-area
     * @param target
     */
    handleScroll = (target) => {
        // target = headActual
        //const delta = target.parentNode.parentNode.offsetWidth - target.parentNode.parentNode.clientWidth;
        // get percentage of delta from offset
        //const percentage = delta / target.parentNode.parentNode.offsetWidth * 100;

        // scroll the amount of px more as the delta is
        //this.headFixed.scrollLeft = target.scrollLeft / 100 * (100 + percentage);
        this.headFixed.scrollLeft = target.scrollLeft;
    };

    //
    // Top Controls
    //

    reload = () => {
        if(this.props.reload) {
            this.props.reload();
        }
    };

    //
    // Bottom Controls/Pagination
    //

    switchPage = (page = 1, cb = false) => {
        const {perPage, list} = this.state;
        if(0 < perPage) {
            const indexOfLast = page * perPage;
            const indexOfFirst = indexOfLast - perPage;
            const currentList = [...list].splice(
                indexOfFirst,
                perPage
            );

            this.setState({
                currentPage: page,
                currentList
            }, cb ? cb : undefined);
        } else {
            this.setState({
                currentList: [...list],
                currentPage: 1,
            }, cb ? cb : undefined);
        }
    };

    changePerPage = (perPage) => {
        this.setState({
            perPage
        }, () => this.switchPage());
    };

    //
    // Active
    //

    /**
     * @todo implement also multiple filter_val, not only string also array
     *
     * @param {string} filter_val
     */
    goToFirstActive = (filter_val) => {
        if(this.props.activeFilter) {
            let activeFilter = this.props.activeFilter.split('|');

            let index = false;

            this.state.list.forEach((obj, ind) => {
                for(let i = 0; i < activeFilter.length; i++) {
                    // for one column SearchTerms are compare in the OR way
                    const filterVal = selectByDot(activeFilter[i], obj);
                    if(filterVal && isString(filterVal) && isString(filter_val) &&
                        -1 !== filterVal.toUpperCase().indexOf(filter_val.toUpperCase())) {
                        // todo: non string search
                        // found!
                        index = ind;
                        break;
                    }
                }
            });

            /**
             * on which page the first active element is
             * @type {number}
             */
            const page = Math.floor((index + 1) / this.state.perPage) + 1;

            this.switchPage(page, () => {
                setTimeout(() => {
                    if(this.body && this.body.querySelector('tr.active')) {
                        // todo: better and not class dependent scroll-top, must be triggered from within active
                        this.body.parentNode.parentNode.scrollTop = this.body.querySelector('tr.active').offsetTop;
                    }
                }, 250);
            });
        }
    };

    //
    // Search
    //

    toggleSearch = (search) => {
        const openSearches = {...this.state.openSearches};

        openSearches[search] = !openSearches[search];
        this.setState({
            openSearches
        });
    };

    handleSearch = (input, search) => {
        if(this.searchTimeouts[search]) {
            // clear timeout if already exists
            clearTimeout(this.searchTimeouts[search]);
        }
        const searchTerms = {...this.state.searchTerms};
        if(0 < input.value.length) {
            searchTerms[search] = input.value;
        } else if(searchTerms[search]) {
            searchTerms[search] = '';
        }

        this.setState({searchTerms}, () => {
            // set timeout, only search if after some ms nothing has been entered
            this.searchTimeouts[search] = setTimeout(() => {
                this.doSearch();
            }, 300);
        });
    };

    clearSearch = () => {
        const searchTerms = {...this.state.searchTerms};
        this.props.headers.forEach(header => {
            if(header.search) {
                searchTerms[header.search] = '';
            }
        });

        this.setState({searchTerms}, this.doSearch);
    };

    doSearch = () => {
        const {searchTerms} = this.state;

        let searchActive = false;

        const list = this.props.list.filter((obj) => {
            /**
             * @var {boolean} found if the entries data is like searched
             */
            let found = false;

            for(let searchTerm in searchTerms) {
                // root SearchTerms are compared in the AND way
                if(searchTerms.hasOwnProperty(searchTerm)) {
                    if(0 === searchTerms[searchTerm].length) {
                        found = true;
                    } else {
                        let searchTermMultiple = searchTerm.split('|');
                        for(let i = 0; i < searchTermMultiple.length; i++) {
                            // for one column SearchTerms are compare in the OR way
                            const filterVal = selectByDot(searchTermMultiple[i], obj);
                            if(filterVal && isString(filterVal) && isString(searchTerms[searchTerm]) &&
                                -1 !== filterVal.toUpperCase().indexOf(searchTerms[searchTerm].toUpperCase())) {
                                // todo: non string search
                                found = true;
                                searchActive = true;
                                break;
                            } else {
                                found = false;
                                searchActive = true;
                            }
                        }

                        if(!found) {
                            break;
                        }
                    }
                }
            }
            return found;
        });

        if(this.state.firstSearch) {
            this.setState({
                list,
                searchActive,
                sortTerm: false,
                firstSearch: false
            }, () => this.props.active ? this.goToFirstActive(this.props.active) : undefined);
        } else {
            this.setState({
                list,
                sortTerm: false,
                searchActive
            });
        }
    };

    handleSort = (sort) => {
        let list = [...sortString(this.state.list, sort)];
        let dir = 'asc';
        if(this.state.sortTerm && this.state.sortTerm.dir === 'desc') {
            this.doSearch();
            return;
        }
        if(this.state.sortTerm && this.state.sortTerm.dir === 'asc') {
            list = list.reverse();
            dir = 'desc';
        }
        this.setState({
            sortTerm: {
                id: sort,
                dir
            },
            list: list,
        });
    };

    render() {
        const {
            t, theme, classes,
            loading, loadingLabel, emptyList, reload, controls,
            colStyle,
            active,
            headers, element: Element,
        } = this.props;

        const {list, perPage, currentPage, searchActive} = this.state;

        const totalPages = 0 < perPage ? Math.ceil(list.length / perPage) : 1;

        if(0 === currentPage) {
            return null;
        }

        return (
            <div style={{display: 'flex', overflowY: 'auto', overflowX: 'hidden', /*minHeight: '100%',*/ height: '100%', flexDirection: 'column', position: 'relative'}}>
                <div className={classes.topControl} style={{
                    display: 'flex',
                    padding: reload || searchActive || (controls && controls.length) ? '6px 0 9px 0' : '3px 0 0 0',
                    flexGrow: 0, flexShrink: 0,
                }}>
                    {reload ? <ButtonSmall active={!loading || true !== loading}
                                           onClick={() => this.reload()}
                                           style={{marginRight: '6px'}}
                    ><MdAutorenew style={{verticalAlign: 'bottom'}}/> {t('common:reload')}</ButtonSmall> : null}

                    {searchActive ? <ButtonSmall
                        onClick={this.clearSearch}
                        style={{marginRight: '6px'}}
                    >{t('list.reset-search')}</ButtonSmall> : null}

                    {controls ? controls.map((Control, i) => <Control key={i}/>) : null}
                </div>

                <div className={classes.fixedHeader}
                     style={{display: 'flex', overflow: 'hidden', flexGrow: 0, flexShrink: 0,}}
                     ref={(r) => {
                         this.headFixed = r
                     }}>

                    {headers.map((header, i) => (
                        <div key={i}
                             style={{flexShrink: 0, display: 'flex', flexDirection: 'column'}}
                        >
                            <p style={{margin: 0, padding: '1px 3px 0 3px', textAlign: 'center', fontWeight: 'bold'}}>
                                {header.lbl}
                            </p>
                            <div className={classes.headerControl}>
                                {header.search ?
                                    <button onClick={() => this.toggleSearch(header.search)}>
                                        <MdSearch color={theme.textColor}/>
                                    </button>
                                    : null}
                                {header.sort ?
                                    <ButtonRaw
                                        active={this.state.sortTerm && this.state.sortTerm.id === header.sort}
                                        onClick={() => this.handleSort(header.sort)}>
                                        {this.state.sortTerm && this.state.sortTerm.id === header.sort ?
                                            this.state.sortTerm.dir === 'desc' ?
                                                <MdSort color={theme.textColor}/> :
                                                <MdSort color={theme.textColor} style={{transform: 'scale(1,-1)'}}/> :
                                            <MdFormatAlignLeft color={theme.textColor}/>}
                                    </ButtonRaw>
                                    : null}
                            </div>
                            {this.state.openSearches[header.search] ? <InputText onChange={evt => this.handleSearch(evt.target, header.search)}
                                                                                 value={this.state.searchTerms[header.search]}
                                                                                 style={{display: 'block', width: 'auto', minWidth: '20px', margin: '0 3px', padding: '3px'}}/> : null}
                        </div>
                    ))}

                    <div style={{flexShrink: 0, height: '1px'}}
                         ref={(r) => {
                             this.headFixedDummy = r
                         }}/>
                </div>

                <div style={{display: 'flex', overflow: 'auto', minHeight: '100px', height: '100%', flexDirection: 'column', borderTop: '1px solid ' + theme.textColorLight, borderBottom: '1px solid ' + theme.textColorLight,}}
                     onScroll={evt => this.handleScroll(evt.target)}>
                    <table className={classes.wrapper}>
                        <thead className={classes.header}
                               ref={(r) => {
                                   this.headActual = r
                               }}>
                        <tr>
                            {headers.map((header, i) => (
                                <th className={classes.cellHead}
                                    key={i}
                                    style={colStyle && colStyle[i] ? colStyle[i] : {}}
                                >{header.lbl}</th>
                            ))}
                        </tr>
                        </thead>

                        <tbody ref={(r) => {
                            this.body = r
                        }}>
                        {!loading || true === loading ?
                            // when no loading is set or finished loading
                            this.state.currentList.length > 0 ? this.state.currentList.map((item, i) => (
                                    <Element item={item}
                                        /* todo: make `id` prop dynamic/custom */
                                             key={item.id}
                                             index={i}
                                             colLength={headers.length}
                                             colStyle={colStyle}
                                             active={active}
                                    />
                                )) :
                                // empty list
                                <tr>
                                    <td colSpan={headers.length} style={{padding: '3px 0', fontStyle: 'italic'}}>
                                        {!searchActive && emptyList ? emptyList : searchActive ? t('list.no-entries-for-search') : t('list.no-entries')}
                                    </td>
                                </tr> :
                            // loading animation
                            'progress' === loading || 'error' === loading ?
                                <tr>
                                    <td colSpan={headers.length} style={{padding: '3px 0', fontStyle: 'italic'}}>
                                        <Loading style={{
                                            wrapper: {margin: 0},
                                            item: ('error' === loading ? {borderColor: theme.errorColor} : {}),
                                        }}>{loadingLabel ? loadingLabel : ''}</Loading>
                                    </td>
                                </tr> :
                                null}
                        </tbody>
                    </table>
                </div>

                <div style={{display: 'flex', alignItems: 'center', flexWrap: 'wrap'}}>
                    {[...Array(totalPages)].map((e, i) =>
                        <button style={{
                            margin: '6px 1px', padding: '6px 3px', color: theme.textColor, textDecoration: 'none',
                            background: currentPage === i + 1 ? theme.btnActive : 'transparent',
                            borderBottom: '1px solid ' + (currentPage === i + 1 ? theme.linkActive : theme.linkHover),
                        }}
                                onClick={() => this.switchPage(i + 1)}
                                key={i + 1}
                        >
                            {i + 1}
                        </button>
                    )}

                    {currentPage < totalPages ?
                        <ButtonSmall
                            onClick={() => this.switchPage(
                                currentPage + 1
                            )}
                            style={{margin: '6px 9px'}}
                        >{t('list.next-page')} {currentPage + 1}/{totalPages}</ButtonSmall> :
                        <p style={{margin: '6px 9px'}}>{currentPage}/{totalPages}</p>
                    }

                    <p style={{margin: '0 9px', flexShrink: 0, marginLeft: 'auto'}}>
                        {0 < perPage ?
                            currentPage * perPage > list.length ?
                                (((currentPage - 1) * perPage) + 1) + '-' + list.length + ' ' + t('list.of-entries') + ' ' + list.length :
                                (((currentPage - 1) * perPage) + 1) + '-' + (currentPage * perPage) + ' ' + t('list.of-entries') + ' ' + list.length :
                            list.length
                        }
                        {searchActive ? <span style={{display: 'block', fontSize: '0.875rem', lineHeight: '0.875rem'}}>{t('list.of-total-entries')} {this.props.list.length}</span> : null}
                    </p>

                    <div style={{display: 'flex', alignItems: 'center', margin: '3px 9px',}}>
                        <Select onChange={(e) => this.changePerPage(e.target.value)}
                                value={this.state.perPage}
                                style={{width: 'max-content'}}
                        >
                            {this.state.perPages.map((val, k) => (
                                <option key={k} value={val}>{0 === val ? t('list.all') : val}</option>
                            ))}
                        </Select>
                        <p style={{margin: '6px 9px 6px 4px', flexShrink: 0}}>
                            {t('list.entries-per-page')}
                        </p>
                    </div>
                </div>
            </div>
        )
    };
}

const DataGrid = withNamespaces('common')(withStyles(styling)(withTheme(DataGridBase)));

export {DataGrid};
