import React from 'react';
import {MdTranslate} from 'react-icons/md';
import Loading from "./Loading";
import {withNamespaces} from "../lib/i18n";
import {useTheme} from '../lib/theme';

const LoadingLangScreen = withNamespaces('common')((props) => {
    const {
        t,
        loading
    } = props;
    const theme = useTheme();

    return (
        loading ? <div style={{
            position: 'absolute',
            paddingTop: '50px',
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
            zIndex: 1000,
            background: theme.name === 'dark' ? 'rgba(20, 20, 20, 0.6)' : 'rgba(220, 220, 220, 0.6)',
        }}>
            <Loading absolute={true} style={{wrapper: {top: '6em'}}}/>
            <p style={{
                position: 'absolute',
                top: '6px',
                left: '50%',
                transform: 'translateX(-50%)',
                textAlign: 'center'
            }}>{t('lang.switched-loading')}<br/><MdTranslate color={theme.textColor} size={'1.5em'}/></p>
        </div> : null
    );
});

export {LoadingLangScreen};
