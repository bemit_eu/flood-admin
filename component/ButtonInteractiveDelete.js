import React from "react";
import {MdHelp, MdDone, MdDelete} from 'react-icons/md';
import {withTheme} from '../lib/theme';
import ButtonInteractive from './ButtonInteractive';
import {withTimeout} from '@formanta/react';

class ButtonInteractiveDeleteBase extends React.Component {
    state = {
        forcePress: false
    };

    evtid = false;

    componentWillUnmount() {
        if(this.evtid) {
            clearTimeout(this.evtid);
        }
    }

    onPress = (pressed_qty) => {
        const {setTimeout} = this.props;

        if(0 === pressed_qty) {
            this.evtid = setTimeout(() => {
                this.setState({
                    forcePress: 0
                });

                setTimeout(this.setState({
                    forcePress: false
                }), 0);
            }, 4000);
        } else if(0 < pressed_qty) {
            if(this.evtid) {
                clearTimeout(this.evtid);
            }
        }
    };

    render() {
        const {
            theme,
            children,
            onClick, comp,
            className, style, size,
            resetTimeout
        } = this.props;

        return (
            <ButtonInteractive
                comp={comp}
                onPress={this.onPress}
                forcePress={this.state.forcePress}
                onClick={onClick}
                children={children}
                className={className}
                style={style}
                resetTimeout={resetTimeout}
                step={[
                    <MdDelete size={size || '1em'} color={theme.textColor} style={{verticalAlign: 'bottom'}}/>,
                    <MdHelp size={size || '1em'} color={theme.textColor} style={{verticalAlign: 'bottom'}}/>,
                    <MdDone size={size || '1em'} color={theme.textColor} style={{verticalAlign: 'bottom'}}/>,
                ]}
            />
        )
    }
}

const ButtonInteractiveDelete = withTimeout(withTheme(ButtonInteractiveDeleteBase));

export default ButtonInteractiveDelete;
export {ButtonInteractiveDelete};
