import React from 'react';

const createContext = (defaultValue) => React.createContext(defaultValue || {});

class PropsProvider extends React.PureComponent {

    constructor(props) {
        super(props);

        this.state = {};

        const {stateProps} = this.props;

        if(stateProps) {
            stateProps.forEach((name) => {
                // eslint-disable-next-line
                this.state[name] = this.props[name];
            });
        }
    }

    componentDidUpdate(prevProps) {
        const {stateProps} = this.props;

        if(stateProps) {
            stateProps.forEach((name) => {
                this.updateIfNeeded(prevProps, name, this.props);
            });
        }
    }

    updateIfNeeded(prevProps, name, props) {
        if(prevProps[name] !== props[name]) {
            this.setState({[name]: props[name]});
        }
    }

    render() {
        const {provider: Provider} = this.props;

        return (
            <Provider value={this.state}>
                {this.props.children}
            </Provider>
        );
    }
}

const createHoc =
    ContextConsumer => (
        Component => (
            props => {
                return <ContextConsumer>
                    {context => <Component {...context} {...props}/>}
                </ContextConsumer>
            }
        )
    );

export {PropsProvider, createContext, createHoc};
