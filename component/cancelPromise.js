import React from 'react';

const cancelPromise = (WrappedComponent) =>
    class extends React.Component {
        state = {
            cancelled: true
        };

        constructor(props) {
            super(props);
            this.cancelPromise = this.cancelPromise.bind(this);
        }

        componentDidMount() {
            this.setState({cancelled: false});
        }

        componentWillUnmount() {
            this.setState({cancelled: true});
        }

        cancelPromise(promise) {
            return new Promise((resolve, reject) => {
                promise.then((...data) => {
                    if(this.state.cancelled) {
                        reject({cancelled: true});
                    } else {
                        resolve(...data);
                    }
                }).catch((...error) => {
                    reject(...error);
                });
            });
        }

        render() {
            return (
                <WrappedComponent {...this.props} cancelPromise={this.cancelPromise}/>
            );
        };
    };

export {cancelPromise};