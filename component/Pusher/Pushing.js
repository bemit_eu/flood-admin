import {ApiRequest} from "../../lib/ApiRequest";

class Pushing {
    constructor(name, context, request) {
        this.name = name;
        this.context = context;
        this.request = request;
        this.state = 'init';
        /**
         * if this pushing is bound to component which dispatched this pushing
         * @type {boolean}
         */
        this.boundToDispatcher = true;
        this.time = new Date();
    }
}

function doApiRequest(handleFail, type, endpoint, name, data = {}) {
    return new Pushing(
        name,
        endpoint,
        (resolve, reject) => {
            (new ApiRequest('api', type, endpoint, data))
                .debug(true)
                .header({})
                .exec()
                .then((res) => {
                    if(res && res.data) {
                        if(res.data.error) {
                            return Promise.reject(res);
                        }
                        resolve(res.data);
                    } else {
                        reject();
                    }
                })
                .catch((err) => {
                    handleFail(err, name);
                    reject(err);
                });
        }
    )
};

export default Pushing;

export {doApiRequest};