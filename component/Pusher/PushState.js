import React from 'react';
import {MdFileUpload, MdList, MdAutorenew, MdDone, MdError,} from 'react-icons/md';

import TimeAgo from '../../component/TimeAgo';

import {useTheme, createUseStyles} from '../../lib/theme';
import {withPusher} from './Pusher';

const useStyles = createUseStyles(theme => ({
    wrapper: {
        position: 'relative'
    },
    state: {
        position: 'absolute',
        top: 0,
        right: 0,
        fontFamily: theme.monospace,
        fontSize: '0.875rem',
        margin: '0 3px',
    },
    detailsWrapper: {
        position: 'absolute',
        zIndex: 3,
        top: '100%',
        right: '50%',
        padding: '2px 6px',
        transform: 'translateX(50%)'
    },
    detailItem: {
        display: 'flex',
        minWidth: '250px',
        '& > *:not(:last-child)': {
            marginRight: '6px'
        }
    },
    detailItemInner: {
        margin: '0',
        '&:not(:last-child)': {
            marginRight: '6px'
        }
    },
    stats: {
        display: 'flex',
        minWidth: '150px'
    },
    statsItem: {
        display: 'flex',
        alignItems: 'center',
        fontFamily: theme.monospace,
        fontSize: '0.875rem',
        margin: '0 3px',
        '& svg': {
            marginRight: '3px',
        }
    }
}));

const PushStateBase = (props) => {
    const {
        open, toggleOpen,
        className,
        pushings, pusherStats
    } = props;

    const theme = useTheme();
    const classes = useStyles(theme);

    /*
     * PusherProvider is e.g. not mounted when Login is expired, but header with PushState is
     */
    return pusherStats ? <div className={classes.wrapper}>
        <button className={className} style={{color: theme.textColor}} onClick={toggleOpen}>
            <MdFileUpload color={theme.textColor} size={'1.5em'}/>
            <p className={classes.state}>{pusherStats.doing ? pusherStats.doing : null}</p>
        </button>
        {open ?
            <div className={classes.detailsWrapper} style={{background: theme.bgPage, border: '1px solid ' + theme.textColorLight}}>
                {Object.keys(pushings).slice(0).reverse().map((id) => (
                    <div key={id} data-key={id} className={classes.detailItem}>
                        <p className={classes.detailItemInner}><TimeAgo time={pushings[id].time}/></p>
                        <p className={classes.detailItemInner}>{pushings[id].name}</p>
                        {'init' === pushings[id].state ? <p className={classes.detailItemInner}><MdList color={theme.textColor} size={'1em'}/></p> : null}
                        {'begin' === pushings[id].state ? <p className={classes.detailItemInner}><MdAutorenew color={theme.textColor} size={'1em'}/></p> : null}
                        {'success' === pushings[id].state ? <p className={classes.detailItemInner}><MdDone color={theme.textColor} size={'1em'}/></p> : null}
                        {'error' === pushings[id].state ? <p className={classes.detailItemInner}><MdError color={theme.textColor} size={'1em'}/></p> : null}
                    </div>
                ))}
                <div className={classes.stats}>
                    <p className={classes.statsItem} style={{marginLeft: 'auto'}}><MdList color={theme.textColor} size={'1em'}/> {pusherStats.all ? pusherStats.all : 0}</p>
                    <p className={classes.statsItem}><MdAutorenew color={theme.textColor} size={'1em'}/> {pusherStats.doing ? pusherStats.doing : 0}</p>
                    <p className={classes.statsItem}><MdDone color={theme.textColor} size={'1em'}/> {pusherStats.success ? pusherStats.success : 0}</p>
                    <p className={classes.statsItem} style={{marginRight: 'auto'}}><MdError color={theme.textColor} size={'1em'}/> {pusherStats.failed ? pusherStats.failed : 0}</p>
                </div>
            </div> : null}
    </div> : null;
};

const PushState = withPusher(PushStateBase);

class PushStateDrop extends React.Component {
    state = {
        open: false,
    };

    render() {
        return <PushState open={this.state.open} toggleOpen={() => this.setState({open: !this.state.open})} {...this.props}/>;
    }
}

export default PushStateDrop;
