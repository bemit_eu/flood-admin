import React from 'react';
import {withStyles} from '../lib/theme';
import StyleGrid from '../lib/StyleGrid';

const styles = {
    wrapper: {
        display: 'flex',
        overflow: 'hidden auto',
        height: '100%',
        width: '100%',
        flexShrink: 1,
        flexDirection: 'column',
        [StyleGrid.smUp]: {}
    },
    wrapperInner: {
        display: 'flex',
        overflow: 'auto',
        height: '100%',
        flexShrink: 1,
        flexDirection: 'column',
        paddingRight: '9px',
        [StyleGrid.smUp]: {}
    },
};

class SplitScreenBase extends React.Component {
    wrapper = null;
    top = null;
    center = null;

    scrollTop = () => {

    };

    render() {
        const {
            classes,
            isTopOpen,
            topRender, top, center
        } = this.props;

        return (
            <div
                className={classes.wrapper}
                style={{}}
                ref={(r) => {
                    this.wrapper = r
                }}
            >
                <div
                    className={classes.wrapperInner}
                    ref={(r) => {
                        this.wrapperInner = r
                    }}
                >
                    <div
                        style={{
                            display: 'flex',
                            overflow: 'auto',
                            /*height: 'min-content',*/
                            flexShrink: 0,
                            flexDirection: 'column'
                        }}
                        ref={(r) => {
                            this.top = r
                        }}
                    >
                        {topRender ? topRender({scrollTop: this.scrollTop}) : top}
                    </div>
                    <div
                        style={{
                            display: 'flex',
                            overflow: isTopOpen ? 'visible' : 'hidden',
                            maxHeight: isTopOpen ? '100%' : 'unset',
                            height: isTopOpen ? 'auto' : 'auto',
                            flexShrink: isTopOpen ? 0 : 'unset',
                            flexGrow: 3,
                            flexDirection: 'column',
                            marginBottom: 'auto'
                        }}
                        ref={(r) => {
                            this.center = r
                        }}
                    >
                        {center}
                    </div>
                </div>
            </div>
        );
    }
}

const SplitScreen = withStyles(styles)(SplitScreenBase);

const ContainerStyle = {
    display: 'flex',
    overflowY: 'auto',
    height: '100%',
    flexShrink: 1,
    flexDirection: 'column',
    marginRight: '-9px'
};

export {
    SplitScreen,
    ContainerStyle
};
