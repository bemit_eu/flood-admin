import React from 'react';
import {withNamespaces} from 'react-i18next';
import {withStyles, withTheme} from '../../lib/theme';
import StyleGrid from "../../lib/StyleGrid";
import Loading from "../../component/Loading";
import ContentItem from './ContentItem';

const styles = theme => ({
    wrapper: {
        margin: theme.gutter.base * 4 + 'px 0',
        [StyleGrid.smUp]: {},
        '&.list': {
            display: 'flex',
            flexWrap: 'wrap',
        },
        '&.grid': {
            display: 'grid',
            gridTemplateColumns: '1fr',
            gridColumnGap: '12px',
            gridRowGap: '12px',
            [StyleGrid.smUp]: {
                gridTemplateColumns: '1fr 1fr',
            },
            [StyleGrid.mdUp]: {},
            [StyleGrid.lgUp]: {
                gridTemplateColumns: '1fr 1fr 1fr 1fr'
            }
        }
    },
});

class ContentPane extends React.Component {

    render() {
        const {
            t, theme, classes,
            list, loaded,
            gotoSubDir, currentDir,
            onSelect, selectFile, selectDir, multiple, selected,
            hasSelected, viewMode,
        } = this.props;

        let path_slug = '';
        if(currentDir) {
            path_slug = currentDir;
        }

        return (
            <div className={classes.wrapper + ' ' + viewMode}>
                {('progress' === loaded || 'error' === loaded ?
                    <Loading style={{
                        wrapper: {margin: 0},
                        item: ('error' === loaded ? {borderColor: theme.errorColor} : {}),
                    }}/> :
                    null)}

                {true === loaded && list ?
                    <React.Fragment>
                        {list.dir && 0 < list.dir.length ?
                            list.dir.map((dir, i) => (
                                <ContentItem key={i}
                                             currentDir={currentDir}
                                             className={classes.item}
                                             type='dir'
                                             item={dir}
                                             viewMode={viewMode}
                                             gotoSubDir={gotoSubDir}
                                             hasSelected={hasSelected}
                                             multiple={multiple}
                                             selected={(selected && selected['dir'] && selected['dir'][path_slug + '/' + dir.name])}
                                             onSelect={selectDir ? onSelect : false}
                                />
                            )) : null}

                        {list.file && 0 < list.file.length ?
                            list.file.map((file, i) => (
                                <ContentItem key={i}
                                             currentDir={currentDir}
                                             className={classes.item}
                                             type='file'
                                             item={file}
                                             viewMode={viewMode}
                                             hasSelected={hasSelected}
                                             multiple={multiple}
                                             selected={(selected && selected['file'] && selected['file'][path_slug + '/' + file.name])}
                                             onSelect={selectFile ? onSelect : false}
                                />
                            )) : null}

                        {((!list.dir || 0 === list.dir.length) && (!list.file || 0 === list.file.length)) ?
                            t('nothing-in-dir') : null}
                    </React.Fragment>
                    : null}
            </div>
        );
    }
}

export default withNamespaces('file-explorer')(withStyles(styles)(withTheme(ContentPane)));
