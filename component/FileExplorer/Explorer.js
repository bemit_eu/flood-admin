import React from 'react';
import {withNamespaces} from 'react-i18next';

import ControlBar from "./ControlBar";
import ContentPane from "./ContentPane";
import {storeMedia} from "../../feature/Media/Stores/Media";
import {cancelPromise} from "../cancelPromise";

class Explorer extends React.PureComponent {

    state = {
        currentDir: '',
        viewMode: 'list',
    };

    toggleViewMode = () => {
        let next = 'list';
        if('list' === this.state.viewMode) {
            next = 'grid';
        }
        window.localStorage.setItem('explorer-viewMode', next);
        this.setState({
            viewMode: next
        })
    };

    componentDidMount() {
        let last_view_mode = window.localStorage.getItem('explorer-viewMode');
        if(null !== last_view_mode && '' !== last_view_mode.trim()) {
            this.setState({
                viewMode: last_view_mode
            })
        }
        this.loadList();
    }

    loadList = (folder, force_load = false) => {
        const {loadFolder} = this.props;

        loadFolder(folder, force_load);
    };

    loadParent = () => {
        let currentDir = this.state.currentDir;
        if(0 === currentDir.indexOf('/')) {
            currentDir = currentDir.substr(1);
        }

        currentDir = currentDir.split('/');
        currentDir.splice(currentDir.length - 1, 1);

        let parentDir = '/' + currentDir.join('/');
        if(0 === currentDir.length) {
            parentDir = '';
        }

        this.setState({
            currentDir: parentDir
        });

        this.loadList(parentDir);

        if(this.props.onSwitchDir) {
            this.props.onSwitchDir(parentDir);
        }

        if(this.props.clearSelect) {
            this.props.clearSelect();
        }
    };

    gotoSubDir = (dir) => {
        const nextDir = this.state.currentDir + '/' + dir;
        this.setState({
            currentDir: nextDir
        });

        this.loadList(nextDir);

        if(this.props.onSwitchDir) {
            this.props.onSwitchDir(nextDir);
        }

        if(this.props.clearSelect) {
            this.props.clearSelect();
        }
    };

    select = (type, itemId, shouldAdd) => {
        const {onSelect} = this.props;
        if(onSelect) {
            let fullId = '/' + itemId;
            if(this.state.currentDir) {
                fullId = this.state.currentDir + fullId;
            }
            onSelect(type, fullId, shouldAdd);
        }
    };

    render() {
        const {
            selectFile, selectDir,
            multiple,
            hasSelected, selected,
            isFolderLoaded, folders, folderRoot
        } = this.props;
        const {viewMode} = this.state;
        const loaded = isFolderLoaded(this.state.currentDir);

        return (
            <div>
                <ControlBar loadList={this.loadList}
                            viewMode={viewMode}
                            toggleViewMode={this.toggleViewMode}
                            loadParent={this.loadParent}
                            hasSelected={hasSelected}
                            currentDir={this.state.currentDir}/>

                <ContentPane gotoSubDir={this.gotoSubDir}
                             selectFile={selectFile}
                             selectDir={selectDir}
                             multiple={multiple}
                             onSelect={this.select}
                             currentDir={this.state.currentDir}
                             loaded={loaded}
                             list={this.state.currentDir ? folders[this.state.currentDir] : folderRoot}
                             hasSelected={hasSelected}
                             selected={selected}
                             viewMode={viewMode}/>
            </div>
        );
    }
}

export default withNamespaces('file-explorer')(cancelPromise(storeMedia(Explorer)));
