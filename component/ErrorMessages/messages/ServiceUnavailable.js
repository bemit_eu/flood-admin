import React from 'react';
import BaseMessage from '../BaseMessage';

class ServiceUnavailable extends React.Component {

    render() {
        const {index, t, /*fatal, */label} = this.props;

        return (
            <BaseMessage index={index} {...this.props}
                         type={t('error-types.service-unavailable')}
                         label={label}
            />
        );
    }
}

export default ServiceUnavailable;
