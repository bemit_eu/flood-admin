import React from 'react';
import BaseMessage from '../BaseMessage';

class BadRequest extends React.Component {

    render() {
        const {index, t, /*fatal, */label} = this.props;

        return (
            <BaseMessage index={index} {...this.props}
                         type={t('error-types.bad-request')}
                         label={label}
            />
        );
    }
}

export default BadRequest;
