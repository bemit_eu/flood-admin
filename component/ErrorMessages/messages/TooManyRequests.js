import React from 'react';
import BaseMessage from '../BaseMessage';

class TooManyRequests extends React.Component {

    render() {
        const {index, t, /*fatal, */label} = this.props;

        return (
            <BaseMessage index={index} {...this.props}
                         type={t('error-types.too-many-requests')}
                         label={label}
            />
        );
    }
}

export default TooManyRequests;
