import React from 'react';
import BaseMessage from '../BaseMessage';

class LoginExpired extends React.Component {

    render() {
        const {index, t, /*fatal, */label} = this.props;

        return (
            <BaseMessage index={index} {...this.props}
                         type={t('error-types.login-expired')}
                         label={label}
            />
        );
    }
}

export default LoginExpired;
