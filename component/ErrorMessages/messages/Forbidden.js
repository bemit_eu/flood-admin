import React from 'react';
import BaseMessage from '../BaseMessage';

class Forbidden extends React.Component {

    render() {
        const {index, t, /*fatal, */label} = this.props;

        return (
            <BaseMessage index={index} {...this.props}
                         type={t('error-types.forbidden')}
                         label={label}
            />
        );
    }
}

export default Forbidden;
