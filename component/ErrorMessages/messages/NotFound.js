import React from 'react';
import BaseMessage from '../BaseMessage';

class NotFound extends React.Component {

    render() {
        const {index, t, /*fatal, */label} = this.props;

        return (
            <BaseMessage index={index} {...this.props}
                         type={t('error-types.not-found')}
                         label={label}
            />
        );
    }
}

export default NotFound;
