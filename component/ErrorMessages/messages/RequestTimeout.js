import React from 'react';
import BaseMessage from '../BaseMessage';

class RequestTimeout extends React.Component {

    render() {
        const {index, t, /*fatal, */label} = this.props;

        return (
            <BaseMessage index={index} {...this.props}
                         type={t('error-types.request-timeout')}
                         label={label}
            />
        );
    }
}

export default RequestTimeout;
