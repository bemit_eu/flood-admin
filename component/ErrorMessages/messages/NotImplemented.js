import React from 'react';
import BaseMessage from '../BaseMessage';

class NotImplemented extends React.Component {

    render() {
        const {index, t, /*fatal, */label} = this.props;

        return (
            <BaseMessage index={index} {...this.props}
                         type={t('error-types.not-implemented')}
                         label={label}
            />
        );
    }
}

export default NotImplemented;
