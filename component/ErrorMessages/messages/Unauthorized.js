import React from 'react';
import BaseMessage from '../BaseMessage';

class Unauthorized extends React.Component {

    render() {
        const {index, t, /*fatal, */label} = this.props;

        return (
            <BaseMessage index={index} {...this.props}
                         type={t('error-types.unauthorized')}
                         label={label}
            />
        );
    }
}

export default Unauthorized;
