import React from 'react';
import BaseMessage from '../BaseMessage';

class ServerError extends React.Component {

    render() {
        const {index, t, fatal, label} = this.props;

        return (
            <BaseMessage index={index} {...this.props}
                         type={t('error-types.server-error')}
                         label={label}
                         text={fatal ? t('error-messages.reload-or-contact') : t('error-messages.retry-or-contact')}
            />
        );
    }
}

export default ServerError;
