import React from 'react';
import {createUseStyles} from '../../lib/theme';
import {withErrorControl} from "../ErrorControl";
import StyleGrid from "../../lib/StyleGrid";
import {withNamespaces} from "../../lib/i18n";

const useStyles = createUseStyles({
    wrapper: {
        position: 'fixed',
        maxHeight: '100vh',
        overflow: 'auto',
        bottom: 0,
        left: '50%',
        transform: 'translateX(-50%)',
        width: '95vw',
        [StyleGrid.smUp]: {
            width: '80vw',
        },
        [StyleGrid.mdUp]: {
            width: 'auto'
        }
    }
});

const MessagesContainerBase = (props) => {
    const {
        // pushing down t from here to reduce i18n performance impact
        t,
        errors
    } = props;
    const classes = useStyles();

    return (
        0 < errors.length ?
            <div className={classes.wrapper}>
                {errors.map((Err, i) => (
                    <Err.render key={Err.key} index={i} t={t} fatal={Err.fatal} details={Err.details} timestamp={Err.timestamp} label={Err.label}/>
                ))}
            </div>
            : null
    );
};

const MessagesContainer = withNamespaces('common')(withErrorControl(MessagesContainerBase));

export {MessagesContainer};
