import React from 'react';
import Loadable from 'react-loadable';
import Loading from '../../component/Loading';

const errorMessages = {
    BadRequest: {
        id: 'bad-request',
        render: Loadable({
            loader: () => import('./messages/BadRequest'),
            loading: (props) => <Loading {...props} name='ErrorMessages/BadRequest'/>,
        })
    },
    Unauthorized: {
        id: 'client-unauthorized',
        render: Loadable({
            loader: () => import('./messages/Unauthorized'),
            loading: (props) => <Loading {...props} name='ErrorMessages/Unauthorized'/>,
        })
    },
    LoginExpired: {
        id: 'client-login-expired',
        render: Loadable({
            loader: () => import('./messages/LoginExpired'),
            loading: (props) => <Loading {...props} name='ErrorMessages/LoginExpired'/>,
        })
    },
    Forbidden: {
        id: 'client-is-forbidden',
        render: Loadable({
            loader: () => import('./messages/Forbidden'),
            loading: (props) => <Loading {...props} name='ErrorMessages/Forbidden'/>,
        })
    },
    NotFound: {
        id: 'not-found',
        render: Loadable({
            loader: () => import('./messages/NotFound'),
            loading: (props) => <Loading {...props} name='ErrorMessages/NotFound'/>,
        })
    },
    RequestTimeout: {
        id: 'client-request-timeout',
        render: Loadable({
            loader: () => import('./messages/RequestTimeout'),
            loading: (props) => <Loading {...props} name='ErrorMessages/RequestTimeout'/>,
        })
    },
    TooManyRequests: {
        id: 'client-too-many-requests',
        render: Loadable({
            loader: () => import('./messages/TooManyRequests'),
            loading: (props) => <Loading {...props} name='ErrorMessages/TooManyRequests'/>,
        })
    },
    ServerError: {
        id: 'server-error-fatal',
        render: Loadable({
            loader: () => import('./messages/ServerError'),
            loading: (props) => <Loading {...props} name='ErrorMessages/ServerError'/>,
        })
    },
    NotImplemented: {
        id: 'server-not-implemented',
        render: Loadable({
            loader: () => import('./messages/NotImplemented'),
            loading: (props) => <Loading {...props} name='ErrorMessages/NotImplemented'/>,
        })
    },
    ServiceUnavailable: {
        id: 'server-service-unavailable',
        render: Loadable({
            loader: () => import('./messages/ServiceUnavailable'),
            loading: (props) => <Loading {...props} name='ErrorMessages/ServiceUnavailable'/>,
        })
    },
};

export {errorMessages};