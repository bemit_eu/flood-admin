import React from 'react';
import posed from 'react-pose';
import {MdClose, MdInfo} from "react-icons/md";
import StyleGrid from "../../lib/StyleGrid";
import {withTheme, withStyles} from '../../lib/theme';
import {withErrorControl} from "../ErrorControl";
import {timeToIso} from "@formanta/js";

const styles = {
    wrapper: {
        display: 'flex',
        flexWrap: 'wrap',
        overflow: 'hidden',
        padding: '6px 12px',
        margin: '0 0 6px 0'
    },
    close: {
        marginLeft: 'auto'
    },
    textWrapper: {
        width: '100%',
        margin: '0 0 6px 0',
        textAlign: 'center',
        display: 'flex',
        flexWrap: 'wrap',
        [StyleGrid.smUp]: {
            display: 'block',
        }
    },
    lbl: {
        paddingRight: '3px',
        fontSize: '0.85rem'
    },
    type: {},
    label: {
        marginRight: 'auto',
        [StyleGrid.smUp]: {
            marginRight: '0',
        }
    },
    textDash: {
        display: 'none',
        [StyleGrid.smUp]: {
            display: 'inline-block'
        }
    },
    text: {
        [StyleGrid.smUp]: {
            display: 'inline'
        }
    },
    detailsBtn: {
        marginLeft: '6px',
        verticalAlign: 'middle'
    },
    details: {
        width: '100%'
    },
    detailsText: {
        fontSize: '0.85rem',
        [StyleGrid.smUp]: {
            fontSize: '1rem',
        }
    }
};

const Wrapper = posed.div({
    hidden: {
        height: 0,
    },
    visible: {
        height: 'auto',
    }
});

class BaseMessage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: true,
            showDetails: false
        };
    }

    hideAndSolve = () => {
        this.setState({visible: false});

        setTimeout(() => {
            this.props.solveError(this.props.index);
        }, 400);
    };

    render() {
        const {
            theme, classes,
            timestamp, type, label, text, details
        } = this.props;

        return (
            <Wrapper className={classes.wrapper}
                     withParent={false}
                     pose={this.state.visible ? 'visible' : 'hidden'}
                     style={{
                         background: theme.bgPage,
                         border: '2px solid ' + theme.errorColor
                     }}
            >
                <button className={classes.close} onClick={this.hideAndSolve}><MdClose color={theme.textColor}/></button>
                <p className={classes.textWrapper}>
                    <span className={classes.lbl}>{timeToIso(timestamp)}</span>
                    {type || label ? <span className={classes.lbl}>-</span> : null}
                    {type ? <span className={classes.lbl + ' ' + classes.type}>{type}</span> : null}
                    {type && label ? <span className={classes.lbl}>-</span> : null}
                    {label ? <span className={classes.lbl + ' ' + classes.label}>{label}</span> : null}
                    {/* this dash is intended to be bigger than other texts here */}
                    {(type || label) && text ? <span className={classes.textDash} style={{paddingRight: '3px'}}>-</span> : null}
                    {text ? <span className={classes.lbl + ' ' + classes.text}>{text}</span> : null}
                    {details ?
                        <button
                            onClick={() => this.setState({showDetails: !this.state.showDetails})}
                            className={classes.detailsBtn}
                        ><MdInfo color={theme.textColor} size={'0.85em'}/></button>
                        : null}
                </p>

                {details && this.state.showDetails ? (
                    <div className={classes.details}>{
                        Object.keys(details).map((key, i) => (
                            <p className={classes.detailsText} key={i + key} style={{margin: '0 0 3px 0'}}>▪ {key}: {details[key]}</p>
                        ))
                    }</div>
                ) : null}
            </Wrapper>
        );
    }
}

export default withErrorControl(withStyles(styles)(withTheme(BaseMessage)));
