import React from 'react';
import {MdTranslate} from 'react-icons/md';
import {withNamespaces} from 'react-i18next';
import {useTheme, withStyles, withTheme,} from '../lib/theme';
import {i18nEvents} from "../lib/i18n";

const styles = theme => ({
    selection: {
        position: 'absolute',
        zIndex: 100,
        background: theme.accent.blue,
        transform: 'translateX(-50%)',
        left: '50%',
        top: 'calc(100% - ' + theme.gutter.base * 2 + 'px)',
        padding: '3px 0'
    },
    toggle: {
        /*display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',*/
        height: '100%',
        margin: '0 -6px',
        padding: '0 6px'
    },
    btn: {
        display: 'block',
        // todo: add theme color
        color: '#ffffff',
        //color: theme.textColor,
        padding: theme.gutter.base * 2 + 'px 12px',
        margin: '0 0 2px 0'
    }
});
const IcTranslate = () => {
    const theme = useTheme();
    return <MdTranslate color={theme.textColor} size={'1.5em'}/>;
};

class LocaleSwitch extends React.Component {
    state = {
        open: false
    };

    render() {
        const {
            classes,
            match, history, i18n
        } = this.props;

        /**
         * Change the current language, get the current url and url scheme, search for key `:lng` in pathscheme and use that index to manipulate history
         * @param lng
         */
        const changeLanguage = (lng) => {
            let cur_path = history.location.pathname.substr(1).split('/');
            let path_pos = match.path.substr(1).split('/').indexOf(':lng');

            i18nEvents.trigger('languageChanging', [lng, history]);

            cur_path[path_pos] = lng;
            i18n.changeLanguage(lng);
            history.push('/' + cur_path.join('/'));
        };

        /**
         * Open/Close Language Switch
         */
        const toggle = () => {
            this.setState({open: !this.state.open})
        };

        return (
            <div className={this.props.className} style={{position: 'relative',}}>
                <button onClick={toggle} className={classes.toggle}><IcTranslate/></button>

                {this.state.open ? <div className={classes.selection}>
                    <button className={classes.btn} onClick={() => {
                        toggle();
                        changeLanguage('de')
                    }}>de
                    </button>
                    <button className={classes.btn} onClick={() => {
                        toggle();
                        changeLanguage('en')
                    }}>en
                    </button>
                </div> : null}
            </div>
        )
    }
}

export default withNamespaces()(withTheme(withStyles(styles)(LocaleSwitch)));
