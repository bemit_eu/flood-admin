import React from 'react';

import Nav from '../component/Nav/Nav';

import {navContent} from "../nav";
import {withFeatureManager} from "../component/FeatureManager";
import {MdMoreVert} from "react-icons/md";
import {withTheme} from '../lib/theme';

class NavMainBase extends React.PureComponent {
    state = {
        navMain: false,
    };

    constructor(props) {
        super(props);

        const {isEnabled} = props;

        this.state.navMain = navContent(isEnabled);
    }

    componentDidUpdate(prevProps) {
        if(prevProps.features !== this.props.features) {
            this.setState({
                navMain: navContent(this.props.isEnabled),
            });
        }
    }

    render() {
        const {
            theme,
            toggleSidebar
        } = this.props;

        return <React.Fragment>
            <button className={'mobile-nav-toggle'} onClick={toggleSidebar}>
                <MdMoreVert size={'1.75em'} color={theme.linkColor}/>
            </button>
            <Nav content={this.state.navMain} notxt={this.props.notxt} switchBorderPos={true}/>
        </React.Fragment>;
    }
}

export const NavMain = withFeatureManager(withTheme(NavMainBase));
