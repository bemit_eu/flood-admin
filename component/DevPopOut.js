import React from "react";
import {ButtonRaw} from "./Form";
import {MdOpenInNew} from "react-icons/md";
import ReactJson from 'react-json-view';
import NewWindow from 'react-new-window';
import {withTheme, withStyles} from '../lib/theme';

const styles = {
    reactJson: {
        '& .react-json-view': {
            backgroundColor: 'transparent !important',
        },
        '& .react-json-view .object-key-val': {
            fontSize: '0.85rem',
            fontFamily: 'Inconsolata, monospace',
        },
    },
};

class DevPopOutBase extends React.Component {

    state = {
        poppedOut: false,
    };

    render() {
        const {
            theme, classes,
            name, title, label,
            json, jsons,
        } = this.props;

        const elems = json ? [json] : jsons;

        return window.localStorage.getItem('flood-dev') * 1 ? <div style={{textAlign: 'right'}}>
            {this.state.poppedOut ? <NewWindow
                name={name}
                title={title}
                onUnload={(e) => this.setState({poppedOut: false})}
            >
                {elems.map((elem, i) => (
                    <div key={i} className={classes.reactJson} style={{padding: 12}}>
                        <ReactJson src={elem}
                                   theme={theme.name === 'dark' ? 'apathy' : 'apathy:inverted'}
                                   name={name}
                                   displayObjectSize
                                   displayDataTypes
                                   renderOnlyWhenOpen
                        />
                    </div>
                ))}
            </NewWindow> : null}
            <ButtonRaw
                style={{
                    color: theme.textColor, background: theme.bgSpecial,
                    display: 'flex',
                    flexShrink: 0,
                    padding: label ? '0 3px' : 0
                }}
                active={this.state.poppedOut} onClick={() => this.setState({poppedOut: !this.state.poppedOut})}>
                {label ? <span style={{margin: 'auto 4px auto 3px'}}>{label}</span> : null}
                <MdOpenInNew color={this.state.poppedOut ? theme.linkColor : theme.textColor} style={{margin: '6px 3px'}}/>
            </ButtonRaw>
        </div> : null
    }
}

const DevPopOut = withStyles(styles)(withTheme(DevPopOutBase));

export {DevPopOut}
