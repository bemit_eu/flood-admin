import React from "react";
import {MdCheck, MdError} from "react-icons/md";

import {fileSize2Human} from "@formanta/js";
import {useTheme, createUseStyles} from '../../lib/theme';
import ButtonInteractiveDelete from '../ButtonInteractiveDelete';
import StyleGrid from "../../lib/StyleGrid";

const useStyles = createUseStyles(theme => ({
    wrapper: {
        padding: theme.gutter.base + 'px 3px',
        width: '50%',
        [StyleGrid.smUp]: {
            width: '33%'
        },
        [StyleGrid.lgUp]: {
            width: '25%'
        }
    },
    innerWrapper: {
        position: 'relative',
        height: '100%',
        padding: '2px 6px',
    },
    preview: {
        width: '100%'
    },
    head: {
        display: 'flex'
    },
    info: {},
    infoName: {
        wordBreak: 'break-all'
    },
    infoSize: {
        wordBreak: 'break-all'
    },
    infoType: {
        wordBreak: 'break-all'
    },
    progress: {},
    progressBar: {
        width: '100%',
        border: 0,
        padding: '3px',
    },
}));

class FilePreviewReader extends React.PureComponent {

    state = {
        src: false
    };

    componentDidUpdate() {
        this.readFile();
    }

    componentDidMount() {
        this.readFile();
    }

    readFile = () => {
        let reader = new FileReader();
        reader.readAsDataURL(this.props.file);
        reader.onloadend = () => {
            this.setState({src: reader.result});
        }
    };

    render() {
        return (
            this.state.src ?
                <img src={this.state.src}
                     style={{width: '100%'}}
                     alt={this.props.file.name}
                />
                : null
        )
    }
}

const FilePreview = (props) => {

    const theme = useTheme();
    const classes = useStyles(theme);

    return (
        <div className={classes.wrapper}>
            <div className={classes.innerWrapper} style={{border: '1px solid ' + theme.textColorLight}}>
                <div className={classes.head}>
                    <ButtonInteractiveDelete resetTimeout={0}
                                             onClick={() => {
                                                 props.delete(props.id);
                                             }}/>

                    {'init' !== props.progress ?
                        'success' === props.progress ?
                            <MdCheck style={{marginLeft: 'auto'}}/> :
                            'error' === props.progress ?
                                <MdError style={{marginLeft: 'auto'}}/> : null
                        : null}
                </div>

                {'image/jpeg' === props.file.type || 'image/png' === props.file.type || 'image/gif' === props.file.type ?
                    <div className={classes.preview}>
                        <FilePreviewReader file={props.file}/>
                    </div>
                    : null}

                <div className={classes.info}>
                    <p className={classes.infoName}>{props.file.name}</p>
                    <p className={classes.infoSize}>{fileSize2Human(props.file.size)}</p>
                    <p className={classes.infoType}>{props.file.type}</p>
                </div>

                {'init' !== props.progress ?
                    <div className={classes.progress}>
                        {'success' !== props.progress && 'error' !== props.progress ?
                            <progress className={classes.progressBar}
                                      max={100}
                                      value={props.progress}
                                      style={{background: theme.bgPage}}
                            >{Math.round(props.progress)}%</progress> : null}
                    </div>
                    : null}
            </div>
        </div>
    );
};

export default FilePreview;
