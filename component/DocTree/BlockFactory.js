import React from 'react';

import Loadable from 'react-loadable';
import Loading from '../../component/LoadingSmall';

const factory = {
    'content': Loadable({
        loader: () => import('./Block/ContentBlock'),
        loading: (props) => <Loading {...props} name='DocTreeEditor/Block/ContentBlock'/>,
    }),
    'mainText': Loadable({
        loader: () => import('./Block/MainText'),
        loading: (props) => <Loading {...props} name='DocTreeEditor/Block/MainText'/>,
    }),
};

const UnkownBlock = (props) => (
    <React.Fragment>
        <p>Not-Supported-Type: `{props.data.type}{props.data.id ? '`, ID: `' + props.data.id : null}{props.data.alias ? '`, Alias: `' + props.data.alias : null}{props.data.tpl ? '`, tpl: `' + props.data.tpl : null}`</p>
    </React.Fragment>
);

class BlockTreeFactory extends React.PureComponent {
    render() {
        const {
            t,
            data,
            root_name, index,
            name, uid,
            head,
            deleteItem,
            onToggle, open,
            elemRef,
        } = this.props;

        const Comp = data && factory.hasOwnProperty(data.type) ? factory[data.type] : false;

        return (
            Comp ?
                <Comp
                    name={name}
                    uid={uid}
                    head={head}
                    innerRef={elemRef}
                    deleteItem={() => deleteItem(root_name, index)}
                    onToggle={onToggle}
                    open={open}
                    root_name={root_name}
                    index={index}
                    t={t}
                    data={data}
                /> :
                <UnkownBlock {...this.props}/>
        );
    };
}

export default BlockTreeFactory;