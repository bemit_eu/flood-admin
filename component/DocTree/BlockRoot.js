// Lib
import React from 'react';
import {Droppable, Draggable} from 'react-beautiful-dnd';

import {withStyles} from '../../lib/theme';
import BlockItem from './BlockItem';
import {rootBlockName} from "./BlockName";
import StyleGrid from "../../lib/StyleGrid";

const styles = theme => ({
    wrapper: {
        margin: '6px 0',
        [StyleGrid.mdUp]: {
            margin: '6px 0 12px 0',
        },
        '& .doctree--blocks-wrapper': {
            minHeight: '50px',
            padding: '0 3px',
            background: theme.drop.area,
            transition: '0.2s linear background',
            overflow: 'hidden'
        },
        '& .doctree--blocks-wrapper.targeted': {
            background: theme.drop.area_targeted,
        },
        '& .doctree--block-item': {
            position: 'relative',
            margin: '6px 0'
        },
        '& .doctree--block-item--inner': {
            position: 'relative',
            minHeight: '30px',
            padding: theme.gutter.base * 2 + 'px ' + theme.gutter.base * 3 + 'px',
        },
        '& .doctree--block-item.sortable-chosen': {},
        '& .doctree--block-item .doctree--block-item--drag-handle': {},
        '& .doctree--block-item.sortable-chosen .doctree--block-item--inner': {
            transition: '0.2s linear background',
        },
    },
});

class BlockRoot extends React.Component {

    render() {
        const {
            classes,
            name,
            blocks,
            deleteItem, addElemRef,
            t
        } = this.props;

        return (
            <Droppable droppableId={name}
                       ignoreContainerClipping={true}>
                {(provided, snapshot) => (
                    <div className={classes.wrapper}>

                        <h4>{rootBlockName(name, t)}</h4>

                        <div className={'doctree--blocks-wrapper ' + (snapshot.isDraggingOver ? 'targeted' : '')}
                             ref={provided.innerRef}
                             {...provided.droppableProps}
                        >
                            {blocks.map((elem, i) => {
                                return (
                                    <Draggable key={elem.uuid}
                                               draggableId={elem.uuid}
                                               index={i}>
                                        {(provided, snapshot) => (
                                            <BlockItem
                                                uid={name + '#' + i}
                                                uuid={elem.uuid}
                                                root_name={name}
                                                provided={provided}
                                                deleteItem={deleteItem}
                                                index={i}
                                                t={t}
                                                data={elem}
                                                isDragging={snapshot.isDragging}
                                                isOverTarget={null !== snapshot.draggingOver}
                                                elemRef={(instance) => {
                                                    addElemRef(name, elem.uuid, i, instance);
                                                }}
                                            />
                                        )}
                                    </Draggable>
                                );
                            })}

                            {provided.placeholder}

                        </div>
                    </div>
                )}
            </Droppable>
        );
    }
}

export default withStyles(styles)(BlockRoot);
