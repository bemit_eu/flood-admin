import React from 'react';
import {withNamespaces} from 'react-i18next';
import {withStyles} from '../../lib/theme';

import BlockTree from './BlockTree';

const styles = {
    wrapper: {
        margin: '15px 0',
        minHeight: '100%'
    },
};

class DocTreeEditor extends React.Component {

    constructor(props) {
        super(props);

        this.block_root = [];
        this.blocks = {};

        const {data, blockroot} = this.props;

        if(data && blockroot) {
            blockroot.forEach((root_name, i) => {
                let root_block = {};
                if(data[root_name]) {
                    root_block = data[root_name];
                }

                // build content blocks for current root block
                this.blocks[root_name] = [];

                // add `default` containing blocks when empty root_block
                if('object' === typeof root_block.children && 0 < root_block.children.length) {
                    this.blocks[root_name] = root_block.children;
                }

                // add current root block
                this.block_root.push(root_name);
            });
        } else {
            if(!data) {
                console.error('DocTreeEditor: error, no `data` specified.');
            }
            if(!blockroot) {
                console.error('DocTreeEditor: error, no `blockroot` specified.');
            }
        }
    }

    componentWillUnmount() {
        // todo: split construct to here
    }

    componentDidMount() {
        // todo: split construct to here
    }

    render() {
        const {
            t, classes,
            addSave, rmSave,
            blockSelection
        } = this.props;

        if(this.block_root && this.blocks) {
            return (
                <div className={classes.wrapper}>
                    <BlockTree root_blocks={this.block_root}
                               blocks={this.blocks}
                               t={t}
                               blockSelection={blockSelection}
                               addSave={addSave}
                               rmSave={rmSave}
                    />
                </div>
            );
        } else {
            return null
        }
    }
}

export default withNamespaces('defaults--doc-tree')(withStyles(styles)(DocTreeEditor));
