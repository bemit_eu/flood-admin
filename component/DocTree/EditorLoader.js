import React from 'react';
import Loadable from 'react-loadable';
import {withNamespaces} from 'react-i18next';
import Loading from '../../component/Loading';

const EditorLoader = Loadable({
    loader: () => import('./Editor'),
    loading: withNamespaces('common')((props) =>
        <Loading
            {...props}
            style={{wrapper: {margin: 0}}}
            name='DocTreeEditor'
            center={true}><p style={{margin: '2px 0', textAlign: 'center'}}>{props.t('loading.doctree')}</p></Loading>
    ),
});

export default EditorLoader;