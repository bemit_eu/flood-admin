// Lib
import React, {Component} from 'react';
import {Droppable, Draggable} from 'react-beautiful-dnd';
import StyleGrid from '../../lib/StyleGrid';
import {withStyles} from '../../lib/theme';
import BlockSelectionFactory from './BlockSelectionFactory';

const styles = theme => ({
    wrapper: {
        display: 'flex',
        width: '100%',
        padding: '12px 0 12px 0',
        margin: '6px 0 12px 0',
        flexWrap: 'wrap',
        [StyleGrid.mdUp]: {
            width: '100%',
            margin: '6px 0 12px 0',
        },
        '&.doctree--blocks-wrapper': {
            minHeight: '50px',
            padding: '6px 0',
            background: 'rgba(150, 150, 150, 0.2)',
            transition: '0.2s linear background',
            overflow: 'auto'
        },
        '& .doctree--block-item': {
            position: 'relative',
            userSelect: 'none',
            width: 'calc(50% - 6px)',
            margin: theme.gutter.base + 'px 3px',
            flexShrink: 0,
            [StyleGrid.smUp]: {
                width: 'calc(33% - 6px)',
            },
            [StyleGrid.mdUp]: {
                width: 'calc(25% - 6px)',
            },
        },
        '& .doctree--block-item.placeholder ~ div': {
            transform: 'none !important'
        },
        '& .doctree--block-item--inner': {
            position: 'relative',
            minHeight: '20px',
            whiteSpace: 'nowrap',
            padding: '6px 12px 6px 9px',
        },
        '& .doctree--block-item.sortable-chosen': {},
        '& .doctree--block-item .doctree--block-item--drag-handle': {},
        '& .doctree--block-item.sortable-chosen .doctree--block-item--inner': {
            transition: '0.2s linear background',
        },
    },
});

class BlockSelection extends Component {

    render() {
        const {
            t,
            classes,
            blocks,
        } = this.props;

        return (<React.Fragment>
            <h4 style={{width: '100%', padding: '0px', textAlign: 'left', margin: 0}}>{t('selection.title')}</h4>
            {blocks && Array.isArray(blocks) ? <Droppable droppableId='BlockSelection'
                                                          isDropDisabled={true}
                                                          direction={'horizontal'}
                                                          ignoreContainerClipping={true}>
                {(provided, snapshot) => (
                    <div className={classes.wrapper + ' doctree--blocks-wrapper ' + (snapshot.isDraggingOver ? 'targeted' : '')}
                         ref={provided.innerRef}
                         {...provided.droppableProps}
                    >
                        {blocks.map((elem, i) => {
                            return (
                                <Draggable key={i}
                                           draggableId={'BlockSelection#' + i}
                                           index={i}>
                                    {(provided, snapshot) => (
                                        <React.Fragment>
                                            <BlockSelectionFactory provided={provided}
                                                                   isDragging={snapshot.isDragging}
                                                                   isOverTarget={null !== snapshot.draggingOver}
                                                                   t={t}
                                                                   data={elem}/>
                                            {snapshot.isDragging && (
                                                <BlockSelectionFactory t={t}
                                                                       data={elem}/>
                                            )}
                                        </React.Fragment>
                                    )}
                                </Draggable>
                            );
                        })}

                        {provided.placeholder}

                    </div>
                )}
            </Droppable> : null}
        </React.Fragment>);
    }
}

export default withStyles(styles)(BlockSelection);
