import {isAllowedTScope} from "../../lib/i18n";
import {beautifyProp} from "../../lib/beautify";

function blockName(name, t) {
    if(isAllowedTScope('doc-tree', 'selection')) {
        let t_scope = (isAllowedTScope('doc-tree', 'selection') ? 'defaults.selection.' : '');
        return beautifyProp(t(t_scope + name, name));
    }

    return beautifyProp(t(name));
}

function rootBlockName(name, t) {
    if(isAllowedTScope('doc-tree', 'root')) {
        let t_scope = (isAllowedTScope('doc-tree', 'root') ? 'defaults.root.' : '');
        return beautifyProp(t(t_scope + name, name));
    }

    return beautifyProp(t(name));
}

export {blockName, rootBlockName};
