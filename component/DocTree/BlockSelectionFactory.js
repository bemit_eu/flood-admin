import React from 'react';

import ContentBlock from './BlockSelection/ContentBlock';
import MainText from './BlockSelection/MainText';
import {withTheme} from '../../lib/theme';

const factory = {
    'content': ContentBlock,
    'mainText': MainText,
};

const UnkownBlock = (props) => (
    <React.Fragment>
        <p>Generic: {props.data ? props.data.type + ' ' + (props.data.id ? props.data.id : '') : null}</p>
    </React.Fragment>
);

class BlockSelectionFactoryBase extends React.Component {
    render() {
        const {
            t, theme,
            isDragging, provided, isOverTarget, data,
        } = this.props;

        const Comp = data && factory.hasOwnProperty(data.type) ? factory[data.type] : false;

        return (
            provided ?
                // actual item
                <div className={'doctree--block-item ' + (isDragging ? 'sortable-chosen' : '')}
                     ref={provided.innerRef}
                     {...provided.draggableProps}
                     {...provided.dragHandleProps}
                >
                    <div className='doctree--block-item--inner' style={(isDragging ? {
                        background: isOverTarget ? theme.bgPage : theme.drop.item_dragging_err,
                        border: '1px solid ' + theme.textColorLight,
                    } : {
                        background: theme.drop.item,
                        border: '1px solid transparent'
                    })}
                    >
                        {Comp ?
                            <Comp
                                data={data}
                                t={t}
                            /> :
                            <UnkownBlock {...this.props}/>}
                    </div>
                </div> :
                // placeholder item, during dragging actual item
                <div className={'doctree--block-item placeholder'}>
                    <div className='doctree--block-item--inner' style={{
                        background: theme.drop.item,
                        border: '1px solid transparent'
                    }}>
                        {Comp ?
                            <Comp
                                data={data}
                                t={t}
                            /> :
                            <UnkownBlock {...this.props}/>}
                    </div>
                </div>
        );
    };
}

const BlockSelectionFactoryStyled = withTheme(BlockSelectionFactoryBase);

const BlockSelectionFactory = React.forwardRef((props, ref) => {
    return <BlockSelectionFactoryStyled {...props} innerRef={ref}/>
});

export default BlockSelectionFactory;
