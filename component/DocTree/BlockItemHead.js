// Lib
import React from 'react';
import Loading from "../../component/LoadingSmall";
import {MdEdit, MdInfo,} from 'react-icons/md';
import ButtonInteractiveDelete from '../ButtonInteractiveDelete';
import {withTheme, withStyles} from '../../lib/theme';

const styles = theme => ({
    wrapper: {
        margin: 0,
        display: 'flex',
        alignItems: 'center'
    },
    icon: {
        minWidth: '30px',
        flexShrink: 0,
        marginTop: '3px',
        alignSelf: 'flex-start',
    },
    lbl: {
        alignSelf: 'flex-start',
        margin: theme.gutter.base * 3 + 'px 0 0 ' + theme.gutter.base * 3 + 'px',
    },
    openToggle: {
        alignSelf: 'flex-start',
        cursor: 'pointer',
        width: '30px',
        padding: theme.gutter.base * 3 + 'px',
        margin: '0 3px',
        /*maxWidth: '50%',
        flexGrow: 2,
        textAlign: 'right',
        background: 'rgba(0, 0, 0, 0.1)'*/
    },
    openToggleIcon: {},
    positionHelper: {
        marginLeft: 'auto'
    },
    dragHandle: {
        marginLeft: '-6px',
        marginRight: '6px',
    },
});


const styleOpen = {
    border: '1px solid transparent',
    transition: '0.35s linear border'
};

const styleClose = {
    transition: '0.35s linear border'
};

const BlockDragItemHead = (props) => {
    const {
        theme, classes,
        icon: Icon, lbl: Lbl,
        showInfo, onInfo, isInfoOpen,
        showEdit, onEdit, isEditOpen,
        loading,
        deleteItem,
    } = props;

    return (
        <div className={classes.wrapper}>

            {/* tmp draghandle fix: todo, allow head dragging */}
            {/*<div className={classes.dragHandle}><MdDragHandle size={18} color={('dark' === props.theme.name ? props.theme.textColorLight : props.theme.textColor)}/></div>*/}
            <div style={{width: '27px', flexShrink: 0}}/>

            {Icon ? <div className={classes.icon}><Icon/></div> : null}
            {Lbl ? <div className={classes.lbl}><Lbl/></div> : null}

            {/* no elem is surely rendered, but one need to be rendered for CSS style where it is split into right and left, just for that is the positionHelper */}
            <div className={classes.positionHelper}/>

            {loading ? <Loading
            /> : null}

            {showEdit && onEdit ? (
                <button className={classes.openToggle} onClick={onEdit}>
                    <MdEdit size={'1.125em'}
                            color={theme.textColor}
                            className={classes.openToggleIcon}
                            style={isEditOpen ? styleOpen : styleClose}
                    />
                </button>
            ) : null}
            {showInfo && showInfo ? (
                <button className={classes.openToggle} onClick={onInfo}>
                    <MdInfo size={'1.125em'}
                            color={theme.textColor}
                            className={classes.openToggleIcon}
                            style={isInfoOpen ? styleOpen : styleClose}
                    />
                </button>
            ) : null}

            {deleteItem ? <ButtonInteractiveDelete size={'1.125em'}
                                                   style={{
                                                       padding: (theme.gutter.base * 3) + 'px',
                                                       alignSelf: 'flex-start',
                                                   }}
                                                   resetTimeout={0}
                                                   onClick={() => deleteItem()}
            /> : null}
        </div>
    );
};

export default withStyles(styles)(withTheme(BlockDragItemHead));
