// Lib
import React from 'react';
import {DragDropContext} from 'react-beautiful-dnd';
import Observer from '@researchgate/react-intersection-observer';
import {withStyles} from '../../lib/theme';
import StyleGrid from '../../lib/StyleGrid';
import BlockRoot from './BlockRoot';
import BlockSelection from './BlockSelection';
import {ID} from "@formanta/js";
import {DevPopOut} from "../DevPopOut";

const styles = {
    wrapper: {
        display: 'flex',
        flexDirection: 'column',
        [StyleGrid.mdUp]: {
            //flexDirection: 'row',
        }
    },
    targets: {
        [StyleGrid.mdUp]: {
            marginRight: '12px',
            width: '100%',
            flexGrow: 2,
        }
    },
    sources: {
        display: 'flex',
        alignSelf: 'flex-start',
        flexWrap: 'wrap',
        width: '100%',
        //maxWidth: '100vw',
        [StyleGrid.mdUp]: {
            width: '100%',
            //height: '100%'
        },
        '& .doctree--blocks-wrapper': {}
    },
};

class BlockTree extends React.Component {

    state = {
        blocks: {},
        root_blocks: {},
        // state based styles for sticky BlockSelection
        styleSelection: {}
    };

    refsBlocks = {};

    constructor(props) {
        super(props);

        const blocks = {...this.props.blocks};

        for(let root in blocks) {
            // create default values, when some are not existing
            if(blocks.hasOwnProperty(root)) {
                blocks[root].forEach((elem) => {
                    if(!elem.uuid) {
                        elem.uuid = ID(11);
                    }
                });
            }
        }

        this.observerOpts = {
            onChange: this.toggleSticky,
            root: '#control-panel--content',
            rootMargin: '0% 0% -100%',
        };

        this.state = {
            blocks,
            root_blocks: this.props.root_blocks
        };
    }

    componentWillUnmount() {
        if(this.props.rmSave) {
            this.props.rmSave();
        }
    }

    componentDidMount() {
        if(this.props.addSave) {
            this.props.addSave(() => {
                const blocks = this.triggerAll();
                this.setState({blocks});

                let data = {};
                for(let root_name in blocks) {
                    // todo optimize sanitizing/data crawling loop stacks / here and in trigger, evt. registering etc.
                    if(blocks.hasOwnProperty(root_name)) {
                        data[root_name] = {children: []};
                        blocks[root_name].forEach((elem) => {
                            data[root_name].children.push({...elem});
                        });
                    }
                }

                return data;
            });
        }
    }

    triggerAll = () => {
        const blocks = {...this.state.blocks};

        for(let root in this.refsBlocks) {
            if(this.refsBlocks.hasOwnProperty(root) && Array.isArray(blocks[root])) {
                blocks[root] = [...blocks[root]];

                for(let item_uuid in this.refsBlocks[root]) {
                    if(this.refsBlocks[root].hasOwnProperty(item_uuid)) {
                        const itm = this.refsBlocks[root][item_uuid];
                        if(itm.instance && itm.instance.__save) {
                            const block = {...blocks[root][itm.index]};
                            block.data = itm.instance.__save();
                            block.data = {...block.data};
                            blocks[root][itm.index] = block;
                        }
                    }
                }
            }
        }

        return blocks;
    };

    deleteItem = (root_name, item_index) => {
        if(this.state.blocks[root_name] && this.state.blocks[root_name][item_index]) {
            const blocks = this.triggerAll();
            blocks[root_name].splice(item_index, 1);
            this.setState({
                blocks
            });
        }
    };

    onDragEnd = result => {
        const {destination, source} = result;

        if(!destination) {
            return;
        }

        const {droppableId: srcRootId, index: srcItemId} = source;
        const {droppableId: destRootId, index: destItemId} = destination;

        if(
            destRootId === srcRootId &&
            destItemId === srcItemId
        ) {
            return;
        }

        // todo: make it contextual based, e.g. on reorder only trigger snapshot for that root and not all
        // get changed data snapshot
        // get all blocks `{root_name: [{}, {}, ], }`
        let blocks = this.triggerAll();

        if('BlockSelection' === srcRootId) {
            // when dragging item from `BlockSelection`:
            // copy and add new, not move
            const {blockSelection} = this.props;
            const dest_root = [...blocks[destRootId]];

            const sel_blocks = [...blockSelection];
            const elem = {...sel_blocks[srcItemId]};
            elem.uuid = ID();

            dest_root.splice(destItemId, 0, elem);

            blocks[destRootId] = dest_root;

            this.setState({
                blocks
            });
            return;
        }
        // else reorder or move & reorder

        // save choosen for manipulation
        const src_block = {...blocks[srcRootId][srcItemId]};

        if(destRootId === srcRootId) {
            // on same source and target root: only reorder
            const src_root = [...blocks[srcRootId]];

            // delete from old pos
            src_root.splice(srcItemId, 1);
            // add in new pos
            src_root.splice(destItemId, 0, {...src_block});

            blocks[srcRootId] = src_root;
        } else {
            // move from one to another
            const src_root = [...blocks[srcRootId]];
            const dest_root = [...blocks[destRootId]];

            // delete from old pos
            src_root.splice(srcItemId, 1);
            // add in new pos
            dest_root.splice(destItemId, 0, src_block);

            blocks[srcRootId] = src_root;
            blocks[destRootId] = dest_root;
        }

        this.setState({
            blocks
        });
    };

    toggleSticky = (evt) => {
        //console.log(evt)
    };

    addElemRef = (name, uuid, index, instance) => {
        if(!this.refsBlocks[name]) this.refsBlocks[name] = {};
        if(null === instance) {
            delete this.refsBlocks[name][uuid];
            return;
        }
        this.refsBlocks[name][uuid] = {
            index,
            instance,
        };
    };

    render() {
        const {
            t, classes,
            blockSelection
        } = this.props;

        return (
            <DragDropContext onDragEnd={this.onDragEnd}>
                <div className={classes.wrapper}>
                    <Observer {...this.observerOpts}>
                        <div className={classes.sources}>
                            {blockSelection ?
                                <BlockSelection
                                    blocks={blockSelection}
                                    t={t}
                                /> : null}
                        </div>
                    </Observer>
                    <div className={classes.targets}>
                        {this.state.root_blocks ? this.state.root_blocks.map((name) => (
                            <BlockRoot
                                key={name}
                                name={name}
                                deleteItem={this.deleteItem}
                                blocks={this.state.blocks[name]}
                                addElemRef={this.addElemRef}
                                t={t}
                            />
                        )) : null}
                    </div>
                </div>

                <DevPopOut
                    name={'Blocks'}
                    title={'Blocks'}
                    json={this.state.blocks}
                />

                <DevPopOut
                    /* this doesn't get updated on every change, as state independent, so no real time values */
                    name={'Ref. Blocks'}
                    title={'Ref. Blocks'}
                    json={Object.keys(this.refsBlocks).map(root =>
                        Object.keys(this.refsBlocks[root]).map(uuid => ({
                            root,
                            uuid,
                            index: this.refsBlocks[root][uuid].index,
                            instance: !!this.refsBlocks[root][uuid].instance
                        }))
                    )}
                />
            </DragDropContext>
        );
    }
}

export default withStyles(styles)(BlockTree);
