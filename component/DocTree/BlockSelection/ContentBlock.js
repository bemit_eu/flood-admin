import React from 'react';
import {MdWidgets} from 'react-icons/md';
import {withStyles} from '../../../lib/theme';

import {blockName} from '../BlockName';

const styles = {
    wrapper: {
        margin: 0,
        display: 'flex',
        alignItems: 'center',
        flexShrink: 0
    }
};

class ContentBlock extends React.Component {

    render() {
        const {
            t, classes,
            data,
        } = this.props;

        return (
            <React.Fragment>
                <div className={classes.wrapper}>
                    <MdWidgets size={'1.5em'} style={{flexShrink: 0, marginRight: '6px'}}/>
                    {blockName(data.id, t)}
                </div>
            </React.Fragment>
        );
    };
}

export default withStyles(styles)(ContentBlock);
