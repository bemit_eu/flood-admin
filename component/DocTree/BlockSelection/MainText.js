import React from 'react';
import {MdDescription} from 'react-icons/md';
import {withStyles} from '../../../lib/theme';

const styles = {
    wrapper: {
        margin: 0,
        display: 'flex',
        alignItems: 'center',
        flexShrink: 0
    }
};

class MainText extends React.Component {

    render() {
        const {t, classes,} = this.props;

        return (
            <React.Fragment>
                <p className={classes.wrapper}>
                    <MdDescription size={'1.5em'} style={{flexShrink: 0, marginRight: '6px'}}/>
                    {t('block.mainText')}
                </p>
            </React.Fragment>
        );
    };
}

export default withStyles(styles)(MainText);
