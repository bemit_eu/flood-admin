import React from 'react';
import {MdDescription} from 'react-icons/md';

class MainText extends React.Component {

    render() {
        const {t, head, deleteItem} = this.props;
        const Hd = head;

        return (
            <React.Fragment>
                <Hd
                    icon={(p) => <MdDescription size={'1.875em'} style={{display: 'block'}} {...p}/>}
                    lbl={() => t('block.mainText')}
                    deleteItem={deleteItem}
                />
            </React.Fragment>
        );
    };
}

export default MainText;
