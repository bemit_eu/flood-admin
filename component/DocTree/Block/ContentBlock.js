import React from 'react';
import {MdWidgets} from 'react-icons/md';
import posed from 'react-pose';
import {selectByDot} from "@formanta/js";

import Loading from "../../../component/LoadingSmall";

import {SchemaEditor} from "../../../component/Schema/EditorLoader";

import {withStyles} from '../../../lib/theme';
import {blockName} from "../BlockName";
import {cancelPromise} from "../../cancelPromise";
import {storeBlock} from "../../../feature/Content/Stores/Block";
import {SchemaEditorStore} from "../../Schema/EditorStore";

const styles = {
    wrapper: {
        overflow: 'hidden',
    }
};

const Wrapper = posed.div({
    hidden: {
        height: 0,
        paddingBottom: 0,
    },
    visible: {
        height: 'auto',
        paddingBottom: '3px',
    }
});

class ContentBlock extends React.PureComponent {

    /**
     * @type {{schemaEditorStore: boolean|SchemaEditorStore}}
     */
    state = {
        schemaEditorStore: false,
        changed: false,
    };

    componentDidMount() {
        this.fetch();
        this.createContentStore();
    }

    componentDidUpdate(prevProps) {
        const {
            data,
        } = this.props;

        if(prevProps.data !== data) {
            this.fetch();
            this.createContentStore();
        }
    }

    componentWillUnmount() {
        this.setState({schemaEditorStore: false});
    }

    /**
     * Magic Fn used by BlockTree
     * @return {*}
     */
    __save = () => {
        return this.state.schemaEditorStore.data;
    };

    fetch() {
        const {loadBlock, cancelPromise} = this.props;
        if(!this.props.data || !this.props.data.id) {
            return;
        }
        cancelPromise(loadBlock(this.props.data.id));
    }

    createContentStore = () => {
        if(!this.props.data || !this.props.data.data) {
            return;
        }

        let schemaEditorStore = SchemaEditorStore.createFromData(this.props.data.data);
        this.setState({schemaEditorStore});
    };

    handleStateChange = (newStore) => {
        this.setState({schemaEditorStore: newStore}, () => {
            if(!this.state.changed) {
                this.setState({changed: true});
            }
            if(this.props.onChange) {
                this.props.onChange(newStore.data);
            }
        });
    };

    render() {
        const {
            t, classes,
            isBlockLoaded, blocks,
            data, head, deleteItem, onToggle, open
        } = this.props;

        if(!data || !data.id) return null;

        const Hd = head;

        let title = false;

        if(blocks && blocks[data.id] && blocks[data.id].schema && blocks[data.id].schema.title) {
            // build title
            title = selectByDot(blocks[data.id].schema.title, data.data);

            if('string' === typeof title) {
                let tmp_title = document.createElement('DIV');
                tmp_title.innerHTML = title;
                title = tmp_title.textContent || tmp_title.innerText || "";

                if(50 < title.length) {
                    title = title.substr(0, 50) + '...';
                }
            }
        }

        return (
            <React.Fragment>
                <Hd
                    icon={(p) => <MdWidgets size={'1.875em'} style={{display: 'block'}} {...p}/>}
                    lbl={() => (
                        <React.Fragment>
                            {blockName(data.id, t)}
                            {title && ('string' === typeof title || 'boolean' === typeof title || 'number' === typeof title) ?
                                /* todo: remove y:n with locales*/
                                <span
                                    style={{marginLeft: '3px', fontStyle: 'italic'}}
                                >· {'string' === typeof title || 'number' === typeof title ? title : title ? 'y' : 'n'}
                                </span> : null}
                            {this.state.changed ? ' *' : null}
                        </React.Fragment>
                    )}
                    deleteItem={deleteItem}
                    onEdit={onToggle}
                    showEdit={blocks && blocks[data.id] && !!blocks[data.id].schema}
                    loading={'progress' === isBlockLoaded(data.id)}
                    isEditOpen={open}
                />

                <Wrapper className={classes.wrapper} withParent={false} pose={open ? 'visible' : 'hidden'}>

                    {('progress' === isBlockLoaded(data.id) || 'error' === isBlockLoaded(data.id) ?
                        <Loading
                            center={true}
                            errorWarning={'error' === isBlockLoaded(data.id)}
                        /> :
                        null)}

                    {true === isBlockLoaded(data.id) ?
                        <SchemaEditor
                            t={t}
                            store={this.state.schemaEditorStore}
                            onChange={this.handleStateChange}
                            schema={blocks[data.id].schema}
                            tScope={'doc-tree.block.' + data.id}
                        /> :
                        t('no-editable-meta')
                    }
                </Wrapper>
            </React.Fragment>
        )
    };
}

export default cancelPromise(storeBlock(withStyles(styles)(ContentBlock)));
