import React from "react";
import {withNamespaces} from "react-i18next";
import {MdVisibility, MdVisibilityOff, MdCheckBox, MdCheckBoxOutlineBlank} from 'react-icons/md';


import StyleGrid from '../lib/StyleGrid';
import {withTheme, combineStyle, createUseStyles, useTheme} from "../lib/theme";

const useStyles = createUseStyles(theme => ({
    button: {
        border: '1px solid transparent',
        background: 'none',
        transition: 'background ' + theme.transition,
        '&:focus': {
            borderStyle: 'dashed'
        },
        /*'&:focus:active': {
            borderStyle: 'solid'
        },
        '&:active': {
            borderStyle: 'solid'
        },*/
        '&:hover': {
            borderStyle: 'dotted'
        },
    }
}));

/**
 * Style for Form Components
 */
const styling = {
    button: theme => ({
        color: theme.textColor,
        borderColor: theme.accent.blue,
        padding: theme.gutter.base * 2 + 'px 12px',
    }),
    buttonSmall: theme => ({
        fontSize: '0.875rem',
        color: theme.textColor,
        borderColor: theme.accent.blue,
        padding: '2px 6px',
    }),
    label: theme => ({
        display: 'block',
        padding: '6px 0',
    }),
    input: theme => ({
        text: {
            color: theme.textColor,
            width: '100%',
            border: '0',
            //borderBottom: '1px solid ' + theme.input.borderColor,
            background: theme.input.background,
            padding: theme.gutter.base * 2 + 'px 12px',
            transition: theme.transition + ' background',
            '&[readonly]': {
                // todo: optimize readonly and apply bg on after which will be overlayed
                background: 'repeating-linear-gradient(-45deg, #222 4px,' + theme.input.background + ' 20px);'
            }
        },
        textarea: {
            border: 0,
            color: theme.textColor,
            background: theme.input.background,
            padding: theme.gutter.base * 2 + 'px 12px',
            width: '100%',
            transition: theme.transition + ' background',
            [StyleGrid.jss.mdUp]: {
                width: 'auto'
            }
        },
        select: {
            background: theme.bgPage,
            transition: theme.transition + ' background, ' + theme.transition + ' border',
            border: '1px solid ' + theme.textColorLight,
            borderRight: '0',
            color: theme.textColor,
        },
        checkbox: {
            position: 'relative'
        }
    }),
    group: theme => ({
        wrapper: {
            display: 'block',
            padding: '6px 0',
        }
    }),
    req: theme => ({
        required: theme.formReq.required,
        optional: theme.formReq.optional
    })
};

//
// Form Render Components

// Button
const Button = props => {
    const theme = useTheme();
    let style = combineStyle(styling.button(theme), props.style);
    const classes = useStyles(theme);

    // todo: better active/props deletion
    if(props.active) {
        style.background = theme.btnActive;
    }
    const p = {...props};
    p.active = false;
    delete p.active;

    return <button {...props} className={classes.button} style={style}/>
};

const ButtonSmall = props => {
    const theme = useTheme();
    let style = combineStyle(styling.buttonSmall(theme), props.style);
    const classes = useStyles(theme);

    // todo: better active/props deletion
    if(props.active) {
        style.background = theme.btnActive;
    }
    const p = {...props};
    p.active = false;
    delete p.active;

    return <button {...p} className={classes.button} style={style}/>
};

const ButtonRaw = props => {
    const theme = useTheme();
    let style = {...props.style};

    // todo: better active/props deletion
    if(props.active) {
        style.background = theme.btnActive;
    }
    const p = {...props};
    p.active = false;
    delete p.active;

    return <button {...p} style={style}/>
};

// Label
const Label = props => {
    const theme = useTheme();
    let style = combineStyle(styling.label(theme), props.style);
    return <label {...props} style={style}/>
};

// Input: Text
const InputTextStyled = (props) => {
    const theme = useTheme();
    let style = combineStyle(styling.input(theme).text, props.style);
    // todo: try to move out of render
    const p = {...props};
    const innerRef = p.innerRef;
    delete p.innerRef;
    return <input type='text' ref={innerRef} {...p} style={style}/>
};

const InputText = React.forwardRef((props, ref) => {
    return <InputTextStyled {...props} innerRef={ref}/>
});

class InputPasswordBase extends React.Component {
    state = {
        visibility: false
    };

    render() {
        const {forwardedRef, ...rest} = this.props;
        return <div style={{display: 'flex', width: '100%'}}>
            <InputTextStyled innerRef={forwardedRef} {...rest} type={this.state.visibility ? 'text' : 'password'}/>
            <button style={{padding: 6}} onClick={() => this.setState({visibility: !this.state.visibility})} type={'button'}>
                {this.state.visibility ?
                    <MdVisibilityOff size={'1.25em'} color={this.props.theme.textColor}/> :
                    <MdVisibility size={'1.25em'} color={this.props.theme.textColor}/>}
            </button>
        </div>
    }
}

const InputPassword = withTheme(InputPasswordBase);

// Input: Textarea
const InputTextareaStyled = (props) => {
    const theme = useTheme();
    let style = combineStyle(styling.input(theme).textarea, props.style);
    // todo: try to move out of render
    const p = {...props};
    const innerRef = p.innerRef;
    delete p.innerRef;
    return <textarea ref={innerRef} {...p} style={style}/>
};

const InputTextarea = React.forwardRef((props, ref) => {
    return <InputTextareaStyled {...props} innerRef={ref}/>
});

// Input: CheckBox
const InputCheckboxStyled = (props) => {
    const theme = useTheme();
    let style = combineStyle(styling.input(theme).checkbox, props.style);
    // todo: try to move out of render
    const p = {...props};
    const innerRef = p.innerRef;
    const size = p.size || '1em';
    delete p.innerRef;
    delete p.size;

    return <span style={style}>
        <label htmlFor={p.id} style={{padding: 0, margin: 0, cursor: 'pointer'}}>{p.checked ? <MdCheckBox color={theme.color} size={size} style={{verticalAlign: 'bottom'}}/> :
            <MdCheckBoxOutlineBlank color={theme.color} size={size} style={{verticalAlign: 'bottom'}}/>}</label>
        <input type='checkbox' ref={innerRef} {...p} style={{
            width: 0, height: 0, overflow: 'hidden', margin: 0, padding: 0, opacity: 0, position: 'absolute'
            //left: '-9999rem'
        }}/>
    </span>
};

const InputCheckbox = React.forwardRef((props, ref) => {
    return <InputCheckboxStyled {...props} innerRef={ref}/>
});

//
// Input Select

const SelectStyled = (props) => {
    const theme = useTheme();
    let style = combineStyle(styling.input(theme).select, props.style);
    // todo: try to move out of render
    const p = {...props};
    const innerRef = p.innerRef;
    delete p.innerRef;
    return <select ref={innerRef} {...p} style={style}/>
};

const Select = React.forwardRef((props, ref) => {
    return <SelectStyled {...props} innerRef={ref}/>
});

//
// Input Grouping

const InputGroup = props => {
    const theme = useTheme();
    let style = {
        wrapper: styling.group(theme)
    };
    if(props.style) {
        style.wrapper = combineStyle(style.wrapper, props.style.wrapper);
    }
    return <div {...props} style={style.wrapper}/>
};

//
// Form Text Utils

const TextReq = withNamespaces(['form'])(props => {
    const theme = useTheme();
    let style = combineStyle(styling.req(theme), props.style);
    return <p><span style={style.required}>{props.t('required')}</span>, <span style={style.optional}>{props.t('optional')}</span></p>
});


//
// Exporting Public Components

export {Button, ButtonSmall, ButtonRaw, Label};

export {InputText, InputPassword, InputTextarea, InputCheckbox};

export {Select};

export {InputGroup};

export {TextReq};
