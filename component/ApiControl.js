import React from 'react';
import {withContextConsumer} from "@formanta/react";
import {isNumber} from "@formanta/js";
import {withErrorControl} from "./ErrorControl";
import {errorMessages} from "./ErrorMessages/Messages";

const ApiControlContext = React.createContext({});

class ApiControlProviderBase extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            handleApiFail: this.handleApiFail,
            // 401
            errUnauthorized: false,
            // 403
            errForbidden: false,
            // 408
            errRequestTimeout: false,
            // 429
            errTooManyRequests: false,
            // 440 (unofficial)
            errLoginExpired: false,
            // 500
            errServerError: false,
            // 501
            errNotImplemented: false,
            // 503
            errServiceUnavailable: false,
        };
    }

    /**
     *
     * @param {{}} err
     * @param {boolean|string} label
     * @param {boolean} fatal
     * @param {boolean|function} redo a function that should be called if the root of the error is fixed, e.g. on LoginExpired after re-login
     */
    handleApiFail = (err, label = false, fatal = true, redo = false) => {
        const {addError, showLogin} = this.props;

        if(isNumber(err.status)) {
            const details = {
                status: err.status
            };
            if(err.endpoint) {
                details.endpoint = err.endpoint;
            }
            if(err.method) {
                details.method = err.method;
            }

            console.error('ApiControl found status `' + err.status + '`');
            switch(err.status) {
                case 401:
                    this.setState({
                        errUnauthorized: true
                    });
                    addError(errorMessages.Unauthorized, details, label, fatal);
                    break;
                case 403:
                    this.setState({
                        errForbidden: true
                    });
                    addError(errorMessages.Forbidden, details, label, fatal);
                    break;
                case 408:
                    this.setState({
                        errRequestTimeout: true
                    });
                    addError(errorMessages.RequestTimeout, details, label, fatal);
                    break;
                case 429:
                    this.setState({
                        errTooManyRequests: true
                    });
                    addError(errorMessages.TooManyRequests, details, label, fatal);
                    break;
                case 440:
                    this.setState({
                        errLoginExpired: true
                    });
                    showLogin(redo);
                    addError(errorMessages.LoginExpired, details, label, fatal);
                    break;
                case 500:
                    this.setState({
                        errServerError: true
                    });
                    addError(errorMessages.ServerError, details, label, fatal);
                    break;
                case 501:
                    this.setState({
                        errNotImplemented: true
                    });
                    addError(errorMessages.NotImplemented, details, label, fatal);
                    break;
                case 503:
                    this.setState({
                        errServiceUnavailable: true
                    });
                    addError(errorMessages.ServiceUnavailable, details, label, fatal);
                    break;
                default:
                    break;
            }
        }
    };

    render() {
        return (
            <ApiControlContext.Provider value={this.state}>
                {this.props.children}
            </ApiControlContext.Provider>
        );
    }
}

const withApiControl = withContextConsumer(ApiControlContext.Consumer);

const ApiControlProvider = withErrorControl(ApiControlProviderBase);

export {ApiControlProvider, withApiControl};