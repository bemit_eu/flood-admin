import React from "react";
import {ID, isObject} from "@formanta/js";
import {createContext, createHoc, PropsProvider} from "../PropsProvider";
import {strReplaceAll} from "../../lib/beautify";

/**
 * @param select_part
 * @return {string}
 */
function escapeDot(select_part) {
    return strReplaceAll(select_part, '.', '\\.')
}

/**
 * @param select
 * @param {int} i only set internally as endless-loop prevention
 * @return {{select_esc: string, unique: boolean|string}}
 */
function safeEscapeDot(select, i = 0) {
    let select_esc = select;
    /**
     * @type {boolean|string}
     */
    let unique = false;
    if(-1 !== select.indexOf('\\.')) {
        unique = ID() + '_' + i;
        if(i < 7 && -1 !== select.indexOf(unique)) {
            // when the unique phrase is already in string, create new
            return safeEscapeDot(select, i + 1);
        }

        select_esc = strReplaceAll(select, '\\.', unique);
    }

    return {
        unique,
        select_esc,
    }
}

/**
 * @param {string} select_part
 * @param {string|boolean} unique
 * @return {string|*}
 */
function unescapeDot(select_part, unique) {
    if(unique) {
        return strReplaceAll(select_part, unique, '.');
    }

    return select_part;
}

function selectByDot(select, obj) {
    let {unique, select_esc} = safeEscapeDot(select);

    return select_esc.split('.').reduce(((o, i) => {
        if(o) {
            let rev = unescapeDot(i, unique);
            return o[rev];
        } else {
            return undefined;
        }
    }), obj);
}

function createVarDefaults(variable, next_key) {
    let nan = isNaN(next_key * 1);
    if(
        !isObject(variable) || !variable || // when not a object, not existing
        (!Array.isArray(variable) && !nan) || // when not an array and a numeric index
        (Array.isArray(variable) && nan)// when an array and not a numeric index
    ) {
        if(nan) {
            variable = {};
        } else {
            variable = [];
        }
    }

    return variable;
}

/**
 * Sets a value on the object property that gets selected with select.
 * @param select
 * @param {string} select a string like 'some.thing'
 * @param {object} obj an object containing data like {some:{ thing: mixed }}
 * @param {*} value
 */
function setByDot(select, obj, value) {
    let {unique, select_esc} = safeEscapeDot(select);

    const arr = select_esc.split('.');
    arr.reduce((o, k, i) => {
        k = unescapeDot(k, unique);
        /*console.log(select, k, k * 1, isObject(o[k]) ? 'y' : 'n', Array.isArray(o[k]) ? 'y' : 'n', isNaN(k * 1) ? 'y' : 'n', arr.length === (i + 1) ? 'y' : 'n', (
            !isObject(o[k]) || !o[k] ||
            (!Array.isArray(o) && !isNaN(k * 1)) ||
            (Array.isArray(o) && isNaN(k * 1))
        ) ? 'y' : 'n');*/
        if(arr.length === (i + 1)) {
            if(
                (!o || !isObject(o)) ||
                (!Array.isArray(o) && !isNaN(k * 1))
            ) {
                // todo: remove this, not be necessary said the test of using next_key down
                console.error('setByDot target non existing', o)
            }

            o[k] = value;
        } else {
            let next_key = arr.length > i + 1 ? arr[i + 1] : k;
            o[k] = createVarDefaults(o[k], next_key);
        }
        return o[k];
    }, obj);
}

function rmByDot(select, obj) {
    let {unique, select_esc} = safeEscapeDot(select);
    const arr = select_esc.split('.');
    let i = 0;
    arr.reduce(((o, k) => {
        k = unescapeDot(k, unique);
        if(arr.length === (i + 1)) {
            if(o) {
                if(isObject(o) && !Array.isArray(o)) {
                    delete o[k];
                } else if(Array.isArray(o)) {
                    o.splice(k, 1);
                }
            }
        } else if(!isObject(o[k]) || !o[k]) {
            o[k] = {};
        }
        i++;
        return o[k];
    }), obj);
}


class EditorStore {
    constructor(data = {}) {
        this.data = data;
    }

    /**
     * @param {string} ident
     * @return {*}
     */
    get = (ident) => {
        return selectByDot(ident, this.data);
    };

    /**
     * @param {string} ident
     * @param {*} value
     * @return {*}
     */
    set = (ident, value) => {
        setByDot(ident, this.data, value);
        return Object.assign(Object.create(Object.getPrototypeOf(this)), this);
    };

    /**
     * @param {string} ident
     * @return {*}
     */
    rm = (ident) => {
        rmByDot(ident, this.data);
        return Object.assign(Object.create(Object.getPrototypeOf(this)), this);
    };

    props = () => {
        return Object.keys(this.data);
    };
}

/**
 *
 * @param {EditorStore} parentStore
 * @param {string} scope
 */
const nestedEditorStore = (parentStore, scope) => {
    return {
        data: parentStore.data,
        get: (ident) => parentStore.get(scope + '.' + ident),
        set: (ident, value) => parentStore.set(scope + '.' + ident, value),
        rm: (ident) => parentStore.rm(scope + '.' + ident),
        props: () => parentStore.props(),
    }
};

const SchemaEditorStore = {
    createEmpty: () => {
        return new EditorStore();
    },

    createFromData: (data) => {
        return new EditorStore(data);
    },
};

const stateProps = [
    't', 'getTScope', 'tScope', // translates, on all editor's
    'store', // data store class object, on root-editor's
    'onChange', // handle fn receives changes class object, on all editor's
    'parentStore', 'scope', // when nesting schema editor's, this connects to the parents scope, on nested-editor's
    'schema', // schema for that particular data-scope, on all editor's
    'displayBlock', // on all editor's
];
const EditorProviderContext = createContext();
const withSchemaEditorStore = createHoc(EditorProviderContext.Consumer);

const EditorStoreProvider = (props) => <PropsProvider
    provider={EditorProviderContext.Provider}
    stateProps={stateProps}
    {...props}
/>;


export {SchemaEditorStore, stateProps, withSchemaEditorStore, EditorStoreProvider, nestedEditorStore, escapeDot};
