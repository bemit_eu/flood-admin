// Lib
import React from 'react';
import {SchemaGroup} from './Group';
import {withStyles} from "../../lib/theme";
import {cancelPromise} from "../../component/cancelPromise";
import {editorStyles} from "./EditorStyle";
import {EditorStoreProvider, nestedEditorStore, withSchemaEditorStore} from "./EditorStore";
import {getAlias, isAllowedTScope} from "../../lib/i18n";

class SchemaEditorBase extends React.PureComponent {
    getTScope = (name) => {
        const {tScope} = this.props;
        if(!tScope) {
            return '';
        }
        return (tScope && isAllowedTScope(name, tScope) ? getAlias(name, tScope) + '.' : '');
    };

    render() {
        const {
            classes,
            parentStore, scope,
        } = this.props;

        let store = this.props.store;
        if(parentStore) {
            // todo: optimize no-recreation if not needed
            store = nestedEditorStore(parentStore, scope);
        }

        return (
            <EditorStoreProvider getTScope={this.getTScope} {...this.props} store={store}>
                <EditorList className={classes.wrapper + ' ' + (this.props.displayBlock ? 'as-block' : '')}/>
            </EditorStoreProvider>
        );
    }
}

const EditorList = withSchemaEditorStore((props) => {
    const {schema, className} = props;

    // in updating process between pages, store is not available on first render
    return props && props.store && schema.properties ?
        <div className={className}>
            {Object.keys(schema.properties).map((key) => (
                key !== '_schema' ?
                    <SchemaGroup
                        key={key}
                        currentSchema={schema.properties[key]}
                        name={key}
                        name_abs={key}
                    />
                    : null
            ))}
        </div>
        : null
});

const SchemaEditor = withStyles(editorStyles)(cancelPromise(SchemaEditorBase));

export default SchemaEditor;

