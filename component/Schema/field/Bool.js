// Lib
import React from 'react';

import {InputCheckbox} from '../../Form';
import {withSchemaEditorStore} from "../EditorStore";

const Bool = withSchemaEditorStore((props) => {
    const {
        lblId,
        type,
        name, name_abs,
        store, onChange,
    } = props;

    return (
        <InputCheckbox
            id={lblId}
            data-name={name}
            type={type || 'checkbox'}
            checked={!!+store.get(name_abs)}
            onChange={(e) => onChange(store.set(name_abs, !!+e.target.checked))}
        />
    );
});

export default Bool;
