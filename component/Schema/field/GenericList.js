// Lib
import React, {Component} from 'react';
import {MdAdd, MdDragHandle} from 'react-icons/md';
import {SortableHandle, SortableContainer, SortableElement} from 'react-sortable-hoc';

import {useTheme, withStyles, withTheme} from '../../../lib/theme';
import {SchemaEditor} from "../EditorLoader";
import ButtonInteractiveDelete from "../../ButtonInteractiveDelete";
import {withSchemaEditorStore} from "../EditorStore";

const styles = {
    wrapper: {},
    wrapperSelect: {
        overflow: 'hidden'
    },
    toggle: {
        padding: '9px'
    }
};

class DragHandleBase extends React.Component {
    timeoutid = false;

    componentWillUnmount() {
        if(this.timeoutid) {
            clearTimeout(this.timeoutid);
            this.timeoutid = false;
        }
    }

    render() {
        const {
            theme,
            small
        } = this.props;
        const style = {
            cursor: 'inherit',
            boxSizing: 'content-box',
            marginTop: small ? '4px' : theme.gutter.base * -2 + 'px',
            marginLeft: theme.gutter.base * -2 + 'px',
            padding: '6px'
        };

        return <button style={style}
                       onMouseEnter={() => {
                           document.body.style.cursor = 'grab';
                       }}
                       onMouseLeave={() => {
                           if('grabbing' !== document.body.style.cursor) {
                               document.body.style.removeProperty('cursor');
                               if(this.timeoutid) {
                                   clearTimeout(this.timeoutid);
                               }
                           }
                       }}
                       onClick={() => {
                           this.timeoutid = setTimeout(() => {
                               document.body.style.cursor = 'grabbing';
                           }, 105);
                       }}
                       onMouseUp={() => {
                           document.body.style.removeProperty('cursor');
                           if(this.timeoutid) {
                               clearTimeout(this.timeoutid);
                           }
                       }}
        ><MdDragHandle size={'1.125em'} color={theme.textColor}/></button>
    }
}

const DragHandle = SortableHandle(withTheme(DragHandleBase));

const SortItemNo = props => <p style={{display: (props.small ? 'inline-block' : 'block'), wordBreak: 'break-all', fontStyle: 'italic', margin: (props.small ? '2px 4px 2px 0' : '12px 0')}}>{props.index + 1}.</p>;

const SortItem = SortableElement(withSchemaEditorStore((props) => {
    const theme = useTheme();
    return <div style={{
        display: 'flex',
        flexShrink: 0,
        width: 'y' === props.viewAxis ? '100%' : props.width || '50%',
        padding: '0 ' + theme.gutter.base + 'px ' + theme.gutter.base * 2 + 'px 3px',
    }}>
        <div style={{
            display: 'flex',
            flexDirection: props.small ? 'row' : 'column',
            alignItems: props.small ? 'center' : 'normal',
            width: '100%',
            background: theme.bgPage,
            transition: theme.transition + ' background',
            padding: theme.gutter.base * (props.small ? 1 : 2) + 'px',
            border: '1px solid ' + theme.textColorLight,
        }}>
            <div style={{
                display: props.small ? 'flex' : 'block',
                marginTop: props.small ? '9px' : 0,
                alignItems: props.small ? 'center' : 'auto'
            }}>
                {props.sortable ? <DragHandle small={props.small}/> : null}
                {props.small ? <SortItemNo small={props.small} index={props.sortIndex}/> : null}
                {props.small ? null :
                    <ButtonInteractiveDelete onClick={props.delete} size={'1.125em'} style={{
                        marginLeft: 'auto',
                        marginTop: theme.gutter.base * -2 + 'px',
                        marginRight: theme.gutter.base * -2 + 'px',
                        padding: '6px'
                    }}/>}
            </div>

            {props.small ? null : <SortItemNo small={props.small} index={props.sortIndex}/>}

            <div style={{display: (props.small ? 'inline-block' : 'block'), flexGrow: props.small ? 2 : 0, margin: props.small ? '-12px 0' : 0}}>
                {props.store ? <SchemaEditor
                    t={props.t}
                    scope={props.name_abs + '.' + props.name}
                    parentStore={props.store}
                    onChange={props.onChange}
                    schema={props.schema}
                    tScope={props.tScope}
                /> : null}
            </div>

            {props.small ?
                <ButtonInteractiveDelete onClick={props.delete} size={'1.125em'} style={{
                    marginLeft: 'auto',
                    marginTop: theme.gutter.base * 3 + 'px',
                    marginRight: theme.gutter.base * -2 + 'px',
                    padding: '6px'
                }}/> : null}
        </div>
    </div>
}));

const SortContainer = SortableContainer((props) => {
    return <div style={{
        display: 'flex',
        flexDirection: 'x' === props.view.axis || 'xy' === props.view.axis ? 'row' : 'column',
        flexWrap: 'xy' === props.view.axis ? 'wrap' : 'nowrap',
        overflowX: 'x' === props.view.axis ? 'scroll' : 'no-scroll',
        margin: '0 -3px'
    }}>{props.children}</div>;
});

class GenericList extends Component {


    createItem() {
        const {
            name_abs,
            store, onChange,
        } = this.props;

        let list = store.get(name_abs) || [];
        list.push({});

        onChange(store.set(name_abs, list));
    }

    itemDelete(index) {
        const {onChange, store, name_abs} = this.props;
        let list = store.get(name_abs) || [];

        list.splice(index, 1);

        onChange(store.set(name_abs, list));
    }

    onDragStart = () => {
        document.body.style.cursor = 'grabbing';
    };

    onDragEnd = ({oldIndex, newIndex}) => {
        const {onChange, store, name_abs} = this.props;
        let list = store.get(name_abs) || [];

        const movingItem = {...list[oldIndex]};
        list.splice(oldIndex, 1);
        list.splice(newIndex, 0, movingItem);

        onChange(store.set(name_abs, list));

        document.body.style.removeProperty('cursor');
        this.setState({
            prevCursor: false,
        });
    };

    render() {
        const {
            theme, classes, tScope,
        } = this.props;

        const {
            store, name_abs,
            currentSchema,
        } = this.props;

        let list = store.get(name_abs) || [];

        const sortable = true;

        return (
            <div className={classes.wrapper}>
                <SortContainer updateBeforeSortStart={this.onDragStart}
                               onSortEnd={this.onDragEnd}
                               useWindowAsScrollContainer={true}
                               view={currentSchema.view || {}}
                               pressDelay={25}
                               useDragHandle={true}>
                    {list.map((elem, i) => (
                        <SortItem key={i}
                                  index={i}
                                  sortIndex={i}
                                  sortable={sortable}
                                  view={currentSchema.view || {}}
                                  name={i}
                                  name_abs={name_abs}
                                  delete={() => {
                                      this.itemDelete(i)
                                  }}
                                  data={elem}
                                  schema={{
                                      type: 'object',
                                      properties: currentSchema.properties
                                  }}
                                  tScope={tScope}
                        />
                    ))}
                </SortContainer>
                <button className={classes.toggle} onClick={this.createItem.bind(this)}><MdAdd size={'1em'} color={theme.textColor} style={{display: 'block'}}/></button>
            </div>
        );
    };
}

export default withStyles(styles)(withTheme(withSchemaEditorStore(GenericList)));
