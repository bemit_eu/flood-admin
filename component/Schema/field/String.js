// Lib
import React from 'react';

import {InputText} from '../../Form';
import {withSchemaEditorStore} from "../EditorStore";

const String = withSchemaEditorStore((props) => {
    const {
        lblId,
        type,
        name, name_abs,
        store, onChange,
    } = props;

    return (
        <InputText
            id={lblId}
            data-name={name}
            type={type || 'text'}
            value={store.get(name_abs) || ''}
            onChange={(e) => onChange(store.set(name_abs, e.target.value))}
        />
    );
});

export default String;
