// Lib
import React from 'react';

import {InputTextarea} from '../../Form';
import {withSchemaEditorStore} from "../EditorStore";

const Text = withSchemaEditorStore((props) => {
    const {
        lblId,
        currentSchema, name, name_abs,
        store, onChange,
    } = props;

    const value = store.get(name_abs) || '';

    return (
        <div style={{width: '100%'}}>
            <InputTextarea
                id={lblId}
                data-name={name}
                value={value}
                onChange={(e) => onChange(store.set(name_abs, e.target.value))}
                style={{
                    minHeight: value.length > 300 ? '400px' : value.length > 100 ? '150px' : '50px',
                    transition: '0.3s min-height linear',
                }}
            />
            {currentSchema && currentSchema.option && currentSchema.option.show_count ?
                <small>{value.length}</small>
                : null}
        </div>
    );
});

export default Text;
