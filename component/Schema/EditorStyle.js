import StyleGrid from '../../lib/StyleGrid';

const customView = [
    '.w-md-1',
    '.w-md-1-2',
    '.w-md-1-3',
    '.w-md-2-3',
    '.w-md-1-4',
    '.w-md-3-4',
    '.w-md-1-5',
    '.w-md-2-5',
    '.w-md-3-5',
    '.w-md-4-5',
];

const getNot = () => {
    return customView.map(e => ':not(' + e + ')').join('');
};

const editorStyles = {
    wrapper: {
        display: 'flex',
        flexWrap: 'wrap',
        margin: '12px -6px',
        '& .schema-edit--group input': {
            minWidth: '10px'
        },
        '& > .schema-edit--group': {
            width: '100%',
            position: 'relative'
        },
        ['& > .schema-edit--group' + getNot()]: {
            [StyleGrid.smUp]: {
                width: '50%',
            },
            [StyleGrid.mdUp]: {
                width: '33%',
            }
        },
        '& > .schema-edit--group.w-border:after': {
            content: '""',
            position: 'absolute',
            height: '1px',
            width: '25%',
            top: '100%',
            left: '50%',
            transform: 'translateX(-50%)',
            background: 'rgba(150, 150, 150, 0.2)',
        },
        '& .schema-edit--group.TextRich': {
            width: '100%',
        },
        '& .schema-edit--group.GenericList': {
            width: '100%',
        },
        '& > .schema-edit--group .schema-edit--lbl': {
            //margin: '0 0 9px 0'
            userSelect:'none',
        },
        '& > .schema-edit--group .schema-edit--desc': {
            fontSize: '0.85em',
            margin: '4px 0',
        },
        '& .schema-edit--group-list': {
            display: 'flex',
            flexWrap: 'wrap',
            //justifyContent: 'space-between'
        },
        '& .schema-edit--group > .schema-edit--group-list': {
            margin: '0 -6px 0 -6px',
        },
        /*['& .schema-edit--group-list.as-block .schema-edit--group' + getNot()]: {
            width: '100%'
        },*/
        ['&.as-block .schema-edit--group' + getNot()]: {
            width: '100%'
        },
        '& .schema-edit--group': {
            margin: '0 0 6px 0',
            padding: '0 6px 3px 6px',
            flexGrow: 2
        },
        '& .schema-edit--group.w-md-1': {
            [StyleGrid.smUp]: {},
            [StyleGrid.mdUp]: {
                width: '100%',
            }
        },
        '& .schema-edit--group.w-md-1-2': {
            [StyleGrid.smUp]: {},
            [StyleGrid.mdUp]: {
                width: '50%',
            }
        },
        '& .schema-edit--group.w-md-1-3': {
            [StyleGrid.smUp]: {},
            [StyleGrid.mdUp]: {
                width: '33%',
            }
        },
        '& .schema-edit--group.w-md-2-3': {
            [StyleGrid.smUp]: {},
            [StyleGrid.mdUp]: {
                width: '66%',
            }
        },
        '& .schema-edit--group.w-md-1-4': {
            [StyleGrid.smUp]: {},
            [StyleGrid.mdUp]: {
                width: '25%',
            }
        },
        '& .schema-edit--group.w-md-3-4': {
            [StyleGrid.smUp]: {},
            [StyleGrid.mdUp]: {
                width: '75%',
            }
        },
        '& .schema-edit--group.w-md-1-5': {
            [StyleGrid.smUp]: {},
            [StyleGrid.mdUp]: {
                width: '20%',
            }
        },
        '& .schema-edit--group.w-md-2-5': {
            [StyleGrid.smUp]: {},
            [StyleGrid.mdUp]: {
                width: '40%',
            }
        },
        '& .schema-edit--group.w-md-3-5': {
            [StyleGrid.smUp]: {},
            [StyleGrid.mdUp]: {
                width: '60%',
            }
        },
        '& .schema-edit--group.w-md-4-5': {
            [StyleGrid.smUp]: {},
            [StyleGrid.mdUp]: {
                width: '80%',
            }
        }
    }
};

export {editorStyles};
