// Lib
import React from 'react';
import {ID} from '@formanta/js';
import {SchemaField} from "./FieldFactory";
import {useTheme} from "../../lib/theme";
import {i18n} from "../../lib/i18n";
import {beautifyProp} from "../../lib/beautify";
import {withSchemaEditorStore} from "./EditorStore";
import {ErrorBoundary} from "../ErrorBoundary";

const SchemaGroupContainer = (props) => {
    const {type, view} = props;
    const theme = useTheme();

    return <div
        className={
            'schema-edit--group ' +
            (type ? type + ' ' : '') +
            (view && view.block ? 'as-block ' : '') +
            (view && view.widthM ? 'w-md-' + view.widthM.replace('/', '-') + ' ' : '') +
            (view && view.border ? 'w-border ' : '')
        }
        style={view && view.special ? {
            padding: '12px',
            background: theme.bgLight,
            transition: theme.transition + ' background',
        } : {}}
    >{props.children}</div>
};

const getSchemaDesc = (schema) => {
    let desc = false;
    if(schema.desc && schema.desc.hasOwnProperty(i18n.languages[0])) {
        desc = schema.desc[i18n.languages[0]];
    } else if(schema.desc && schema.desc.hasOwnProperty(i18n.defaultLanguage)) {
        desc = schema.desc[i18n.defaultLanguage];
    }
    return desc;
};

const getSchemaLabel = (parentName, name, schema) => {
    let label = name;

    if(parentName) {
        label = name.replace(parentName + '.', '');
    }

    if(schema.textTransform) {
        if(schema.textTransform === 'upper') {
            label = label.toUpperCase()
        } else if(schema.textTransform === 'lower') {
            label = label.toLowerCase();
        }
    }

    return beautifyProp(label);
};

const calcSchemaHide = (store, schema) => {
    let hide = false;
    if(schema.hide) {
        hide = true;
        for(let k in schema.hide) {
            if(schema.hide.hasOwnProperty(k)) {
                // todo: add flag for strict checking, per default non-strict
                let val = store.get(k);
                if(undefined === val || null === val) {
                    // undefined/null is not same as false but should be treated not only as `falsy` but as `false`
                    val = false;
                }
                // eslint-disable-next-line
                if(val != schema.hide[k]) {
                    // when `hide` condition fails one time, display the element group
                    hide = false;
                }
            }
        }
    }

    return hide;
};

class SchemaGroupBase extends React.Component {
    constructor(props) {
        super(props);
        this.id = ID();
    }

    render() {
        const {
            currentSchema, name, name_abs, parentName,
            t, getTScope,
            noTrans
        } = this.props;

        let hasSubGroup = 'object' === currentSchema.type && 'undefined' === typeof currentSchema.object && 'undefined' !== typeof currentSchema.properties;

        let lblStyle = {margin: '0 0 ' + (hasSubGroup ? '4px' : '3px') + ' 0'};
        let useTrans = !(currentSchema.noTrans || noTrans);

        let desc = getSchemaDesc(currentSchema);
        let label = getSchemaLabel(parentName, name, currentSchema);

        let t_scope = getTScope('schema');

        let hide = calcSchemaHide(this.props.store, currentSchema);

        return hide ? null : <SchemaGroupContainer view={currentSchema.view}>
            <label htmlFor={this.id} className='schema-edit--lbl' style={lblStyle}>{(
                // build lbl, depending if the current one has subgroups OR only a normal group, to be able to use . as separator/json format etc.
                // when no `t` (trans, translate; from i18n) has been passed
                hasSubGroup ?
                    (useTrans && t ?
                        <b>{t(t_scope + 'group--' + name_abs)}</b> :
                        <b>{label}</b>) :
                    (useTrans && t ?
                        t(t_scope + name_abs) :
                        label)
            )}</label>

            {desc ? <p className='schema-edit--desc'>{desc}</p> : null}

            {'object' === currentSchema.type && 'undefined' === typeof currentSchema.object ?
                'undefined' !== typeof currentSchema.properties ?
                    <div className={
                        'schema-edit--group-list ' +
                        (currentSchema && currentSchema.view && currentSchema.view.block ? 'as-block ' : '')
                    }>
                        {Object.keys(currentSchema.properties).map((key) => (
                            <SchemaGroup
                                key={name_abs + '.' + key}
                                currentSchema={currentSchema.properties[key]}
                                name={key}
                                name_abs={name_abs + '.' + key}

                                parentName={name}
                                noTrans={currentSchema.noTrans || noTrans}
                            />
                        ))}
                    </div>
                    : 'object-error'
                : <div className='schema-edit--input'>
                    <ErrorBoundary>
                        <SchemaField
                            name={name}
                            name_abs={name_abs}
                            currentSchema={currentSchema}
                            lblId={this.id}
                        />
                    </ErrorBoundary>
                </div>}
        </SchemaGroupContainer>
    }
}

const SchemaGroup = withSchemaEditorStore(SchemaGroupBase);

export {SchemaGroup};
