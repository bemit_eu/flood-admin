import React from 'react';

import Loadable from 'react-loadable';
import Loading from '../../component/Loading';

const factory = {
    'bool': Loadable({
        loader: () => import('./field/Bool'),
        loading: (props) => <Loading {...props} name='SchemaEditor/field/Bool'/>,
    }),
    'string': Loadable({
        loader: () => import('./field/String'),
        loading: (props) => <Loading {...props} name='SchemaEditor/field/String'/>,
    }),
    'GenericList': Loadable({
        loader: () => import('./field/GenericList'),
        loading: (props) => <Loading {...props} name='SchemaEditor/field/GenericList'/>,
    }),
    'text': Loadable({
        loader: () => import('./field/Text'),
        loading: (props) => <Loading {...props} name='SchemaEditor/field/Text'/>,
    }),
};

const addField = (ident, component) => {
    factory[ident] = component;
};

class SchemaField extends React.PureComponent {
    render() {
        const {name, name_abs, currentSchema, lblId} = this.props;
        let type = currentSchema.type;
        if(currentSchema.object) {
            // when an `object` (string) is applied, overwrite the native JSON-Schema type with the object, contains a string like type
            type = currentSchema.object;
        }
        if(!currentSchema || !factory.hasOwnProperty(type)) {
            console.error('SchemaField, tried to create field without registered currentSchema.type: `' + type + '` in `' + name + '`.');
            return null;
        }

        const Comp = factory[type];

        return <Comp
            name={name}
            name_abs={name_abs}
            currentSchema={currentSchema}
            lblId={lblId}
        />;
    };
}

export {SchemaField, addField};
