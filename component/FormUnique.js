/**
 * Form helper for easy creation of matching `htmlFor` in `<label>` with `id` in `<input>` in as many forms as needed.
 *
 * Creates a unique string that should be prefixed with the input name. Recommended to use for one form and not per input,
 */
class FormUnique {
    static keys = [];

    /**
     * Tries to add one key to the storage
     * @private
     * @param key
     * @return {boolean} true when key wasn't existing till now
     */
    static add(key) {
        if(-1 === FormUnique.keys.indexOf(key)) {
            // key isn't existing till now
            FormUnique.keys.push(key);
            return true;
        }

        return false;
    }

    /**
     * Create a new unique key (8 chars)
     * @return {string}
     */
    static create() {
        let key = Math.floor((1 + Math.random()) * 0x1000000).toString(16).substring(1);
        if(FormUnique.add(key)) {
            // when the key was an unique return it
            return key;
        }

        // or create new ones until unique
        return FormUnique.create();
    }
}

export default FormUnique.create;