import React from 'react';
import {isBool, isObject, isString} from "@formanta/js";
import ReactJson from 'react-json-view';
import {withStyles, withTheme} from "../lib/theme";

const styleJson = {
    reactJson: {
        '& .react-json-view': {
            backgroundColor: 'transparent !important',
        },
        '& .react-json-view .object-key-val': {
            fontSize: '0.85rem',
            fontFamily: 'Inconsolata, monospace',
        },
    },
};

class JsonViewerBase extends React.Component {
    state = {
        hasError: false,
        jsonError: false,
        json: {}
    };

    static getDerivedStateFromError(error) {
        return {hasError: true};
    }

    componentDidMount() {
        this.createJson();
    }

    componentDidUpdate(prevProps) {
        if(prevProps.src !== this.props.src) {
            this.createJson();
        }
    }

    createJson() {
        if(isString(this.props.src)) {
            try {
                this.setState({json: JSON.parse(this.props.src), jsonError: false, hasError: false});
            } catch(e) {
                console.warn(e);
                this.setState({jsonError: true});
            }
        } else {
            this.setState({json: this.props.src, jsonError: false, hasError: false});
        }
    }

    render() {
        const {name, src, classes, theme,} = this.props;
        if(this.state.hasError) {
            return 'json-view-error ' + name;
        }

        if(this.state.jsonError || !isObject(this.state.json)) {
            return <div style={{margin: '18px 0'}}>
                <p style={{margin: '0 0 6px'}}>{name}</p>
                <p style={{margin: '0', wordBreak: 'break-all'}}>{isBool(this.state.json) ? this.state.json ? 'y' : 'n' : src}</p>
            </div>;
        }

        return <div className={classes.reactJson} style={{margin: '12px 0'}}>
            <ReactJson src={this.state.json}
                       theme={theme.name === 'dark' ? 'apathy' : 'apathy:inverted'}
                       name={name}
                       displayObjectSize
                       displayDataTypes
                       renderOnlyWhenOpen
            />
        </div>;
    }
}

const JsonViewer = withTheme(withStyles(styleJson)(JsonViewerBase));

export {JsonViewer};
