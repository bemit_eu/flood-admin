import React from 'react';
import {withContextConsumer} from "@formanta/react";
import {routesControlPanel, routesPage} from "../routes";
import {withFeatureManager} from "./FeatureManager";

const RoutesStoreContext = React.createContext({});

class RoutesStoreProviderBase extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};

        const {routesComponents} = this.props;

        const pages = routesComponents.pages || {};
        const controlPanel = routesComponents.controlPanel || {};

        this.state.routes_page = routesPage(pages);
        this.state.routes_control_panel = routesControlPanel(controlPanel);
    }

    componentDidUpdate(prevProps) {
        //if(prevProps.)
    }

    render() {
        return (
            <RoutesStoreContext.Provider value={this.state}>
                {this.props.children}
            </RoutesStoreContext.Provider>
        );
    }
}

const withRoutesStore = withContextConsumer(RoutesStoreContext.Consumer);

const RoutesStoreProvider = withFeatureManager(RoutesStoreProviderBase);

export {RoutesStoreProvider, withRoutesStore};