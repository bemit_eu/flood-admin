import React from 'react';
import {
    NavLink as Link
} from 'react-router-dom';

import {useTheme, createUseStyles} from '../../lib/theme';
import StyleGrid from "../../lib/StyleGrid";

const useStyles = createUseStyles(theme => ({
    li: {
        flexShrink: 0,
        [StyleGrid.smUp]: {
            width: '100%',
        },
    },
    link: {
        display: 'flex',
        position: 'relative',
        overflow: 'auto',
        padding: theme.gutter.base * 2.5 + 'px  6px',
        transition: '0.25s linear border-color, 0.15s linear background',
        border: '2px solid transparent',
        [StyleGrid.smUp]: {
            height: '100%',
            width: '100%',
            padding: '6px',
        },
        '&.active': {
            borderLeftColor: theme.linkActive,
            [StyleGrid.smUp]: {},
        },
        '&:hover': {
            textDecoration: 'none',
            background: theme.bgNavItemActive,
            borderLeftColor: theme.linkHover,
            [StyleGrid.smUp]: {},
        },
        '&:focus:not(.active)': {
            background: theme.bgNavItemActive,
        },
        '&.switch-border-pos.active': {
            borderLeftColor: 'transparent',
            borderBottomColor: theme.linkActive,
            [StyleGrid.smUp]: {
                borderLeftColor: theme.linkActive,
                borderBottomColor: 'transparent',
            },
        },
        '&.switch-border-pos:hover': {

            borderLeftColor: 'transparent',
            borderBottomColor: theme.linkHover,
            [StyleGrid.smUp]: {
                borderLeftColor: theme.linkHover,
                borderBottomColor: 'transparent',
            },
        },
    },
    icon: {
        display: 'block',
        transition: '0.2s linear width, 0.2s linear height'
    },
    txt: {
        //maxWidth: '150px',
        lineHeight: '1.125rem',
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        wordBreak: 'keep-all',
        paddingLeft: '3px',
        transition: '0.4s linear max-width',
    },
    notxt: {
        maxWidth: '0',
        opacity: 0,
        //height: '0',
        lineHeight: '1.125rem',
        paddingLeft: 0,
        overflow: 'hidden',
        transition: '0.4s linear max-width, 0.4s linear opacity, 0.01s linear padding-left 0.4s',
    }
}));

const NavItemBase = (props) => {
    const {route, down, switchBorderPos} = props;

    const theme = useTheme();
    const classes = useStyles(theme);

    let notxt = false;
    if(props.notxt) {
        notxt = props.notxt;
    }

    return <li className={classes.li} style={{marginTop: down === route.path ? 'auto' : 0}}>
        <Link to={route.path} className={classes.link + (switchBorderPos ? ' switch-border-pos' : '')}>
            {route.lbl.icon ? <route.lbl.icon className={classes.icon} notxt={notxt}/> : (notxt ? ' • ' : null)}
            <span className={classes.txt + ' ' + (notxt ? classes.notxt : '')}>{route.lbl.txt}</span>
        </Link>
    </li>
};

const NavItem = (props) => {
    const {route} = props;

    return (
        route.wrap ?
            <route.wrap>
                <NavItemBase {...props}/>
            </route.wrap> :
            <NavItemBase {...props}/>
    )
};

export default NavItem;
