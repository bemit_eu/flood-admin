import React from 'react';
import {
    Route
} from 'react-router-dom';

import NavItem from './Item';
import {withNamespaces} from "react-i18next";

import {createUseStyles} from '../../lib/theme';
import StyleGrid from '../../lib/StyleGrid';

const useStyles = createUseStyles(theme => ({
    wrapper: {
        padding: 0,
        //alignItems: 'center',
        [StyleGrid.smUp]: {
            padding: 0,
        },
    },
}));

const Nav = (props) => {
    const {
        t,
        notxt, down,
        content, switchBorderPos
    } = props;
    const path = props.path || '';

    const classes = useStyles();

    return (
        <React.Fragment>
            {content ?
                <Route path={'/:lng' + path} render={props => (
                    <ul className={classes ? classes.wrapper : ''}>
                        {content(props.match, t).map((route, index) => (
                            <NavItem key={index} route={route} notxt={notxt} down={down} switchBorderPos={switchBorderPos}/>
                        ))}
                    </ul>
                )}/> : null}
        </React.Fragment>
    )
};

export default withNamespaces('common')(Nav);
