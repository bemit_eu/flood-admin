import React from 'react';
import {ReactComponent as ToggleOnBase} from "./toggle-on.svg";
import {ReactComponent as ToggleOffBase} from "./toggle-off.svg";
import {useTheme} from '../../lib/theme';

const ToggleOff = (p) => {
    const theme = useTheme();

    return <ToggleOffBase
        style={{
            width: p.size || '1em', height: p.size || '1em', marginTop: '0',
            fill: theme.textColorLight, display: 'block',
        }}/>
};

const ToggleOn = (p) => {
    const theme = useTheme();

    return <ToggleOnBase
        style={{
            width: p.size || '1em', height: p.size || '1em', marginTop: '0',
            fill: theme.linkActive, display: 'block',
        }}/>
};

const ToggleOnOff = p => p.active ? <ToggleOn {...p}/> : <ToggleOff {...p}/>;

export {ToggleOnOff, ToggleOff, ToggleOn};
