import React from "react";
import Loadable from "react-loadable";
import Loading from "./component/Loading";
import {addField} from "./component/Schema/FieldFactory";

const setupComponents = () => {

    addField('color', Loadable({
        loader: () => import('./component/SchemaFields/Color'),
        loading: (props) => <Loading {...props} name='SchemaEditor/field/Color'/>,
    }));

    addField('StringList', Loadable({
        loader: () => import('./component/SchemaFields/StringList'),
        loading: (props) => <Loading {...props} name='SchemaEditor/field/StringList'/>,
    }));

    addField('EMail', Loadable({
        loader: () => import('./component/SchemaFields/EMail'),
        loading: (props) => <Loading {...props} name='SchemaEditor/field/EMail'/>,
    }));

    addField('Tel', Loadable({
        loader: () => import('./component/SchemaFields/Tel'),
        loading: (props) => <Loading {...props} name='SchemaEditor/field/Tel'/>,
    }));

    addField('Code', Loadable({
        loader: () => import('./component/SchemaFields/Code'),
        loading: (props) => <Loading {...props} name='SchemaEditor/field/Code'/>,
    }));

    addField('TextRich', Loadable({
        loader: () => import('./component/SchemaFields/TextRich'),
        loading: (props) => <Loading {...props} name='SchemaEditor/field/TextRich'/>,
    }));

    addField('TextRichInline', Loadable({
        loader: () => import('./component/SchemaFields/TextRichInline'),
        loading: (props) => <Loading {...props} name='SchemaEditor/field/TextRichInline'/>,
    }));

    addField('File', Loadable({
        loader: () => import('./component/SchemaFields/File'),
        loading: (props) => <Loading {...props} name='SchemaEditor/field/File'/>,
    }));

    addField('Files', Loadable({
        loader: () => import('./component/SchemaFields/Files'),
        loading: (props) => <Loading {...props} name='SchemaEditor/field/Files'/>,
    }));
};

export {setupComponents};
