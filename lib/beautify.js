/**
 *
 * @param {string} str
 * @param search
 * @param replacement
 * @return {string}
 */
const strReplaceAll = (str, search, replacement) => {
    return str.split(search).join(replacement);
};

const beautifyProp = (name) => {
    return strReplaceAll(strReplaceAll(strReplaceAll(strReplaceAll(name, '__', ' '), '_', ' '), '.', ' '), '-', ' ')
        .split(' ')
        .map((s) => s.charAt(0).toUpperCase() + s.slice(1))// make first letter uppercase
        .join(' ');
};

export {beautifyProp, strReplaceAll};
