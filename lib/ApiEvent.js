/**
 *
 */
class ApiEvent {
    constructor(evts) {
        this.evts = evts;
    }

    trigger = (type) => {
        if(this.evts[type]) {
            for(let name in this.evts[type]) {
                if(this.evts[type].hasOwnProperty(name)) {
                    this.evts[type][name]();
                }
            }
        }
    };

    on = (type, name, evt) => {
        if(!this.evts[type]) {
            this.evts[type] = {};
        }
        this.evts[type][name] = evt;
    };

    off = (type, name) => {
        if(this.evts[type]) {
            delete this.evts[type][name];
        }
    };
}

export {ApiEvent};