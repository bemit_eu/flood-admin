class ApiError extends Error {
    constructor(err, endpoint, method) {
        super('ApiError');

        this.original = err;

        this.status = err.status || 500;

        this.endpoint = endpoint;
        this.method = method;
    }
}


export {ApiError};