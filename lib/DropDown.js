import React from 'react';
import posed from 'react-pose';
import {useTheme, createUseStyles} from './theme';
import StyleGrid from "./StyleGrid";

const WrapperDrop = posed.div({
    hidden: {
        height: 0,
        delay: 0,
    },
    visible: {
        height: '100%',
        delay: 0,
    }
});

const DropDown = (props) => {
    return <WrapperDrop withParent={props.parent || false}
                        className={props.className}
                        duration={props.duration}
                        pose={props.open ? 'visible' : 'hidden'}
                        style={Object.assign({overflow: 'hidden'}, props.style || {})}>
        {props.children}
    </WrapperDrop>
};

const WrapperDropAuto = posed.div({
    hidden: {
        height: 0,
        delay: 0,
    },
    visible: {
        height: 'auto',
        delay: 0,
    }
});

const DropDownAuto = (props) => {
    return <WrapperDropAuto withParent={props.parent || false}
                            className={props.className}
                            duration={props.duration}
                            pose={props.open ? 'visible' : 'hidden'}
                            style={Object.assign({overflow: 'hidden'}, props.style || {})}>
        {props.children}
    </WrapperDropAuto>
};

const WrapperDropDelayed = posed.div({
    hidden: {
        height: 0,
        delay: 65
    },
    visible: {
        height: '100%',
        delay: 5
    }
});


const WrapperGrowWidth = posed.div({
    hidden: {
        width: 0,
    },
    visible: {
        width: 'auto',
    }
});

const GrowWidth = (props) => {
    return <WrapperGrowWidth withParent={props.parent || false}
                             className={props.className}
                             duration={props.duration}
                             pose={props.open ? 'visible' : 'hidden'}
                             style={Object.assign({overflow: 'hidden'}, props.style || {})}>
        {props.children}
    </WrapperGrowWidth>
};


const stylingHorizontal = createUseStyles(theme => ({
    navInner: {
        display: 'flex',
        overflowY: 'hidden',
        overflowX: 'auto',
        marginBottom: '-1px',
        position: 'relative',
    },
    navWrapper: {
        display: 'flex',
        flexDirection: 'column',
        overflowY: 'hidden',
        overflowX: 'auto',
        margin: '3px 0 0 0',
        padding: 0,
        flexShrink: 0,
    },
    toggleFullScreen: {
        flexShrink: 0,
        display: 'block',
        alignItems: 'center',
        textAlign: 'center',
        position: 'relative',
        zIndex: 1,
        width: '2rem',
        height: '2rem',
        lineHeight: '2rem',
        marginBottom: '0px',
        padding: 0,
        transition: 'background ' + theme.transition,
        background: theme.bgDark,
        '&.active': {
            background: theme.bgLight,
        }
    },
    navBtn: {
        flexShrink: 0,
        display: 'inline-block',
        padding: '6px 12px',
        border: '1px solid transparent',
        color: theme.textColor,
        height: '2rem',
        borderColor: theme.textColorLight,
        borderBottom: 0,
        transition: 'background ' + theme.transition + ', border ' + theme.transition,
        background: 'transparent',
        '&.active': {
            background: theme.btnActive,
        },
        [StyleGrid.mdUp]: {
            width: 'auto',
            borderRightWidth: 0,
        },
        '&:last-child': {
            borderRightWidth: 1,
        },
    },
    navBorder: {
        borderBottom: '1px solid ' + theme.textColorLight,
        transition: 'border ' + theme.transition
    }
}));

const DropDownHorizontalStyled = props => {
    const theme = useTheme();
    const classes = stylingHorizontal({theme});
    return <props.parentComponent classDropHorizontal={classes} {...props.parentProps}/>
};
const styleDropDownHorizontal = Component => (p) => <DropDownHorizontalStyled parentProps={p} parentComponent={Component}/>;

const DropHorizontalWrapper = (props) => {
    return <div style={Object.assign({
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'no-wrap',
        overflowX: 'hidden',
        position: 'relative',
        flexGrow: 2
    }, props.style || {})}>
        {props.children}
    </div>
};

const DropHorizontal = (props) => {
    const theme = useTheme();
    let hidden = props.pos ? (-100 * (props.pos - 1)) + '%' : '+100%';
    let visible = props.pos ? (-100 * props.pos) + '%' : 0;

    return <div className={props.className}
                style={Object.assign({
                    position: 'relative',
                    flexShrink: 0,
                    width: '100%',
                    height: '100%',
                    background: theme.bgPage,
                    transition: 'transform .65s cubic-bezier(.15,1.5,.5,1.05), background ' + theme.transition,
                    transform: 'translateX(' + (props.open ? visible : hidden) + ')'
                }, props.style || {})}>
        <div style={{
            padding: '12px 0',
            height: '100%',
        }}>
            <WrapperDropDelayed pose={props.open ? 'visible' : 'hidden'}
                                style={{overflow: props.open ? 'visible' : 'hidden'}}
                                duration={props.open ? 0 : 5}>
                {props.children}
            </WrapperDropDelayed>
        </div>
    </div>
};

const createDropDown = (height = 'auto', padding = 6) => {
    const WrapperDropDyn = posed.div({
        hidden: {
            height: 0,
            padding: 0,
        },
        visible: {
            height: height,
            padding: padding,
        }
    });

    return (props) => {
        return <WrapperDropDyn withParent={false} className={props.className} pose={props.open ? 'visible' : 'hidden'} duration={250} style={Object.assign({overflow: 'hidden'}, props.style || {})}>
            {props.children}
        </WrapperDropDyn>
    };
};

const createDropRight = (width = 'auto', padding = 6) => {
    const WrapperDropDyn = posed.div({
        hidden: {
            width: 0,
            padding: 0,
        },
        visible: {
            width: width,
            padding: padding,
        }
    });

    return (props) => {
        return <WrapperDropDyn withParent={false} className={props.className} pose={props.open ? 'visible' : 'hidden'} duration={250} style={Object.assign({overflow: 'hidden'}, props.style || {})}>
            {props.children}
        </WrapperDropDyn>
    };
};

export {
    DropDown, DropDownAuto,
    GrowWidth,
    DropHorizontalWrapper, DropHorizontal, styleDropDownHorizontal,
    createDropDown, createDropRight
};
