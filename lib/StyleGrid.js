let config = {
    sm: 768,
    md: 992,
    lg: 1200,
    lgx: 2400,
};

export default {
    // use in things that will be used by `injectSheet` w/`className={classes.thing}` or in `<style>`
    smx: `@media (max-width: ${config.sm - 1}px)`,
    smUp: `@media (min-width: ${config.sm}px)`,
    sm: `@media (min-width: ${config.sm}px) and (max-width: ${config.md - 1}px)`,
    smDown: `@media (max-width: ${config.md - 1}px)`,
    mdUp: `@media (min-width: ${config.md}px)`,
    md: `@media (min-width: ${config.md}px) and (max-width: ${config.lg - 1}px)`,
    mdDown: `@media (max-width: ${config.lg - 1}px)`,
    lgUp: `@media (min-width: ${config.lg}px)`,
    lg: `@media (min-width: ${config.lg}px) and (max-width: ${config.lgx - 1}px)`,
    lgDown: `@media (max-width: ${config.lgx - 1}px)`,
    lgx: `@media (min-width: ${config.lgx}px)`,

    // use in things that get used as `style={}` prop
    jss: {
        smx: `@media (maxWidth: ${config.sm - 1}px)`,
        smUp: `@media (minWidth: ${config.sm}px)`,
        sm: `@media (minWidth: ${config.sm}px) and (maxWidth: ${config.md - 1}px)`,
        smDown: `@media (maxWidth: ${config.md - 1}px)`,
        mdUp: `@media (minWidth: ${config.md}px)`,
        md: `@media (minWidth: ${config.md}px) and (maxWidth: ${config.lg - 1}px)`,
        mdDown: `@media (maxWidth: ${config.lg - 1}px)`,
        lgUp: `@media (minWidth: ${config.lg}px)`,
        lg: `@media (minWidth: ${config.lg}px) and (maxWidth: ${config.lgx - 1}px)`,
        lgDown: `@media (maxWidth: ${config.lgx - 1}px)`,
        lgx: `@media (minWidth: ${config.lgx}px)`,
    }
};
