import React from 'react';

import {
    MdDashboard,
    MdLibraryBooks,
    MdQuestionAnswer,
    MdPeople,
    MdShoppingCart,
    MdDeveloperBoard,
    MdCollections,
    MdWidgets,
    MdDeviceHub,
    MdTimeline,
    MdSettings,
} from 'react-icons/md';
import {needsPermission} from "./feature/AuthPermission";
import {FEATURE_ANALYZE, FEATURE_BLOCKS, FEATURE_CONNECTIONS, FEATURE_CONTENT, FEATURE_MARKETING, FEATURE_MEDIA, FEATURE_MESSAGE, FEATURE_SHOP, FEATURE_USER, PERM_ADMIN} from "./permissions";

const navContent = (isEnabled) =>
    (/** object **/match, /** function translation prop with namespace common**/t) => {
        const items = [];
        items.push({
            path: '/' + match.params.lng + '/dashboard',
            lbl: {icon: (p) => <MdDashboard className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.main.dashboard')},
        });

        if(isEnabled('message')) {
            items.push({
                path: '/' + match.params.lng + '/messages',
                lbl: {icon: (p) => <MdQuestionAnswer className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.main.messages')},
                wrap: needsPermission([PERM_ADMIN, FEATURE_MESSAGE])((props) => props.children)
            });
        }

        if(isEnabled('content')) {
            items.push({
                path: '/' + match.params.lng + '/content',
                lbl: {icon: (p) => <MdLibraryBooks className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.main.content')},
                wrap: needsPermission([PERM_ADMIN, FEATURE_CONTENT])((props) => props.children)
            });
        }

        if(isEnabled('shop')) {
            items.push({
                path: '/' + match.params.lng + '/shop',
                lbl: {icon: (p) => <MdShoppingCart className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.main.shop')},
                wrap: needsPermission([PERM_ADMIN, FEATURE_SHOP])((props) => props.children)
            });
        }
        if(isEnabled('marketing')) {
            items.push({
                path: '/' + match.params.lng + '/marketing',
                lbl: {icon: (p) => <MdDeveloperBoard className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.main.marketing')},
                wrap: needsPermission([PERM_ADMIN, FEATURE_MARKETING])((props) => props.children)
            });
        }
        if(isEnabled('media')) {
            items.push({
                path: '/' + match.params.lng + '/media',
                lbl: {icon: (p) => <MdCollections className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.main.media')},
                wrap: needsPermission([PERM_ADMIN, FEATURE_MEDIA])((props) => props.children)
            });
        }
        if(isEnabled('user')) {
            items.push({
                path: '/' + match.params.lng + '/users',
                lbl: {icon: (p) => <MdPeople className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.main.users')},
                wrap: needsPermission([PERM_ADMIN, FEATURE_USER])((props) => props.children)
            });
        }
        if(isEnabled('analyze')) {
            items.push({
                path: '/' + match.params.lng + '/analyze',
                lbl: {icon: (p) => <MdTimeline className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.main.analyze')},
                wrap: needsPermission([PERM_ADMIN, FEATURE_ANALYZE])((props) => props.children)
            });
        }
        if(isEnabled('block')) {
            items.push({
                path: '/' + match.params.lng + '/blocks',
                lbl: {icon: (p) => <MdWidgets className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.main.blocks')},
                wrap: needsPermission([PERM_ADMIN, FEATURE_BLOCKS])((props) => props.children)
            });
        }
        if(isEnabled('connection')) {
            items.push({
                path: '/' + match.params.lng + '/connections',
                lbl: {icon: (p) => <MdDeviceHub className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.main.connections')},
                wrap: needsPermission([PERM_ADMIN, FEATURE_CONNECTIONS])((props) => props.children)
            });
        }

        items.push({
            path: '/' + match.params.lng + '/settings',
            lbl: {icon: (p) => <MdSettings className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.main.settings')},
        });

        return items;
    };

export {navContent};