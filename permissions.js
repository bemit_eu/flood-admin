export const PERM_ADMIN = 'admin';

export const FEATURE_MESSAGE = 'feature.message';

export const FEATURE_CONTENT = 'feature.content';
export const FEATURE_CONTENT_SECTION = 'feature.content.section';
export const FEATURE_CONTENT_ARTICLE = 'feature.content.article';

export const FEATURE_SHOP = 'feature.shop';
export const FEATURE_SHOP_SETTING = 'feature.shop.setting';
export const FEATURE_SHOP_ORDER = 'feature.shop.order';
export const FEATURE_SHOP_GROUP = 'feature.shop.group';
export const FEATURE_SHOP_PRODUCT = 'feature.shop.product';

export const FEATURE_MARKETING = 'feature.marketing';
export const FEATURE_MARKETING_NEWSLETTER = 'feature.marketing.newsletter';

export const FEATURE_MEDIA = 'feature.media';

export const FEATURE_USER = 'feature.user';

export const FEATURE_ANALYZE = 'feature.analyze';

export const FEATURE_BLOCKS = 'feature.blocks';

export const FEATURE_CONNECTIONS = 'feature.connections';