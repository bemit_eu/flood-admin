import React from 'react';
import {storeAnalyzeDiagrams} from "./Stores/AnalyzeDiagrams";
import {AnalyzeDiagramFactory} from './AnalyzeDiagramFactory';

class AnalyzeDiagramViewerBase extends React.PureComponent {
    render() {
        const {analyzeDiagrams} = this.props;

        return (
            <div style={{display: 'flex', flexWrap: 'wrap', margin: '0 -6px'}}>
                {Object.keys(analyzeDiagrams).map(id => <AnalyzeDiagramFactory key={id} id={id} diagram={analyzeDiagrams[id]}/>)}
            </div>
        )
    }
}


const AnalyzeDiagramViewer = storeAnalyzeDiagrams(AnalyzeDiagramViewerBase);

export {AnalyzeDiagramViewer};
