import React from 'react';
import {withNamespaces} from 'react-i18next';
import posed from 'react-pose';
import {MdPlaylistAdd, MdEdit, MdSave, MdPlayCircleFilled, MdKeyboardArrowDown} from "react-icons/md";
import {ID} from "@formanta/js";

import {ButtonSmall, Select, Label, InputText} from "../../component/Form";
import {cancelPromise} from "../../component/cancelPromise";
import LoadingSmall from "../../component/LoadingSmall";
import {withBackend} from "../BackendConnect";
import {withAuthPermission} from "../AuthPermission";
import {storeAnalyze} from "./Stores/Analyze";
import {beautifyProp} from "../../lib/beautify";
import QueryManagerConditionBuilder from "./QueryManagerConditionBuilder";
import {ButtonInteractiveDelete} from "../../component/ButtonInteractiveDelete";
import {AnalyzeQuerySelector} from "./AnalyzeQuerySelector";

const DropDown = posed.div({
    hidden: {
        height: 0,
        delay: 0,
    },
    visible: {
        height: 'auto',
        delay: 0,
    }
});

class QueryManager extends React.PureComponent {
    state = {
        openBase: false,
        openAdvanced: false,
    };

    constructor(props) {
        super(props);
        this.id = ID();
    }

    render() {
        const {
            // general
            t,
            // from parent
            executeQuery,
            selectedTypes, selectType,
            selectedTypeFields, selectTypeFields,
            activeQuery, selectActiveQuery,
            createOrOverwriteQuery, deleteQuery, selectQuery,
            selectedCondition, addConditionField, updateConditionField, addConditionConjunction,
            selectedConditionBetween, selectedConditionLimit, updateBetween, updateLimit,
            activeQueryName, updateQueryName,
            // from providers
            analyzeTypes, analyzeTypesLoaded,
            analyzeTypeFields, getAnalyzePropsLoaded,
        } = this.props;

        return (
            <React.Fragment>
                <h2 style={{margin: '0 0 3px 0', fontSize: '1.4rem'}}>{t('title-query-manager')}</h2>
                <div style={{display: 'flex'}}>
                    <div style={{marginRight: 12, display: 'flex', flexDirection: 'column', maxWidth: '220px', flexShrink: 0}}>
                        <h3 style={{width: '100%', margin: '12px 0 6px 0'}}>{t('title-qm-queries')}</h3>

                        <AnalyzeQuerySelector
                            style={{margin: '24px 0 12px 0', maxHeight: '10rem', minWidth: '100px', overflowY: 'auto',}}
                            activeQuery={activeQuery}
                            onSelect={selectActiveQuery}
                        />

                        <div style={{margin: '9px 0 9px 0', width: '100%', flexShrink: 1}}>
                            <Label htmlFor={this.id + '-qname'} style={{padding: '3px 0 2px 0', fontSize: '0.85rem',}}>{t('title-qm-query-name')}</Label>
                            <InputText id={this.id + '-qname'} value={activeQueryName} onChange={updateQueryName} style={{
                                width: '220px', padding: '4px 6px',
                                fontSize: '0.85rem',
                            }}/>
                        </div>

                        <div style={{display: 'flex', flexWrap: 'wrap', marginBottom: 12}}>
                            {activeQuery !== '-' ?
                                <ButtonSmall style={{margin: '6px 0 0 0', width: '100%', flexGrow: 1,}} onClick={selectQuery}>
                                    <MdEdit style={{verticalAlign: 'bottom'}}/> select
                                </ButtonSmall> : null}

                            {activeQuery !== '-' ?
                                <br/> : null}

                            <ButtonSmall style={{
                                margin: activeQuery !== '-' ? '6px 0 0 0' : '6px 0 0 0',
                                width: '100%',
                                flexGrow: 1,
                            }} onClick={createOrOverwriteQuery}>{activeQuery === '-' ?
                                <span><MdPlaylistAdd style={{verticalAlign: 'bottom'}}/> create</span> :
                                <span><MdSave style={{verticalAlign: 'bottom'}}/> overwrite</span>
                            }</ButtonSmall>

                            {activeQuery !== '-' ?
                                <ButtonInteractiveDelete
                                    comp={(p) => (
                                        <ButtonSmall {...p} style={{margin: '6px 0 0 0', width: '100%', flexGrow: 1,}}>
                                            {p.children} delete
                                        </ButtonSmall>
                                    )}
                                    onClick={deleteQuery}
                                /> : null}
                        </div>

                        <div style={{display: 'flex', flexWrap: 'wrap', marginTop: 'auto'}}>
                            {activeQuery !== '-' ? <ButtonSmall style={{margin: '0', width: '45%', flexGrow: 1,}} onClick={executeQuery}>
                                <MdPlayCircleFilled style={{verticalAlign: 'bottom'}}/> execute
                            </ButtonSmall> : null}
                        </div>
                    </div>

                    <div style={{
                        display: 'flex', flexWrap: 'wrap',
                        alignItems: 'flex-start',
                    }}>
                        <div style={{paddingRight: 12}}>
                            <h3 style={{margin: '12px 0 6px 0'}}>{t('title-qm-types')}</h3>
                            <div style={{height: '1.15rem', width: '100%', margin: '0 0 6px 0'}}/>
                            {'progress' === analyzeTypesLoaded || 'error' === analyzeTypesLoaded ?
                                <LoadingSmall
                                    center={true}
                                    errorWarning={'error' === analyzeTypesLoaded}
                                /> :
                                true === analyzeTypesLoaded ? <div>
                                    <Select multiple value={selectedTypes} data-name="selectedTypes" onChange={selectType} style={{
                                        minHeight: '10rem', maxHeight: '25rem', minWidth: '150px'
                                    }}>
                                        {analyzeTypes.map((id) => (
                                            <option key={id}
                                                    value={id}>{id}</option>
                                        ))}
                                    </Select>
                                </div> : null}
                        </div>

                        {selectedTypes.length ?
                            <div style={{display: 'flex', flexDirection: 'column'}}>
                                <h3 style={{margin: '12px 0 6px 0'}}>{t('title-qm-props')}</h3>
                                <div style={{display: 'flex', flexWrap: 'wrap'}}>
                                    {selectedTypes.map((type) => (<div key={type} style={{marginRight: 6}}>
                                        <h4 style={{margin: '3px 0'}}>{type}</h4>
                                        {'progress' === getAnalyzePropsLoaded(type) || 'error' === getAnalyzePropsLoaded(type) ?
                                            <LoadingSmall
                                                errorWarning={'error' === getAnalyzePropsLoaded(type)}
                                            /> :
                                            selectedTypeFields && true === getAnalyzePropsLoaded(type) ? <div>
                                                <Select multiple value={selectedTypeFields[type]} data-type={type} onChange={selectTypeFields} style={{
                                                    minHeight: '10rem', maxHeight: '25rem', minWidth: '150px'
                                                }}>
                                                    {analyzeTypeFields[type].map((prop) => (
                                                        <option key={prop}
                                                                value={prop}>{beautifyProp(prop)}</option>
                                                    ))}
                                                </Select>
                                            </div> : null}
                                    </div>))}
                                </div>
                            </div> : null}


                        <ButtonSmall
                            style={{margin: '12px 0 0 0', width: '100%', flexGrow: 0,}}
                            onClick={() => this.setState({openBase: !this.state.openBase})}
                        ><MdKeyboardArrowDown
                            style={{
                                verticalAlign: 'bottom', transition: '0.3s transform linear',
                                transform: 'rotate(' + (this.state.openBase ? '180' : 0) + 'deg)'
                            }}
                        /> {t('qm-base.title')}</ButtonSmall>


                        <DropDown style={{width: '100%', overflow: 'hidden', display: 'flex', flexWrap: 'wrap'}} withParent={false}
                                  pose={this.state.openBase ? 'visible' : 'hidden'}
                        >
                            <div style={{marginRight: 6}}>
                                <p style={{width: '100%', margin: '6px 0 0 0', fontWeight: 'bold'}}>{t('qm-base.between')}</p>
                                <div style={{display: 'flex'}}>
                                    <div style={{marginRight: 6}}>
                                        <Label htmlFor={this.id + '_between-since'} style={{padding: '3px 0 2px 0'}}>{t('qm-base.between-since')}</Label>
                                        <InputText id={this.id + '_between-since'} type={'datetime-local'} placeholder={'YYYY-MM-DD HH:MM:SS'}
                                                   style={{width: '220px'}}
                                                   onChange={(e) => updateBetween('since', e.target.value)}
                                                   value={selectedConditionBetween.since}
                                        />
                                    </div>
                                    <div style={{marginRight: 6}}>
                                        <Label htmlFor={this.id + '_between-till'} style={{padding: '3px 0 2px 0'}}>{t('qm-base.between-till')}</Label>
                                        <InputText id={this.id + '_between-till'} type={'datetime-local'} placeholder={'YYYY-MM-DD HH:MM:SS'}
                                                   style={{width: '220px'}}
                                                   onChange={(e) => updateBetween('till', e.target.value)}
                                                   value={selectedConditionBetween.till}
                                        />
                                    </div>
                                </div>
                            </div>
                            <div>
                                <p style={{width: '100%', margin: '6px 0 0 0', fontWeight: 'bold'}}>{t('qm-base.limit')}</p>
                                <div style={{display: 'flex'}}>
                                    <div style={{marginRight: 6}}>
                                        <Label htmlFor={this.id + '_limit-length'} style={{padding: '3px 0 2px 0'}}>{t('qm-base.limit-length')}</Label>
                                        <InputText id={this.id + '_limit-length'} type={'number'} min={0}
                                                   style={{width: '107px'}}
                                                   onChange={(e) => updateLimit('length', e.target.value)}
                                                   value={selectedConditionLimit.length}
                                        />
                                    </div>
                                    <div>
                                        <Label htmlFor={this.id + '_limit-offset'} style={{padding: '3px 0 2px 0'}}>{t('qm-base.limit-offset')}</Label>
                                        <InputText id={this.id + '_limit-offset'} type={'number'} min={0}
                                                   style={{width: '107px'}}
                                                   onChange={(e) => updateLimit('offset', e.target.value)}
                                                   value={selectedConditionLimit.offset}
                                        />
                                    </div>
                                </div>
                            </div>
                        </DropDown>

                        <ButtonSmall
                            style={{margin: '12px 0 0 0', width: '100%', flexGrow: 0,}}
                            onClick={() => this.setState({openAdvanced: !this.state.openAdvanced})}
                        ><MdKeyboardArrowDown
                            style={{verticalAlign: 'bottom', transition: '0.3s transform linear', transform: 'rotate(' + (this.state.openAdvanced ? '180' : 0) + 'deg)'}}
                        /> {t('qm-custom.title')}</ButtonSmall>

                        <DropDown style={{width: '100%', overflow: 'hidden'}} withParent={false}
                                  pose={this.state.openAdvanced ? 'visible' : 'hidden'}
                        >
                            <div style={{marginTop: 12}}>
                                <QueryManagerConditionBuilder
                                    rootLevel
                                    selectedCondition={selectedCondition}
                                    addField={addConditionField}
                                    updateField={updateConditionField}
                                    addConjunction={addConditionConjunction}
                                    selectedTypes={selectedTypes}
                                    selectedTypeFields={selectedTypeFields}
                                />
                            </div>
                        </DropDown>
                    </div>
                </div>
            </React.Fragment>
        )
    };
}

export default withNamespaces('page--analyze')(cancelPromise(withAuthPermission(withBackend(storeAnalyze(QueryManager)))));
