import React from 'react';
import {isBool, isNumber, isString, isObject} from "@formanta/js";

import {beautifyProp} from "../../lib/beautify";
import {cancelPromise} from "../../component/cancelPromise";
import {withBackend} from "../BackendConnect";
import {withAuthPermission} from "../AuthPermission";
import {storeAnalyze} from "./Stores/Analyze";
import {ButtonRaw} from "../../component/Form";
import {MdCancel, MdCheck, MdChevronLeft} from "react-icons/md";

const font = '"Inconsolata", "Menlo", "Consolas", monospace';

class ItemCell extends React.PureComponent {
    render() {
        const p = this.props;
        return <div style={{display: 'flex', padding: p.asTable ? p.isChild ? '4px 6px' : '11px 5px 3px 5px ' : '4px', flexDirection: 'column', height: '100%', width: '100%', position: 'relative'}}>
            {p.asTable ? null :
                <p style={{
                    fontWeight: 'bold', margin: '0 0 2px 0',
                    fontSize: p.asTable ? '0.87rem' : '1rem', fontFamily: p.asTable ? font : p.theme.monospace
                }}>{beautifyProp(p.name)}</p>}
            {isString(p.val) || isNumber(p.val) ?
                <p style={{
                    margin: '2px 0 2px 0',
                    fontSize: p.asTable ? '0.87rem' : '1rem', fontFamily: p.asTable ? font : 'inital'
                }}>{p.val}</p> : null}
            {isBool(p.val) ?
                <p style={{
                    margin: '2px 0 2px 0',
                    fontSize: p.asTable ? '0.87rem' : '1rem', fontFamily: p.asTable ? font : 'inital'
                }}>{p.val ?
                    <span><MdCheck/> <small>{p.t('common:yes')}</small></span> :
                    <span><MdCancel/> <small>{p.t('common:no')}</small></span>
                }</p> : null}
            {Array.isArray(p.val) ?
                p.val.map((val, i) => (
                    <div key={val} style={{margin: '2px 0 2px 0', fontSize: p.asTable ? '0.87rem' : '1rem'}}>{val ? isString(val) ? val :
                        JSON.stringify(val) : null}</div>
                )) : null}
            {p.val && isObject(p.val) ?
                <div style={{
                    display: 'grid',
                    gridColumnGap: '2px',
                    gridRowGap: '3px',
                    gridTemplateColumns: '1fr 1fr',
                }}>
                    {Object.keys(p.val).map(key => (
                        <ItemCell key={key} name={key} val={p.val[key]} t={p.t} theme={p.theme} asTable={p.asTable} isChild/>
                    ))}
                </div> : null}
        </div>;
    }
}

class AnalyzeOverview extends React.PureComponent {

    state = {
        open: false,
    };

    buttonHeader = (p) => <ButtonRaw
        onClick={() => this.setState({open: !this.state.open})}
        style={{
            color: this.props.theme.textColor,
            padding: this.state.open ? '6px 0 2px 0' : '4px 0',
            width: '100%', display: 'flex',
            height: '100%',
            marginLeft: -4
        }}
    >{p.children}</ButtonRaw>;

    render() {
        const {
            t, theme, classes,
            colLength, item,
            asTable, lastResultColumns,
        } = this.props;

        return <React.Fragment>
            <tr className={classes.row + ' ' + classes.rowHeader}>
                <td colSpan={colLength} style={{
                    verticalAlign: 'top',
                    zIndex: 2,
                    padding: 0,
                    background: asTable ? 'transparent' : theme.bgPage,
                    borderLeft: this.state.open && !asTable ? '1px solid ' + (asTable ? 'rgba(96,119,102,0.08)' : theme.borderLight) : 0,
                    borderBottom: this.state.open && !asTable ? '1px solid ' + (asTable ? 'rgba(96,119,102,0.3)' : theme.borderLight) : 0,
                    position: asTable ? 'absolute' : 'static',
                    top: asTable ? -5 : 0,
                }}>
                    {asTable ?
                        <React.Fragment>
                            {item.evt && item.evt.ID ?
                                <span style={{
                                    opacity: 0.8, marginRight: 6, fontSize: '0.8rem',
                                    fontFamily: asTable ? '"Inconsolata", "Menlo", "Consolas", monospace' : 'inherit',
                                }}>{item.evt.ID},</span>
                                : null}
                            {item.evt && item.evt.type ?
                                <span style={{
                                    opacity: 0.8, marginRight: 6, fontSize: '0.8rem',
                                    fontFamily: asTable ? '"Inconsolata", "Menlo", "Consolas", monospace' : 'inherit',
                                }}>{item.evt.type},</span>
                                : null}
                            {item.evt && item.evt.time ?
                                <span style={{
                                    opacity: 0.8, marginRight: 6, fontSize: '0.8rem',
                                    fontFamily: asTable ? '"Inconsolata", "Menlo", "Consolas", monospace' : 'inherit',
                                }}>{item.evt.time}</span>
                                : null}
                        </React.Fragment> :
                        <this.buttonHeader>
                            <MdChevronLeft color={'inherit'} size={'1.3em'} style={{transform: 'rotate(' + (this.state.open ? '90deg' : '-90deg') + ')', transition: '0.3s transform linear'}}/>
                            {item.evt && item.evt.ID ?
                                <span style={{opacity: 0.6, marginRight: 6}}>{item.evt.ID}</span>
                                : null}

                            {item.evt && item.evt.type ?
                                <span style={{opacity: 0.6, marginRight: 6}}>{item.evt.type}</span>
                                : null}
                            {item.evt && item.evt.time ?
                                <span style={{opacity: 0.6, marginRight: 6}}>{item.evt.time}</span>
                                : null}
                        </this.buttonHeader>}
                </td>
            </tr>
            <tr className={classes.row + ' ' + classes.rowContent}>
                {Object.keys(lastResultColumns).map((key) => (
                    <td key={key} style={{
                        borderLeft: asTable || this.state.open ? '1px solid ' + (asTable ? 'rgba(96,119,102,0.1)' : theme.borderLight) : 0,
                        borderBottom: asTable || this.state.open ? '1px solid ' + (asTable ? 'rgba(96,119,102,0.3)' : theme.borderLight) : 0,
                        verticalAlign: 'top',
                        padding: 0,
                    }}>
                        {item.data.hasOwnProperty(key) && null !== item.data[key] ?
                            <div style={{
                                transition: asTable || this.state.open ?
                                    '0.2s ease-out margin, 0.18s ease-out opacity 0.1s' :
                                    '0.2s ease-out margin, 0.18s ease-out opacity',
                                position: 'relative',
                                zIndex: asTable || this.state.open ?
                                    1 :
                                    -1,
                                opacity: asTable || this.state.open ?
                                    1 :
                                    0,
                                display: asTable || this.state.open ?
                                    'block' : 'none',
                                marginTop: asTable || this.state.open ?
                                    0 :
                                    '-55%'
                            }}>
                                <ItemCell name={key} val={item.data[key]} t={t} theme={theme} asTable={asTable}/>
                            </div> : <div style={{minHeight: asTable ? '1.2rem' : 0}}/>}
                    </td>
                ))}
            </tr>
        </React.Fragment>;
    };
}

export default cancelPromise(withAuthPermission(withBackend(storeAnalyze(AnalyzeOverview))));

export {beautifyProp};
