import React from "react";
import {ID} from '@formanta/js';
import {storeAnalyze} from "./Stores/Analyze";
import {ButtonSmall, Label, InputText, Select} from "../../component/Form";
import {storeAnalyzeDiagrams} from "./Stores/AnalyzeDiagrams";
import {AnalyzeQuerySelector} from "./AnalyzeQuerySelector";

class AnalyzeDiagramUpdateBase extends React.PureComponent {

    constructor(props) {
        super(props);
        this.id = ID();
    }

    handleNewDiagram = () => {
        if('-' === this.state.selectedQuery) {
            return;
        }
        const {diagramAdd} = this.props;
        const diagram = {
            id: ID(),
            name: this.state.inpName,
            type: this.state.selectedDiagramType,
            query: this.state.selectedQuery,
        };

        diagramAdd(diagram);
    };

    render() {
        const {
            diagram,
            diagramTypes
        } = this.props;

        return <React.Fragment>
            <div style={{padding: '0', position: 'relative'}}>
                <div>
                    <Label htmlFor={this.id + '-diagram-name'}>Diagram Name</Label>
                    <InputText id={this.id + '-diagram-name'} value={diagram.name} onChange={(e) => this.setState({inpName: e.target.value})}/>
                </div>

                <Label style={{marginTop: 12}} htmlFor={this.id + '-diagram-type'}>Diagram Type</Label>
                <Select id={this.id + '-diagram-type'}
                        value={diagram.type}
                        onChange={(e) => this.setState({selectedDiagramType: e.target.value})}
                >
                    {Object.keys(diagramTypes).map(analyzeDiagramType => (
                        <option key={analyzeDiagramType} value={analyzeDiagramType}>{analyzeDiagramType}</option>
                    ))}
                </Select>

                <Label style={{marginTop: 12}}>Query - Data Connection</Label>
                <AnalyzeQuerySelector
                    style={{margin: '0 0 12px 0', maxHeight: '10rem', minWidth: '100px', overflowY: 'auto',}}
                    activeQuery={diagram.query}
                    onSelect={(q_id) => this.setState({selectedQuery: q_id})}
                />

                <ButtonSmall onClick={this.handleNewDiagram}>update diagram</ButtonSmall>
            </div>
        </React.Fragment>;
    }
}

const AnalyzeDiagramUpdate = storeAnalyze(storeAnalyzeDiagrams(AnalyzeDiagramUpdateBase));
export {AnalyzeDiagramUpdate};
