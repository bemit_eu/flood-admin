import React from "react";
import {OptionCustom, SelectCustom} from "../../component/SelectCustom";
import {storeAnalyze} from "./Stores/Analyze";

/**
 * @props
 * activeQuery:string
 * onSelect:fn
 * @type {Function}
 */
const AnalyzeQuerySelector = storeAnalyze(p => <SelectCustom
    style={p.style}
    options={Object.keys(p.analyzeQueries)}
    option={(props) => (
        <OptionCustom
            active={p.activeQuery === props.id}
            onClick={() => p.onSelect(props.id)}
        >
            <span style={{display: 'block', margin: '2px 0'}}>{
                p.analyzeQueries[props.id].name ?
                    <span style={{fontWeight: 'bold'}}>{p.analyzeQueries[props.id].name} <small style={{opacity: 0.7}}>@{p.analyzeQueries[props.id].time}</small></span>
                    : p.analyzeQueries[props.id].time
            }</span>
            <span style={{display: 'block', margin: '2px 0', opacity: 0.7}}><small>{props.id}</small></span>
        </OptionCustom>
    )}
>
    <OptionCustom active={p.activeQuery === '-'}
                  fontSize={'1.2rem'}
                  fontWeight={'bold'}
                  onClick={() => p.onSelect('-')}
    >-</OptionCustom>
</SelectCustom>);

export {AnalyzeQuerySelector};
