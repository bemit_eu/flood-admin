import React from 'react';
import {Select} from "../../component/Form";
import QueryManagerConditionBuilder from './QueryManagerConditionBuilder';
import {withTheme} from '../../lib/theme';

class ConditionBuilderConjunctionBase extends React.PureComponent {

    operators = ['OR', 'AND'];

    selectOperator = (e) => {
        const conjunction = {...this.props.conjunction};

        conjunction.operator = e.target.value;

        const scope = [...this.props.scope];
        scope.push(this.props.currentKey);
        this.props.updateField(conjunction, scope);
    };

    render() {
        const {
            theme,
            // parent
            conjunction,// object
            selectedCondition, addField, updateField, addConjunction,
            selectedTypes,
            selectedTypeFields,
            currentKey,
        } = this.props;

        const scope = [...this.props.scope];
        scope.push(currentKey);

        return <div style={{margin: '21px 0 0 9px', overflow: 'visible', borderLeft: '1px solid ' + theme.borderLight}}>
            <Select value={conjunction.operator} onChange={this.selectOperator}
                    style={{marginRight: 6, marginBottom: -1}}
            >
                <option value={'-'}>-</option>
                {this.operators.map(operator => (
                    <option key={operator}
                            value={operator}>{operator}</option>
                ))}
            </Select>

            {'-' !== conjunction.operator ?
                <QueryManagerConditionBuilder
                    selectedCondition={selectedCondition}
                    addField={addField}
                    updateField={updateField}
                    addConjunction={addConjunction}
                    scope={scope}
                    selectedTypes={selectedTypes}
                    selectedTypeFields={selectedTypeFields}
                    conjunction={conjunction ? conjunction : {}}// object
                />
                : null}
        </div>;
    }
}

const ConditionBuilderConjunction = withTheme(ConditionBuilderConjunctionBase);

class ConditionBuilderConjunctionIterator extends React.PureComponent {

    render() {
        //
        // props to this class are passed 1:1 down to each ConditionBuilderConjunction, and a few new
        //
        let {
            conjunction,// array
            currentKey,
        } = this.props;

        const scope = [...this.props.scope];
        scope.push(currentKey);

        return <div style={{margin: '9px 0'}}>
            {conjunction.map((conjunction, i) => (
                <ConditionBuilderConjunction
                    key={i}
                    {...this.props}
                    scope={scope}
                    currentKey={i}
                    conjunction={conjunction}
                />
            ))}
        </div>;
    }
}

export {ConditionBuilderConjunctionIterator, ConditionBuilderConjunction};
