import React from "react";
import {MdSortByAlpha, MdSort, MdFunctions, MdTimelapse} from "react-icons/md";
import {withTheme} from '../../../../lib/theme';
import {SettingsSelectGroup} from "../../../../component/Settings/SelectGroup";
import {beautifyProp} from "../../../../lib/beautify";
import {ButtonInteractiveDelete} from "../../../../component/ButtonInteractiveDelete";
import {ToggleOnOff} from "../../../../asset/icon/ToggleOnOff";
import {ButtonRaw} from "../../../../component/Form";
import {createDropRight, DropDownAuto} from "../../../../lib/DropDown";
import {SettingsTextGroup} from "../../../../component/Settings/TextGroup";

const timings = {
    'year': [
        'half',
        '1',
        '2',
        '3',
        '5',
        '10',
        'individual',
    ],
    'month': [
        '1',
        '2',
        'quarter',
        'half',
        'individual',
    ],
    'day': [
        '1',
        '2',
        '5',
        '10',
        '14',
        '28',
        '90',
        'individual',
    ],
    'hour': [
        '1',
        '2',
        '3',
        '12',
        '24',
        '48',
        'individual',
    ],
    'minutes': [
        '1',
        '2',
        '5',
        '10',
        '15',
        '30',
        '90',
        'individual',
    ],
    'seconds': [
        '1',
        '5',
        '10',
        '15',
        '30',
        '90',
        'individual',
    ],
};

const GrowWidth = createDropRight('100%', 0);

class EventFieldSelectorBase extends React.PureComponent {
    state = {
        timing: false,
    };

    render() {
        const {
            theme,
            queryFields,
            formId,
            id,
            label,
            getter, updater, deleter, diagram,
            fn, timing, sort,
        } = this.props;

        return <div style={{display: 'flex', flexWrap: 'wrap', width: '100%', margin: '0 0 0 0'}}>
            {label ? <p style={{display: 'flex', width: '100%', margin: '6px 6px 6px 0', fontSize: '0.85rem'}}>{label}</p> : null}

            <div style={{display: 'flex', width: '75%'}}>
                <GrowWidth open={!getter(diagram.id, id + '.timing.active', false) || !getter(diagram.id, id + '.timing.use_event_timestamp', false)} style={{display: 'flex', whiteSpace: 'pre'}}>
                    {queryFields ? <div style={{width: '40%', paddingRight: 6}}>
                        <SettingsSelectGroup
                            formId={formId + id + '-event'}
                            id={id + '-event'}
                            label={'Event'}
                            value={getter(diagram.id, id + '.event', '-')}
                            onChange={(e) => updater(diagram.id, id + '.event', e.target.value)}
                            options={['-', ...Object.keys(queryFields)]}
                            option={(option) => '-' === option ? '-' : beautifyProp(option)}
                        />
                    </div> : null}

                    {queryFields && '-' !== getter(diagram.id, id + '.event', '-') ?
                        Object.keys(queryFields).map((event) => (Array.isArray(queryFields[event]) && queryFields[event].length ? <div key={event} style={{width: '60%', paddingRight: 6}}>
                            <SettingsSelectGroup
                                formId={formId}
                                id={id + '-field'}
                                label={'Field'}
                                value={getter(diagram.id, id + '.field', '-')}
                                onChange={(e) => updater(diagram.id, id + '.field', e.target.value)}
                                options={['-', ...queryFields[event]]}
                                option={(option) => '-' === option ? '-' : beautifyProp(option)}
                            />
                        </div> : null)) : null}
                </GrowWidth>
                <GrowWidth open={getter(diagram.id, id + '.timing.active', false) && getter(diagram.id, id + '.timing.use_event_timestamp', false)}
                           style={{
                               display: 'flex',
                               opacity: 0.7,
                               fontSize: '0.85rem',
                               whiteSpace: 'pre',
                               margin: 'auto 0 auto auto'
                           }}>
                    Timing with event active.<br/>
                    To choose individual time field, turn it off.
                </GrowWidth>
            </div>

            {queryFields ? <div style={{width: '25%', paddingRight: 6, display: 'flex'}}>
                {fn && '-' !== getter(diagram.id, id + '.event', '-') && '-' !== getter(diagram.id, id + '.field', '-') ? <div style={{paddingRight: 6}}>
                    <SettingsSelectGroup
                        formId={formId}
                        id={id + '-field-fn'}
                        label={<span style={{display: 'flex'}}><MdFunctions style={{margin: 'auto 2px'}} size={'1.125em'}/><span style={{margin: 'auto 0'}}>Fn.</span></span>}
                        value={getter(diagram.id, id + '.fn', '-')}
                        onChange={(e) => updater(diagram.id, id + '.fn', e.target.value)}
                        options={['-', 'sum', 'avg', 'count',]}
                        option={option => option}
                    /></div> : null}

                {sort && '-' !== getter(diagram.id, id + '.event', '-') && '-' !== getter(diagram.id, id + '.field', '-') ? <div style={{paddingRight: 6}}>
                    <SettingsSelectGroup
                        formId={formId}
                        id={id + '-field-sort'}
                        label={<span style={{display: 'flex'}}>
                            {getter(diagram.id, id + '.sort', '-') === 'desc' || getter(diagram.id, id + '.sort', '-') === 'asc' ?
                                <MdSort size={'1.125em'} style={{
                                    transform: 'scale(1,' + (getter(diagram.id, id + '.sort', '-') === 'asc' ? '-1' : '1') + ')',
                                    transition: 'transform 0.35s linear',
                                }}/> :
                                <MdSortByAlpha size={'1.125em'}/>}
                            <span style={{margin: 'auto 0 auto 2px'}}>Sort</span></span>}
                        value={getter(diagram.id, id + '.sort', '-')}
                        onChange={(e) => updater(diagram.id, id + '.sort', e.target.value)}
                        options={['-', 'asc', 'desc']}
                        option={option => option}
                    /></div> : null}

                {timing ? <div style={{margin: 'auto 6px auto auto',}}>
                    <ButtonRaw onClick={() => updater(diagram.id, id + '.timing.active', !getter(diagram.id, id + '.timing.active', false))}>
                        <MdTimelapse size={'1em'} color={theme.textColor} style={{verticalAlign: 'bottom'}}/><br/>
                        <ToggleOnOff active={getter(diagram.id, id + '.timing.active', false)} size={'1.5em'}/>
                    </ButtonRaw>
                </div> : null}

                {deleter ?
                    <ButtonInteractiveDelete style={{margin: 'auto 0 auto ' + (timing ? '0' : 'auto'), padding: 3}} size={'1.125em'} onClick={() => {
                        deleter(diagram.id, id);
                    }}/> : null}
            </div> : null}

            {queryFields ? <DropDownAuto open={timing && getter(diagram.id, id + '.timing.active', false)} style={{width: '100%'}}>
                <p style={{width: '100%', margin: '6px 0 3px 0', fontSize: '0.85rem', display: 'flex'}}>
                    <MdTimelapse size={'1.25em'} style={{marginRight: '4px', display: 'inline-block'}}/> Interval
                </p>
                <div style={{display: 'flex', flexWrap: 'wrap', marginLeft: 12}}>
                    <div style={{paddingRight: 6}}>
                        <SettingsSelectGroup
                            formId={formId}
                            id={id + '-timing-unit'}
                            label={<span>Unit</span>}
                            value={getter(diagram.id, id + '.timing.unit', '-')}
                            onChange={(e) => updater(diagram.id, id + '.timing.unit', e.target.value)}
                            options={['-', ...Object.keys(timings)]}
                            option={option => option}
                        />
                    </div>

                    {timings[getter(diagram.id, id + '.timing.unit', 'month')] ?
                        <div style={{paddingRight: 6}}><SettingsSelectGroup
                            formId={formId}
                            id={id + '-interval-frequency-' + getter(diagram.id, id + '.timing.unit', 'month')}
                            label={<span>Frequency</span>}
                            value={getter(diagram.id, id + '.timing.frequency', '-')}
                            onChange={(e) => updater(diagram.id, id + '.timing.frequency', e.target.value)}
                            options={['-', ...timings[getter(diagram.id, id + '.timing.unit', 'month')]]}
                            option={option => option}
                        /></div> : null}

                    {getter(diagram.id, id + '.timing.frequency', '-') === 'individual' ?
                        <SettingsTextGroup
                            formId={formId}
                            id={id + '-interval-frequency-val'}
                            label={'Value'}
                            value={getter(diagram.id, id + '.timing.frequency_val', '1')}
                            onChange={(e) => updater(diagram.id, id + '.timing.frequency_val', e.target.value, undefined, true)}
                            gutter={'0 0 0 0'}
                            labelGutter={'0 0 2px 0'}
                            inpGutter={'6px'}
                        />
                        : null}
                </div>

                <ButtonRaw onClick={() => updater(diagram.id, id + '.timing.use_event_timestamp', !getter(diagram.id, id + '.timing.use_event_timestamp', false))} style={{
                    display: 'flex',
                    marginLeft: 12,
                    color: theme.textColor
                }}>
                    <span style={{margin: 'auto 0 auto 0'}}>
                        <ToggleOnOff active={getter(diagram.id, id + '.timing.use_event_timestamp', false)} size={'1.5em'}/>
                    </span>
                    <span style={{margin: 'auto 0 auto 4px', fontSize: '0.85rem'}}>Timing on Event</span>
                </ButtonRaw>
            </DropDownAuto> : null}
        </div>;
    }
}

const EventFieldSelector = withTheme(EventFieldSelectorBase);

export {EventFieldSelector};
