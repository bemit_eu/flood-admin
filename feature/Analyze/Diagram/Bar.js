import React from 'react';
import {ResponsiveBar} from "@nivo/bar";
import {MdAdd, MdBorderTop, MdBorderRight, MdBorderBottom, MdBorderLeft} from "react-icons/md";
import {useTheme} from '../../../lib/theme';
import {
    SettingsGroup,
    SettingsSelectLine,
    SettingsToggleSection,
    SettingsToggleLine,
    SettingsTextGroup,
    SettingsSortList,
} from "../../../component/Settings";
import {nivoThemes} from "../AnalyzeDiagramFactory";
import {storeAnalyzeDiagrams} from "../Stores/AnalyzeDiagrams";
import {storeAnalyze} from "../Stores/Analyze";
import {EventFieldSelector} from "./Helper/EventFieldSelector";
import {ButtonRaw} from "../../../component/Form";
import {ButtonInteractiveDelete} from "../../../component/ButtonInteractiveDelete";

const demoData = [
    {
        "response__id": "AD",
        "hot dog": 177,
        "hot dogColor": "hsl(226, 70%, 50%)",
        "burger": 46,
        "burgerColor": "hsl(229, 70%, 50%)",
        "sandwich": 137,
        "sandwichColor": "hsl(197, 70%, 50%)",
        "kebab": 52,
        "kebabColor": "hsl(120, 70%, 50%)",
        "fries": 113,
        "friesColor": "hsl(250, 70%, 50%)",
        "donut": 63,
        "donutColor": "hsl(252, 70%, 50%)"
    },
    {
        "response__id": "AE",
        "hot dog": 6,
        "hot dogColor": "hsl(195, 70%, 50%)",
        "burger": 117,
        "burgerColor": "hsl(334, 70%, 50%)",
        "sandwich": 2,
        "sandwichColor": "hsl(204, 70%, 50%)",
        "kebab": 56,
        "kebabColor": "hsl(214, 70%, 50%)",
        "fries": 181,
        "friesColor": "hsl(355, 70%, 50%)",
        "donut": 174,
        "donutColor": "hsl(95, 70%, 50%)"
    },
    {
        "response__id": "AF",
        "hot dog": 148,
        "hot dogColor": "hsl(171, 70%, 50%)",
        "burger": 106,
        "burgerColor": "hsl(268, 70%, 50%)",
        "sandwich": 138,
        "sandwichColor": "hsl(97, 70%, 50%)",
        "kebab": 15,
        "kebabColor": "hsl(89, 70%, 50%)",
        "fries": 15,
        "friesColor": "hsl(130, 70%, 50%)",
        "donut": 37,
        "donutColor": "hsl(51, 70%, 50%)"
    },
    {
        "response__id": "AG",
        "hot dog": 149,
        "hot dogColor": "hsl(247, 70%, 50%)",
        "burger": 3,
        "burgerColor": "hsl(36, 70%, 50%)",
        "sandwich": 183,
        "sandwichColor": "hsl(124, 70%, 50%)",
        "kebab": 181,
        "kebabColor": "hsl(268, 70%, 50%)",
        "fries": 184,
        "friesColor": "hsl(183, 70%, 50%)",
        "donut": 28,
        "donutColor": "hsl(146, 70%, 50%)"
    },
    {
        "response__id": "AI",
        "hot dog": 141,
        "hot dogColor": "hsl(97, 70%, 50%)",
        "burger": 63,
        "burgerColor": "hsl(158, 70%, 50%)",
        "sandwich": 21,
        "sandwichColor": "hsl(230, 70%, 50%)",
        "kebab": 186,
        "kebabColor": "hsl(218, 70%, 50%)",
        "fries": 93,
        "friesColor": "hsl(252, 70%, 50%)",
        "donut": 28,
        "donutColor": "hsl(134, 70%, 50%)"
    },
    {
        "response__id": "AL",
        "hot dog": 98,
        "hot dogColor": "hsl(32, 70%, 50%)",
        "burger": 14,
        "burgerColor": "hsl(238, 70%, 50%)",
        "sandwich": 175,
        "sandwichColor": "hsl(227, 70%, 50%)",
        "kebab": 130,
        "kebabColor": "hsl(212, 70%, 50%)",
        "fries": 164,
        "friesColor": "hsl(18, 70%, 50%)",
        "donut": 160,
        "donutColor": "hsl(45, 70%, 50%)"
    },
    {
        "response__id": "AM",
        "hot dog": 198,
        "hot dogColor": "hsl(319, 70%, 50%)",
        "burger": 6,
        "burgerColor": "hsl(337, 70%, 50%)",
        "sandwich": 14,
        "sandwichColor": "hsl(218, 70%, 50%)",
        "kebab": 11,
        "kebabColor": "hsl(82, 70%, 50%)",
        "fries": 74,
        "friesColor": "hsl(269, 70%, 50%)",
        "donut": 122,
        "donutColor": "hsl(159, 70%, 50%)"
    }
];


class SettingsAxisBase extends React.PureComponent {
    render() {
        const {
            position,
            diagram, formId, icon,
            diagramGetSettings: getSetting, diagramUpdateSettings: updateSetting,
        } = this.props;

        const settings = 'axis.' + position + '.';
        const active = getSetting(diagram.id, settings + 'active', false);
        const legend = getSetting(diagram.id, settings + 'legend', '');

        return (<SettingsToggleSection
            formId={formId}
            id={'axis-' + position}
            icon={icon}
            label={position.charAt(0).toUpperCase() + position.slice(1)}
            onClick={() => updateSetting(diagram.id, settings + 'active', !active)}
            active={active} size={'3em'}
        >
            <SettingsTextGroup
                formId={formId}
                id={'-axis-' + position + '-legend'}
                label={'Legend'}
                value={legend}
                onChange={(e) => updateSetting(diagram.id, settings + 'legend', e.target.value, undefined, true)}
                gutter={'0 0 0 0'}
            />
        </SettingsToggleSection>);
    }
}

const SettingsAxis = storeAnalyzeDiagrams(SettingsAxisBase);

const DiagramBarBase = (props) => {
    const {
        componentContent: CompContent, componentSettings: CompSettings,
        diagram, formId,
        analyzeQueries,
        diagramGetSettings: getSetting, diagramUpdateSettings: updateSetting,
        diagramGetSeries: getSeries, diagramUpdateSeries: updateSeries,
        diagramAddSeriesList: addSeriesList, diagramDeleteFromSeriesList: deleteFromSeriesList,
        diagramAddConditionList: addConditionList, diagramDeleteFromConditionList: deleteFromConditionList,
        diagramGetCondition: getCondition, diagramUpdateCondition: updateCondition,
    } = props;
    const theme = useTheme();

    return (
        <React.Fragment>
            <CompContent>
                <ResponsiveBar
                    /*
                     * Settings existing:
                     * - index : string[id]
                     * - theme : string[nivo]
                     * - axis.top.active : bool:false
                     * - axis.top.legend : string['']
                     * - axis.right.active : bool:false
                     * - axis.right.legend : string['']
                     * - axis.bottom.active : bool:false
                     * - axis.bottom.legend : string['']
                     * - axis.left.active : bool:false
                     * - axis.left.legend : string['']
                     * - gridy : bool[true]
                     * - gridx : bool[false]
                     * - label : string['']
                     */
                    data={demoData}
                    keys={['hot dog', 'burger']}
                    indexBy={getSeries(diagram.id, 'index.field', 'id')}
                    margin={{
                        top: getSetting(diagram.id, 'axis.top.active', false) ? 51 : 30,
                        right: getSetting(diagram.id, 'axis.right.active', false) ? 125 : 115,
                        bottom: getSetting(diagram.id, 'axis.bottom.active', false) ? 51 : 30,
                        left: getSetting(diagram.id, 'axis.left.active', false) ? 54 : 33,
                    }}
                    padding={0.3}
                    colors={{scheme: getSetting(diagram.id, 'theme', 'nivo')}}
                    borderColor={{from: 'color', modifiers: [['darker', 1.6]]}}
                    axisTop={getSetting(diagram.id, 'axis.top.active', false) ? {
                        tickSize: 5,
                        tickPadding: 5,
                        tickRotation: 0,
                        legend: getSetting(diagram.id, 'axis.top.legend', ''),
                        legendPosition: 'middle',
                        legendOffset: -32
                    } : null}
                    axisRight={getSetting(diagram.id, 'axis.right.active', false) ? {
                        tickSize: 5,
                        tickPadding: 5,
                        tickRotation: 0,
                        legend: getSetting(diagram.id, 'axis.right.legend', ''),
                        legendPosition: 'middle',
                        legendOffset: 39
                    } : null}
                    axisBottom={getSetting(diagram.id, 'axis.bottom.active', false) ? {
                        tickSize: 5,
                        tickPadding: 5,
                        tickRotation: 0,
                        legend: getSetting(diagram.id, 'axis.bottom.legend', ''),
                        legendPosition: 'middle',
                        legendOffset: 32
                    } : null}
                    axisLeft={getSetting(diagram.id, 'axis.left.active', false) ? {
                        tickSize: 5,
                        tickPadding: 5,
                        tickRotation: 0,
                        legend: getSetting(diagram.id, 'axis.left.legend', ''),
                        legendPosition: 'middle',
                        legendOffset: -39
                    } : null}
                    enableGridX={getSetting(diagram.id, 'gridx', false)}
                    enableGridY={getSetting(diagram.id, 'gridy', true)}
                    enableLabel={getSetting(diagram.id, 'label', true)}
                    labelSkipWidth={12}
                    labelSkipHeight={12}
                    labelTextColor={{from: 'color', modifiers: [['darker', 1.8]]}}
                    legends={[
                        {
                            dataFrom: 'keys',
                            anchor: 'bottom-right',
                            direction: 'column',
                            justify: false,
                            translateX: 120,
                            translateY: 0,
                            itemsSpacing: 2,
                            itemWidth: 100,
                            itemHeight: 20,
                            itemDirection: 'left-to-right',
                            itemOpacity: 0.85,
                            symbolSize: 20,
                            effects: [
                                {
                                    on: 'hover',
                                    style: {
                                        itemOpacity: 1
                                    }
                                }
                            ]
                        }
                    ]}
                    animate={true}
                    motionStiffness={90}
                    motionDamping={15}
                />
            </CompContent>
            <CompSettings>
                <SettingsSelectLine
                    formId={formId}
                    id={'theme'}
                    label={'Theme'}
                    value={getSetting(diagram.id, 'theme', 'default')}
                    onChange={(e) => updateSetting(diagram.id, 'theme', e.target.value)}
                    options={Object.keys(nivoThemes)}
                    option={(option) => nivoThemes[option]}
                />

                <SettingsGroup label={'Series, Aggregation'}>
                    <p style={{display: 'flex', width: '100%', margin: '12px 0 3px 0'}}>Axis-X</p>
                    <div style={{display: 'flex', paddingLeft: 12}}>
                        <EventFieldSelector
                            formId={formId}
                            id={'index'}
                            queryFields={analyzeQueries[diagram.query].fields}
                            getter={getSeries}
                            updater={updateSeries}
                            diagram={diagram}
                            timing
                        />
                    </div>

                    <p style={{display: 'flex', width: '100%', margin: '12px 0 3px 0'}}>Axis-Y</p>
                    <p style={{display: 'flex', width: '100%', margin: '3px 0 3px 0'}}>
                        <ButtonRaw style={{display: 'flex', color: theme.linkColor}}
                                   onClick={() => addSeriesList(diagram.id, 'y')}
                        >
                            <MdAdd color={theme.linkColor} size={'1.125em'}/> add
                        </ButtonRaw></p>

                    <div style={{display: 'flex', paddingLeft: 12, flexDirection: 'column'}}>
                        {analyzeQueries[diagram.query] && analyzeQueries[diagram.query].fields && getSeries(diagram.id, 'y') && getSeries(diagram.id, 'y').length ?
                            getSeries(diagram.id, 'y').map((propy, i) => (
                                <div key={i + '' + propy.id} style={{paddingBottom: 6}}><EventFieldSelector
                                    formId={formId}
                                    id={'y.' + i}
                                    queryFields={analyzeQueries[diagram.query].fields}
                                    getter={getSeries}
                                    updater={updateSeries}
                                    deleter={deleteFromSeriesList}
                                    diagram={diagram}
                                    fn
                                /></div>))
                            : null}
                    </div>

                    <p style={{display: 'flex', width: '100%', margin: '12px 0 3px 0'}}>Grouping</p>
                    <p style={{display: 'flex', width: '100%', margin: '3px 0 3px 0'}}>
                        <ButtonRaw style={{display: 'flex', color: theme.linkColor}}
                                   onClick={() => addConditionList(diagram.id, 'group')}
                        >
                            <MdAdd color={theme.linkColor} size={'1.125em'}/> add
                        </ButtonRaw></p>

                    <div style={{display: 'flex', paddingLeft: 12, flexDirection: 'column'}}>
                        {getCondition(diagram.id, 'group') && getCondition(diagram.id, 'group').length ?
                            <SettingsSortList
                                small
                                items={getCondition(diagram.id, 'group')}
                                update={(group) => updateCondition(diagram.id, 'group', group)}
                                content={(p) => (
                                    <div style={{display: 'flex', flexGrow: 2, padding: '0 3px 2px 3px'}}>
                                        <EventFieldSelector
                                            formId={formId}
                                            id={'group.' + p.sortIndex}
                                            queryFields={analyzeQueries[diagram.query].fields}
                                            getter={getCondition}
                                            updater={updateCondition}
                                            diagram={diagram}
                                        />
                                        <ButtonInteractiveDelete style={{margin: 'auto 3px auto auto', padding: 3}}
                                                                 onClick={() => deleteFromConditionList(diagram.id, 'group.' + p.sortIndex)}/>
                                    </div>
                                )}
                            /> : null}
                    </div>

                    <p style={{display: 'flex', width: '100%', margin: '12px 0 0 0'}}>Sorting</p>
                    <p style={{display: 'flex', width: '100%', margin: '3px 0 3px 0'}}>
                        <ButtonRaw style={{display: 'flex', color: theme.linkColor}}
                                   onClick={() => addConditionList(diagram.id, 'sort')}
                        >
                            <MdAdd color={theme.linkColor} size={'1.125em'}/> add
                        </ButtonRaw></p>

                    <div style={{display: 'flex', paddingLeft: 12, flexDirection: 'column'}}>
                        {getCondition(diagram.id, 'sort') && getCondition(diagram.id, 'sort').length ?
                            <SettingsSortList
                                small
                                items={getCondition(diagram.id, 'sort')}
                                update={(sort) => updateCondition(diagram.id, 'sort', sort)}
                                content={(p) => (
                                    <div style={{display: 'flex', flexGrow: 2, padding: '0 3px 2px 3px'}}>
                                        <EventFieldSelector
                                            formId={formId}
                                            id={'sort.' + p.sortIndex}
                                            queryFields={analyzeQueries[diagram.query].fields}
                                            getter={getCondition}
                                            updater={updateCondition}
                                            diagram={diagram}
                                            sort
                                        />
                                        <ButtonInteractiveDelete
                                            style={{margin: 'auto 3px auto auto', padding: 3}}
                                            onClick={() => deleteFromConditionList(diagram.id, 'sort.' + p.sortIndex)}/>
                                    </div>
                                )}
                            /> : null}
                    </div>

                </SettingsGroup>

                <SettingsGroup label={'Labels'}>
                    <SettingsToggleLine
                        formId={formId}
                        id={'label'}
                        label={'Bar Labels'}
                        active={getSetting(diagram.id, 'label', true)}
                        onClick={() => updateSetting(diagram.id, 'label', !getSetting(diagram.id, 'label', false))}
                        size={'2em'}
                    />

                    <SettingsAxis
                        position={'top'}
                        icon={<MdBorderTop
                            size={'1.25em'}
                            style={{margin: 'auto 0'}}
                        />}
                        diagram={diagram} formId={formId}/>

                    <SettingsAxis
                        position={'right'}
                        icon={<MdBorderRight
                            size={'1.25em'}
                            style={{margin: 'auto 0'}}
                        />}
                        diagram={diagram} formId={formId}/>

                    <SettingsAxis
                        position={'bottom'}
                        icon={<MdBorderBottom
                            size={'1.25em'}
                            style={{margin: 'auto 0'}}
                        />}
                        diagram={diagram} formId={formId}/>

                    <SettingsAxis
                        position={'left'}
                        icon={<MdBorderLeft
                            size={'1.25em'}
                            style={{margin: 'auto 0'}}
                        />}
                        diagram={diagram} formId={formId}/>
                </SettingsGroup>

                <SettingsGroup label={'Grid'}>
                    <SettingsToggleLine
                        formId={formId}
                        id={'gridy'}
                        label={'Grid for Y'}
                        active={getSetting(diagram.id, 'gridy', true)}
                        onClick={() => updateSetting(diagram.id, 'gridy', !getSetting(diagram.id, 'gridy', true))}
                        size={'2em'}
                    />
                    <SettingsToggleLine
                        formId={formId}
                        id={'gridx'}
                        label={'Grid for X'}
                        active={getSetting(diagram.id, 'gridx', false)}
                        onClick={() => updateSetting(diagram.id, 'gridx', !getSetting(diagram.id, 'gridx', false))}
                        size={'2em'}
                    />
                </SettingsGroup>
            </CompSettings>
        </React.Fragment>
    );
};

const DiagramBar = storeAnalyze(storeAnalyzeDiagrams(DiagramBarBase));

/**
 * builds the fields for SQL out of the series, custom for each diagram...
 *
 * @param diagram
 */
const barFieldPrepare = (diagram) => {
    const fields = [];

    Object.keys(diagram.series).forEach((serie) => {
        if(diagram.series[serie]) {
            if(Array.isArray(diagram.series[serie])) {
                fields.push(...diagram.series[serie]);
            } else if(diagram.series[serie].field) {
                fields.push(diagram.series[serie]);
            }
        }
    });
    return fields;
};

export default DiagramBar;

export {barFieldPrepare};
