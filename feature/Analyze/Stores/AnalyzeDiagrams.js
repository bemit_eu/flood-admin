import React from 'react';
import {ID, isObject, setByDot, selectByDot, isUndefined} from "@formanta/js";
import {withContextConsumer, withTimeout} from "@formanta/react";
import {withPusher} from '../../../component/Pusher/Pusher';
import {cancelPromise} from "../../../component/cancelPromise";
import {withApiControl} from "../../../component/ApiControl";

const StoreAnalyzeDiagramsContext = React.createContext({});

class StoreAnalyzeDiagramsProviderBase extends React.PureComponent {
    timeouts = {};

    constructor(props) {
        super(props);

        this.state = {
            analyzeDiagrams: {},
            diagramAdd: this.diagramAdd,
            diagramGetSettings: this.diagramGetSettings,
            diagramUpdateSettings: this.diagramUpdateSettings,
            diagramAddSeriesList: this.diagramAddSeriesList,
            diagramDeleteFromSeriesList: this.diagramDeleteFromSeriesList,
            diagramGetSeries: this.diagramGetSeries,
            diagramUpdateSeries: this.diagramUpdateSeries,
            diagramDelete: this.diagramDelete,
            diagramAddConditionList: this.diagramAddConditionList,
            diagramDeleteFromConditionList: this.diagramDeleteFromConditionList,
            diagramGetCondition: this.diagramGetCondition,
            diagramUpdateCondition: this.diagramUpdateCondition,
            diagramTypes: {
                bar: 'Bar',
                sunburst: 'Sunburst',
                calendar: 'Calendar',
                circle: 'Circle',
                line: 'Line',
                heatmap: 'Heatmap',
                pie: 'Pie',
                radar: 'Radar',
                stream: 'Stream',
            }
        };

        if(window.localStorage.getItem('flood--analyze-diagrams')) {
            let analyzeDiagrams = JSON.parse(window.localStorage.getItem('flood--analyze-diagrams'));
            if(isObject(analyzeDiagrams)) {
                this.state.analyzeDiagrams = analyzeDiagrams;
            }
        }
    }

    descructGetObject = (stateKey, id) => {
        const stateObj = this.state[stateKey];
        if(!stateObj.hasOwnProperty(id)) {
            return;
        }
        return {...stateObj[id]};
    };

    descructGetObjectSubkey = (targetObj, subKey) => {
        if(!targetObj.hasOwnProperty(subKey)) {
            targetObj[subKey] = {};
        } else {
            targetObj[subKey] = {...targetObj[subKey]};
        }
        return targetObj;
    };

    checkCreateOrUpdate = (stateKey, id, subKey, key, value, dot = false) => {
        let targetObj = this.descructGetObject(stateKey, id);
        targetObj = this.descructGetObjectSubkey(targetObj, subKey);

        if(dot) {
            setByDot(key, targetObj[subKey], value);
            targetObj[subKey] = JSON.parse(JSON.stringify(targetObj[subKey]));
        } else {
            targetObj[subKey][key] = value;
        }

        return targetObj;
    };

    addToList = (stateKey, id, subKey, key, dot = false) => {
        // creates list if not exists and adds a new entry to it
        // todo: currently only supports array in level of key and not nested (dot key), setByDot doesn't support detection of arrays when not existing previously
        // works: `key=single` this.state[stateKey][id][subKey][key] = []
        // doesn't work: `key=multiple.key` this.state[stateKey][id][subKey]['multiple']['key'] = []

        let targetObj = this.descructGetObject(stateKey, id);
        targetObj = this.descructGetObjectSubkey(targetObj, subKey);

        /*if(dot) {
            setByDot(key, targetObj[subKey], value);
            targetObj[subKey] = JSON.parse(JSON.stringify(targetObj[subKey]));
        } else {*/
        if(!Array.isArray(targetObj[subKey][key])) {
            targetObj[subKey][key] = [];
        }
        //console.log(targetObj, subKey, key, targetObj[subKey][key]);

        targetObj[subKey][key].push({id: ID()});

        //}

        return targetObj;
    };

    deleteFromList = (stateKey, id, subKey, key) => {
        let targetObj = this.descructGetObject(stateKey, id);
        targetObj = this.descructGetObjectSubkey(targetObj, subKey);

        let tmp = key.split('.');
        let arr_index = tmp.pop();
        let arr = selectByDot(tmp.join('.'), targetObj[subKey]);
        if(Array.isArray(arr)) {
            arr.splice(arr_index, 1);
            setByDot(tmp.join('.'), targetObj[subKey], arr);
            targetObj[subKey] = JSON.parse(JSON.stringify(targetObj[subKey]));
        }

        return targetObj;
    };

    updateInternal = (stateKey, obj, localKey, cb = undefined, polling = false) => {
        const storeObj = {...this.state[stateKey]};

        storeObj[obj.id] = obj;

        this.setState({[stateKey]: storeObj}, () => {
            if(polling) {
                if(this.timeouts.hasOwnProperty(obj.id)) {
                    this.props.clearTimeout(this.timeouts[obj.id]);
                }
                this.timeouts[obj.id] = this.props.setTimeout(() => {
                    window.localStorage.setItem('flood--' + localKey,
                        JSON.stringify(this.state[stateKey]));
                }, 350);
                return;
            }

            window.localStorage.setItem('flood--' + localKey,
                JSON.stringify(this.state[stateKey]));
        });
    };

    /**
     *
     * @param {string} stateKey
     * @param {string} id
     * @param {string} stateSubKey
     * @param {boolean|string} key
     * @param {*} def
     * @param {boolean} dot
     * @return {string|*}
     */
    getInternal = (stateKey, id, stateSubKey, key = false, def = '', dot = false) => {
        const storeObj = {...this.state[stateKey]};
        if(!storeObj.hasOwnProperty(id) || !key) {
            return def;
        }
        const diagram = storeObj[id];
        if(diagram.hasOwnProperty(stateSubKey)) {
            if(false === key) {
                return diagram[stateSubKey];
            }
            if(dot) {
                const res = selectByDot(key, diagram[stateSubKey]);
                if(!isUndefined(res)) {
                    return res;
                }
            } else if(diagram[stateSubKey].hasOwnProperty(key)) {
                return diagram[stateSubKey][key];
            }
        }
        return def;
    };

    diagramGetSettings = (id, key, def = '') => {
        return this.getInternal('analyzeDiagrams', id, 'settings', key, def);
    };

    diagramUpdateSettings = (id, key, value, cb = undefined, polling = false) => {
        const diagram = this.checkCreateOrUpdate('analyzeDiagrams', id, 'settings', key, value);
        this.updateInternal('analyzeDiagrams', diagram, 'analyze-diagrams', cb, polling);
    };

    diagramAddSeriesList = (id, key, cb = undefined, polling = false) => {
        const diagram = this.addToList('analyzeDiagrams', id, 'series', key, true);
        this.updateInternal('analyzeDiagrams', diagram, 'analyze-diagrams', cb, polling);
    };

    diagramDeleteFromSeriesList = (id, key, cb = undefined, polling = false) => {
        const diagram = this.deleteFromList('analyzeDiagrams', id, 'series', key);
        this.updateInternal('analyzeDiagrams', diagram, 'analyze-diagrams', cb, polling);
    };

    diagramGetSeries = (id, key = false, def = {}) => {
        return this.getInternal('analyzeDiagrams', id, 'series', key, def, true);
    };

    diagramUpdateSeries = (id, key, value, cb = undefined, polling = false) => {
        const diagram = this.checkCreateOrUpdate('analyzeDiagrams', id, 'series', key, value, true);
        this.updateInternal('analyzeDiagrams', diagram, 'analyze-diagrams', cb, polling);
    };

    diagramAddConditionList = (id, key, cb = undefined, polling = false) => {
        const diagram = this.addToList('analyzeDiagrams', id, 'condition', key, true);
        this.updateInternal('analyzeDiagrams', diagram, 'analyze-diagrams', cb, polling);
    };

    diagramDeleteFromConditionList = (id, key, cb = undefined, polling = false) => {
        const diagram = this.deleteFromList('analyzeDiagrams', id, 'condition', key);
        this.updateInternal('analyzeDiagrams', diagram, 'analyze-diagrams', cb, polling);
    };

    diagramGetCondition = (id, key = false, def = {}) => {
        return this.getInternal('analyzeDiagrams', id, 'condition', key, def, true);
    };

    diagramUpdateCondition = (id, key, value, cb = undefined, polling = false) => {
        const diagram = this.checkCreateOrUpdate('analyzeDiagrams', id, 'condition', key, value, true);
        this.updateInternal('analyzeDiagrams', diagram, 'analyze-diagrams', cb, polling);
    };

    diagramAdd = (diagram, cb = undefined) => {
        const analyzeDiagrams = {...this.state.analyzeDiagrams};

        analyzeDiagrams[diagram.id] = diagram;

        window.localStorage.setItem('flood--analyze-diagrams', JSON.stringify(analyzeDiagrams));
        this.setState({analyzeDiagrams}, cb);
    };

    diagramDelete = (activeDiagram) => {
        const analyzeDiagrams = {...this.state.analyzeDiagrams};
        if(analyzeDiagrams.hasOwnProperty(activeDiagram)) {
            delete analyzeDiagrams[activeDiagram];
        }

        if(Object.keys(analyzeDiagrams).length) {
            window.localStorage.setItem('flood--analyze-diagrams', JSON.stringify(analyzeDiagrams));
        } else {
            window.localStorage.removeItem('flood--analyze-diagrams');
        }

        this.setState({
            analyzeDiagrams,
        });
    };

    render() {
        return (
            <StoreAnalyzeDiagramsContext.Provider value={this.state}>
                {this.props.children}
            </StoreAnalyzeDiagramsContext.Provider>
        );
    }
}

const storeAnalyzeDiagrams = withContextConsumer(StoreAnalyzeDiagramsContext.Consumer);

const StoreAnalyzeDiagramsProvider = cancelPromise(withTimeout(withApiControl(withPusher(StoreAnalyzeDiagramsProviderBase))));

export {StoreAnalyzeDiagramsProvider, storeAnalyzeDiagrams};