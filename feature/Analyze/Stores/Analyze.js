import React from 'react';
import {isUndefined, isObject} from '@formanta/js';
import {withContextConsumer} from '@formanta/react';
import {withPusher} from '../../../component/Pusher/Pusher';
import {cancelPromise} from '../../../component/cancelPromise';
import {withApiControl} from '../../../component/ApiControl';
import {ApiRequest, POST} from '../../../lib/ApiRequest';

const StoreAnalyzeContext = React.createContext({});

class StoreAnalyzeProviderBase extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            analyzeQueries: {},
            analyzeTypes: [],
            analyzeTypeFields: {},
            analyzeTypesLoaded: false,
            analyzeTypeFieldsLoaded: {},
            loadAnalyzeTypes: this.loadAnalyzeTypes,
            loadAnalyzeProps: this.loadAnalyzeProps,
            getAnalyzePropsLoaded: this.getAnalyzePropsLoaded,
            analyzeDetails: this.analyzeDetails,
            analyzeAddQuery: this.analyzeAddQuery,
            analyzeDeleteQuery: this.analyzeDeleteQuery,
        };

        if(window.localStorage.getItem('flood--analyze')) {
            let analyzeQueries = JSON.parse(window.localStorage.getItem('flood--analyze'));
            if(isObject(analyzeQueries)) {
                this.state.analyzeQueries = analyzeQueries;
            }
        }
    }

    analyzeAddQuery = (query, cb = undefined) => {
        const analyzeQueries = {...this.state.analyzeQueries};

        analyzeQueries[query.q_id] = query;

        window.localStorage.setItem('flood--analyze', JSON.stringify(analyzeQueries));
        this.setState({analyzeQueries}, cb);
    };

    analyzeDeleteQuery = (activeQuery) => {
        const analyzeQueries = {...this.state.analyzeQueries};
        if(analyzeQueries.hasOwnProperty(activeQuery)) {
            delete analyzeQueries[activeQuery];
        }

        if(Object.keys(analyzeQueries).length) {
            window.localStorage.setItem('flood--analyze', JSON.stringify(analyzeQueries));
        } else {
            window.localStorage.removeItem('flood--analyze');
        }

        this.setState({
            analyzeQueries,
        });
    };

    loadAnalyzeTypes = (forceLoad = false) => {
        if(false === this.state.analyzeTypesLoaded || forceLoad) {

            this.setState({analyzeTypesLoaded: 'progress'});

            (new ApiRequest('api', POST, 'analyze/query', {request: 'type'}))
                .debug(true)
                .exec()
                .then((res) => {
                    if(res && res.data) {
                        if(Array.isArray(res.data)) {
                            this.setState({
                                analyzeTypes: res.data,
                                analyzeTypesLoaded: true
                            });
                        }
                    } else {
                        this.setState({
                            analyzeTypesLoaded: 'error'
                        });
                    }
                })
                .catch((err) => {
                    this.props.handleApiFail(err, 'listing Analyze Event Types');
                    if(err && !err.cancelled) {
                        this.setState({
                            analyzeTypesLoaded: 'error'
                        });
                    }
                });
        }
    };

    getAnalyzePropsLoaded = (stock) => {
        if(isUndefined(this.state.analyzeTypeFieldsLoaded[stock])) {
            return false;
        }
        return this.state.analyzeTypeFieldsLoaded[stock];
    };

    updateAnalyzePropsLoaded = (type, status) => {
        const analyzeTypeFieldsLoaded = {...this.state.analyzeTypeFieldsLoaded};
        analyzeTypeFieldsLoaded[type] = status;
        this.setState({analyzeTypeFieldsLoaded});
    };

    loadAnalyzeProps = (type, forceLoad = false) => {
        if(isUndefined(this.state.analyzeTypeFields[type]) || forceLoad) {

            this.updateAnalyzePropsLoaded(type, 'progress');

            (new ApiRequest('api', POST, 'analyze/query', {
                'request': 'fields',
                'type': type
            }))
                .debug(true)
                .exec()
                .then((res) => {
                    if(res && res.data && Array.isArray(res.data)) {

                        const analyzeTypeFields = {...this.state.analyzeTypeFields};
                        analyzeTypeFields[type] = res.data.sort();

                        this.setState({
                            analyzeTypeFields,
                        }, () => this.updateAnalyzePropsLoaded(type, true));
                    } else {
                        this.updateAnalyzePropsLoaded(type, 'error');
                    }
                })
                .catch((err) => {
                    this.props.handleApiFail(err, 'listing AnalyzeProps');
                    if(err && !err.cancelled) {
                        this.updateAnalyzePropsLoaded(type, 'error');
                    }
                });
        }
    };

    analyzeDetails = (type, filter = [], condition = {}, between = {}, limit = {}, group = [], sort = []) => {
        return (new ApiRequest('api', POST, 'analyze/query', {
                request: 'details',
                type,
                filter,
                condition,
                between,
                limit,
                group,
                sort,
            }
        ))
            .debug(true)
            .header({})
            .exec()
            .then((res) => {
                if(res && res.data) {
                    if(res.data.error) {
                        return Promise.reject(res);
                    }
                    return res.data;
                } else {
                    return Promise.reject(res);
                }
            })
            .catch((err) => {
                this.props.handleApiFail(err, 'analyzeDetails');
                return Promise.reject(err);
            });
    };

    render() {
        return (
            <StoreAnalyzeContext.Provider value={this.state}>
                {this.props.children}
            </StoreAnalyzeContext.Provider>
        );
    }
}

const storeAnalyze = withContextConsumer(StoreAnalyzeContext.Consumer);

const StoreAnalyzeProvider = cancelPromise(withApiControl(withPusher(StoreAnalyzeProviderBase)));

export {StoreAnalyzeProvider, storeAnalyze};