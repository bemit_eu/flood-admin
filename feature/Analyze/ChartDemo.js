import {ResponsiveBubble} from "@nivo/circle-packing";
import {ResponsivePie} from "@nivo/pie";
import {ResponsiveRadar} from "@nivo/radar";
import {ResponsiveStream} from "@nivo/stream";
import {ResponsiveSunburst} from "@nivo/sunburst";
import React from "react";

export default (p) => (
    <React.Fragment>
        <div style={{width: '50%', paddingTop: '50%', position: 'relative'}}>
            <div style={{position: 'absolute', top: 0, left: 0, bottom: 0, right: 0}}>
                {/* example data could be: group responses by utm_campaign and utm_source and last count the sites that where visited, so visualizes for campaign is more successfull and inside which source with which page performs better */}
                <ResponsiveBubble
                    root={{
                        "name": "nivo",
                        "color": "hsl(138, 70%, 50%)",
                        "children": [
                            {
                                "name": "viz",
                                "color": "hsl(68, 70%, 50%)",
                                "children": [
                                    {
                                        "name": "stack",
                                        "color": "hsl(94, 70%, 50%)",
                                        "children": [
                                            {
                                                "name": "chart",
                                                "color": "hsl(213, 70%, 50%)",
                                                "loc": 142899
                                            },
                                            {
                                                "name": "xAxis",
                                                "color": "hsl(192, 70%, 50%)",
                                                "loc": 27975
                                            },
                                            {
                                                "name": "yAxis",
                                                "color": "hsl(204, 70%, 50%)",
                                                "loc": 3994
                                            },
                                            {
                                                "name": "layers",
                                                "color": "hsl(328, 70%, 50%)",
                                                "loc": 84479
                                            }
                                        ]
                                    },
                                    {
                                        "name": "pie",
                                        "color": "hsl(320, 70%, 50%)",
                                        "children": [
                                            {
                                                "name": "chart",
                                                "color": "hsl(38, 70%, 50%)",
                                                "children": [
                                                    {
                                                        "name": "pie",
                                                        "color": "hsl(115, 70%, 50%)",
                                                        "children": [
                                                            {
                                                                "name": "outline",
                                                                "color": "hsl(354, 70%, 50%)",
                                                                "loc": 61932
                                                            },
                                                            {
                                                                "name": "slices",
                                                                "color": "hsl(68, 70%, 50%)",
                                                                "loc": 81322
                                                            },
                                                            {
                                                                "name": "bbox",
                                                                "color": "hsl(214, 70%, 50%)",
                                                                "loc": 25923
                                                            }
                                                        ]
                                                    },
                                                    {
                                                        "name": "donut",
                                                        "color": "hsl(294, 70%, 50%)",
                                                        "loc": 110812
                                                    },
                                                    {
                                                        "name": "gauge",
                                                        "color": "hsl(268, 70%, 50%)",
                                                        "loc": 149376
                                                    }
                                                ]
                                            },
                                            {
                                                "name": "legends",
                                                "color": "hsl(245, 70%, 50%)",
                                                "loc": 157111
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                "name": "colors",
                                "color": "hsl(147, 70%, 50%)",
                                "children": [
                                    {
                                        "name": "rgb",
                                        "color": "hsl(250, 70%, 50%)",
                                        "loc": 22002
                                    },
                                    {
                                        "name": "hsl",
                                        "color": "hsl(351, 70%, 50%)",
                                        "loc": 68427
                                    }
                                ]
                            },
                            {
                                "name": "utils",
                                "color": "hsl(100, 70%, 50%)",
                                "children": [
                                    {
                                        "name": "randomize",
                                        "color": "hsl(105, 70%, 50%)",
                                        "loc": 172394
                                    },
                                    {
                                        "name": "resetClock",
                                        "color": "hsl(173, 70%, 50%)",
                                        "loc": 164147
                                    },
                                    {
                                        "name": "noop",
                                        "color": "hsl(93, 70%, 50%)",
                                        "loc": 194021
                                    },
                                    {
                                        "name": "tick",
                                        "color": "hsl(187, 70%, 50%)",
                                        "loc": 167319
                                    },
                                    {
                                        "name": "forceGC",
                                        "color": "hsl(216, 70%, 50%)",
                                        "loc": 135604
                                    },
                                    {
                                        "name": "stackTrace",
                                        "color": "hsl(89, 70%, 50%)",
                                        "loc": 49353
                                    },
                                    {
                                        "name": "dbg",
                                        "color": "hsl(234, 70%, 50%)",
                                        "loc": 20525
                                    }
                                ]
                            },
                            {
                                "name": "generators",
                                "color": "hsl(279, 70%, 50%)",
                                "children": [
                                    {
                                        "name": "address",
                                        "color": "hsl(240, 70%, 50%)",
                                        "loc": 99952
                                    },
                                    {
                                        "name": "city",
                                        "color": "hsl(249, 70%, 50%)",
                                        "loc": 110695
                                    },
                                    {
                                        "name": "animal",
                                        "color": "hsl(76, 70%, 50%)",
                                        "loc": 40277
                                    },
                                    {
                                        "name": "movie",
                                        "color": "hsl(256, 70%, 50%)",
                                        "loc": 105550
                                    },
                                    {
                                        "name": "user",
                                        "color": "hsl(40, 70%, 50%)",
                                        "loc": 137953
                                    }
                                ]
                            },
                            {
                                "name": "set",
                                "color": "hsl(196, 70%, 50%)",
                                "children": [
                                    {
                                        "name": "clone",
                                        "color": "hsl(305, 70%, 50%)",
                                        "loc": 50112
                                    },
                                    {
                                        "name": "intersect",
                                        "color": "hsl(11, 70%, 50%)",
                                        "loc": 188302
                                    },
                                    {
                                        "name": "merge",
                                        "color": "hsl(110, 70%, 50%)",
                                        "loc": 55467
                                    },
                                    {
                                        "name": "reverse",
                                        "color": "hsl(260, 70%, 50%)",
                                        "loc": 80572
                                    },
                                    {
                                        "name": "toArray",
                                        "color": "hsl(225, 70%, 50%)",
                                        "loc": 191042
                                    },
                                    {
                                        "name": "toObject",
                                        "color": "hsl(321, 70%, 50%)",
                                        "loc": 3622
                                    },
                                    {
                                        "name": "fromCSV",
                                        "color": "hsl(179, 70%, 50%)",
                                        "loc": 113
                                    },
                                    {
                                        "name": "slice",
                                        "color": "hsl(204, 70%, 50%)",
                                        "loc": 82480
                                    },
                                    {
                                        "name": "append",
                                        "color": "hsl(155, 70%, 50%)",
                                        "loc": 92572
                                    },
                                    {
                                        "name": "prepend",
                                        "color": "hsl(299, 70%, 50%)",
                                        "loc": 40567
                                    },
                                    {
                                        "name": "shuffle",
                                        "color": "hsl(137, 70%, 50%)",
                                        "loc": 178083
                                    },
                                    {
                                        "name": "pick",
                                        "color": "hsl(109, 70%, 50%)",
                                        "loc": 95782
                                    },
                                    {
                                        "name": "plouc",
                                        "color": "hsl(328, 70%, 50%)",
                                        "loc": 190062
                                    }
                                ]
                            },
                            {
                                "name": "text",
                                "color": "hsl(151, 70%, 50%)",
                                "children": [
                                    {
                                        "name": "trim",
                                        "color": "hsl(7, 70%, 50%)",
                                        "loc": 71662
                                    },
                                    {
                                        "name": "slugify",
                                        "color": "hsl(18, 70%, 50%)",
                                        "loc": 196254
                                    },
                                    {
                                        "name": "snakeCase",
                                        "color": "hsl(254, 70%, 50%)",
                                        "loc": 77093
                                    },
                                    {
                                        "name": "camelCase",
                                        "color": "hsl(292, 70%, 50%)",
                                        "loc": 143451
                                    },
                                    {
                                        "name": "repeat",
                                        "color": "hsl(354, 70%, 50%)",
                                        "loc": 49372
                                    },
                                    {
                                        "name": "padLeft",
                                        "color": "hsl(40, 70%, 50%)",
                                        "loc": 71478
                                    },
                                    {
                                        "name": "padRight",
                                        "color": "hsl(89, 70%, 50%)",
                                        "loc": 167288
                                    },
                                    {
                                        "name": "sanitize",
                                        "color": "hsl(60, 70%, 50%)",
                                        "loc": 165827
                                    },
                                    {
                                        "name": "ploucify",
                                        "color": "hsl(219, 70%, 50%)",
                                        "loc": 197314
                                    }
                                ]
                            },
                            {
                                "name": "misc",
                                "color": "hsl(102, 70%, 50%)",
                                "children": [
                                    {
                                        "name": "whatever",
                                        "color": "hsl(353, 70%, 50%)",
                                        "children": [
                                            {
                                                "name": "hey",
                                                "color": "hsl(273, 70%, 50%)",
                                                "loc": 76202
                                            },
                                            {
                                                "name": "WTF",
                                                "color": "hsl(248, 70%, 50%)",
                                                "loc": 90647
                                            },
                                            {
                                                "name": "lol",
                                                "color": "hsl(278, 70%, 50%)",
                                                "loc": 69546
                                            },
                                            {
                                                "name": "IMHO",
                                                "color": "hsl(137, 70%, 50%)",
                                                "loc": 186926
                                            }
                                        ]
                                    },
                                    {
                                        "name": "other",
                                        "color": "hsl(319, 70%, 50%)",
                                        "loc": 174339
                                    },
                                    {
                                        "name": "crap",
                                        "color": "hsl(85, 70%, 50%)",
                                        "children": [
                                            {
                                                "name": "crapA",
                                                "color": "hsl(3, 70%, 50%)",
                                                "loc": 63227
                                            },
                                            {
                                                "name": "crapB",
                                                "color": "hsl(179, 70%, 50%)",
                                                "children": [
                                                    {
                                                        "name": "crapB1",
                                                        "color": "hsl(3, 70%, 50%)",
                                                        "loc": 39484
                                                    },
                                                    {
                                                        "name": "crapB2",
                                                        "color": "hsl(26, 70%, 50%)",
                                                        "loc": 116874
                                                    },
                                                    {
                                                        "name": "crapB3",
                                                        "color": "hsl(154, 70%, 50%)",
                                                        "loc": 125885
                                                    },
                                                    {
                                                        "name": "crapB4",
                                                        "color": "hsl(34, 70%, 50%)",
                                                        "loc": 177692
                                                    }
                                                ]
                                            },
                                            {
                                                "name": "crapC",
                                                "color": "hsl(23, 70%, 50%)",
                                                "children": [
                                                    {
                                                        "name": "crapC1",
                                                        "color": "hsl(358, 70%, 50%)",
                                                        "loc": 142530
                                                    },
                                                    {
                                                        "name": "crapC2",
                                                        "color": "hsl(7, 70%, 50%)",
                                                        "loc": 168701
                                                    },
                                                    {
                                                        "name": "crapC3",
                                                        "color": "hsl(299, 70%, 50%)",
                                                        "loc": 70802
                                                    },
                                                    {
                                                        "name": "crapC4",
                                                        "color": "hsl(218, 70%, 50%)",
                                                        "loc": 105485
                                                    },
                                                    {
                                                        "name": "crapC5",
                                                        "color": "hsl(349, 70%, 50%)",
                                                        "loc": 121521
                                                    },
                                                    {
                                                        "name": "crapC6",
                                                        "color": "hsl(18, 70%, 50%)",
                                                        "loc": 161437
                                                    },
                                                    {
                                                        "name": "crapC7",
                                                        "color": "hsl(303, 70%, 50%)",
                                                        "loc": 863
                                                    },
                                                    {
                                                        "name": "crapC8",
                                                        "color": "hsl(36, 70%, 50%)",
                                                        "loc": 48467
                                                    },
                                                    {
                                                        "name": "crapC9",
                                                        "color": "hsl(19, 70%, 50%)",
                                                        "loc": 133424
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }}
                    margin={{top: 20, right: 20, bottom: 20, left: 20}}
                    identity="name"
                    value="loc"
                    colors={{scheme: 'nivo'}}
                    padding={6}
                    labelTextColor={{from: 'color', modifiers: [['darker', 0.8]]}}
                    borderWidth={2}
                    borderColor={{from: 'color'}}
                    defs={[
                        {
                            id: 'lines',
                            type: 'patternLines',
                            background: 'none',
                            color: 'inherit',
                            rotation: -45,
                            lineWidth: 5,
                            spacing: 8
                        }
                    ]}
                    fill={[{match: {depth: 1}, id: 'lines'}]}
                    animate={true}
                    motionStiffness={90}
                    motionDamping={12}
                />
            </div>
        </div>
        <div style={{width: '50%', paddingTop: '50%', position: 'relative'}}>
            <div style={{position: 'absolute', top: 0, left: 0, bottom: 0, right: 0}}>
                <ResponsivePie
                    data={[
                        {
                            "id": "css",
                            "label": "css",
                            "value": 137,
                            "color": "hsl(305, 70%, 50%)"
                        },
                        {
                            "id": "sass",
                            "label": "sass",
                            "value": 23,
                            "color": "hsl(22, 70%, 50%)"
                        },
                        {
                            "id": "lisp",
                            "label": "lisp",
                            "value": 385,
                            "color": "hsl(227, 70%, 50%)"
                        },
                        {
                            "id": "java",
                            "label": "java",
                            "value": 312,
                            "color": "hsl(85, 70%, 50%)"
                        },
                        {
                            "id": "javascript",
                            "label": "javascript",
                            "value": 144,
                            "color": "hsl(213, 70%, 50%)"
                        }
                    ]}
                    margin={{top: 40, right: 80, bottom: 80, left: 80}}
                    innerRadius={0.5}
                    padAngle={0.7}
                    cornerRadius={3}
                    colors={{scheme: 'nivo'}}
                    borderWidth={1}
                    borderColor={{from: 'color', modifiers: [['darker', 0.2]]}}
                    radialLabelsSkipAngle={10}
                    radialLabelsTextXOffset={6}
                    radialLabelsTextColor="#333333"
                    radialLabelsLinkOffset={0}
                    radialLabelsLinkDiagonalLength={16}
                    radialLabelsLinkHorizontalLength={24}
                    radialLabelsLinkStrokeWidth={1}
                    radialLabelsLinkColor={{from: 'color'}}
                    slicesLabelsSkipAngle={10}
                    slicesLabelsTextColor="#333333"
                    animate={true}
                    motionStiffness={90}
                    motionDamping={15}
                    defs={[
                        {
                            id: 'dots',
                            type: 'patternDots',
                            background: 'inherit',
                            color: 'rgba(255, 255, 255, 0.3)',
                            size: 4,
                            padding: 1,
                            stagger: true
                        },
                        {
                            id: 'lines',
                            type: 'patternLines',
                            background: 'inherit',
                            color: 'rgba(255, 255, 255, 0.3)',
                            rotation: -45,
                            lineWidth: 6,
                            spacing: 10
                        }
                    ]}
                    fill={[
                        {
                            match: {
                                id: 'ruby'
                            },
                            id: 'dots'
                        },
                        {
                            match: {
                                id: 'c'
                            },
                            id: 'dots'
                        },
                        {
                            match: {
                                id: 'go'
                            },
                            id: 'dots'
                        },
                        {
                            match: {
                                id: 'python'
                            },
                            id: 'dots'
                        },
                        {
                            match: {
                                id: 'scala'
                            },
                            id: 'lines'
                        },
                        {
                            match: {
                                id: 'lisp'
                            },
                            id: 'lines'
                        },
                        {
                            match: {
                                id: 'elixir'
                            },
                            id: 'lines'
                        },
                        {
                            match: {
                                id: 'javascript'
                            },
                            id: 'lines'
                        }
                    ]}
                    legends={[
                        {
                            anchor: 'bottom',
                            direction: 'row',
                            translateY: 56,
                            itemWidth: 100,
                            itemHeight: 18,
                            itemTextColor: '#999999',
                            symbolSize: 18,
                            symbolShape: 'circle',
                            effects: [
                                {
                                    on: 'hover',
                                    style: {
                                        itemTextColor: '#000000'
                                    }
                                }
                            ]
                        }
                    ]}
                />
            </div>
        </div>
        <div style={{width: '50%', paddingTop: '50%', position: 'relative'}}>
            <div style={{position: 'absolute', top: 0, left: 0, bottom: 0, right: 0}}>
                <ResponsiveRadar
                    data={[
                        {
                            "taste": "fruity",
                            "chardonay": 71,
                            "carmenere": 103,
                            "syrah": 83
                        },
                        {
                            "taste": "bitter",
                            "chardonay": 82,
                            "carmenere": 40,
                            "syrah": 106
                        },
                        {
                            "taste": "heavy",
                            "chardonay": 76,
                            "carmenere": 24,
                            "syrah": 33
                        },
                        {
                            "taste": "strong",
                            "chardonay": 30,
                            "carmenere": 88,
                            "syrah": 98
                        },
                        {
                            "taste": "sunny",
                            "chardonay": 103,
                            "carmenere": 113,
                            "syrah": 67
                        }
                    ]}
                    keys={['chardonay', 'carmenere', 'syrah']}
                    indexBy="taste"
                    maxValue="auto"
                    margin={{top: 70, right: 80, bottom: 40, left: 80}}
                    curve="linearClosed"
                    borderWidth={2}
                    borderColor={{from: 'color'}}
                    gridLevels={5}
                    gridShape="circular"
                    gridLabelOffset={36}
                    enableDots={true}
                    dotSize={10}
                    dotColor={{theme: 'background'}}
                    dotBorderWidth={2}
                    dotBorderColor={{from: 'color'}}
                    enableDotLabel={true}
                    dotLabel="value"
                    dotLabelYOffset={-12}
                    colors={{scheme: 'nivo'}}
                    fillOpacity={0.25}
                    blendMode="multiply"
                    animate={true}
                    motionStiffness={90}
                    motionDamping={15}
                    isInteractive={true}
                    legends={[
                        {
                            anchor: 'top-left',
                            direction: 'column',
                            translateX: -50,
                            translateY: -40,
                            itemWidth: 80,
                            itemHeight: 20,
                            itemTextColor: '#999999',
                            symbolSize: 12,
                            symbolShape: 'circle',
                            effects: [
                                {
                                    on: 'hover',
                                    style: {
                                        itemTextColor: '#000000'
                                    }
                                }
                            ]
                        }
                    ]}
                />
            </div>
        </div>
        <div style={{width: '50%', paddingTop: '50%', position: 'relative'}}>
            <div style={{position: 'absolute', top: 0, left: 0, bottom: 0, right: 0}}>
                <ResponsiveStream
                    data={[
                        {
                            "Raoul": 110,
                            "Josiane": 85,
                            "Marcel": 47,
                            "René": 12,
                            "Paul": 199,
                            "Jacques": 132
                        },
                        {
                            "Raoul": 158,
                            "Josiane": 170,
                            "Marcel": 168,
                            "René": 67,
                            "Paul": 55,
                            "Jacques": 104
                        },
                        {
                            "Raoul": 164,
                            "Josiane": 84,
                            "Marcel": 34,
                            "René": 162,
                            "Paul": 184,
                            "Jacques": 118
                        },
                        {
                            "Raoul": 174,
                            "Josiane": 37,
                            "Marcel": 137,
                            "René": 65,
                            "Paul": 174,
                            "Jacques": 133
                        },
                        {
                            "Raoul": 153,
                            "Josiane": 37,
                            "Marcel": 162,
                            "René": 170,
                            "Paul": 140,
                            "Jacques": 181
                        },
                        {
                            "Raoul": 158,
                            "Josiane": 58,
                            "Marcel": 172,
                            "René": 144,
                            "Paul": 31,
                            "Jacques": 152
                        },
                        {
                            "Raoul": 188,
                            "Josiane": 54,
                            "Marcel": 117,
                            "René": 81,
                            "Paul": 73,
                            "Jacques": 198
                        },
                        {
                            "Raoul": 70,
                            "Josiane": 191,
                            "Marcel": 30,
                            "René": 180,
                            "Paul": 166,
                            "Jacques": 122
                        },
                        {
                            "Raoul": 124,
                            "Josiane": 56,
                            "Marcel": 195,
                            "René": 33,
                            "Paul": 112,
                            "Jacques": 76
                        }
                    ]}
                    keys={['Raoul', 'Josiane', 'Marcel', 'René', 'Paul', 'Jacques']}
                    margin={{top: 50, right: 110, bottom: 50, left: 60}}
                    axisTop={null}
                    axisRight={null}
                    axisBottom={{
                        orient: 'bottom',
                        tickSize: 5,
                        tickPadding: 5,
                        tickRotation: 0,
                        legend: '',
                        legendOffset: 36
                    }}
                    axisLeft={{orient: 'left', tickSize: 5, tickPadding: 5, tickRotation: 0, legend: '', legendOffset: -40}}
                    offsetType="silhouette"
                    colors={{scheme: 'nivo'}}
                    fillOpacity={0.85}
                    borderColor={{theme: 'background'}}
                    defs={[
                        {
                            id: 'dots',
                            type: 'patternDots',
                            background: 'inherit',
                            color: '#2c998f',
                            size: 4,
                            padding: 2,
                            stagger: true
                        },
                        {
                            id: 'squares',
                            type: 'patternSquares',
                            background: 'inherit',
                            color: '#e4c912',
                            size: 6,
                            padding: 2,
                            stagger: true
                        }
                    ]}
                    fill={[
                        {
                            match: {
                                id: 'Paul'
                            },
                            id: 'dots'
                        },
                        {
                            match: {
                                id: 'Marcel'
                            },
                            id: 'squares'
                        }
                    ]}
                    dotSize={8}
                    dotColor={{from: 'color'}}
                    dotBorderWidth={2}
                    dotBorderColor={{from: 'color', modifiers: [['darker', 0.7]]}}
                    animate={true}
                    motionStiffness={90}
                    motionDamping={15}
                    legends={[
                        {
                            anchor: 'bottom-right',
                            direction: 'column',
                            translateX: 100,
                            itemWidth: 80,
                            itemHeight: 20,
                            itemTextColor: '#999999',
                            symbolSize: 12,
                            symbolShape: 'circle',
                            effects: [
                                {
                                    on: 'hover',
                                    style: {
                                        itemTextColor: '#000000'
                                    }
                                }
                            ]
                        }
                    ]}
                />
            </div>
        </div>
        <div style={{width: '50%', paddingTop: '50%', position: 'relative'}}>
            <div style={{position: 'absolute', top: 0, left: 0, bottom: 0, right: 0}}>
                <ResponsiveSunburst
                    data={{
                        "name": "nivo",
                        "color": "hsl(231, 70%, 50%)",
                        "children": [
                            {
                                "name": "viz",
                                "color": "hsl(140, 70%, 50%)",
                                "children": [
                                    {
                                        "name": "stack",
                                        "color": "hsl(99, 70%, 50%)",
                                        "children": [
                                            {
                                                "name": "chart",
                                                "color": "hsl(174, 70%, 50%)",
                                                "loc": 148112
                                            },
                                            {
                                                "name": "xAxis",
                                                "color": "hsl(275, 70%, 50%)",
                                                "loc": 172089
                                            },
                                            {
                                                "name": "yAxis",
                                                "color": "hsl(197, 70%, 50%)",
                                                "loc": 55151
                                            },
                                            {
                                                "name": "layers",
                                                "color": "hsl(160, 70%, 50%)",
                                                "loc": 190903
                                            }
                                        ]
                                    },
                                    {
                                        "name": "pie",
                                        "color": "hsl(253, 70%, 50%)",
                                        "children": [
                                            {
                                                "name": "chart",
                                                "color": "hsl(88, 70%, 50%)",
                                                "children": [
                                                    {
                                                        "name": "pie",
                                                        "color": "hsl(204, 70%, 50%)",
                                                        "children": [
                                                            {
                                                                "name": "outline",
                                                                "color": "hsl(106, 70%, 50%)",
                                                                "loc": 27349
                                                            },
                                                            {
                                                                "name": "slices",
                                                                "color": "hsl(48, 70%, 50%)",
                                                                "loc": 50914
                                                            },
                                                            {
                                                                "name": "bbox",
                                                                "color": "hsl(77, 70%, 50%)",
                                                                "loc": 99014
                                                            }
                                                        ]
                                                    },
                                                    {
                                                        "name": "donut",
                                                        "color": "hsl(350, 70%, 50%)",
                                                        "loc": 196636
                                                    },
                                                    {
                                                        "name": "gauge",
                                                        "color": "hsl(352, 70%, 50%)",
                                                        "loc": 69969
                                                    }
                                                ]
                                            },
                                            {
                                                "name": "legends",
                                                "color": "hsl(104, 70%, 50%)",
                                                "loc": 42703
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                "name": "colors",
                                "color": "hsl(295, 70%, 50%)",
                                "children": [
                                    {
                                        "name": "rgb",
                                        "color": "hsl(193, 70%, 50%)",
                                        "loc": 126188
                                    },
                                    {
                                        "name": "hsl",
                                        "color": "hsl(36, 70%, 50%)",
                                        "loc": 99320
                                    }
                                ]
                            },
                            {
                                "name": "utils",
                                "color": "hsl(37, 70%, 50%)",
                                "children": [
                                    {
                                        "name": "randomize",
                                        "color": "hsl(151, 70%, 50%)",
                                        "loc": 84266
                                    },
                                    {
                                        "name": "resetClock",
                                        "color": "hsl(37, 70%, 50%)",
                                        "loc": 18731
                                    },
                                    {
                                        "name": "noop",
                                        "color": "hsl(109, 70%, 50%)",
                                        "loc": 136153
                                    },
                                    {
                                        "name": "tick",
                                        "color": "hsl(92, 70%, 50%)",
                                        "loc": 182012
                                    },
                                    {
                                        "name": "forceGC",
                                        "color": "hsl(141, 70%, 50%)",
                                        "loc": 3277
                                    },
                                    {
                                        "name": "stackTrace",
                                        "color": "hsl(194, 70%, 50%)",
                                        "loc": 2355
                                    },
                                    {
                                        "name": "dbg",
                                        "color": "hsl(72, 70%, 50%)",
                                        "loc": 153190
                                    }
                                ]
                            },
                            {
                                "name": "generators",
                                "color": "hsl(39, 70%, 50%)",
                                "children": [
                                    {
                                        "name": "address",
                                        "color": "hsl(296, 70%, 50%)",
                                        "loc": 181569
                                    },
                                    {
                                        "name": "city",
                                        "color": "hsl(207, 70%, 50%)",
                                        "loc": 181343
                                    },
                                    {
                                        "name": "animal",
                                        "color": "hsl(138, 70%, 50%)",
                                        "loc": 163593
                                    },
                                    {
                                        "name": "movie",
                                        "color": "hsl(123, 70%, 50%)",
                                        "loc": 165776
                                    },
                                    {
                                        "name": "user",
                                        "color": "hsl(42, 70%, 50%)",
                                        "loc": 52541
                                    }
                                ]
                            },
                            {
                                "name": "set",
                                "color": "hsl(321, 70%, 50%)",
                                "children": [
                                    {
                                        "name": "clone",
                                        "color": "hsl(2, 70%, 50%)",
                                        "loc": 78498
                                    },
                                    {
                                        "name": "intersect",
                                        "color": "hsl(334, 70%, 50%)",
                                        "loc": 19023
                                    },
                                    {
                                        "name": "merge",
                                        "color": "hsl(252, 70%, 50%)",
                                        "loc": 61395
                                    },
                                    {
                                        "name": "reverse",
                                        "color": "hsl(73, 70%, 50%)",
                                        "loc": 134344
                                    },
                                    {
                                        "name": "toArray",
                                        "color": "hsl(144, 70%, 50%)",
                                        "loc": 123566
                                    },
                                    {
                                        "name": "toObject",
                                        "color": "hsl(0, 70%, 50%)",
                                        "loc": 181682
                                    },
                                    {
                                        "name": "fromCSV",
                                        "color": "hsl(158, 70%, 50%)",
                                        "loc": 40123
                                    },
                                    {
                                        "name": "slice",
                                        "color": "hsl(132, 70%, 50%)",
                                        "loc": 85443
                                    },
                                    {
                                        "name": "append",
                                        "color": "hsl(125, 70%, 50%)",
                                        "loc": 76501
                                    },
                                    {
                                        "name": "prepend",
                                        "color": "hsl(195, 70%, 50%)",
                                        "loc": 7497
                                    },
                                    {
                                        "name": "shuffle",
                                        "color": "hsl(213, 70%, 50%)",
                                        "loc": 114368
                                    },
                                    {
                                        "name": "pick",
                                        "color": "hsl(94, 70%, 50%)",
                                        "loc": 68870
                                    },
                                    {
                                        "name": "plouc",
                                        "color": "hsl(276, 70%, 50%)",
                                        "loc": 171045
                                    }
                                ]
                            },
                            {
                                "name": "text",
                                "color": "hsl(88, 70%, 50%)",
                                "children": [
                                    {
                                        "name": "trim",
                                        "color": "hsl(211, 70%, 50%)",
                                        "loc": 48640
                                    },
                                    {
                                        "name": "slugify",
                                        "color": "hsl(332, 70%, 50%)",
                                        "loc": 106103
                                    },
                                    {
                                        "name": "snakeCase",
                                        "color": "hsl(67, 70%, 50%)",
                                        "loc": 175542
                                    },
                                    {
                                        "name": "camelCase",
                                        "color": "hsl(290, 70%, 50%)",
                                        "loc": 129094
                                    },
                                    {
                                        "name": "repeat",
                                        "color": "hsl(137, 70%, 50%)",
                                        "loc": 43377
                                    },
                                    {
                                        "name": "padLeft",
                                        "color": "hsl(240, 70%, 50%)",
                                        "loc": 55616
                                    },
                                    {
                                        "name": "padRight",
                                        "color": "hsl(52, 70%, 50%)",
                                        "loc": 156828
                                    },
                                    {
                                        "name": "sanitize",
                                        "color": "hsl(275, 70%, 50%)",
                                        "loc": 180294
                                    },
                                    {
                                        "name": "ploucify",
                                        "color": "hsl(119, 70%, 50%)",
                                        "loc": 163263
                                    }
                                ]
                            },
                            {
                                "name": "misc",
                                "color": "hsl(293, 70%, 50%)",
                                "children": [
                                    {
                                        "name": "whatever",
                                        "color": "hsl(284, 70%, 50%)",
                                        "children": [
                                            {
                                                "name": "hey",
                                                "color": "hsl(173, 70%, 50%)",
                                                "loc": 66659
                                            },
                                            {
                                                "name": "WTF",
                                                "color": "hsl(243, 70%, 50%)",
                                                "loc": 45508
                                            },
                                            {
                                                "name": "lol",
                                                "color": "hsl(314, 70%, 50%)",
                                                "loc": 198554
                                            },
                                            {
                                                "name": "IMHO",
                                                "color": "hsl(7, 70%, 50%)",
                                                "loc": 32449
                                            }
                                        ]
                                    },
                                    {
                                        "name": "other",
                                        "color": "hsl(188, 70%, 50%)",
                                        "loc": 4214
                                    },
                                    {
                                        "name": "crap",
                                        "color": "hsl(196, 70%, 50%)",
                                        "children": [
                                            {
                                                "name": "crapA",
                                                "color": "hsl(66, 70%, 50%)",
                                                "loc": 121543
                                            },
                                            {
                                                "name": "crapB",
                                                "color": "hsl(70, 70%, 50%)",
                                                "children": [
                                                    {
                                                        "name": "crapB1",
                                                        "color": "hsl(344, 70%, 50%)",
                                                        "loc": 168476
                                                    },
                                                    {
                                                        "name": "crapB2",
                                                        "color": "hsl(146, 70%, 50%)",
                                                        "loc": 6500
                                                    },
                                                    {
                                                        "name": "crapB3",
                                                        "color": "hsl(71, 70%, 50%)",
                                                        "loc": 84545
                                                    },
                                                    {
                                                        "name": "crapB4",
                                                        "color": "hsl(41, 70%, 50%)",
                                                        "loc": 131559
                                                    }
                                                ]
                                            },
                                            {
                                                "name": "crapC",
                                                "color": "hsl(318, 70%, 50%)",
                                                "children": [
                                                    {
                                                        "name": "crapC1",
                                                        "color": "hsl(92, 70%, 50%)",
                                                        "loc": 70161
                                                    },
                                                    {
                                                        "name": "crapC2",
                                                        "color": "hsl(93, 70%, 50%)",
                                                        "loc": 105185
                                                    },
                                                    {
                                                        "name": "crapC3",
                                                        "color": "hsl(61, 70%, 50%)",
                                                        "loc": 152421
                                                    },
                                                    {
                                                        "name": "crapC4",
                                                        "color": "hsl(154, 70%, 50%)",
                                                        "loc": 100499
                                                    },
                                                    {
                                                        "name": "crapC5",
                                                        "color": "hsl(36, 70%, 50%)",
                                                        "loc": 27944
                                                    },
                                                    {
                                                        "name": "crapC6",
                                                        "color": "hsl(32, 70%, 50%)",
                                                        "loc": 32608
                                                    },
                                                    {
                                                        "name": "crapC7",
                                                        "color": "hsl(139, 70%, 50%)",
                                                        "loc": 52637
                                                    },
                                                    {
                                                        "name": "crapC8",
                                                        "color": "hsl(174, 70%, 50%)",
                                                        "loc": 89226
                                                    },
                                                    {
                                                        "name": "crapC9",
                                                        "color": "hsl(72, 70%, 50%)",
                                                        "loc": 113780
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }}
                    margin={{top: 40, right: 20, bottom: 20, left: 20}}
                    identity="name"
                    value="loc"
                    cornerRadius={2}
                    borderWidth={1}
                    borderColor="white"
                    colors={{scheme: 'nivo'}}
                    childColor={{from: 'color'}}
                    animate={true}
                    motionStiffness={90}
                    motionDamping={15}
                    isInteractive={true}
                />
            </div>
        </div>
    </React.Fragment>
);