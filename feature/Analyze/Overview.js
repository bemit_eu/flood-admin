import React from 'react';
import {withNamespaces} from 'react-i18next';
import {MdAutorenew, MdDataUsage, MdGridOff, MdGridOn, MdFullscreen, MdFullscreenExit, MdPieChart} from 'react-icons/md';
import {ID, timeToIso, isObject, isString} from '@formanta/js';
import posed from 'react-pose';

import {useTheme, createUseStyles, withTheme} from '../../lib/theme';
import Loading from "../../component/Loading";
import {cancelPromise} from "../../component/cancelPromise";
import {DataGrid} from "../../component/DataGrid/DataGrid";
import {ButtonSmall} from "../../component/Form";
import {withBackend} from "../BackendConnect";
import {withAuthPermission} from "../AuthPermission";
import {ContainerStyle, SplitScreen} from "../../component/SplitScreen";

import {beautifyProp} from "../../lib/beautify";
import OverviewItem from "./OverviewItem";
import {DropDownAuto, DropHorizontal, DropHorizontalWrapper, styleDropDownHorizontal} from "../../lib/DropDown";
import QueryManager from "./QueryManager";

import {storeAnalyze} from "./Stores/Analyze";
import {storeAnalyzeDiagrams} from "./Stores/AnalyzeDiagrams";
import {AnalyzeDiagramCreate} from "./AnalyzeDiagramCreate";
import {AnalyzeDiagramViewer} from "./AnalyzeDiagramViewer";

const QueryManagerDropDown = posed.div({
    hidden: {
        height: 0,
        marginBottom: 0,
    },
    visible: {
        height: 'min-content',
        marginBottom: '12px',
    }
});

const useStyles = createUseStyles(theme => ({
    row: {},
    cell: {},
    rowHeader: {
        border: 0,
        position: 'relative',
        zIndex: 2,
        background: theme.bgPage,
    },
    rowContent: {
        border: 0,
        background: theme.bgPage,
        transition: '0.2s linear background',
        '&:hover': {
            background: theme.bgLight
        }
    },
}));

const betweenDefault = {
    since: '',
    till: '',
};

const limitDefault = {
    length: 0,
    offset: 0,
};

const initialState = {
    showQueryManager: false,
    showAsTable: false,
    showAddDiagram: false,
    lastResult: [],
    lastResultColumns: {},
    selectedTypes: [],
    selectedTypeFields: {},
    selectedCondition: {},
    selectedConditionBetween: {...betweenDefault},
    selectedConditionLimit: {...limitDefault},
    activeQuery: '-',
    activeQueryName: '',
    loadingDetails: false,
    overview: 'diagram'
};

class AnalyzeOverview extends React.PureComponent {

    state = initialState;

    componentDidMount() {
        const {
            changeStyleInnerWrapper
        } = this.props;

        changeStyleInnerWrapper(ContainerStyle);
        this.fetch();

        window.snapshotCondition = () => this.state.selectedCondition;
    }

    componentWillUnmount() {
        this.setState(initialState);
        this.props.changeStyleInnerWrapper({});
    }

    fetch(force = false) {
        this.props.loadAnalyzeTypes(force);
        this.state.selectedTypes.forEach(type => {
            this.props.loadAnalyzeProps(type, force);
        });
        // don't add analyzeDetails here, as this fetch here loads only when needed or forced, details loads everytime
    }

    selectMultiple = (e, cb) => {
        this.setState({
            [e.target.dataset.name]: [...e.target.options].filter(o => o.selected).map(o => o.value)
        }, cb);
    };

    selectType = (e) => {
        this.selectMultiple(e, () => {
            const selectedTypeFields = {...this.state.selectedTypeFields};
            let changedProp = false;
            this.state.selectedTypes.forEach(type => {
                if(!selectedTypeFields.hasOwnProperty(type)) {
                    selectedTypeFields[type] = [];
                    changedProp = true;
                }
                this.props.loadAnalyzeProps(type);
            });
            if(changedProp) {
                this.setState({selectedTypeFields});
            }
            this.fetch();
        });
    };

    selectTypeFields = (e) => {
        const selectedTypeFields = {...this.state.selectedTypeFields};
        selectedTypeFields[e.target.dataset.type] = [...e.target.options].filter(o => o.selected).map(o => o.value);

        this.setState({
            selectedTypeFields
        }, () => {
            this.fetch();
        });
    };

    /**
     * Helper function to search a value down in an object
     *
     * @param target
     * @param restTargets
     * @param condition
     * @return {*}
     */
    findTarget = (target, restTargets = [], condition) => {
        if(!condition.hasOwnProperty(target)) {
            if(Array.isArray(condition[target])) {
                condition[target] = [];
            } else {
                condition[target] = {};
            }
            return condition;
        }

        if(Array.isArray(condition[target])) {
            condition = [...condition[target]];
        } else {
            condition = {...condition[target]};
        }

        if(restTargets.length) {
            return this.findTarget(restTargets.shift(), restTargets, condition);
        }

        return condition;
    };

    /**
     * Helper function to set a value down in an object
     * @param value
     * @param target
     * @param restTargets
     * @param condition
     */
    setTarget = (value, target, restTargets = [], condition) => {
        let org_condition = {};
        if(Array.isArray(condition)) {
            org_condition = [...condition];
        } else {
            org_condition = {...condition};
        }
        if(!condition.hasOwnProperty(target)) {
            // todo implement this `if`
            console.warn('condition with no prop ', target);
            condition[target] = value;
            return;
        }

        if(restTargets.length) {
            condition = condition[target];
            this.setTarget(value, restTargets.shift(), restTargets, condition);
        } else {
            condition[target] = value;
            return;
        }
        return org_condition;
    };

    addConditionValues = (valueAdder, targets = false, cb = undefined) => {
        if(Array.isArray(targets) && !targets.length) {
            targets = false;
        }
        let selectedCondition = {...this.state.selectedCondition};

        if(targets) {
            if(Array.isArray(targets)) {
                const recurs_target = [...targets];
                let found = this.findTarget(recurs_target.shift(), recurs_target, selectedCondition);
                if(found) {
                    selectedCondition = found;
                } else {
                    console.warn('target not found for: ', targets);
                }
            }
        }

        selectedCondition = valueAdder(selectedCondition);

        if(targets) {
            if(Array.isArray(targets)) {
                const recurs_target = [...targets];
                let org_condition = this.setTarget(selectedCondition, recurs_target.shift(), recurs_target, {...this.state.selectedCondition});
                this.setState({selectedCondition: org_condition}, cb);
            }
        } else {
            this.setState({selectedCondition}, cb);
        }
    };

    updateConditionField = (field_val, targets = false, cb = undefined) => {
        this.addConditionValues((field) => {
            field = {...field};
            Object.keys(field_val).forEach(field_k => {
                field[field_k] = field_val[field_k]
            });
            return field;
        }, targets, cb);
    };

    addConditionField = (targets = false) => {
        this.addConditionValues((selectedCondition) => {
            if(!Array.isArray(selectedCondition['fields'])) {
                selectedCondition['fields'] = [];
            }

            selectedCondition['fields'].push({
                event: '-',
                prop: '-',
                operator: '-',
                value: '',
            });

            return selectedCondition;
        }, targets);
    };

    addConditionConjunction = (targets = false) => {
        this.addConditionValues((selectedCondition) => {
            if(!Array.isArray(selectedCondition['conjunctions'])) {
                selectedCondition['conjunctions'] = [];
            }

            selectedCondition['conjunctions'].push({
                operator: false,
                fields: [],
                conjunctions: [],
            });

            return selectedCondition;
        }, targets);
    };

    createOrOverwriteQuery = () => {
        if(!this.state.selectedTypes.length || (isObject(this.state.selectedTypeFields) && !Object.keys(this.state.selectedTypeFields).length)) {
            // no type and type props selected, nothing to create
            return;
        }
        const query = {
            time: timeToIso(new Date()),
            name: this.state.activeQueryName,
            types: this.state.selectedTypes,
            fields: this.state.selectedTypeFields,
            condition: this.state.selectedCondition,
            between: this.state.selectedConditionBetween,
            limit: this.state.selectedConditionLimit,
        };

        if('-' === this.state.activeQuery) {
            query.q_id = ID();
        } else {
            query.q_id = this.state.activeQuery;
        }

        this.setState({
            activeQuery: query.q_id,
        }, () => {
            this.props.analyzeAddQuery(query);
            ///this.props.analyzeAddQuery(query, this.loadDetails);
        });
    };

    deleteQuery = () => {
        this.props.analyzeDeleteQuery(this.state.activeQuery);

        this.setState({
            selectedTypes: [],
            selectedTypeFields: {},
            selectedCondition: {},
            selectedConditionBetween: {...betweenDefault},
            selectedConditionLimit: {...limitDefault},
            activeQueryName: '',
            activeQuery: '-',
        });
    };

    selectQuery = () => {
        const {analyzeQueries} = this.props;
        if(analyzeQueries.hasOwnProperty(this.state.activeQuery)) {
            const query = analyzeQueries[this.state.activeQuery];
            this.setState({
                selectedTypes: query.types,
                selectedTypeFields: query.fields,
                activeQueryName: query.name || '',
                selectedCondition: query.condition || {},
                selectedConditionBetween: query.between || {...betweenDefault},
                selectedConditionLimit: query.limit || {...limitDefault},
            }, () => this.fetch());
        } else {
            console.warn('Analyze Overview Load unkown saved query');
        }
    };

    updateBetween = (target, val) => {
        if(this.state.selectedConditionBetween.hasOwnProperty(target)) {
            const selectedConditionBetween = {...this.state.selectedConditionBetween};
            selectedConditionBetween[target] = val;
            this.setState({selectedConditionBetween});
        }
    };

    updateLimit = (target, val) => {
        if(this.state.selectedConditionLimit.hasOwnProperty(target)) {
            const selectedConditionLimit = {...this.state.selectedConditionLimit};
            selectedConditionLimit[target] = val;
            this.setState({selectedConditionLimit});
        }
    };

    loadDetails = () => {
        this.setState({
            lastResult: [],
            lastResultColumns: {},
        });

        if(!this.state.activeQuery) {
            console.warn('Analyze activeQuery is invalid');
            return;
        }

        let query = false;
        if(this.props.analyzeQueries.hasOwnProperty(this.state.activeQuery)) {
            query = this.props.analyzeQueries[this.state.activeQuery];
        }

        if(!query) {
            console.warn('Analyze query fetched is invalid');
            return;
        }

        this.setState({
            loadingDetails: 'progress',
        });

        const fields = {};

        Object.keys(query.fields).forEach((type) => {
            fields[type] = query.fields[type];
        });

        // between = {}, limit = {}

        this.props.analyzeDetails(
            query.types,
            fields,
            this.state.selectedCondition,
            this.state.selectedConditionBetween,
            this.state.selectedConditionLimit
        )
            .then(analyzed => {
                if(analyzed.events && Array.isArray(analyzed.events)) {
                    analyzed.events = analyzed.events.map(evt_item => {
                        // convert the nested ID api syntax to react syntax
                        if(evt_item && evt_item.evt && evt_item.evt.ID) {
                            evt_item.id = evt_item.evt.ID;
                        }
                        return evt_item;
                    });
                    const lastResultColumns = {};
                    if(isObject(analyzed.columns)) {
                        // get first level of columns to build table headers
                        Object.keys(analyzed.columns).sort().forEach((key) => {
                            lastResultColumns[key] = analyzed.columns[key];
                        });
                    }
                    this.setState({
                        loadingDetails: true,
                        lastResult: analyzed.events,
                        lastResultColumns,
                    });
                } else {
                    this.setState({loadingDetails: 'error'});
                }
            })
            .catch((e) => {
                this.setState({loadingDetails: 'error'});
            });
    };

    toggleToOverview = (name) => {
        this.setState({overview: name});
    };

    overviewActive = (name) => {
        return this.state.overview === name;
    };

    showAsTable = (p) => this.overviewActive('table') ?
        <ButtonSmall onClick={() => this.setState({showAsTable: !this.state.showAsTable})}
                     style={{marginRight: 6}}
                     active={this.state.showAsTable}>{this.state.showAsTable ? <MdGridOff style={{verticalAlign: 'bottom'}}/> :
            <MdGridOn style={{verticalAlign: 'bottom'}}/>} {this.props.t('show-as-table')}</ButtonSmall> : null;

    addDiagram = (p) => this.overviewActive('diagram') ?
        <ButtonSmall onClick={() => this.setState({showAddDiagram: !this.state.showAddDiagram})} active={this.state.showAddDiagram}>
            <MdPieChart style={{verticalAlign: 'bottom'}}/> {this.props.t('add-diagram')}
        </ButtonSmall> : null;

    showQueryManager = (p) => <ButtonSmall
        onClick={() => {
            this.setState({
                showQueryManager: !this.state.showQueryManager,
            });
        }}
        style={{marginRight: 6}}
        active={this.state.showQueryManager}><MdDataUsage style={{verticalAlign: 'bottom'}}/> {this.props.t('show-query-manager')}</ButtonSmall>;

    element = (p) => {
        const theme = useTheme();
        const classes = useStyles({theme});

        return <OverviewItem
            t={this.props.t}
            theme={theme} classes={classes}
            asTable={this.state.showAsTable}
            lastResultColumns={this.state.lastResultColumns}
            {...p}/>
    };

    updateQueryName = (e) => {
        this.setState({activeQueryName: e.target.value});
    };

    selectActiveQuery = (q_id) => {
        this.setState({activeQuery: q_id || '-'});
    };

    render() {
        const {
            t, theme, classDropHorizontal,
            analyzeTypesLoaded,
        } = this.props;

        let headers = [];

        if(this.state.lastResultColumns && isObject(this.state.lastResultColumns) && Object.keys(this.state.lastResultColumns).length) {
            for(let name in this.state.lastResultColumns) {
                if(this.state.lastResultColumns.hasOwnProperty(name)) {
                    let sortAble = false;
                    let searchString = false;
                    if(isString(this.state.lastResultColumns[name])) {
                        sortAble = -1 !== ['string', 'integer', 'boolean', 'double'].indexOf(this.state.lastResultColumns[name]);
                    } else if(isObject(this.state.lastResultColumns[name])) {
                        // one column that is nested, build "or search"
                        searchString = Object.keys(this.state.lastResultColumns[name]).join('|data.');
                    }
                    headers.push({
                        lbl: beautifyProp(name),
                        search: sortAble ? 'data.' + name : 'data.' + searchString,
                        sort: sortAble ? 'data.' + name : false
                    });
                }
            }
        } else {
            headers = [
                {
                    lbl: t('list.id'),
                    search: 'evt.ID',
                    sort: 'evt.ID',
                },
                {
                    lbl: t('list.type'),
                    search: 'evt.type',
                    sort: 'evt.type',
                },
                {
                    lbl: t('list.time'),
                    search: 'evt.time',
                    sort: 'evt.time',
                },
            ];
        }

        return (
            <React.Fragment>
                <SplitScreen

                    isTopOpen={this.state.showQueryManager}
                    top={(
                        <React.Fragment>
                            {!this.state.showQueryManager ? <h1 style={{marginBottom: 6, fontSize: '1.4rem'}}>{t('title')}</h1> : null}

                            <QueryManagerDropDown withParent={false} pose={this.state.showQueryManager ? 'visible' : 'hidden'} style={{
                                overflow: this.state.showQueryManager ? null : 'hidden', display: 'flex', flexDirection: 'column'
                            }}>
                                <QueryManager
                                    selectedTypes={this.state.selectedTypes}
                                    selectType={this.selectType}
                                    selectedTypeFields={this.state.selectedTypeFields}
                                    selectTypeFields={this.selectTypeFields}
                                    selectedCondition={this.state.selectedCondition}
                                    selectedConditionBetween={this.state.selectedConditionBetween}
                                    selectedConditionLimit={this.state.selectedConditionLimit}
                                    updateBetween={this.updateBetween}
                                    updateLimit={this.updateLimit}
                                    addConditionField={this.addConditionField}
                                    updateConditionField={this.updateConditionField}
                                    addConditionConjunction={this.addConditionConjunction}
                                    activeQueryName={this.state.activeQueryName}
                                    updateQueryName={this.updateQueryName}
                                    activeQuery={this.state.activeQuery}
                                    selectActiveQuery={this.selectActiveQuery}
                                    createOrOverwriteQuery={this.createOrOverwriteQuery}
                                    deleteQuery={this.deleteQuery}
                                    selectQuery={this.selectQuery}
                                    executeQuery={() => {
                                        this.loadDetails();
                                    }}
                                />
                            </QueryManagerDropDown>

                            <div style={{marginBottom: 'auto', paddingBottom: 6, marginTop: 0, alignSelf: 'flex-start'}}>
                                <React.Fragment>
                                    <ButtonSmall active={true !== this.state.loadingDetails && false !== this.state.loadingDetails}
                                                 onClick={() => {
                                                     this.loadDetails();
                                                 }}
                                                 style={{marginRight: '6px'}}
                                    ><MdAutorenew style={{verticalAlign: 'bottom'}}/> {t('common:reload')}</ButtonSmall>
                                    <this.showQueryManager/>
                                </React.Fragment>
                            </div>
                        </React.Fragment>
                    )}

                    center={(
                        <React.Fragment>
                            <div className={classDropHorizontal.navWrapper}>
                                <div className={classDropHorizontal.navInner}>
                                    <button
                                        className={classDropHorizontal.toggleFullScreen + ' ' + (this.state.showQueryManager ? 'active' : '')}
                                        onClick={() => {
                                            this.setState({showQueryManager: !this.state.showQueryManager})
                                        }}>
                                        {this.state.showQueryManager ?
                                            <MdFullscreen color={theme.textColor} size={'1.25em'} style={{margin: 'auto', display: 'block'}}/>
                                            : <MdFullscreenExit color={theme.textColor} size={'1em'} style={{margin: 'auto', display: 'block'}}/>}
                                    </button>
                                    <button
                                        className={classDropHorizontal.navBtn + ' ' + (this.overviewActive('table') ? 'active' : '')}
                                        onClick={() => this.toggleToOverview('table')}
                                    >{t('show-table')}</button>
                                    <button
                                        className={classDropHorizontal.navBtn + ' ' + (this.overviewActive('diagram') ? 'active' : '')}
                                        onClick={() => this.toggleToOverview('diagram')}
                                    >{t('show-diagram')}
                                    </button>
                                </div>
                                <div className={classDropHorizontal.navBorder}/>
                            </div>

                            <DropHorizontalWrapper>
                                <DropHorizontal open={this.overviewActive('table')} pos={0} style={{padding: '0 3px', margin: '-12px 0'}}>
                                    {this.overviewActive('table') ? <DataGrid
                                        headers={headers}
                                        colStyle={[]}
                                        emptyList={t('list.no-result')}
                                        loadingLabel={t('list.loading-result')}
                                        list={this.state.lastResult}
                                        loading={this.state.loadingDetails || analyzeTypesLoaded}
                                        reload={() => {
                                            this.loadDetails();
                                        }}
                                        controls={[this.showAsTable]}
                                        element={this.element}
                                    /> : null}
                                </DropHorizontal>

                                <DropHorizontal open={this.overviewActive('diagram')} pos={1} style={{padding: '0 3px'}}>

                                    {'progress' === this.state.loadingDetails || 'error' === this.state.loadingDetails ?
                                        <Loading
                                            errorWarning={'error' === this.state.loadingDetails}
                                        /> : null}

                                    <div style={{display: 'flex', flexDirection: 'column'}}>
                                        <this.addDiagram/>
                                        <DropDownAuto open={this.state.showAddDiagram}>
                                            <AnalyzeDiagramCreate/>
                                        </DropDownAuto>
                                    </div>

                                    <AnalyzeDiagramViewer/>

                                    <div style={{display: 'flex', flexWrap: 'wrap'}}>
                                        <div style={{width: '50%', paddingTop: '50%', position: 'relative'}}>
                                            <div style={{position: 'absolute', top: 0, left: 0, bottom: 0, right: 0}}>

                                            </div>
                                        </div>
                                    </div>

                                    {this.state.lastResult.length ? <p>{t('total-counted')}: {this.state.lastResult.length}</p> : null}
                                    {this.state.lastResultColumns.length ? <p>{t('total-counted-columns')}: {this.state.lastResultColumns.length}</p> : null}
                                </DropHorizontal>
                            </DropHorizontalWrapper>
                        </React.Fragment>
                    )}
                />
            </React.Fragment>
        )
    };
}

export default withNamespaces('page--analyze')(styleDropDownHorizontal(cancelPromise(withAuthPermission(withBackend(storeAnalyze(withTheme(storeAnalyzeDiagrams((AnalyzeOverview)))))))));
