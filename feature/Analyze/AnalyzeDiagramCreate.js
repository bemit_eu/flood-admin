import React from "react";
import {ID} from '@formanta/js';
import {storeAnalyze} from "./Stores/Analyze";
import {ButtonSmall, Label, InputText, Select} from "../../component/Form";
import {storeAnalyzeDiagrams} from "./Stores/AnalyzeDiagrams";
import {AnalyzeQuerySelector} from "./AnalyzeQuerySelector";

class AnalyzeDiagramCreateBase extends React.PureComponent {
    state = {
        inpName: '',
        selectedDiagramType: 'bar',
        selectedQuery: '-',
    };

    constructor(props) {
        super(props);
        this.id = ID();
    }

    handleNewDiagram = () => {
        if('-' === this.state.selectedQuery) {
            return;
        }
        const {diagramAdd} = this.props;
        const diagram = {
            id: ID(),
            name: this.state.inpName,
            type: this.state.selectedDiagramType,
            query: this.state.selectedQuery,
        };

        diagramAdd(diagram);
    };

    render() {
        const {
            diagramTypes
        } = this.props;

        return <React.Fragment>
            <div style={{maxWidth: '500px', margin: '0 auto', padding: '12px', position: 'relative'}}>
                <h3 style={{marginTop: 0}}>New Diagram</h3>

                <div>
                    <Label htmlFor={this.id + '-diagram-name'}>Diagram Name</Label>
                    <InputText id={this.id + '-diagram-name'} value={this.state.inpName} onChange={(e) => this.setState({inpName: e.target.value})}/>
                </div>

                <Label style={{marginTop: 12}} htmlFor={this.id + '-diagram-type'}>Diagram Type</Label>
                <Select id={this.id + '-diagram-type'}
                        onChange={(e) => this.setState({selectedDiagramType: e.target.value})}
                        value={this.state.selectedDiagramType}
                >
                    {Object.keys(diagramTypes).map(analyzeDiagramType => (
                        <option key={analyzeDiagramType} value={analyzeDiagramType}>{analyzeDiagramType}</option>
                    ))}
                </Select>

                <Label style={{marginTop: 12}}>Query - Data Connection</Label>
                <AnalyzeQuerySelector
                    style={{margin: '0 0 12px 0', maxHeight: '10rem', minWidth: '100px', overflowY: 'auto',}}
                    activeQuery={this.state.selectedQuery}
                    onSelect={(q_id) => this.setState({selectedQuery: q_id})}
                />

                <ButtonSmall onClick={this.handleNewDiagram}>new diagram</ButtonSmall>
            </div>
        </React.Fragment>;
    }
}

const AnalyzeDiagramCreate = storeAnalyze(storeAnalyzeDiagrams(AnalyzeDiagramCreateBase));
export {AnalyzeDiagramCreate};
