import React from 'react';
import {withNamespaces} from 'react-i18next';
import {MdAdd,} from "react-icons/md";
import {ID} from "@formanta/js";

import {cancelPromise} from "../../component/cancelPromise";
import {withBackend} from "../BackendConnect";
import {withAuthPermission} from "../AuthPermission";
import {storeAnalyze} from "./Stores/Analyze";
import {ButtonSmall} from "../../component/Form";
import {ConditionBuilderFieldIterator} from "./QueryManagerConditionField";
import {ConditionBuilderConjunctionIterator} from "./QueryManagerConditionConjunction";

class QueryManagerConditionBuilderBase extends React.PureComponent {
    constructor(props) {
        super(props);
        this.id = ID;
    }

    render() {
        const {
            // general
            t,
            // parent
            selectedCondition, addField, updateField, addConjunction,
            selectedTypes,
            selectedTypeFields,
            // in rercursion
            conjunction,
            // not in rercursion
            rootLevel,
        } = this.props;

        let scope = this.props.scope || [];

        let condition = selectedCondition;
        if(conjunction) {
            condition = conjunction;
        }

        return (
            <React.Fragment>
                <div>
                    <div style={{display: 'flex'}}>
                        <ButtonSmall
                            style={{margin: '0 6px 0 0'}}
                            onClick={() => addField(scope)}
                        ><MdAdd style={{verticalAlign: 'bottom'}}/> {t('qm-custom.add-field')}</ButtonSmall>
                        <ButtonSmall
                            style={{margin: '0 0 0'}}
                            onClick={() => addConjunction(scope)}
                        ><MdAdd style={{verticalAlign: 'bottom'}}/> {t('qm-custom.add-conjunction')}</ButtonSmall>
                        {rootLevel ?
                            <React.Fragment>
                                <ButtonSmall
                                    style={{margin: '0 6px 0 6px'}}
                                    onClick={() => addField(scope)}
                                ><MdAdd style={{verticalAlign: 'bottom'}}/> {t('qm-custom.add-sort')}</ButtonSmall>
                                <ButtonSmall
                                    style={{margin: ' 0 0 0'}}
                                    onClick={() => addConjunction(scope)}
                                ><MdAdd style={{verticalAlign: 'bottom'}}/> {t('qm-custom.add-group')}</ButtonSmall>
                            </React.Fragment>
                            : null}
                    </div>

                    {Object.keys(condition).map((key => (
                        'fields' === key ?
                            <ConditionBuilderFieldIterator
                                key={key}
                                currentKey={key}
                                scope={scope}
                                fields={condition[key]}
                                updateField={updateField}
                                selectedTypeFields={selectedTypeFields}
                                selectedTypes={selectedTypes}
                            /> :
                            'conjunctions' === key ?
                                <ConditionBuilderConjunctionIterator
                                    key={key}
                                    currentKey={key}
                                    scope={scope}
                                    conjunction={condition[key]}// array
                                    selectedCondition={condition}
                                    addField={addField}
                                    updateField={updateField}
                                    addConjunction={addConjunction}
                                    selectedTypes={selectedTypes}
                                    selectedTypeFields={selectedTypeFields}
                                /> : null
                    )))}
                </div>
            </React.Fragment>
        )
    };
}

const QueryManagerConditionBuilder = withNamespaces('page--analyze')(cancelPromise(withAuthPermission(withBackend(storeAnalyze(QueryManagerConditionBuilderBase)))));

export default QueryManagerConditionBuilder;
