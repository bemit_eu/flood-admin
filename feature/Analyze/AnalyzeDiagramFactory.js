import React from 'react';
import {ID, isObject, isUndefined} from '@formanta/js';
import {MdEdit, MdMoreHoriz, MdSettingsEthernet, MdAspectRatio, MdSync} from "react-icons/md";
import Loadable from 'react-loadable';
import {useTheme, createUseStyles, withTheme, withStyles} from '../../lib/theme';
import Loading from '../../component/Loading';
import {Select, ButtonSmall, ButtonRaw} from "../../component/Form";
import {GrowWidth as GrowWidthAuto, createDropRight, DropDownAuto} from "../../lib/DropDown";
import {ButtonInteractiveDelete} from "../../component/ButtonInteractiveDelete";
import {AnalyzeDiagramUpdate} from './AnalyzeDiagramUpdate';
import StyleGrid from "../../lib/StyleGrid";
import ReactJson from 'react-json-view';
import {
    SettingsGroup,
} from "../../component/Settings";
import {storeAnalyzeDiagrams} from "./Stores/AnalyzeDiagrams";
import {storeAnalyze} from "./Stores/Analyze";
import {barFieldPrepare} from "./Diagram/Bar";

const factory = {
    'bar': Loadable({
        loader: () => import('./Diagram/Bar'),
        loading: (props) => <Loading {...props} name='Diagram/Bar'/>,
    }),
    /*
    sunburst: null,
    calendar: null,
    circle: null,
    line: null,
    heatmap: null,
    pie: null,
    radar: null,
    stream: null,
    */
};

const factoryFieldPrepare = {
    'bar': barFieldPrepare,
};

const GrowWidth = createDropRight('calc(50% - 6px)', '0');

const nivoThemes = {
    'nivo': 'default',
    'spectral': 'Spectral',
    'blue_green': 'Blue Green',
    'green_blue': 'Green Blue',
    'greens': 'Greens',
    'oranges': 'Oranges',
    'greys': 'Greys',
    'red_yellow_green': 'Red Yellow Green',
    'yellow_green': 'Yellow Green',
    'yellow_green_blue': 'Yellow Green Blue',
};

const useInnerStyle = createUseStyles(theme => ({
    content: {
        width: '100%',
        marginRight: 0,
        position: 'relative',
        zIndex: 2,
        background: theme.bgSpecial,
        transition: 'background ' + theme.transition,
        [StyleGrid.smUp]: {
            '&.show-more': {
                width: 'calc(50% - 6px)',
                marginRight: 6,
            },
        }
    },
    settings: {
        display: 'flex',
        transition: 'left 0.3s ease-out',
        marginTop: 12,
        flexDirection: 'column',
        left: 0,
        zIndex: -1,
        overflow: 'hidden',
        '&.show-more': {
            left: 0,
            zIndex: 1,
        },
        [StyleGrid.smUp]: {
            height: 'auto',
            marginTop: 0,
            top: 12,
            bottom: 12,
            position: 'absolute',
            '&.show-more': {
                left: '50%',
            },
        },
        [StyleGrid.smx]: {
            width: '100% !important',
            height: 0,
            '&.show-more': {
                height: 'auto',
            },
        },
    },
    reactJson: {
        '& .react-json-view': {
            backgroundColor: 'transparent !important',
        },
        '& .react-json-view .object-key-val': {
            fontSize: '0.85rem',
            fontFamily: 'Inconsolata, monospace',
        },
    },
}));

const stylesWrapper = theme => ({
    wrapper: {
        width: '100%',
        [StyleGrid.smUp]: {
            width: '50%',
        },
        [StyleGrid.mdUp]: {
            width: '33%',
        },
        [StyleGrid.lgUp]: {
            width: '33%',
        },
        [StyleGrid.lgx]: {
            width: '25%',
        },
        '&.width_1': {
            width: '100%',
        },
        '&.width_1_2': {
            width: '50%',
        },
        '&.width_1_3': {
            width: '33%',
        },
        '&.width_2_3': {
            width: '66%',
        },
        '&.width_1_4': {
            width: '25%',
        },
        '&.width_1_8': {
            width: '12.5%',
        },
        '&.show-more': {
            width: '100% !important',
        },
        [StyleGrid.smx]: {
            width: '100% !important',
        },
    },
    diagramMenu: {
        width: '100%',
        display: 'flex',
        alignContent: 'flex-end',
        marginBottom: 2,
        background: theme.bgPage,
        transition: 'background ' + theme.transition,
        '& button': {
            //margin: '1px 1px',
            padding: 4
        },
        '& button svg': {
            width: '1.35rem',
            height: '1.35rem',
            verticalAlign: 'middle',
            color: theme.textColor
        }
    }
});

class AnalyzeDiagramFactoryBase extends React.PureComponent {

    state = {
        result: {},
        showFormat: false,
        showWidth: false,
        showMore: true,
        showEditBase: false,
        loadingDetails: false,
        format: '1:1',
        width: '-',
    };

    formats = [
        '1:1',
        '1:auto',
        '16:9',
        '9:16',
        '4:3',
        '20:9',
        '9:20',
    ];

    widths = [
        '1',
        '1/2',
        '1/3',
        '2/3',
        '1/4',
        '1/8',
    ];

    constructor(props) {
        super(props);

        this.id = ID();
    }

    loadDetails = () => {
        const {
            analyzeQueries,
            diagram
        } = this.props;

        let query = false;
        if(analyzeQueries.hasOwnProperty(diagram.query)) {
            query = {...analyzeQueries[diagram.query]};
        }

        if(!query) {
            console.warn('Analyze no query fetched for diagram `' + diagram.id + '`.');
            return;
        }

        this.setState({
            loadingDetails: 'progress',
        });

        delete query.fields;
        if(!isObject(diagram.series)) return;
        if(isUndefined(factoryFieldPrepare[diagram.type])) return;
        //query.props = {};
        const fields = factoryFieldPrepare[diagram.type](diagram);

        //console.log(fields, diagram.condition.group, diagram.condition.sort);
        //return;
        // between = {}, limit = {}

        this.props.analyzeDetails(
            query.types,
            fields,
            query.condition,
            query.between,
            query.limit,
            diagram.condition && diagram.condition.group ? diagram.condition.group : [],
            diagram.condition && diagram.condition.sort ? diagram.condition.sort : [],
        )
            .then(analyzed => {
                console.log(analyzed);
                this.setState({
                    loadingDetails: true,
                });
                /*if(analyzed.events && Array.isArray(analyzed.events)) {
                    analyzed.events = analyzed.events.map(evt_item => {
                        // convert the nested ID api syntax to react syntax
                        if(evt_item && evt_item.evt && evt_item.evt.ID) {
                            evt_item.id = evt_item.evt.ID;
                        }
                        return evt_item;
                    });
                    const lastResultColumns = {};
                    if(isObject(analyzed.columns)) {
                        // get first level of columns to build table headers
                        Object.keys(analyzed.columns).sort().forEach((key) => {
                            lastResultColumns[key] = analyzed.columns[key];
                        });
                    }
                    this.setState({
                        loadingDetails: true,
                        lastResult: analyzed.events,
                        lastResultColumns,
                    });
                } else {
                    this.setState({loadingDetails: 'error'});
                }*/
            })
            .catch((e) => {
                console.error(e);
                this.setState({loadingDetails: 'error'});
            });
    };

    selectWidth = p => {
        return <GrowWidthAuto open={this.state.showWidth} style={{
            display: 'flex', transition: 'marginRight 0.2s ease-out',
            marginRight: this.state.showWidth ? 4 : 0
        }}>
            <label htmlFor={this.id + '-width'} style={{display: 'flex'}}><MdSettingsEthernet size={'1em'} style={{margin: 'auto 3px'}}/></label>
            <Select id={this.id + '-width'} onChange={(e) => this.setState({width: e.target.value})}
                    value={this.state.width}
            >
                <option value={'-'}>-</option>
                {this.widths.map(width => (
                    <option key={width} value={width}>{width}</option>
                ))}
            </Select>
        </GrowWidthAuto>
    };

    selectAspectRatio = p => {
        return <GrowWidthAuto
            open={this.state.showFormat}
            style={{
                display: 'flex', transition: 'margin-right 0.2s ease-out',
                marginRight: this.state.showFormat ? 4 : 0
            }}
        >
            <label htmlFor={this.id + '-format'} style={{display: 'flex'}}><MdAspectRatio size={'1em'} style={{margin: 'auto 2px'}}/></label>
            <Select id={this.id + '-format'} onChange={(e) => this.setState({format: e.target.value})}
                    value={this.state.format}
            >
                {this.formats.map(format => (
                    <option key={format} value={format}>{format}</option>
                ))}
            </Select>
        </GrowWidthAuto>
    };

    diagramContent = (p) => {
        const theme = useTheme();
        const classes = useInnerStyle(theme);
        let [f_width, f_height] = [0, 0];
        if(this.state.format !== '1:auto') {
            [f_width, f_height] = [...this.state.format.split(':')];
        }

        return <div className={classes.content + (this.state.showMore ? ' show-more' : '')} style={{
            flexGrow: (!this.state.showMore && this.state.format === '1:auto' ? 2 : 0),
            paddingTop: (this.state.showMore ?
                /* switch to format 16:9 when more is open and screen is bigger then tablets/often 4:3 screen */
                /* half the normal padding needed, as it now shares the space with the settings screen */
                ((window.innerWidth > 1281 ? (9 / 16 * 100) : 100) / 2) :
                (this.state.format === '1:auto' ? 0 : (f_height / f_width * 100)))
                + '%',
        }}>
            <div style={{position: 'absolute', top: 0, left: 0, bottom: 0, right: 0}}>
                {p.children}
            </div>
        </div>;
    };

    diagramSettings = (p) => {
        const {
            diagram, id,
        } = this.props;
        const theme = useTheme();
        const classes = useInnerStyle(theme);

        return <GrowWidth className={classes.settings + (this.state.showMore ? ' show-more' : '')} open={this.state.showMore}>
            <h3 style={{marginBottom: 2, lineHeight: '1.85rem'}}>Diagram Settings</h3>
            <div style={{
                overflow: 'auto',
                flexGrow: 2,
                borderTop: '1px solid ' + theme.bgSpecial,
                borderBottom: '1px solid ' + theme.bgSpecial,
            }}>
                <div>
                    <ButtonSmall
                        onClick={() => this.setState({showEditBase: !this.state.showEditBase})}
                        active={this.state.showEditBase}
                        style={{marginBottom: 12, marginRight: 6}}
                    ><MdEdit style={{verticalAlign: 'bottom'}} color={'inherit'}/> Edit Base</ButtonSmall>
                    <ButtonInteractiveDelete
                        onClick={() => this.props.diagramDelete(diagram.id)}
                        comp={(p) => (
                            <ButtonSmall {...p} >{p.children} Delete Diagram</ButtonSmall>
                        )}
                        style={{marginBottom: 12}}
                    />
                </div>
                <DropDownAuto open={this.state.showEditBase}>
                    <AnalyzeDiagramUpdate diagram={diagram} id={id}/>
                </DropDownAuto>
                <DropDownAuto open={!this.state.showEditBase}>
                    <p style={{margin: '0 0 6px 0', fontSize: '1rem',}}>Name: <strong>{diagram.name}</strong></p>
                    <p style={{margin: '6px 0', fontSize: '1rem',}}>Type: <strong>{diagram.type}</strong></p>
                    <p style={{margin: '6px 0', fontSize: '1rem',}}>Query: <strong>{diagram.query}</strong></p>
                </DropDownAuto>

                {p.children}

                <SettingsGroup
                    formId={this.id}
                    id={'json-raw'}
                    label={'Data Raw'}
                    popout
                >
                    <div className={classes.reactJson} style={{margin: '12px 0'}}>
                        <ReactJson src={diagram}
                                   theme={theme.name === 'dark' ? 'apathy' : 'apathy:inverted'}
                                   name={'Diagram'}
                                   displayObjectSize
                                   displayDataTypes
                                   renderOnlyWhenOpen
                        />
                    </div>
                    <div className={classes.reactJson} style={{margin: '12px 0'}}>
                        <ReactJson src={this.state.result}
                                   theme={theme.name === 'dark' ? 'apathy' : 'apathy:inverted'}
                                   name={'Data'}
                                   displayObjectSize
                                   displayDataTypes
                                   renderOnlyWhenOpen
                        />
                    </div>
                </SettingsGroup>
                <p style={{marginBottom: 0, opacity: 0.7, textAlign: 'right'}}><small>ID: {diagram.id}</small></p>
            </div>
        </GrowWidth>;
    };

    render() {
        const {
            classes, theme,
            diagram
        } = this.props;

        let Comp = null;
        if(diagram.type && factory.hasOwnProperty(diagram.type)) {
            Comp = factory[diagram.type];
        }

        return (
            Comp ?
                <div className={classes.wrapper + (this.state.showMore ? ' show-more' : '') + (' width_' + this.state.width.split('/').join('_'))} style={{
                    position: 'relative',
                    padding: '12px 6px',
                    display: 'flex', flexDirection: this.state.showMore ? 'row' : 'column',
                    flexWrap: 'wrap',
                }}>
                    <div className={classes.diagramMenu}
                         style={{
                             width: (this.state.showMore ? 'calc(50% - 6px)' : '100%'),
                             marginRight: (this.state.showMore ? 'calc(50% + 6px)' : 0),
                         }}
                    >
                        {diagram.name ? <h3 style={{margin: 'auto 0'}}>{diagram.name}</h3> : null}
                        <div style={{marginLeft: 'auto'}}/>
                        {this.state.showMore ? null :
                            <React.Fragment>
                                <this.selectAspectRatio/>
                                <this.selectWidth/>

                                <ButtonRaw active={this.state.showFormat} onClick={() => this.setState({showFormat: !this.state.showFormat, showWidth: false})}><MdAspectRatio/></ButtonRaw>
                                <ButtonRaw active={this.state.showWidth} onClick={() => this.setState({showWidth: !this.state.showWidth, showFormat: false})}><MdSettingsEthernet/></ButtonRaw>
                            </React.Fragment>}
                        <ButtonRaw active={false !== this.state.loadingDetails} onClick={this.loadDetails}><MdSync
                            color={'error' === this.state.loadingDetails ? theme.errorColor : theme.textColor}
                        /></ButtonRaw>
                        <ButtonRaw active={this.state.showMore} onClick={() => this.setState({showMore: !this.state.showMore})}><MdMoreHoriz/></ButtonRaw>
                    </div>

                    <Comp
                        diagram={diagram}
                        formId={this.id}

                        componentContent={this.diagramContent}
                        componentSettings={this.diagramSettings}
                    />

                </div> :
                null);
    };
}

const AnalyzeDiagramFactory = storeAnalyze(storeAnalyzeDiagrams(withTheme(withStyles(stylesWrapper)(AnalyzeDiagramFactoryBase))));

export {AnalyzeDiagramFactory};

export {nivoThemes};
