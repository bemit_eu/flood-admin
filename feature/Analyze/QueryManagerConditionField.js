import React from 'react';
import {InputText, Select} from "../../component/Form";
import {beautifyProp} from "../../lib/beautify";

class ConditionBuilderField extends React.PureComponent {
    state = {
        // todo: implement live update of condition with selected values
        field: '-',
        operator: '-',
        value: ''
    };

    operators = [
        'IS',
        'IS NOT',
        'IS EMPTY',
        'NOT EMPTY',
        'LIKE',
        'NOT LIKE',
        'IN',
        'NOT IN',
        'SMALLER THEN',
        'BIGGER THEN',
        'BETWEEN',
    ];

    componentDidMount() {
        //this.indexTypes(this.props.selectedTypes);
    }

    componentDidUpdate(prevProps) {
        if(prevProps.selectedTypes !== this.props.selectedTypes) {
            //this.indexTypes(this.props.selectedTypes);
        } else if(prevProps.selectedTypeFields !== this.props.selectedTypeFields) {
            //this.indexTypeFields();
        }
    }

    indexTypes = (types) => {
        if(!Array.isArray(types) || types.length === 0) {
            return;
        }
        if(types.length === 1 && this.props.field.event !== types[0]) {
            this.setState({
                field: false,
                event: types[0],
            }, this.indexTypeFields)
        } else {
            if(-1 === types.indexOf(this.props.field.event)) {
                this.setState({
                    field: false,
                    event: false,
                }, this.indexTypeFields)
            }
        }
    };

    indexTypeFields = () => {
        const {selectedTypeFields} = this.props;

        if(!selectedTypeFields || !Array.isArray(selectedTypeFields[this.props.field.event]) || selectedTypeFields[this.props.field.event].length === 0) {
            return;
        }

        if(-1 === selectedTypeFields[this.props.field.event].indexOf(this.props.field.field)) {
            this.setState({
                field: selectedTypeFields[this.props.field.event][0],
            });
        }
    };

    selectEvent = (e) => {
        const field = {...this.props.field};

        field.event = e.target.value;
        field.field = '-';

        const scope = [...this.props.scope];
        scope.push(this.props.currentKey);
        this.props.updateField(field, scope,/* () => this.indexTypeFields()*/);
    };

    selectField = (e) => {
        const field = {...this.props.field};

        field.field = e.target.value;

        const scope = [...this.props.scope];
        scope.push(this.props.currentKey);
        this.props.updateField(field, scope);
    };

    changeValue = (e) => {
        const field = {...this.props.field};

        field.value = e.target.value;

        const scope = [...this.props.scope];
        scope.push(this.props.currentKey);
        this.props.updateField(field, scope);
    };

    selectOperator = (e) => {
        const field = {...this.props.field};

        field.operator = e.target.value;

        const scope = [...this.props.scope];
        scope.push(this.props.currentKey);
        this.props.updateField(field, scope);
    };

    render() {
        const {
            field,
            selectedTypes,
            selectedTypeFields,
        } = this.props;

        return <div style={{margin: '4px 0'}}>
            <div style={{display: 'flex'}}>
                {selectedTypes ? <Select
                    value={field.event} onChange={this.selectEvent}
                    style={{marginRight: 6, width: '25%'}}
                >
                    <option value={'-'}>-</option>
                    {selectedTypes.map(event_type => (
                        <option key={event_type}
                                value={event_type}>{beautifyProp(event_type)}</option>
                    ))}
                </Select> : null}

                {selectedTypeFields && selectedTypeFields[field.event] && selectedTypeFields[field.event].length ?
                    <Select value={field.field} onChange={this.selectField}
                            style={{marginRight: 6, width: '25%'}}
                    >
                        <option value={'-'}>-</option>
                        {selectedTypeFields[field.event].map(prop => (
                            <option key={prop}
                                    value={prop}>{beautifyProp(prop)}</option>
                        ))}
                    </Select> : null}

                {'-' !== field.event && '-' !== field.field ?
                    <Select value={field.operator} onChange={this.selectOperator}
                            style={{marginRight: 6, width: '25%'}}
                    >
                        <option value={'-'}>-</option>
                        {this.operators.map(operator => (
                            <option key={operator}
                                    value={operator}>{operator}</option>
                        ))}
                    </Select> : null}

                <div style={{width: '25%', display: 'flex'}}>
                    {'-' !== field.operator && '-' !== field.event && '-' !== field.field ?
                        'IS' === field.operator ||
                        'IS NOT' === field.operator ||
                        'LIKE' === field.operator ||
                        'NOT LIKE' === field.operator ||
                        'SMALLER THEN' === field.operator ||
                        'BIGGER THEN' === field.operator
                            ? <InputText
                                style={{width: '100px', flexGrow: 2}}
                                value={field.value} onChange={this.changeValue}/>
                            : null
                        : null}
                </div>
            </div>
        </div>;
    }
}

class ConditionBuilderFieldIterator extends React.PureComponent {

    render() {
        let {
            fields,// array
            updateField,
            currentKey,
        } = this.props;

        const scope = [...this.props.scope];
        scope.push(currentKey);

        return <div style={{margin: '12px 0'}}>
            {fields.map((field, i) => (
                <ConditionBuilderField
                    key={i}
                    {...this.props}
                    scope={scope}
                    currentKey={i}
                    field={field}
                    updateField={updateField}
                />
            ))}
        </div>;
    }
}

export {ConditionBuilderFieldIterator, ConditionBuilderField};
