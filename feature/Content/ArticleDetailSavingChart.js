import React from 'react';
import {MdClose} from 'react-icons/md';

import Loading from "../../component/Loading";

import {withTheme, withStyles} from '../../lib/theme';
import StyleGrid from '../../lib/StyleGrid';

const styles = {
    wrapper: {
        position: 'fixed',
        zIndex: 100,
        right: '0',
        bottom: '0',
        padding: '15px',
        [StyleGrid.mdUp]: {
            padding: '30px'
        }
    },
    close: {
        position: 'absolute',
        top: '6px',
        right: '6px',
    },
    singleOne: {
        display: 'flex',
        flexWrap: 'no-wrap',
        padding: '6px',
        /*[StyleGrid.mdUp]: {
            flexWrap: 'nowrap'
        }*/
    },
    name: {
        margin: 'auto 12px auto 0'
    },
    info: {
        width: '80%',
        flexGrow: 1,
    }
};

class ArticleDetailSavingChart extends React.PureComponent {

    state = {
        open: false,
    };


    toggle = () => {
        this.setState({open: !this.state.open});
    };

    someThingIsSaving() {
        return (this.props.saving && (this.props.saving.meta || this.props.saving.mainText || this.props.saving.docTree));
    }

    componentDidUpdate(prevProps) {
        if(prevProps.saving !== this.props.saving &&
            prevProps.saving !== this.props.saving) {
            if(this.someThingIsSaving()) {
                this.setState({
                    open: true,
                });
            }
        }
    }

    render() {
        const {
            t,
            theme, classes,
            saving
        } = this.props;

        return (
            <React.Fragment>
                {this.someThingIsSaving() && this.state.open ?
                    <div className={classes.wrapper} style={{border: '3px solid ' + theme.textColorLight, background: theme.bgPage}}>
                        <button className={classes.close} onClick={this.toggle}><MdClose size={'1.25em'} color={theme.linkColor}/></button>
                        {Object.keys(saving).map((name) => (
                            saving[name] ?
                                <div className={classes.singleOne} key={name}>
                                    <p className={classes.name}>{name}</p>
                                    <div className={classes.info}>
                                        {'progress' === saving[name] || 'error' === saving[name] ?
                                            <React.Fragment>
                                                <Loading style={{
                                                    wrapper: {margin: 0},
                                                    item: ('error' === saving[name] ? {borderColor: theme.errorColor} : {}),
                                                }}/>

                                            </React.Fragment> :
                                            ('success' === saving[name] ?
                                                t('saved-msg') :
                                                null)}
                                    </div>
                                </div>
                                : null
                        ))}
                    </div>
                    : null}
            </React.Fragment>
        )
    };
}

export default withStyles(styles)(withTheme(ArticleDetailSavingChart));
