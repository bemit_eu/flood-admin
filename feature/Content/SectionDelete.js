import React from 'react';
import {withNamespaces} from 'react-i18next';
import {withRouter} from 'react-router-dom';
import {withTimeout} from "@formanta/react";

import {withTheme,} from '../../lib/theme';
import {i18n} from "../../lib/i18n";

import {storeSection} from "./Stores/Section";
import {cancelPromise} from "../../component/cancelPromise";
import {ButtonSmall} from "../../component/Form";
import ButtonInteractiveDelete from "../../component/ButtonInteractiveDelete";
import Loading from "../../component/Loading";

class SectionDelete extends React.Component {
    state = {
        deleting: false
    };

    deleteSection = () => {
        const {
            section,
            deleteSection, cancelPromise,
            history
        } = this.props;

        this.setState({deleting: 'progress'});
        cancelPromise(deleteSection(section))
            .then((data) => {
                if(data.error) {
                    this.setState({deleting: 'error'});
                    return;
                }

                history.push('/' + i18n.languages[0] + '/content');

                this.setState({
                    deleting: true
                });
            })
            .catch((err) => {
                if(err && !err.cancelled) {
                    this.setState({deleting: 'error'});
                }
            });
    };

    render() {
        const {
            t, theme,
        } = this.props;

        return (
            <React.Fragment>
                <ButtonInteractiveDelete
                    comp={(p) => (
                        <ButtonSmall {...p}>
                            {t('edit.btn-delete-section')} {p.children}
                        </ButtonSmall>
                    )}
                    onClick={this.deleteSection}
                />
                {('progress' === this.state.deleting || 'error' === this.state.deleting ?
                    <Loading style={{
                        wrapper: {margin: 0},
                        item: ('error' === this.state.deleting ? {borderColor: theme.errorColor} : {}),
                    }}/> :
                    null)}
            </React.Fragment>
        )
    };
}

export default withNamespaces('page--content-section')(withRouter(withTimeout(cancelPromise(storeSection(withTheme(SectionDelete))))));
