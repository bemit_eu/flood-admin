import React from 'react';

import {ButtonSmall, Select} from "../../component/Form";

class ArticleDetailLocaleSwitch extends React.PureComponent {

    state = {
        selectedLocale: false
    };

    localeSwitch = () => {
        const {localeSwitch} = this.props;
        if(localeSwitch) {
            if(this.state.selectedLocale) {
                localeSwitch(this.state.selectedLocale);
            }
        } else {
            console.error('localeSwitch not assigned to ArticleDetailLocaleSwitch.');
        }
    };

    render() {
        const {
            t,
            localesPossible, locales, localeActive,
        } = this.props;

        return (
            <div style={{display:'flex'}}>
                <Select
                        defaultValue={localeActive}
                        onChange={(e) => this.setState({selectedLocale: e.target.value})}
                        style={{
                            maxWidth: '150px',
                        }}
                >
                    {0 < localesPossible.length ?
                        localesPossible.map((locale, i) => (
                            <option key={i} value={locale}
                            >{'*' === locale ? t('locale.universal-locale') : locale}{-1 !== locales.indexOf(locale) ? ' •' : ''}</option>
                        )) : null
                    }
                </Select>
                {localeActive !== this.state.selectedLocale && false !== this.state.selectedLocale ?
                    <ButtonSmall onClick={this.localeSwitch} style={{flexShrink: 0}}>{t('locale.switch-to')}</ButtonSmall> :
                    null}
            </div>
        )
    };
}

export default ArticleDetailLocaleSwitch;
