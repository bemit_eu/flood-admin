import React from 'react';
import {withNamespaces} from 'react-i18next';
import {Redirect, withRouter} from 'react-router-dom';
import {ID} from "@formanta/js";
import {withTimeout} from "@formanta/react";

import {withTheme,} from '../../lib/theme';
import {i18n} from "../../lib/i18n";

import {withPusher} from "../../component/Pusher/Pusher";
import {storeSection} from "./Stores/Section";
import {cancelPromise} from "../../component/cancelPromise";
import {ButtonSmall, InputText} from "../../component/Form";
import ButtonInteractive from "../../component/ButtonInteractive";
import {MdDone, MdHelp} from "react-icons/md";
import Loading from "../../component/Loading";
import {withBackend} from "../BackendConnect";

class SectionCreate extends React.Component {
    state = {
        name: '',
        saving: false,
        newSection: false,
        loaded: false
    };

    constructor(props) {
        super(props);
        this.id = ID();
    }

    componentDidMount() {
        if(!this.props.create) {
            this.loadSection();
        }
    }

    componentDidUpdate(prevProps) {
        if(prevProps.section !== this.props.section) {
            if(!this.props.create) {
                this.loadSection();
            }
        }
    }

    loadSection = () => {
        const {
            section,
            loadSection, cancelPromise,
            hook_active, history
        } = this.props;

        this.setState({loading: 'progress'});
        cancelPromise(loadSection(section))
            .then((data) => {
                if(data.error) {
                    this.setState({loading: 'error'});
                    return;
                }

                if(hook_active.id !== data.hook) {
                    // if section hook is not the same like active hook, force to content overview
                    // happens when a user reloads the page and another hook then the default was active
                    history.push('/' + i18n.languages[0] + '/content');
                }

                this.setState({
                    loading: true,
                    name: data.name
                });
            })
            .catch((err) => {
                if(err && !err.cancelled) {
                    this.setState({loading: 'error'});
                }
            });
    };

    pushNewSection = () => {
        const {
            createSection, cancelPromise,
            setTimeout
        } = this.props;

        this.setState({saving: 'progress'});
        cancelPromise(createSection(this.state.name))
            .then((data) => {
                if(data.error) {
                    this.setState({saving: 'error'});
                    return;
                }

                this.setState({
                    saving: 'success',
                    newSection: data.success
                });

                setTimeout(() => {
                    this.setState({
                        newSection: false
                    });
                }, 0);

                setTimeout(() => {
                    this.setState({saving: false});
                }, 4000);
            })
            .catch((err) => {
                if(err && !err.cancelled) {
                    this.setState({saving: 'error'});
                }
            });
    };

    pushUpdateSection = () => {
        const {
            section,
            updateSection, cancelPromise,
            setTimeout
        } = this.props;

        this.setState({saving: 'progress'});
        cancelPromise(updateSection(section, this.state.name))
            .then((data) => {
                if(data.error) {
                    this.setState({saving: 'error'});
                    return;
                }

                this.setState({
                    saving: 'success'
                });

                setTimeout(() => {
                    this.setState({saving: false});
                }, 4000);
            })
            .catch((err) => {
                if(err && !err.cancelled) {
                    this.setState({saving: 'error'});
                }
            });
    };

    render() {
        const {
            t, theme,
            create
        } = this.props;

        return (
            <React.Fragment>
                {
                    (
                        this.state.newSection
                    ) ? <Redirect to={'/' + i18n.languages[0] + '/content/' + this.state.newSection}/>
                        : null
                }

                {('progress' === this.state.loading || 'error' === this.state.loading ?
                    <Loading style={{
                        wrapper: {margin: 0},
                        item: ('error' === this.state.loading ? {borderColor: theme.errorColor} : {}),
                    }}/> :
                    null)}

                {create || true === this.state.loading ?
                    <form onSubmit={(e) => {
                        e.preventDefault();
                        e.target.reportValidity()
                    }}>
                        <div style={{marginBottom: '12px'}}>
                            <label htmlFor={this.id + '-name'}>{t('edit.section-name')}</label>

                            <InputText id={this.id + '-name'}
                                       value={this.state.name}
                                       onChange={(e) => this.setState({name: e.target.value})}
                                       ref={(r) => {
                                           this.input_name = r
                                       }}
                                       required
                            />
                        </div>

                        <ButtonInteractive
                            comp={(p) => (
                                <ButtonSmall {...p}>
                                    {t('edit.btn-' + (create ? 'create' : 'update') + '-section')} {p.children}
                                </ButtonSmall>
                            )}
                            step={create ? false : [
                                null,
                                <MdHelp size={'1em'} color={theme.textColor}/>,
                                <MdDone size={'1em'} color={theme.textColor}/>,
                            ]}
                            onClick={create ? this.pushNewSection : this.pushUpdateSection}
                        />
                        {('progress' === this.state.saving || 'error' === this.state.saving ?
                            <Loading style={{
                                wrapper: {margin: 0},
                                item: ('error' === this.state.saving ? {borderColor: theme.errorColor} : {}),
                            }}/> :
                            null)}
                    </form> : null}
            </React.Fragment>
        )
    };
}

export default withNamespaces('page--content-section')(withPusher(withTimeout(cancelPromise(withRouter(withBackend(storeSection(withTheme(SectionCreate))))))));
