import React from 'react';
import {MdDone} from 'react-icons/md';

import Loading from "../../component/Loading";

import {withTheme, withStyles} from '../../lib/theme';
import {ButtonSmall, Select} from "../../component/Form";
import {ButtonInteractiveDelete} from "../../component/ButtonInteractiveDelete";
import StyleGrid from "../../lib/StyleGrid";

const styles = {
    wrapperLocaleActive: {
        display: 'flex',
        flexWrap: 'wrap',
        margin: '6px 0',
        [StyleGrid.smUp]: {
            flexWrap: 'no-wrap',
        }
    },
    localeDeleteBtn: {
        display: 'flex',
        alignItems: 'center',
        marginLeft: 'auto',
        '& svg': {
            marginLeft: '3px'
        }
    }
};

class ArticleDetailHead extends React.PureComponent {
    state = {
        selectedPageType: false
    };

    pushNewLocale = () => {
        const {pushNewLocale} = this.props;
        if(pushNewLocale) {
            if(this.state.selectedPageType) {
                pushNewLocale(this.state.selectedPageType);
            } else {
                console.error('ArticleDetailLocaleHead selectedPageType is invalid.');
            }
        } else {
            console.error('pushNewLocale not assigned to ArticleDetailLocaleHead.');
        }
    };

    componentDidUpdate(prevProps) {
        const {pageTypesPossible} = this.props;
        if(prevProps.pageTypesPossible !== pageTypesPossible) {
            this.setState({
                selectedPageType: pageTypesPossible[0] ? pageTypesPossible[0] : false
            });
        }
    }

    componentDidMount() {
        const {pageTypesPossible} = this.props;
        this.setState({
            selectedPageType: pageTypesPossible[0] ? pageTypesPossible[0] : false
        });
    }

    render() {
        const {
            t, theme, classes,
            activeLocaleExists, activeLocalePossible, localeActive, pushDeleteLocale, savingDelete, savingNew, pageTypesPossible
        } = this.props;

        return (
            activeLocaleExists ?
                // when locale exists, show info and delete button
                <div className={classes.wrapperLocaleActive}>
                    <p style={{margin: 0, fontSize: '0.875rem'}}> {t('locale.active-is')} {'*' === localeActive ? t('locale.universal-locale') : localeActive}</p>
                    <ButtonInteractiveDelete
                        className={classes.localeDeleteBtn}
                        comp={(p) => (
                            <button {...p} style={{fontSize: '0.875rem', color: theme.textColor}}>{t('locale.delete')} {p.children}</button>
                        )}
                        onClick={pushDeleteLocale}
                    />
                    {savingDelete ?
                        ('progress' === savingDelete || 'error' === savingDelete ?
                            <React.Fragment>
                                <Loading style={{
                                    wrapper: {margin: 0},
                                    item: ('error' === savingDelete ? {borderColor: theme.errorColor} : {}),
                                }}/>

                            </React.Fragment> :
                            ('success' === savingDelete ?
                                <MdDone/>
                                : null))
                        : null}
                </div> :
                activeLocalePossible ?
                    // when locale not exists but possible, show info and create form
                    (<React.Fragment>
                        <p>{t('locale.active-not-existing')} <em><strong>{'*' === localeActive ? t('locale.universal-locale') : localeActive}</strong></em></p>
                        <div>
                            <div style={{marginBottom: '6px'}}>
                                <label style={{marginBottom: '6px'}}>{t('locale.new-locale.choose-page-type')}</label>
                                <Select onChange={(e) => this.setState({selectedPageType: e.target.value})}
                                        style={{
                                            background: theme.bgPage,
                                            border: '1px solid ' + theme.textColorLight,
                                            color: theme.textColor,
                                        }}
                                >
                                    {0 < pageTypesPossible.length ?
                                        pageTypesPossible.map((page_type, i) => (
                                            <option key={i} value={page_type}>{page_type}</option>
                                        )) : null
                                    }
                                </Select>
                            </div>

                            <ButtonSmall onClick={this.pushNewLocale}
                                         style={{marginTop: theme.gutter.base * 8 + 'px'}}
                            >{t('locale.new-locale.btn-create')}</ButtonSmall>

                            {savingNew ?
                                ('progress' === savingNew || 'error' === savingNew ?
                                    <React.Fragment>
                                        <Loading style={{
                                            wrapper: {margin: 0},
                                            item: ('error' === savingNew ? {borderColor: theme.errorColor} : {}),
                                        }}/>

                                    </React.Fragment> :
                                    ('success' === savingNew ?
                                        <MdDone/>
                                        : null))
                                : null}
                        </div>
                    </React.Fragment>)
                    :
                    // when locale not exists and not possible, show info
                    <p>{t('locale.active-not-valid')} <i>{'*' === localeActive ? t('locale.universal-locale') : localeActive}</i></p>
        )
    };
}

export default withStyles(styles)(withTheme(ArticleDetailHead));
