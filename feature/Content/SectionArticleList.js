import React from 'react';
import {withNamespaces} from "react-i18next";

import {createUseStyles} from '../../lib/theme';
import RouteCascaded from '../../lib/RouteCascaded';

import Loading from "../../component/LoadingSmall";

import Nav from "../../component/Nav/Nav";
import Sidebar from "../../layout/ControlPanel/Sidebar";
import {storeArticle} from "./Stores/Article";

const useStyles = createUseStyles({
    sidebarTop: {
        padding: '0 3px',
        margin: '0 0 6px 0',
    }
});

class SectionArticleList extends React.PureComponent {
    fetch(force = false) {
        const {loadArticleList} = this.props;
        loadArticleList(this.props.match.params.section, force);
    }

    componentDidUpdate(prevProps) {
        if(prevProps.match.params.section !== this.props.match.params.section) {
            this.fetch(true);
        }
    }

    componentDidMount() {
        this.fetch();
    }

    render() {
        const {
            t,
            articles, loadingArticles, isArticleListLoaded
        } = this.props;

        const navContent = (match) => {
            let content = [];

            articles[this.props.match.params.section].forEach((article) => {
                content.push({
                    path: this.props.match.url + '/' + article.id,
                    lbl: {txt: article.name},
                })
            });

            return content;
        };
        const SideBarTop = () => {
            const classes = useStyles();
            return <p className={classes.sidebarTop}>{t('sidebar.article-list')}</p>
        };

        return (
            <React.Fragment>
                {
                    <Sidebar top={SideBarTop}
                             center={props => (
                                 'progress' === isArticleListLoaded(this.props.match.params.section) || 'error' === isArticleListLoaded(this.props.match.params.section) ?
                                     <Loading
                                         center={true}
                                         errorWarning={'error' === loadingArticles}
                                     /> :
                                     (true === isArticleListLoaded(this.props.match.params.section) ?
                                         (0 < articles[this.props.match.params.section].length ?
                                             <Nav path='/content' content={navContent}/> :
                                             <p style={{
                                                 padding: '0 3px',
                                                 margin: '0 0 6px 0',
                                                 fontStyle: 'italic'
                                             }}>{t('view-error.no-articles-in-section')}</p>)
                                         : 'error')
                             )}/>
                }

                {this.props.routes ? this.props.routes.map((route, i) => (
                    route.sidebar ?
                        <RouteCascaded key={i} {...route} component={route.sidebar}/>
                        : null
                )) : null}
            </React.Fragment>
        )
    };
}

export default withNamespaces('page--content-article')(storeArticle(SectionArticleList));
