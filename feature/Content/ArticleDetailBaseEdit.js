import React from 'react';
import {withNamespaces} from 'react-i18next';
import {MdDone, MdHelp} from 'react-icons/md';
import posed from 'react-pose';

import Loading from "../../component/Loading";

import {ButtonSmall, InputText, Select} from '../../component/Form';
import {withTheme} from '../../lib/theme';
import FormUnique from "../../component/FormUnique";
import ButtonInteractive from "../../component/ButtonInteractive";
import {storeSection} from "./Stores/Section";


const Wrapper = posed.div({
    hidden: {
        height: 0,
        marginTop: 0,
        marginBottom: 0,
    },
    visible: {
        height: 'auto',
        marginTop: '24px',
        marginBottom: '24px',
    }
});

class ArticleDetailBaseEdit extends React.PureComponent {

    state = {
        section: '',
        name: false,
        tag: '',
    };

    input_name = null;

    constructor(props) {
        super(props);
        this.id = FormUnique();
    }

    componentDidMount() {
        const {articleInfo} = this.props;

        this.setState({
            section: articleInfo.section,
            name: articleInfo.name || '',
            tag: articleInfo.tag || ''
        });
    }

    pushBaseData = () => {
        if(this.input_name.checkValidity()) {
            const {pushBaseData, articleInfo} = this.props;
            if(articleInfo.section === this.state.section) {
                pushBaseData(this.state.name, this.state.tag);
            } else {
                pushBaseData(this.state.name, this.state.tag, this.state.section);
            }
        }
    };

    render() {
        const {
            t, theme,
            saving, open, create,
            sections, loadingSections
        } = this.props;

        return (
            <Wrapper withParent={false} pose={open ? 'visible' : 'hidden'} style={{overflow: 'hidden'}}>
                {('progress' === loadingSections || 'error' === loadingSections ?
                    <Loading style={{
                        wrapper: {margin: 0},
                        item: ('error' === loadingSections ? {borderColor: theme.errorColor} : {}),
                    }}/> :
                    null)}

                {true === loadingSections ?
                    <form onSubmit={(e) => {
                        e.preventDefault();
                        e.target.reportValidity()
                    }}>
                        <div style={{marginBottom: '12px'}}>
                            <label htmlFor={this.id + '-name'}>{t('edit-base.article-name')}</label>

                            <InputText id={this.id + '-name'}
                                       value={this.state.name}
                                       onChange={(e) => this.setState({name: e.target.value})}
                                       ref={(r) => {
                                           this.input_name = r
                                       }}
                                       required
                            />
                        </div>

                        <div style={{marginBottom: '12px'}}>
                            <label htmlFor={this.id + '-tag'}>{t('edit-base.article-tag')}</label>
                            <InputText id={this.id + '-tag'}
                                       value={this.state.tag}
                                       onChange={(e) => this.setState({tag: e.target.value})}
                            />
                        </div>

                        {!create ?
                            <div style={{marginBottom: '12px'}}>
                                <label htmlFor={this.id + '-section'} style={{marginBottom: '6px'}}>{t('edit-base.in-section')}</label>
                                <Select id={this.id + '-section'}
                                        value={this.state.section}
                                        onChange={(e) => this.setState({section: e.target.value})}
                                >
                                    {0 < sections.length ?
                                        sections.map((section, i) => (
                                            <option key={i} value={section.id}>{section.name}</option>
                                        )) : null
                                    }
                                </Select>
                            </div>
                            : null}

                        <ButtonInteractive
                            comp={(p) => (
                                <ButtonSmall {...p}>
                                    {t('edit-base.btn-' + (create ? 'create' : 'update') + '-article')} {p.children}
                                </ButtonSmall>
                            )}
                            step={create ? false : [
                                null,
                                <MdHelp size={'1em'} color={theme.textColor}/>,
                                <MdDone size={'1em'} color={theme.textColor}/>,
                            ]}
                            onClick={this.pushBaseData}
                        />
                        {('progress' === saving || 'error' === saving ?
                            <Loading style={{
                                wrapper: {margin: 0},
                                item: ('error' === saving ? {borderColor: theme.errorColor} : {}),
                            }}/> :
                            null)}
                    </form> : null}
            </Wrapper>
        )
    };
}

export default withNamespaces('page--content-article')(storeSection(withTheme(ArticleDetailBaseEdit)));
