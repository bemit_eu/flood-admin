import React from 'react';
import StyleGrid from "../../lib/StyleGrid";
import {createUseStyles} from '../../lib/theme';

const useStyles = createUseStyles({
    subTitle: {},
    dateWrapper: {
        display: 'flex',
        width: '100%',
        flexWrap: 'wrap',
        margin: '6px 0'
    },
    dateSingle: {
        [StyleGrid.smx]: {
            width: '100%'
        },
        '&:last-child': {
            [StyleGrid.smUp]: {
                marginLeft: '12px'
            }
        }
    },
    dateLbl: {
        marginRight: '6px',
        fontSize: '0.875rem'
    },
    dateValue: {
        fontSize: '0.875rem'
    }
});

const ArticleDetailHead = (props) => {
    const {data, t} = props;
    const classes = useStyles();

    return (
        data.date_create || data.date_update ?
            <div className={classes.dateWrapper}>
                {data.date_create ?
                    <div className={classes.dateSingle}>
                        <span className={classes.dateLbl}>{t('date.created')}</span>
                        <span className={classes.dateValue}>{data.date_create}</span>
                    </div>
                    : null}
                {data.date_update ?
                    <div className={classes.dateSingle}>
                        <span className={classes.dateLbl}>{t('date.updated')}</span>
                        <span className={classes.dateValue}>{data.date_update}</span>
                    </div>
                    : null}
            </div>
            : null
    )
};

export default ArticleDetailHead;
