import React from 'react';
import posed from 'react-pose';
import {MdDone} from 'react-icons/md';

import Loading from "../../component/Loading";

import {ButtonSmall as Button} from '../../component/Form';
import {SchemaEditor} from "../../component/Schema/EditorLoader";

import {withTheme} from '../../lib/theme';

import {storeSchema} from "./Stores/Schema";
import {SchemaEditorStore} from "../../component/Schema/EditorStore";

const Wrapper = posed.div({
    hidden: {
        height: 0,
        marginTop: 0,
    },
    visible: {
        height: 'auto',
        marginTop: '24px',
    }
});

class ArticleDetailMeta extends React.PureComponent {
    /**
     * @type {{schemaEditorStore: boolean|EditorStore}}
     */
    state = {schemaEditorStore: false};

    fetch(schema, force = false) {
        const {loadSchema} = this.props;
        loadSchema(schema, force);
    }

    componentDidMount() {
        if(this.props.meta) {
            this.createMetaStore();
        }
        if(this.props.meta._schema) {
            this.fetch(this.props.meta._schema);
        }
    }

    componentDidUpdate(prevProps) {
        if(prevProps.meta._schema !== this.props.meta._schema) {
            this.fetch(this.props.meta._schema);
        }
        if(prevProps.meta !== this.props.meta) {
            this.createMetaStore();
        }
    }

    createMetaStore = () => {
        if(!this.props.meta) {
            return;
        }

        let schemaEditorStore = SchemaEditorStore.createFromData(this.props.meta);
        this.setState({schemaEditorStore});
    };

    handleStateChange = (newStore) => {
        this.setState({schemaEditorStore: newStore}, () => {
            if(this.props.onChange) {
                this.props.onChange(newStore.data);
            }
        });
    };

    handleSave = () => {
        if(!this.props.pushMeta) {
            console.error('ArticleMeta: can not save, no prop `pushMeta` exits.');
            return;
        }

        if(!this.state.schemaEditorStore) {
            return;
        }

        this.props.pushMeta(this.state.schemaEditorStore.data);
    };

    render() {
        const {
            t, theme,
            open, saving,
            isSchemaLoaded, schemas
        } = this.props;

        const schema = this.props.meta._schema || false;

        return (
            <Wrapper style={{overflow: 'hidden'}} withParent={false} pose={(open ? 'visible' : 'hidden')}>

                <h3 style={{width: '100%'}}>{t('editmeta.title')}</h3>

                {('progress' === isSchemaLoaded(schema) || 'error' === isSchemaLoaded(schema) ?
                    <Loading
                        style={{
                            wrapper: {margin: 0},
                        }}
                        errorWarning={'error' === isSchemaLoaded(schema)}
                    /> :
                    null)}

                {this.props.meta && true === isSchemaLoaded(schema) ?
                    <React.Fragment>
                        <SchemaEditor
                            t={t}
                            store={this.state.schemaEditorStore}
                            onChange={this.handleStateChange}
                            schema={schemas[schema]}
                            tScope={'article-meta'}
                        />

                        <Button onClick={this.handleSave}>{t('editmeta.save')}</Button>

                        {saving ?
                            ('progress' === saving || 'error' === saving ?
                                <React.Fragment>
                                    <Loading style={{
                                        wrapper: {margin: 0},
                                        item: ('error' === saving ? {borderColor: theme.errorColor} : {}),
                                    }}/>

                                </React.Fragment> :
                                ('success' === saving ?
                                    <MdDone/> : null))
                            : null}
                    </React.Fragment> :
                    (!this.props.meta._schema || (false !== isSchemaLoaded(schema) && 'progress' !== isSchemaLoaded(schema))) ? <p>{t('view-error.no-editable-meta')}</p> : null
                }
            </Wrapper>
        )
    };
}

export default storeSchema(withTheme(ArticleDetailMeta));
