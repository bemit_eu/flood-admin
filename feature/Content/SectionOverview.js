import React from 'react';
import {withNamespaces} from "react-i18next";

import ArticleCreate from './ArticleCreate';
import SectionEdit from './SectionEdit';
import SectionDelete from './SectionDelete';
import {withFeatureManager} from "../../component/FeatureManager";


class SectionOverview extends React.PureComponent {
    render() {
        const {
            t, match,
            isAbilityEnabled
        } = this.props;

        return (
            <div>
                <h1>{t('title')}</h1>
                {match.params.section ?
                    <p>{t('choosen-section-is')}{match.params.section}</p> :
                    <p>{t('choose-section')}</p>}

                {match.params.section && isAbilityEnabled('content', 'create-article') ?
                    <React.Fragment>
                        <h2>{t('create-article-in-section')}</h2>
                        <ArticleCreate section={match.params.section}/>
                    </React.Fragment> : null}

                {!match.params.section && isAbilityEnabled('content', 'create-section') ?
                    <React.Fragment>
                        <h2>{t('create-section')}</h2>
                        <SectionEdit create={true}/>
                    </React.Fragment> : null}

                {match.params.section && isAbilityEnabled('content', 'update-section') ?
                    <div style={{marginBottom: '60px'}}>
                        <h2>{t('update-section')}</h2>
                        <SectionEdit section={match.params.section}/>
                        <div style={{marginTop: '12px'}}>
                            <p style={{marginBottom: '3px'}}>{t('delete-section-warning')}</p>
                            <SectionDelete section={match.params.section}/>
                        </div>
                    </div> : null}
            </div>
        );
    }
}

export default withNamespaces('page--content-section')(withFeatureManager(SectionOverview));
