import React from 'react';

const initialState = {
    data: {},
    loaded: false
};

class ArticleDetailHelp extends React.Component {

    state = initialState;

    componentWillUnmount() {
        this.setState(initialState);
    }

    fetch() {
        this.setState({loaded: 'progress'});
    }

    componentDidUpdate(prevProps) {
    }

    componentDidMount() {
        this.fetch();
    }

    render() {
        //const {t} = this.props;

        return (
            <React.Fragment>
                {
                    ('progress' === this.state.loaded || 'error' === this.state.loaded ?
                        null :
                        null)
                }
            </React.Fragment>
        )
    };
}

export default ArticleDetailHelp;
