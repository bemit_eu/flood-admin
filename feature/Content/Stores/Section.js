import React from 'react';
import {sortString} from "@formanta/js";
import {withContextConsumer} from "@formanta/react";
import {ApiRequest, DELETE, GET, POST, PUT} from "../../../lib/ApiRequest";
import {cancelPromise} from "../../../component/cancelPromise";
import {withApiControl} from "../../../component/ApiControl";
import {Pushing, withPusher} from "../../../component/Pusher/Pusher";

const StoreSectionContext = React.createContext({});

class StoreSectionProviderBase extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            sections: [],
            loadingSections: false,
            createSection: this.createSection,
            deleteSection: this.deleteSection,
            loadSection: this.loadSection,
            updateSection: this.updateSection,
            loadSectionList: this.loadSectionList
        };
    }

    createSection = (name) => {
        const endpoint = 'content/section';

        const {execPushing} = this.props;

        return execPushing(
            new Pushing(
                'createSection',
                endpoint,
                (resolve, reject) => {
                    const data = {
                        name
                    };
                    (new ApiRequest('api', POST, endpoint, data))
                        .debug(true)
                        .header({})
                        .exec()
                        .then((res) => {
                            if(res && res.data) {
                                resolve(res.data);
                            } else {
                                reject();
                            }
                        })
                        .catch((err) => {
                            this.props.handleApiFail(err, 'createSection');
                            reject();
                        });
                }
            )
        ).then((data) => {
            if(data.error) {
            } else {
                this.loadSectionList(true);
            }
            return data;
        });
    };

    updateSection = (section, name) => {
        const {execPushing} = this.props;

        const endpoint = 'content/section/' + section;

        return execPushing(
            new Pushing(
                'updateSection',
                endpoint,
                (resolve, reject) => {
                    (new ApiRequest('api', PUT, endpoint, {name}))
                        .debug(true)
                        .header({})
                        .exec()
                        .then((res) => {
                            if(res && res.data) {
                                resolve(res.data);
                                this.loadSectionList(true);
                            } else {
                                reject();
                            }
                        })
                        .catch((err) => {
                            this.props.handleApiFail(err, 'updateSection');
                            reject();
                        });
                }
            )
        );
    };

    deleteSection = (section) => {
        const endpoint = 'content/section/' + section;

        const {execPushing} = this.props;

        return execPushing(
            new Pushing(
                'deleteSection',
                endpoint,
                (resolve, reject) => {
                    (new ApiRequest('api', DELETE, endpoint))
                        .debug(true)
                        .header({})
                        .exec()
                        .then((res) => {
                            if(res && res.data) {
                                resolve(res.data);
                            } else {
                                reject();
                            }
                        })
                        .catch((err) => {
                            this.props.handleApiFail(err, 'deleteSection');
                            reject();
                        });
                }
            )
        ).then((data) => {
            if(data.error) {
            } else {
                this.loadSectionList(true);
            }
            return data;
        });
    };

    loadSection = (section) => {
        return (new ApiRequest('api', GET, 'content/section/' + section))
            .debug(true)
            .header({})
            .exec()
            .then((res) => {
                if(res && res.data) {
                    if(res.data.error) {
                        return Promise.reject(res);
                    }
                    return res.data;
                } else {
                    return Promise.reject(res);
                }
            })
            .catch((err) => {
                this.props.handleApiFail(err, 'loadSection');
                return Promise.reject(err);
            });
    };

    loadSectionList = (forceLoad = false) => {
        if(false === this.state.loadingSections || forceLoad) {

            this.setState({loadingSections: 'progress'});

            (new ApiRequest('api', GET, 'content/section'))
                .debug(true)
                .exec()
                .then((res) => {
                    if(res && res.data) {
                        this.setState({
                            sections: sortString(res.data, 'name'),
                            loadingSections: true
                        });
                    } else {
                        this.setState({
                            loadingSections: 'error'
                        });
                    }
                })
                .catch((err) => {
                    this.props.handleApiFail(err, 'listing Sections', true, () => this.loadSectionList(forceLoad));
                    if(err && !err.cancelled) {
                        this.setState({
                            loadingSections: 'error'
                        });
                    }
                });
        }
    };

    render() {
        return (
            <StoreSectionContext.Provider value={this.state}>
                {this.props.children}
            </StoreSectionContext.Provider>
        );
    }
}

const StoreSectionProvider = cancelPromise(withPusher(withApiControl(StoreSectionProviderBase)));

const storeSection = withContextConsumer(StoreSectionContext.Consumer);

export {StoreSectionProvider, storeSection};