import React from 'react';
import {withNamespaces} from 'react-i18next';

import {withStyles} from '../../lib/theme';
import Uploader from '../../component/FileUpload/Uploader';
import Explorer from '../../component/FileExplorer/Explorer';
import StyleGrid from "../../lib/StyleGrid";

const styles = {
    wrapper: {
        display: 'flex',
        flexWrap: 'wrap',
        [StyleGrid.mdUp]: {
            flexWrap: 'no-wrap',
        }
    },
    uploader: {
        width: '100%',
        [StyleGrid.mdUp]: {
            width: '50%',
            order: 1,
        },
        [StyleGrid.lgUp]: {
            width: '30%',
        }
    },
    explorer: {
        width: '100%',
        [StyleGrid.mdUp]: {
            width: '50%',
            paddingRight: '12px',
            order: 0,
        },
        [StyleGrid.lgUp]: {
            width: '70%',
        }
    }
};

class Overview extends React.Component {
    state = {
        currentDir: false
    };

    onSwitchDir = (dir) => {
        this.setState({
            currentDir: dir
        });
    };

    render() {
        const {t, classes} = this.props;

        return (
            <div>
                <h1>{t('title')}</h1>
                <div className={classes.wrapper}>
                    <div className={classes.uploader}>
                        <Uploader target={this.state.currentDir}/>
                    </div>
                    <div className={classes.explorer}>
                        <Explorer onSwitchDir={this.onSwitchDir}/>
                    </div>
                </div>
            </div>
        );
    }
}

export default withNamespaces('page--media')(withStyles(styles)(Overview));
