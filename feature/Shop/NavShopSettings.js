import React from 'react';
import {withNamespaces} from "react-i18next";
import {MdStore, MdLocalShipping, MdAccountBalance} from "react-icons/md";

import RouteCascaded from '../../lib/RouteCascaded';

import Nav from "../../component/Nav/Nav";
import Sidebar from "../../layout/ControlPanel/Sidebar";

class NavShop extends React.Component {

    render() {
        const {t} = this.props;

        const navContent = (match) => {
            let content = [];

            content.push({
                path: match.url + '/store',
                lbl: {icon: (p) => <MdStore className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.settings-nav.store')},
            });
            content.push({
                path: match.url + '/shipping',
                lbl: {icon: (p) => <MdLocalShipping className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.settings-nav.shipping')},
            });
            content.push({
                path: match.url + '/taxes',
                lbl: {icon: (p) => <MdAccountBalance className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.settings-nav.taxes')},
            });

            return content;
        };

        return (
            <React.Fragment>
                <Sidebar
                    center={(props => <Nav path='/shop/settings' content={navContent} notxt={props.notxt}/>)}
                    textToggle={true}
                />

                {this.props.routes ? this.props.routes.map((route, i) => (
                    route.sidebar ?
                        <RouteCascaded key={i} {...route} component={route.sidebar}/> :
                        null
                )) : null}
            </React.Fragment>
        )
    };
}

export default withNamespaces('page--shop')(NavShop);
