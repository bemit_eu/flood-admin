import React from 'react';
import {withNamespaces} from "react-i18next";
import {MdSettings, MdShoppingBasket, MdReceipt, MdGridOn, MdCamera} from "react-icons/md";

import RouteCascaded from '../../lib/RouteCascaded';

import Nav from "../../component/Nav/Nav";
import Sidebar from "../../layout/ControlPanel/Sidebar";
import {needsPermission} from "../../feature/AuthPermission";
import {FEATURE_SHOP_GROUP, FEATURE_SHOP_ORDER, FEATURE_SHOP_PRODUCT, FEATURE_SHOP_SETTING, PERM_ADMIN} from "../../permissions";

class NavShop extends React.Component {

    render() {
        const {t, i18n} = this.props;

        const navContent = (match) => {
            let content = [];

            content.push({
                path: match.url + '/settings',
                lbl: {icon: (p) => <MdSettings className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.settings')},
                wrap: needsPermission([PERM_ADMIN, FEATURE_SHOP_SETTING])((props) => props.children)
            });

            content.push({
                path: match.url + '/products',
                lbl: {icon: (p) => <MdShoppingBasket className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.products')},
                wrap: needsPermission([PERM_ADMIN, FEATURE_SHOP_GROUP])((props) => props.children)
            });

            content.push({
                path: match.url + '/orders/all',
                lbl: {icon: (p) => <MdReceipt className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.orders')},
                wrap: needsPermission([PERM_ADMIN, FEATURE_SHOP_ORDER])((props) => props.children)
            });

            content.push({
                path: match.url + '/inventory',
                lbl: {icon: (p) => <MdGridOn className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.inventory')},
                wrap: needsPermission([PERM_ADMIN, FEATURE_SHOP_ORDER])((props) => props.children)
            });
            content.push({
                path: match.url + '/scan',
                lbl: {icon: (p) => <MdCamera className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.scan')},
                wrap: needsPermission([PERM_ADMIN, FEATURE_SHOP_PRODUCT, FEATURE_SHOP_ORDER])((props) => props.children)
            });

            return content;
        };

        return (
            <React.Fragment>
                <Sidebar
                    center={(props => <Nav path='/shop' content={navContent} notxt={props.notxt} down={'/' + i18n.languages[0] + '/shop/scan'}/>)}
                    textToggle={true}
                />

                {this.props.routes ? this.props.routes.map((route, i) => (
                    route.sidebar ?
                        <RouteCascaded key={i} {...route} component={route.sidebar}/> :
                        null
                )) : null}
            </React.Fragment>
        )
    };
}

export default withNamespaces('page--shop')(NavShop);
