import React from 'react';
import {withNamespaces} from 'react-i18next';

import {cancelPromise} from "../../component/cancelPromise";
import {withBackend} from "../BackendConnect";
import {withAuthPermission} from "../AuthPermission";
import QrReader from 'react-qr-reader';


const initialState = {
    qr: null,
};

class Inventory extends React.Component {

    state = initialState;

    componentDidMount() {
    }

    componentDidUpdate(prevProps) {
    }

    componentWillUnmount() {
        this.setState(initialState);
    }

    render() {
        const {
            t,
        } = this.props;

        return (
            <React.Fragment>
                <h1 style={{marginBottom: '12px'}}>{t('scan.title')}</h1>
                <p style={{margin: '12px 0 18px 0'}}>{t('scan.desc')}</p>

                <QrReader
                    delay={300}
                    onError={(e) => console.log(e)}
                    onScan={(qr) => this.setState({qr})}
                    style={{width: '100%', maxWidth: '320px', margin: '0 auto'}}
                />
                <p style={{margin: '3px 0 6px 0', textAlign: 'center'}}>
                    {this.state.qr ? t('scan.found-qr') : t('scan.no-qr')}&nbsp;
                    {this.state.qr}
                </p>
            </React.Fragment>
        )
    };
}

export default withNamespaces('page--shop')(cancelPromise(withAuthPermission(withBackend(Inventory))));
