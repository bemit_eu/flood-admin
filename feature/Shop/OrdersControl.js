import React from 'react';
import {withNamespaces} from "react-i18next";

import {useTheme, createUseStyles} from '../../lib/theme';
import {Select, InputText} from "../../component/Form";

const useStyles = createUseStyles({
    wrapper: {
        display: 'flex'
    },
    qty: {
        width: '20%',
        marginRight: '6px'
    },
    country: {
        width: '20%'
    },
    timeRange: {
        display: 'flex',
        flexWrap: 'wrap',
        marginRight: '6px'
    },
    timeRangeFrom: {
        width: '20%',
        flexGrow: '3',
        marginRight: '6px'
    },
    timeRangeTo: {
        width: '20%',
        flexGrow: '3',
        marginLeft: '6px'
    }
});

const OrdersControl = (props) => {
    const {t,} = props;
    const theme = useTheme();
    const classes = useStyles(theme);

    return (
        <div className={classes.wrapper}>
            <div className={classes.qty}>
                <p style={{marginTop: 0, marginBottom: theme.gutter.base}}>{t('controls.qty')}</p>
                <Select>
                    <option>50</option>
                    <option>100</option>
                    <option>250</option>
                </Select>
            </div>
            <div className={classes.timeRange}>
                <p style={{width: '100%', marginTop: 0, marginBottom: theme.gutter.base}}>{t('controls.time-range')}</p>
                <div className={classes.timeRangeFrom}>
                    <label>{t('controls.time-range-from')}</label>
                    <InputText type='date' name={'from'}/>
                </div>
                <div className={classes.timeRangeTo}>
                    <label>{t('controls.time-range-to')}</label>
                    <InputText type='date' name={'to'}/>
                </div>
            </div>
            <div className={classes.country}>
                <p style={{marginTop: 0, marginBottom: theme.gutter.base, whiteSpace: 'pre'}}>{t('controls.by-country')}</p>
                <Select>
                    <option>*</option>
                    <option>de-DE</option>
                    <option>en-US</option>
                </Select>
            </div>
        </div>
    )
};

export default withNamespaces('page--shop-order')(OrdersControl);
