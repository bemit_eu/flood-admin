import React from 'react';
import {withNamespaces} from "react-i18next";

import ProductCreate from './ProductCreate';
import {needsPermission} from "../../feature/AuthPermission";
import {PERM_ADMIN, FEATURE_SHOP_PRODUCT} from "../../permissions";
import GroupEdit from "./GroupEdit";
import GroupDelete from "./GroupDelete";

const CreateProduct = needsPermission([PERM_ADMIN, FEATURE_SHOP_PRODUCT])((props) => (
    <React.Fragment>
        <h2>{props.t('group.create-product-in-group')}</h2>
        <ProductCreate group={props.group}/>
    </React.Fragment>
));

class GroupOverview extends React.Component {
    render() {
        const {
            t, match
        } = this.props;

        return (
            <div>
                <h1>{t('group.title')}</h1>
                {this.props.match.params.group ?
                    <p>{t('group.choosen-group-is')} {this.props.match.params.group}</p> :
                    <p>{t('group.choose')}</p>}

                {this.props.match.params.group ?
                    <CreateProduct group={this.props.match.params.group} t={t}/>
                    : null}

                {!match.params.group ?
                    <React.Fragment>
                        <h2>{t('group.create')}</h2>
                        <GroupEdit create={true}/>
                    </React.Fragment> : null}

                {match.params.group ?
                    <div style={{marginBottom: '60px'}}>
                        <h2>{t('group.update')}</h2>
                        <GroupEdit group={match.params.group}/>
                        <div style={{marginTop: '12px'}}>
                            <p style={{marginBottom: '3px'}}>{t('group.delete-warning')}</p>
                            <GroupDelete group={match.params.group}/>
                        </div>
                    </div> : null}
            </div>
        );
    }
}

export default withNamespaces('page--shop')(GroupOverview);
