import React from 'react';
import {withNamespaces} from 'react-i18next';
import {ID} from '@formanta/js';
import {withTimeout} from '@formanta/react';
import {MdCheck, MdChevronLeft, MdEdit, MdSave as MdSa} from "react-icons/md";
import {withStyles, withTheme} from '../../lib/theme';

import {storeWarehouse} from "./Stores/Warehouse";
import {storeStock} from "./Stores/Stock";

import {cancelPromise} from "../../component/cancelPromise";
import Loading from "../../component/Loading";
import {ButtonSmall, Label, InputText, InputCheckbox} from "../../component/Form";
import {storeStockItem} from "./Stores/StockItem";
import {DropDown} from "../../lib/DropDown";
import ProductInventoryActivate from './ProductInventoryActivate';
import ButtonInteractiveDelete from "../../component/ButtonInteractiveDelete";
import LoadingSmallBase from "../../component/LoadingSmall";

const LoadingSmall = (props) => (
    'progress' === props.progress || 'error' === props.progress ?
        <div style={{position: 'relative'}}>
            <LoadingSmallBase
                style={{
                    wrapper: {margin: 0},
                }}
                errorWarning={'error' === props.progress}/>
        </div> : true === props.progress ? <MdCheck/> : null
);

const MdSave = (props) => <MdSa style={{float: 'left', display: 'block', marginRight: '4px'}}/>;

const styles = {
    wrapper: {
        overflow: 'hidden'
    },
    table: {
        borderCollapse: 'collapse',
    },
    tr: {
        borderCollapse: 'collapse',
    },
    th: {
        borderCollapse: 'collapse',
        padding: '3px 3px 3px 0',
        textAlign: 'left',
    },
    td: {
        borderCollapse: 'collapse',
        padding: '3px',
        '&:nth-child(2)': {
            paddingRight: '12px',
            fontFamily: (theme) => theme.monospace,
        },
        '&:last-child': {
            paddingRight: 0
        }
    },
    tdTotal: {
        paddingTop: '6px',
        '&:first-child': {
            paddingLeft: 0
        }
    },
};

const DropDownRow = (props) => (
    <React.Fragment>
        {props.label && props.input && props.btn ?
            <tr className={props.className}>
                <td colSpan={2} style={{padding: props.open ? '2px 4px 2px 0' : 0}}>
                    <DropDown open={props.open} style={{display: 'flex'}}>
                        {props.label}
                    </DropDown>
                </td>
                <td colSpan={2} style={{padding: props.open ? '2px 0' : 0}}>
                    <DropDown open={props.open} style={{display: 'flex'}}>
                        {props.input}
                    </DropDown>
                </td>
                <td colSpan={1} style={{padding: props.open ? '2px 0 2px 4px' : 0}}>
                    <DropDown open={props.open} style={{display: 'flex'}}>
                        {props.btn}
                    </DropDown>
                </td>
                <td colSpan={1} style={{padding: props.open ? '2px 0 2px 4px' : 0}}>
                    <DropDown open={props.progress} style={{display: 'flex'}}>
                        <LoadingSmall progress={props.progress}/>
                    </DropDown>
                </td>
            </tr> : null}
        {props.children ?
            <tr className={props.className}>
                <td colSpan={6} style={{padding: props.open ? '2px' : 0}}>
                    <DropDown open={props.open} style={{display: 'flex'}}>
                        {props.children}
                    </DropDown>
                </td>
            </tr> : null}
    </React.Fragment>
);

class ProductInventoryDetails extends React.Component {
    state = {
        saving: false,
        selectedWarehouse: '-',
        selectedStock: '-',
        selectedName: '',
        selectedSku: '',
        selectedAmountAvailable: '0',
        inpAvailableAdd: '0',
        openAvailableAdd: false,
        savingAvailableAdd: false,
        inpAvailableRemove: '0',
        openAvailableRemove: false,
        savingAvailableRemove: false,
        inpReserve: '0',
        openReserve: false,
        savingReserve: false,
        inpReservedRemove: '0',
        openReservedRemove: false,
        savingReservedRemove: false,
        inpUnreserve: '0',
        openUnreserve: false,
        savingUnreserve: false,
        create: false,
        editLocation: false,
        openLocation: false,
        openHistory: false,
        openOrders: false,
    };

    constructor(props) {
        super(props);
        this.id = ID();
    }

    componentDidMount() {
        const {
            loadWarehouseList,
            loadStockList
        } = this.props;
        loadWarehouseList();
        loadStockList();
    }

    apiQuantityChange = (progress, type, value, reset) => {
        const {
            stockItemDetails,
            updateStockItemQuantity,
            setTimeout,
        } = this.props;

        if(!value) {
            return;
        }

        this.setState({[progress]: 'progress'});
        updateStockItemQuantity(stockItemDetails.id, {[type]: value})
            .then((data) => {
                this.setState({[progress]: true});
                if(reset) {
                    reset();
                }
                setTimeout(() => this.setState({[progress]: false}), 1500);
            })
            .catch(err => {
                this.setState({[progress]: 'error'});
            });
    };

    apiAvailableAdd = () => {
        this.apiQuantityChange('savingAvailableAdd', 'available_add', this.state.inpAvailableAdd,
            () => this.setState({inpAvailableAdd: '0'})
        );
    };

    apiAvailableRemove = () => {
        this.apiQuantityChange('savingAvailableRemove', 'available_remove', this.state.inpAvailableRemove,
            () => this.setState({inpAvailableRemove: '0'})
        );
    };

    apiReserve = () => {
        this.apiQuantityChange('savingReserve', 'available_reserve', this.state.inpReserve,
            () => this.setState({inpReserve: '0'})
        );
    };

    apiReservedRemove = () => {
        this.apiQuantityChange('savingReservedRemove', 'reserved_remove', this.state.inpReservedRemove,
            () => this.setState({inpReservedRemove: '0'})
        );
    };

    apiUnreserve = () => {
        this.apiQuantityChange('savingUnreserve', 'reserved_unreserve', this.state.inpUnreserve,
            () => this.setState({inpUnreserve: '0'})
        );
    };

    handleKeyDown = (e, action) => {
        if(e.keyCode === 27) {
            //this.setState({openAddDir: false});
        }

        if(e.keyCode === 13 && action) {
            e.preventDefault();
            action();
        }
    };

    render() {
        const {
            t, theme, classes,
            displayCompact, product,
            loadingWarehouses,
            loadingStocks,
            stockItemDetails,
            getStockItemsLoaded,
        } = this.props;
        const {saving} = this.state;

        const error = (
            'error' === loadingWarehouses ||
            'error' === loadingStocks ||
            'error' === saving
        );
        const loading = (
            'progress' === loadingWarehouses ||
            'progress' === loadingStocks ||
            'progress' === saving
        );

        return (
            <React.Fragment>
                {!loading && !error ?
                    <div style={{display: 'flex', columnGap: '12px', flexDirection: 'column'}}>
                        <table className={classes.table}>
                            <tbody>
                            <tr className={classes.tr}>
                                <th className={classes.th}>{t('inventory.available')}:</th>
                                <td className={classes.td}>{stockItemDetails.amount_available}</td>
                                <td className={classes.td}>
                                    <ButtonSmall style={{width: '100%'}}
                                                 active={this.state.openAvailableAdd}
                                                 onClick={() => this.setState({
                                                     openAvailableAdd: !this.state.openAvailableAdd,
                                                     openAvailableRemove: false,
                                                     openReserve: false
                                                 })}
                                    >{t('common:addNoun')}</ButtonSmall>
                                </td>
                                <td className={classes.td}>
                                    <ButtonSmall style={{width: '100%'}}
                                                 active={this.state.openAvailableRemove}
                                                 onClick={() => this.setState({
                                                     openAvailableAdd: false,
                                                     openAvailableRemove: !this.state.openAvailableRemove,
                                                     openReserve: false
                                                 })}
                                    >{t('common:removeNoun')}</ButtonSmall>
                                </td>
                                <td className={classes.td}>
                                    <ButtonSmall style={{width: '100%'}}
                                                 active={this.state.openReserve}
                                                 onClick={() => this.setState({
                                                     openAvailableAdd: false,
                                                     openAvailableRemove: false,
                                                     openReserve: !this.state.openReserve
                                                 })}
                                    >{t('inventory.reserve')}</ButtonSmall>
                                </td>
                            </tr>
                            <DropDownRow className={classes.tr} open={this.state.openAvailableAdd}
                                         label={<Label htmlFor={this.id + '--available-add'} style={{flexShrink: 0}}>{t('inventory.titles.available-add')}:</Label>}
                                         input={<InputText id={this.id + '--available-add'} style={{minWidth: '50px', width: null,}} type={'number'} size={4} min={0}
                                                           onKeyUp={(e) => this.handleKeyDown(e, this.apiAvailableAdd)}
                                                           value={this.state.inpAvailableAdd}
                                                           onChange={e => this.setState({inpAvailableAdd: e.target.value})}
                                                           tabIndex={this.state.openAvailableAdd ? null : '-1'}/>}
                                         btn={<ButtonSmall style={{width: '100%'}}
                                                           onClick={this.apiAvailableAdd} tabIndex={this.state.openAvailableAdd ? null : '-1'}
                                         ><MdSave/> {t('common:addNoun')}</ButtonSmall>}
                                         progress={this.state.savingAvailableAdd}
                            />
                            <DropDownRow className={classes.tr} open={this.state.openAvailableRemove}
                                         label={<Label htmlFor={this.id + '--available-remove'} style={{flexShrink: 0}}>{t('inventory.titles.available-remove')}:</Label>}
                                         input={<InputText id={this.id + '--available-remove'} style={{minWidth: '50px', width: null,}} type={'number'} size={4} min={0}
                                                           onKeyUp={(e) => this.handleKeyDown(e, this.apiAvailableRemove)}
                                                           value={this.state.inpAvailableRemove}
                                                           onChange={e => this.setState({inpAvailableRemove: e.target.value})}
                                                           tabIndex={this.state.openAvailableRemove ? null : '-1'}/>}
                                         btn={<ButtonSmall style={{width: '100%'}}
                                                           onClick={this.apiAvailableRemove} tabIndex={this.state.openAvailableRemove ? null : '-1'}
                                         ><MdSave/> {t('common:removeNoun')}</ButtonSmall>}
                                         progress={this.state.savingAvailableRemove}
                            />
                            <DropDownRow className={classes.tr} open={this.state.openReserve}
                                         label={<Label htmlFor={this.id + '--available-reserve'} style={{flexShrink: 0}}>{t('inventory.titles.available-reserve')}:</Label>}
                                         input={<InputText id={this.id + '--available-reserve'} style={{minWidth: '50px', width: null,}} type={'number'} size={4} min={0}
                                                           onKeyUp={(e) => this.handleKeyDown(e, this.apiReserve)}
                                                           value={this.state.inpReserve}
                                                           onChange={e => this.setState({inpReserve: e.target.value})}
                                                           tabIndex={this.state.openReserve ? null : '-1'}/>}
                                         btn={<ButtonSmall style={{width: '100%'}}
                                                           onClick={this.apiReserve} tabIndex={this.state.openReserve ? null : '-1'}
                                         ><MdSave/> {t('inventory.reserve')}</ButtonSmall>}
                                         progress={this.state.savingReserve}
                            />

                            <tr className={classes.tr}>
                                <th className={classes.th}>{t('inventory.reserved')}:</th>
                                <td className={classes.td}>{stockItemDetails.amount_reserved}</td>
                                <td className={classes.td}/>
                                <td className={classes.td}>
                                    <ButtonSmall style={{width: '100%'}}
                                                 active={this.state.openReservedRemove}
                                                 onClick={() => this.setState({
                                                     openReservedRemove: !this.state.openReservedRemove,
                                                     openUnreserve: false,
                                                 })}
                                    >{t('common:removeNoun')}</ButtonSmall>
                                </td>
                                <td className={classes.td}>
                                    <ButtonSmall style={{width: '100%'}}
                                                 active={this.state.openUnreserve}
                                                 onClick={() => this.setState({
                                                     openReservedRemove: false,
                                                     openUnreserve: !this.state.openUnreserve
                                                 })}
                                    >{t('inventory.unreserve')}</ButtonSmall>
                                </td>
                            </tr>
                            <DropDownRow className={classes.tr} open={this.state.openReservedRemove}
                                         label={<Label htmlFor={this.id + '--reserve-remove'} style={{flexShrink: 0}}>{t('inventory.titles.reserved-remove')}:</Label>}
                                         input={<InputText id={this.id + '--reserve-remove'} style={{minWidth: '50px', width: null,}} type={'number'} size={4} min={0}
                                                           onKeyUp={(e) => this.handleKeyDown(e, this.apiReservedRemove)}
                                                           value={this.state.inpReservedRemove}
                                                           onChange={e => this.setState({inpReservedRemove: e.target.value})}
                                                           tabIndex={this.state.openReservedRemove ? null : '-1'}/>}
                                         btn={<ButtonSmall style={{width: '100%'}}
                                                           onClick={this.apiReservedRemove} tabIndex={this.state.openReservedRemove ? null : '-1'}
                                         ><MdSave/> {t('common:removeNoun')}</ButtonSmall>}
                                         progress={this.state.savingReservedRemove}
                            />
                            <DropDownRow className={classes.tr} open={this.state.openUnreserve}
                                         label={<Label htmlFor={this.id + '--reserve-reserve'} style={{flexShrink: 0}}>{t('inventory.unreserve')}:</Label>}
                                         input={<InputText id={this.id + '--reserve-reserve'} style={{minWidth: '50px', width: null,}} type={'number'} size={4} min={0}
                                                           onKeyUp={(e) => this.handleKeyDown(e, this.apiUnreserve)}
                                                           value={this.state.inpUnreserve}
                                                           onChange={e => this.setState({inpUnreserve: e.target.value})}
                                                           tabIndex={this.state.openUnreserve ? null : '-1'}/>}
                                         btn={<ButtonSmall style={{width: '100%'}}
                                                           onClick={this.apiUnreserve} tabIndex={this.state.openUnreserve ? null : '-1'}
                                         ><MdSave/> {t('inventory.unreserve')}</ButtonSmall>}
                                         progress={this.state.savingUnreserve}
                            />

                            <tr className={classes.tr}>
                                <td colSpan={6} style={{paddingTop: '9px', borderBottom: '1px solid ' + theme.textColorLight}}/>
                            </tr>
                            <tr className={classes.tr}>
                                <th className={classes.th + ' ' + classes.tdTotal}>{t('common:total')}:</th>
                                <td className={classes.td + ' ' + classes.tdTotal}>{(stockItemDetails.amount_available * 1) + (stockItemDetails.amount_reserved * 1)}</td>
                                <td className={classes.td + ' ' + classes.tdTotal}>
                                    <ButtonSmall style={{width: '100%'}}
                                                 active={this.state.openHistory}
                                                 onClick={() => this.setState({openHistory: !this.state.openHistory})}
                                    >{t('common:history')}</ButtonSmall>
                                </td>
                                <td className={classes.td + ' ' + classes.tdTotal}>
                                    <ButtonSmall style={{width: '100%'}}
                                                 active={this.state.openOrders}
                                                 onClick={() => this.setState({openOrders: !this.state.openOrders})}
                                    >{t('common:orders')}</ButtonSmall>
                                </td>
                            </tr>

                            <DropDownRow className={classes.tr} open={this.state.openHistory}>
                                <p>{t('common:history')}</p>
                            </DropDownRow>

                            <DropDownRow className={classes.tr} open={this.state.openOrders}>
                                <p>{t('common:orders')}</p>
                            </DropDownRow>

                            </tbody>
                        </table>

                        <h4 style={{margin: displayCompact ? '9px 0 3px 0' : '28px 0 3px 0'}}>{t('inventory.titles.stock-item-location')}
                            {displayCompact ? <button onClick={() => this.setState({openLocation: !this.state.openLocation})}
                                                      style={{margin: '0 0 0 3px', padding: '4px', verticalAlign: 'sub'}}
                            ><MdChevronLeft color={theme.textColor} style={{display: 'block', transition: 'transform ' + theme.transition, transform: 'rotate(' + (this.state.openLocation ? '' : '-') + '90deg)'}}/></button> : null}
                        </h4>
                        <DropDown open={!displayCompact || this.state.openLocation}>
                            <div style={{display: 'flex', columnGap: '12px'}}>
                                <p style={{margin: 0}}>
                                    <span style={{display: 'block', borderBottom: '1px solid ' + theme.textColorLight, padding: '0 3px 0 0'}}>Warehouse</span>
                                    <span style={{padding: '0 3px 0 0'}}>{stockItemDetails.warehouse_name}</span>
                                </p>
                                <p style={{margin: 0}}>
                                    <span style={{display: 'block', borderBottom: '1px solid ' + theme.textColorLight, padding: '0 3px'}}>Stock</span>
                                    <span style={{padding: '0 3px'}}>{stockItemDetails.stock_name}</span>
                                </p>
                                <p style={{margin: 0}}>
                                    <span style={{display: 'block', borderBottom: '1px solid ' + theme.textColorLight, padding: '0 3px'}}>Stock-Item</span>
                                    <span style={{padding: '0 3px'}}>{stockItemDetails.name}</span>
                                </p>
                                <p style={{margin: 0}}>
                                    <span style={{display: 'block', borderBottom: '1px solid ' + theme.textColorLight, padding: '0 3px'}}>SKU</span>
                                    <span style={{padding: '0 3px'}}>{stockItemDetails.sku}</span>
                                </p>
                            </div>

                            <div style={{display: 'flex', columnGap: '4px', alignItems: 'middle'}}>
                                <ButtonInteractiveDelete style={{color: theme.textColor}}>{t('common:deactivateNoun')} </ButtonInteractiveDelete>
                                <InputCheckbox style={{margin: 'auto 0 auto 12px'}}/>
                                <Label>{t('inventory.delete-stock-item')}</Label>
                            </div>

                            <h4 style={{margin: '12px 0 3px 0'}}>
                                {t('inventory.titles.edit-location')}
                                <button onClick={() => this.setState({editLocation: !this.state.editLocation})}
                                        style={{margin: '0 0 0 3px', padding: '4px', verticalAlign: 'sub'}}
                                ><MdEdit color={theme.textColor} style={{display: 'block'}}/></button>
                            </h4>
                            <DropDown open={this.state.editLocation}>
                                <ProductInventoryActivate id={product}
                                                          product_no={this.props.product_no}
                                                          product_name={this.props.product_name}
                                                          localeActive={this.props.localeActive}
                                />
                            </DropDown>
                        </DropDown>
                    </div> : null}

                {loading || error || 'progress' === getStockItemsLoaded(this.state.selectedStock) || 'error' === getStockItemsLoaded(this.state.selectedStock) ?
                    <div style={{position: 'relative'}}>
                        <Loading
                            center={true}
                            style={{
                                wrapper: {margin: 0},
                            }}
                            errorWarning={error}
                        >
                            <div style={{margin: '4px 0 0 0'}}>
                                {'progress' === loadingWarehouses || 'error' === loadingWarehouses ? <p style={{textAlign: 'center', margin: '2px 0 2px 0'}}>{t('inventory.loading-warehouses')}</p> : null}
                                {'progress' === loadingStocks || 'error' === loadingStocks ? <p style={{textAlign: 'center', margin: '2px 0 2px 0'}}>{t('inventory.loading-stocks')}</p> : null}
                                {'progress' === getStockItemsLoaded(this.state.selectedStock) || 'error' === getStockItemsLoaded(this.state.selectedStock) ?
                                    <p style={{textAlign: 'center', margin: '2px 0 2px 0'}}>{t('inventory.loading-stock-items')}</p> : null}
                            </div>
                        </Loading>
                    </div> : null}
            </React.Fragment>
        )
    };
}

export default withNamespaces('page--shop')(cancelPromise(withTimeout(storeWarehouse(storeStock(storeStockItem(withStyles(styles)(withTheme(ProductInventoryDetails))))))));
