import React from 'react';
import {withNamespaces} from 'react-i18next';
import {MdDone, MdFullscreen, MdFullscreenExit, MdSave, MdSync} from 'react-icons/md';
import {selectByDot, setByDot} from "@formanta/js";

import Loading from "../../component/Loading";

import {ButtonSmall as Button, Label, InputText, InputCheckbox} from '../../component/Form';

import {withTheme, withStyles} from '../../lib/theme';
import StyleGrid from '../../lib/StyleGrid';

import FormUnique from "../../component/FormUnique";
import ProductInventoryList from './ProductInventoryList';
import ProductInventory from './ProductInventory';
import {storeSchema} from "../Content/Stores/Schema";
import {DropHorizontal, DropHorizontalWrapper, styleDropDownHorizontal} from "../../lib/DropDown";
import {storeStockItem} from "./Stores/StockItem";
import {storeStock} from "./Stores/Stock";
import {storeWarehouse} from "./Stores/Warehouse";
import {ProductDetailLocaleData} from "./ProductDetailLocaleData";

const styles = {
    wrapper: {
        display: 'flex',
        flexDirection: 'column',
        overflow: 'auto',
        margin: '15px 0 45px '
    },
    groups: {
        display: 'flex',
        flexWrap: 'wrap',
        margin: '0 -3px',
        '& > .schema-edit--group': {
            width: '100%',
            padding: '0 3px',
            marginBottom: '3px',
        },
    },
    inpGroup: {
        margin: '6px 0 6px 0',
        [StyleGrid.mdUp]: {
            margin: '6px 0 18px 0',
        }
    },
    wrapperTop: {
        display: 'flex',
        flexWrap: 'wrap',
        margin: '0 0 12px 0',
        [StyleGrid.mdUp]: {}
    },
    dateWrapper: {
        display: 'flex',
        width: '100%',
        flexWrap: 'wrap',
        margin: '6px 0'
    },
    dateSingle: {
        [StyleGrid.smx]: {
            width: '100%'
        },
        '&:last-child': {
            [StyleGrid.smUp]: {
                marginLeft: '12px'
            }
        }
    },
    dateLbl: {
        marginRight: '6px',
        fontSize: '0.875rem'
    },
    dateValue: {
        fontSize: '0.875rem'
    },
    navProductInner: {
        display: 'flex',
        overflowY: 'hidden',
        overflowX: 'auto',
        marginBottom: '-1px',
        position: 'relative',
    },
    navProduct: {
        display: 'flex',
        flexDirection: 'column',
        overflowY: 'hidden',
        overflowX: 'auto',
        margin: '3px 0 0 0',
        padding: 0,
        flexShrink: 0,
        boxShadow: '0 4px 2px -2px gray',
        '& button': {
            flexShrink: 0,
            padding: '6px 12px',
            border: '1px solid transparent',
        },
        '& button.active': {},
        [StyleGrid.mdUp]: {
            '& button': {
                width: 'auto',
                borderRightWidth: 0,
            }
        },
        '& button:last-child': {
            borderRightWidth: 1,
        },
    },
    boolControl: {
        display: 'flex',
        flexWrap: 'wrap',
        [StyleGrid.smUp]: {
            flexWrap: 'no-wrap',
            //marginLeft: 'auto'
        }
    },
    inpGroupCheckBox: {
        display: 'flex',
        margin: '6px 6px 0 0',
        '&:last-child': {
            marginRight: 0,
        }
    },
    product_no: {
        flexGrow: 2,
        [StyleGrid.mdUp]: {
            marginRight: '12px',
            flexGrow: 0,
        }
    },
    pricing: {
        display: 'flex',
        flexWrap: 'wrap',
        flexGrow: 2,
        columnGap: '12px',
        [StyleGrid.mdUp]: {
            flexGrow: 0,
        }
    },
    inpGroupPricing: {
        flexGrow: 2,
        [StyleGrid.mdUp]: {
            flexGrow: 0,
        }
    }
};

const initialState = {
    productData: false,
    productDataChanged: false,
    overview: 'master',
    varySchema: {},
    variations: []
};

class ProductDetailLocaleBaseDataBase extends React.PureComponent {

    state = initialState;

    constructor(props) {
        super(props);

        this.id = FormUnique();
    }

    setProductData() {
        const {productData} = this.props;
        if(!productData.data) {
            productData.data = {};
        }

        this.setState({
            productData,
        }, () => {
            this.handleStockItem()
        });
    }

    componentDidMount() {
        this.setProductData();
        if(this.props.schema) {
            this.fetch(this.props.schema);
            this.handleBundleVary();
        }
    }

    componentDidUpdate(prevProps) {
        if(prevProps.productData !== this.props.productData) {
            this.setProductData();
        }
        if(this.props.schema && prevProps.schema !== this.props.schema) {
            this.fetch(this.props.schema);
        }
        if(this.props.schema && (
            prevProps.schema !== this.props.schema ||
            prevProps.loadingSchema[this.props.schema] !== this.props.loadingSchema[this.props.schema] ||
            prevProps.data !== this.props.data)
        ) {
            this.handleBundleVary();
        }
    }

    fetch(schema, force = false) {
        const {loadSchema} = this.props;
        loadSchema(schema, force);
    }

    handleStockItem(force = false) {
        const {
            loadStockItemForProduct,
            loadWarehouseList, loadStockList,
            productData: data
        } = this.props;
        if(data && data.id) {
            loadStockItemForProduct(data.id, force);
        }
        loadWarehouseList();
        loadStockList();
    }

    handleBundleVary() {
        const {
            schemas, isSchemaLoaded,
            schema
        } = this.props;

        const {productData: data} = this.state;

        if(true === isSchemaLoaded(schema)) {
            if(data.bundle !== schemas[schema].bundle) {
                this.updateProductData('bundle', schemas[schema].bundle);
            }

            if(schemas[schema].vary) {
                const varySchema = {type: 'object', properties: {}};

                schemas[schema].vary.forEach((selector) => {
                    let sel = 'properties.';
                    const sels = selector.split('.');
                    sels.forEach((s, j) => {
                        sel += j ? '.properties.' + s : s;
                        if(!selectByDot(sel, varySchema)) {
                            let val = selectByDot(sel, schemas[schema]);
                            if(typeof val === 'object') {
                                if(Array.isArray(val)) {
                                    val = [...val];
                                } else {
                                    val = {...val};
                                }
                            }
                            setByDot(sel, varySchema, val);
                            if(!selectByDot(sel, varySchema)) {
                                setByDot(sel, varySchema, {});
                            }
                        }
                        let vs = selectByDot(sel, varySchema) || {};
                        if((sels.length > (j + 1)) && vs.hasOwnProperty('properties')) {
                            delete vs.properties;
                            vs.properties = {};
                        }
                    });
                });

                this.setState({varySchema});
            } else {
                this.setState({varySchema: {}});
            }
        }
    }

    onChangeUpdateProductData = (evt) => {
        let value = evt.target.value;
        if('checkbox' === evt.target.type) {
            value = evt.target.checked;
        }
        this.updateProductData(evt.target.name, value);
    };

    updateProductData = (name, value) => {
        let productDataChanged = {};
        if(this.state.productDataChanged) {
            productDataChanged = {...this.state.productDataChanged};
        } else {
            productDataChanged = {...this.state.productData};
        }
        productDataChanged[name] = value;
        this.setState({productDataChanged});
    };

    componentWillUnmount() {
        this.setState(initialState);
    }

    trigger = () => {
        if(!this.props.pushDataLocale) {
            console.error('ProductData: can not save, no prop `pushDataLocale` exits.');
            return;
        }
        if(!this.state.productDataChanged) {
            return;
        }

        let productData = {...this.state.productDataChanged};

        this.props.pushDataLocale(productData);
    };

    toggleToOverview = (name) => {
        this.setState({overview: name});
    };

    overviewActive = (name) => {
        return this.state.overview === name;
    };

    forceUpdateStockItem = () => {
        const {
            loadStockItemForProduct,
        } = this.props;
        if(this.state.productData && this.state.productData.id) {
            loadStockItemForProduct(this.state.productData.id, true);
        }
    };

    render() {
        const {
            t, theme, classes,
            classDropHorizontal,
            isSchemaLoaded, schemas,
            saving, schema, schemaName, schemaObject,
            isBundle, hasVariation,
            isVariation, productInfo,
            toggleHead, openHead
        } = this.props;

        const {productData} = this.state;

        const productDataLive = this.state.productDataChanged || this.state.productData;

        const includeInventory = false;

        return (
            <React.Fragment>

                {schema && !schemas[schema] && false !== isSchemaLoaded(schema) && 'progress' !== isSchemaLoaded(schema) ? <p>{t('view-error.schema-not-loaded')}</p> : null}

                {!isVariation ?
                    <div className={classDropHorizontal.navWrapper}>
                        <div className={classDropHorizontal.navInner}>
                            <button className={classDropHorizontal.toggleFullScreen + ' ' + (openHead ? 'active' : '')}
                                    onClick={toggleHead}>
                                {openHead ?
                                    <MdFullscreen color={theme.textColor} size={'1.25em'} style={{margin: 'auto', display: 'block'}}/>
                                    : <MdFullscreenExit color={theme.textColor} size={'1em'} style={{margin: 'auto', display: 'block'}}/>}
                            </button>
                            <button
                                className={classDropHorizontal.navBtn + ' ' + (this.overviewActive('master') ? 'active' : '')}
                                onClick={() => this.toggleToOverview('master')}
                            >{t('edit-' + (hasVariation ? 'master' : 'product'))}
                            </button>
                            {!isBundle && hasVariation ?
                                <button
                                    className={classDropHorizontal.navBtn + ' ' + (this.overviewActive('variations') ? 'active' : '')}
                                    onClick={() => this.toggleToOverview('variations')}
                                >{t('edit-variations')}</button> : null}
                            <button
                                className={classDropHorizontal.navBtn + ' ' + (this.overviewActive('inventory') ? 'active' : '')}
                                onClick={() => this.toggleToOverview('inventory')}
                            >{t('inventory')}
                            </button>
                        </div>
                        <div className={classDropHorizontal.navBorder}/>
                    </div>
                    : null}

                <DropHorizontalWrapper>
                    <DropHorizontal open={this.overviewActive('master')} pos={0} style={{padding: (!(hasVariation || isBundle) ? '0' : '0 3px'), margin: ((!isVariation && !hasVariation && !isBundle) ? '-12px ' : '') + '0'}}>

                        {productDataLive && (productDataLive.date_create || productDataLive.date_update) ?
                            <div className={classes.dateWrapper}>
                                {productDataLive.date_create ?
                                    <div className={classes.dateSingle}>
                                        <span className={classes.dateLbl}>{t('date.created')}</span>
                                        <span className={classes.dateValue}>{productDataLive.date_create}</span>
                                    </div>
                                    : null}
                                {productDataLive.date_update ?
                                    <div className={classes.dateSingle}>
                                        <span className={classes.dateLbl}>{t('date.updated')}</span>
                                        <span className={classes.dateValue}>{productDataLive.date_update}</span>
                                    </div>
                                    : null}
                            </div>
                            : null}

                        {productDataLive ?
                            <div className={classes.wrapperTop}>
                                <div className={classes.boolControl}>
                                    <div className={classes.inpGroup + ' ' + classes.inpGroupCheckBox}>
                                        <Label htmlFor={this.id + '-active'} style={{padding: '0 ' + theme.gutter.base + 'px 0 0'}}>{t('editbasedata.active')}</Label>
                                        <InputCheckbox id={this.id + '-active'} name={'active'} checked={!!+productDataLive.active} onChange={this.onChangeUpdateProductData} size={'1.25em'}/>
                                    </div>
                                    <div className={classes.inpGroup + ' ' + classes.inpGroupCheckBox}>
                                        <Label htmlFor={this.id + '-hidden'} style={{padding: '0 ' + theme.gutter.base + 'px 0 0'}}>{t('editbasedata.hidden')}</Label>
                                        <InputCheckbox id={this.id + '-hidden'} name={'hidden'} checked={!!+productDataLive.hidden} onChange={this.onChangeUpdateProductData} size={'1.25em'}/>
                                    </div>
                                    <div className={classes.inpGroup + ' ' + classes.inpGroupCheckBox}>
                                        <Label htmlFor={this.id + '-buy_able'} style={{padding: '0 ' + theme.gutter.base + 'px 0 0'}}>{t('editbasedata.buy_able')}</Label>
                                        <InputCheckbox id={this.id + '-buy_able'} name={'buy_able'} checked={!!+productDataLive.buy_able} onChange={this.onChangeUpdateProductData} size={'1.25em'}/>
                                    </div>
                                </div>
                            </div>
                            : null}

                        {/*isBundle ? <p style={{margin: '3px 0 12px 0', fontStyle: 'italic'}}>{t('is-bundle')}</p> : null}
                        {hasVariation ? <p style={{margin: '3px 0 12px 0', fontStyle: 'italic'}}>{t('is-variation')}</p> : null*/}

                        {isVariation ? <h3 style={{margin: 0}}>{t('editbasedata.title' + (hasVariation ? '-master' : isVariation ? '-variation' : ''))}</h3> : null}

                        {productDataLive ?
                            <React.Fragment>
                                <div className={classes.wrapperTop}>
                                    <div className={classes.inpGroup + ' ' + classes.product_no}>
                                        <Label htmlFor={this.id + '-product_no'} style={{paddingBottom: 2}}>{t('editbasedata.product_no')}</Label>
                                        <InputText id={this.id + '-product_no'} name={'product_no'} value={productDataLive.product_no || ''} onChange={this.onChangeUpdateProductData}/>
                                    </div>

                                    <div className={classes.pricing}>
                                        <div className={classes.inpGroup + ' ' + classes.inpGroupPricing}>
                                            <Label htmlFor={this.id + '-tax_group'} style={{paddingBottom: 2}}>{t('editbasedata.tax_group')}</Label>
                                            <InputText id={this.id + '-tax_group'} name={'tax_group'} value={productDataLive.tax_group || ''} onChange={this.onChangeUpdateProductData}/>
                                        </div>
                                        <div className={classes.inpGroup + ' ' + classes.inpGroupPricing}>
                                            <Label htmlFor={this.id + '-price'} style={{paddingBottom: 2}}>{t('editbasedata.price')}</Label>
                                            <InputText id={this.id + '-price'} name={'price'} value={productDataLive.price || ''} onChange={this.onChangeUpdateProductData}/>
                                        </div>
                                    </div>
                                </div>
                            </React.Fragment> : null}

                        <h3 style={{margin: 0}}>{t('editdata.title' + (hasVariation ? '-master' : isVariation ? '-variation' : ''))}</h3>

                        {(schema && ('progress' === isSchemaLoaded(schema) || 'error' === isSchemaLoaded(schema)) ?
                            <div style={{position: 'relative'}}>
                                <Loading
                                    center={true}
                                    style={{
                                        wrapper: {margin: 0},
                                    }}
                                    errorWarning={'error' === isSchemaLoaded(schema)}
                                ><p style={{textAlign: 'center', margin: '6px 0 2px 0'}}>{t('editbasedata.loading-schema')}</p></Loading>
                            </div> :
                            null)}

                        {productData && (schemaObject || (schema && true === isSchemaLoaded(schema))) ?
                            <ProductDetailLocaleData
                                t={t}
                                onChange={(productDataDataChanged) => this.updateProductData('data', productDataDataChanged)}
                                productData={productData}
                                schema={schema ? schemas[schema] : schemaObject}
                                schemaName={schema ? schema : schemaName}
                            />
                            : null}

                        <Button onClick={this.trigger}><MdSave style={{float: 'left', display: 'block', marginRight: '4px'}}/> {t('btn-save-data' + (hasVariation ? '-master' : isVariation ? '-variation' : ''))}</Button>

                        {saving ?
                            ('progress' === saving || 'error' === saving ?
                                <React.Fragment>
                                    <Loading style={{
                                        wrapper: {margin: 0},
                                        item: ('error' === saving ? {borderColor: theme.errorColor} : {}),
                                    }}/>

                                </React.Fragment> :
                                ('success' === saving ?
                                    <MdDone/> : null))
                            : null}

                        {includeInventory ?
                            <React.Fragment>
                                <h3 style={{margin: '30px 0 18px 0', display: 'flex'}}>{t('title-inventory')}
                                    <button style={{marginLeft: 4}} onClick={this.forceUpdateStockItem}><MdSync color={theme.textColor} size={'1.25em'}/></button>
                                </h3>
                                <ProductInventory
                                    data={productData}
                                    isBundle={isBundle}
                                    product_name={productInfo.name}
                                    localeActive={this.props.localeActive}
                                />
                            </React.Fragment> : null}

                        <div style={{height: '50px'}}/>
                    </DropHorizontal>

                    <DropHorizontal open={this.overviewActive('variations')} pos={1} style={{padding: '0 3px'}}>
                        {hasVariation ?
                            <div>
                                <div>
                                    <h3 style={{width: '100%', margin: '0 0 9px 0'}}>{t('editbasedata.variation-title')}</h3>
                                    <p style={{margin: '6px 0'}}>Those fields are variable:</p>
                                    <ul style={{margin: '6px 0'}}>
                                        {schemas[schema].vary.map((e) =>
                                            <li key={e}>{e}</li>
                                        )}
                                    </ul>
                                    <Button onClick={() => {
                                        const variations = [...this.state.variations];
                                        variations.push({});
                                        this.setState({variations});
                                    }}>+ add variation</Button>
                                </div>

                                {!this.state.variations.length ? <p>no-variations</p> : null}

                                {this.state.variations.map((v, i) =>
                                    <ProductDetailLocaleBaseData
                                        isVariation={true}
                                        key={i}
                                        t={t}
                                        productInfo={productInfo}
                                        productData={v}
                                        schemaName={schema ? schema : schemaName}
                                        localeActive={this.props.localeActive}
                                        schemaObject={this.state.varySchema}
                                        saving={this.props.saving}
                                        cacheid={this.state.cacheid + '.vary.' + (schema ? schema : schemaName) + (i + '')}
                                    />
                                )}
                            </div> : null}
                    </DropHorizontal>

                    <DropHorizontal open={this.overviewActive('inventory')} pos={2} style={{padding: '0 3px'}}>
                        <ProductInventoryList
                            data={productData}
                            product_name={productInfo.name}
                            localeActive={this.props.localeActive}
                            hasVariation={hasVariation}
                            isBundle={isBundle}
                        />
                    </DropHorizontal>
                </DropHorizontalWrapper>

            </React.Fragment>
        )
    };
}

const ProductDetailLocaleBaseData = withNamespaces('page--shop-product')(
    styleDropDownHorizontal(
        withTheme(withStyles(styles)(
            storeSchema(
                storeWarehouse(storeStock(storeStockItem(
                    ProductDetailLocaleBaseDataBase
                )))
            )
        ))
    )
);

export default ProductDetailLocaleBaseData;
