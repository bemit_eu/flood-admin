import React from 'react';
import {withNamespaces} from "react-i18next";

import RouteCascaded from '../../lib/RouteCascaded';

import Loading from "../../component/LoadingSmall";

import Nav from "../../component/Nav/Nav";
import Sidebar from "../../layout/ControlPanel/Sidebar";
import {storeProduct} from "./Stores/Product";

class ProductList extends React.Component {

    fetch(group, force = false) {
        this.props.loadProductList(group, force);
    }

    componentDidUpdate(prevProps) {
        if(prevProps.match.params.group !== this.props.match.params.group) {
            this.fetch(this.props.match.params.group);
        }
    }

    componentDidMount() {
        this.fetch(this.props.match.params.group);
    }

    render() {
        const {
            t,
            products, isProductListLoaded
        } = this.props;

        const navContent = (match) => {

            let content = [];

            products[this.props.match.params.group].forEach((product) => {
                content.push({
                    path: this.props.match.url + '/' + product.id,
                    lbl: {txt: product.name},
                })
            });

            return content;
        };
        const SideBarTop = p => (<p style={{
            padding: '0 3px',
            margin: '0 0 6px 0',
        }}>{t('nav.product-list')}</p>);

        return (
            <React.Fragment>
                {
                    <Sidebar top={SideBarTop}
                             center={props => (
                                 'progress' === isProductListLoaded(this.props.match.params.group) || 'error' === isProductListLoaded(this.props.match.params.group) ?
                                     <Loading
                                         center={true}
                                         errorWarning={'error' === isProductListLoaded(this.props.match.params.group)}
                                     /> :
                                     true === isProductListLoaded(this.props.match.params.group) ?
                                         (0 < products[this.props.match.params.group].length ?
                                             <Nav path={'/shop/'} content={navContent}/> :
                                             <p style={{
                                                 padding: '0 3px',
                                                 margin: '0 0 6px 0',
                                                 fontStyle: 'italic'
                                             }}>{t('view-error.no-product-in-group')}</p>)
                                         : 'error')
                             }
                    />
                }

                {this.props.routes ? this.props.routes.map((route, i) => (
                    route.sidebar ?
                        <RouteCascaded key={i} {...route} component={route.sidebar}/>
                        : null
                )) : null}
            </React.Fragment>
        )
    };
}

export default withNamespaces('page--shop')(storeProduct(ProductList));
