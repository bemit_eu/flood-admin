import React from 'react';

import {SchemaEditor} from "../../component/Schema/EditorLoader";

import {storeSchema} from "../Content/Stores/Schema";
import {SchemaEditorStore} from "../../component/Schema/EditorStore";
import {DevPopOut} from "../../component/DevPopOut";

const initialState = {
    schemaEditorStore: false
};

class ProductDetailLocaleDataBase extends React.PureComponent {
    state = {...initialState};

    componentDidMount(prevProps) {
        if(this.props.productData) {
            this.createContentStore();
        }
    }

    componentDidUpdate(prevProps) {
        if(prevProps.productData !== this.props.productData) {
            this.createContentStore();
        }
    }

    createContentStore = () => {
        if(!this.props.productData || !this.props.productData.data) {
            return;
        }

        let schemaEditorStore = SchemaEditorStore.createFromData(this.props.productData.data);
        this.setState({schemaEditorStore});
    };


    componentWillUnmount() {
        this.setState(initialState);
    }

    handleStateChange = (newStore) => {
        this.setState({schemaEditorStore: newStore}, () => {
            if(this.props.onChange) {
                this.props.onChange(newStore.data);
            }
        });
    };

    render() {
        const {
            t, schema, schemaName
        } = this.props;

        return (
            <div style={{overflow: 'hidden',}}>
                <SchemaEditor
                    t={t}
                    store={this.state.schemaEditorStore}
                    onChange={this.handleStateChange}
                    schema={schema}
                    displayBlock={true}
                    tScope={'product.' + schemaName}
                />

                {window.localStorage.getItem('flood-dev') * 1 ?
                    <div style={{display: 'flex', padding: '9px 0'}}>
                        <div style={{display: 'flex', marginRight: 9}}>
                            <DevPopOut
                                name={'SchemaEditorStore'}
                                title={'SchemaEditorStore'}
                                label={'SchemaEditorStore'}
                                json={this.state.schemaEditorStore}
                            />
                        </div>

                        <div style={{display: 'flex', marginRight: 9}}>
                            <DevPopOut
                                name={'Schema ' + schemaName}
                                title={'Schema ' + schemaName}
                                label={'Schema ' + schemaName}
                                json={schema}
                            />
                        </div>

                        <div style={{display: 'flex', marginRight: 9}}>
                            <DevPopOut
                                name={'Data Raw'}
                                label={'Data Raw'}
                                title={'Data Raw'}
                                json={this.props.productData && this.props.productData.data ? this.props.productData.data : undefined}
                            />
                        </div>
                    </div>
                    : null}
            </div>
        )
    };
}

const ProductDetailLocaleData = storeSchema(ProductDetailLocaleDataBase);

export {ProductDetailLocaleData};
