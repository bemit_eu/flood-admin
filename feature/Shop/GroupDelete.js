import React from 'react';
import {withNamespaces} from 'react-i18next';
import {withRouter} from 'react-router-dom';
import {withTimeout} from "@formanta/react";

import {i18n} from "../../lib/i18n";

import {storeGroup} from "./Stores/Group";
import {cancelPromise} from "../../component/cancelPromise";
import {ButtonSmall} from "../../component/Form";
import ButtonInteractiveDelete from "../../component/ButtonInteractiveDelete";
import {LoadingProgress} from "../../component/LoadingProgress";

class GroupDelete extends React.Component {
    state = {
        deleting: false
    };

    deleteGroup = () => {
        const {
            group,
            deleteGroup, cancelPromise,
            history
        } = this.props;

        this.setState({deleting: 'progress'});
        cancelPromise(deleteGroup(group))
            .then((data) => {
                if(data.error) {
                    this.setState({deleting: 'error'});
                    return;
                }

                history.push('/' + i18n.languages[0] + '/shop/products');

                this.setState({
                    deleting: true
                });
            })
            .catch((err) => {
                if(err && !err.cancelled) {
                    this.setState({deleting: 'error'});
                }
            });
    };

    render() {
        const {
            t,
        } = this.props;

        return (
            <React.Fragment>
                <ButtonInteractiveDelete
                    comp={(p) => (
                        <ButtonSmall {...p}>
                            {t('group.edit.btn-delete')} {p.children}
                        </ButtonSmall>
                    )}
                    onClick={this.deleteGroup}
                />
                <LoadingProgress progress={this.state.deleting}/>
            </React.Fragment>
        )
    };
}

export default withNamespaces('page--shop')(withRouter(withTimeout(cancelPromise(storeGroup(GroupDelete)))));
