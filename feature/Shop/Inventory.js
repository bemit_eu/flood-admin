import React from 'react';
import {withNamespaces} from 'react-i18next';

import {createUseStyles} from '../../lib/theme';
import {cancelPromise} from "../../component/cancelPromise";
import {DataGrid} from "../../component/DataGrid/DataGrid";
import {withBackend} from "../BackendConnect";
import {ContainerStyle, SplitScreen} from "../../component/SplitScreen";
import {withAuthPermission} from "../AuthPermission";
import {storeStockItem} from "./Stores/StockItem";

const useStyles = createUseStyles({
    item: {},
    cell: {
        padding: '6px',
        '&:first-child': {
            paddingLeft: 0
        },
        '&:last-child': {
            paddingRight: 0
        },
        '&.num': {
            textAlign: 'right'
        }
    },
    button: {
        fontSize: '0.875rem',
        background: 'none',
        padding: '2px 6px',
        '&:first-child': {
            paddingLeft: 0,
        },
        '&:last-child': {
            paddingRight: 0,
        }
    },
    subTitle: {
        margin: 0
        //margin: '-12px 0 6px 0',
    }
});

const InventoryStockItemBase = (props) => {
    const {item} = props;
    const classes = useStyles();

    return (
        <React.Fragment>
            <tr className={classes.item}>
                <td className={classes.cell}>
                    {item.warehouse_name}
                </td>
                <td className={classes.cell}>
                    {item.stock_name}
                </td>
                <td className={classes.cell}>
                    {item.name}
                </td>
                <td className={classes.cell}>
                    {item.sku}
                </td>
                <td className={classes.cell + ' num'}>
                    {item.amount_available}
                </td>
                <td className={classes.cell + ' num'}>
                    {item.amount_reserved}
                </td>
                <td className={classes.cell + ' num'}>
                    {item.shop_products}
                </td>
            </tr>
        </React.Fragment>
    );
};

const InventoryStockItem = InventoryStockItemBase;

const initialState = {
    loadedStockItems: [],
    isStockItemsLoaded: false,
};

class Inventory extends React.PureComponent {

    state = initialState;

    componentDidMount() {
        const {
            changeStyleInnerWrapper
        } = this.props;

        changeStyleInnerWrapper(ContainerStyle);

        this.fetch();
    }

    componentDidUpdate(prevProps) {
    }

    componentWillUnmount() {
        this.setState(initialState);
        this.props.changeStyleInnerWrapper({});
    }

    fetch() {
        this.setState({isStockItemsLoaded: 'progress'});
        this.props.loadStockItems()
            .then((data) => this.setState({
                loadedStockItems: data,
                isStockItemsLoaded: false
            }))
            .catch();
    }

    render() {
        const {
            t,
        } = this.props;

        return (
            <React.Fragment>
                <h1>{t('nav.inventory')}</h1>

                <SplitScreen
                    isTopOpen={false}
                    top={
                        null
                    }

                    center={
                        <DataGrid
                            headers={[
                                {
                                    lbl: t('inventory.warehouse'),
                                    search: 'warehouse'
                                },
                                {
                                    lbl: t('inventory.stock'),
                                    search: 'stock'
                                },
                                {
                                    lbl: t('inventory.stock-item'),
                                    search: 'name',
                                    sort: 'name',
                                },
                                {
                                    lbl: t('inventory.sku'),
                                    search: 'sku',
                                    sort: 'sku',
                                },
                                {
                                    lbl: t('inventory.available'),
                                    search: 'amount_available',
                                    sort: 'amount_available',
                                },
                                {
                                    lbl: t('inventory.reserved'),
                                    search: 'amount_reserved',
                                    sort: 'amount_reserved',
                                },
                                {
                                    lbl: t('inventory.list.head.shop_products'),
                                    search: 'shop_products',
                                    sort: 'shop_products',
                                },
                            ]}
                            emptyList={t('inventory.no-stock-items')}
                            list={this.state.loadedStockItems || []}
                            loading={this.state.isStockItemsLoaded}
                            /*controls={[
                                (p) => <ButtonSmall onClick={() => this.setState({showChangeHook: !this.state.showChangeHook})}
                                                    active={this.state.showChangeHook}>{t('list.change-hooks')}</ButtonSmall>
                            ]}*/
                            reload={() => this.fetch(true)}
                            element={(p) => <InventoryStockItem t={t} {...p}/>}
                        />
                    }
                />
            </React.Fragment>
        )
    };
}

export default withNamespaces('page--shop')(cancelPromise(withAuthPermission(storeStockItem(withBackend(Inventory)))));
