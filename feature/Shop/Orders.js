import React from 'react';
import {withNamespaces} from 'react-i18next';
import posed from 'react-pose';

import {storeOrder} from "./Stores/Order";
import {cancelPromise} from "../../component/cancelPromise";
import {DataGrid} from "../../component/DataGrid/DataGrid";
import OrdersListItem from "./OrdersListItem";
import {Select, ButtonSmall} from "../../component/Form";
import {withBackend} from "../BackendConnect";
import {ContainerStyle, SplitScreen} from "../../component/SplitScreen";
import {withAuthPermission} from "../AuthPermission";
import {FEATURE_SHOP_ORDER} from "../../permissions";

const SelectHook = posed.div({
    hidden: {
        height: 0,
        marginBottom: 0,
    },
    visible: {
        height: 'auto',
        marginBottom: '24px',
    }
});

const initialState = {
    savingStateUpdate: {},
    selectedHooks: [],
    loadedHooks: [],
    showChangeHook: false,
    areLoadedHooksDifferent: false
};

class Orders extends React.Component {

    state = initialState;

    componentDidMount() {
        const {
            changeStyleInnerWrapper,
            hook_active
        } = this.props;

        changeStyleInnerWrapper(ContainerStyle);

        this.setState({selectedHooks: [hook_active.id]}, () => this.fetch());
    }

    componentDidUpdate(prevProps) {
        if(prevProps.match.params.state !== this.props.match.params.state ||
            prevProps.match.params.state !== this.props.match.params.state) {
            this.fetch(true);
        }
    }

    componentWillUnmount() {
        this.setState(initialState);
        this.props.changeStyleInnerWrapper({});
    }

    createSavingStateUpdate(name, val) {
        const savingStateUpdate = {...this.state.savingStateUpdate};
        savingStateUpdate[name] = val;
        return savingStateUpdate;
    }

    fetch(force = false) {
        this.props.loadOrderList(this.state.selectedHooks, this.props.match.params.state ? this.props.match.params.state : false, force);
        this.setState({
            loadedHooks: [...this.state.selectedHooks],
            areLoadedHooksDifferent: false
        });
    }

    pushStateUpdate = (index, order_id, new_state) => {
        const {cancelPromise, updateOrderState} = this.props;

        this.setState({savingStateUpdate: this.createSavingStateUpdate(order_id, 'progress')});
        cancelPromise(updateOrderState(index, order_id, this.props.match.params.state, new_state))
            .then((data) => {
                this.setState({
                    savingStateUpdate: this.createSavingStateUpdate(order_id, 'success'),
                });

                setTimeout(() => {
                    this.setState({savingStateUpdate: this.createSavingStateUpdate(order_id, false)});
                }, 4000);
            })
            .catch((data) => {
                if(!data.cancelled) {
                    this.setState({savingStateUpdate: this.createSavingStateUpdate(order_id, 'error')});
                }
            });
    };

    selectHook = (e) => {
        let areLoadedHooksDifferent = false;

        const selectedHooks = [...e.target.options].filter(o => o.selected).map(o => {
            if(!this.state.loadedHooks.includes(o.value)) {
                areLoadedHooksDifferent = true;
            }
            return o.value
        });

        if(this.state.loadedHooks.length !== selectedHooks.length) {
            // when loaded hooks contains the same as selected, it could be that after a multi-select a single-select happened
            areLoadedHooksDifferent = true;
        }

        this.setState({
            selectedHooks,
            areLoadedHooksDifferent
        });
    };

    render() {
        const {
            t,
            orders, loadingOrders,
            isAllowed, isAllowedHook,
            hooks
        } = this.props;

        return (
            <React.Fragment>
                <h1>{t('title')}</h1>

                <SplitScreen
                    isTopOpen={this.state.showChangeHook}
                    top={
                        <SelectHook withParent={false} pose={this.state.showChangeHook ? 'visible' : 'hidden'}
                                    style={{overflow: 'hidden', display: 'flex', flexDirection: 'column'}}>
                            <p style={{margin: '0 0 3px 0'}}>{t('list.load-for-hooks')}</p>
                            <Select multiple value={this.state.selectedHooks} onChange={this.selectHook} style={{maxHeight: '5rem'}}>
                                {Object.keys(hooks).map((id) => (
                                    isAllowedHook(id) && isAllowed(FEATURE_SHOP_ORDER, id) ?
                                        <option key={id}
                                                value={id}>{hooks[id].label}</option> :
                                        null
                                ))}
                            </Select>
                            {this.state.areLoadedHooksDifferent ?
                                <ButtonSmall onClick={() => this.fetch(true)}
                                             style={{marginRight: 'auto', fontSize: '0.875rem',}}
                                >{t('list.load-for-hook-btn' + (1 < this.state.selectedHooks.length ? '-pl' : ''))}</ButtonSmall> :
                                <p style={{margin: '0 0 0 0', fontStyle: 'italic'}}>{t('list.are-load' + (true !== loadingOrders ? 'ing' : 'ed') + '-hook' + (1 < this.state.selectedHooks.length ? '-pl' : ''))}</p>}
                        </SelectHook>
                    }

                    center={
                        <DataGrid
                            headers={[
                                {
                                    lbl: t('list.head.date'),
                                    search: 'date'
                                },
                                {
                                    lbl: t('list.head.customer'),
                                    search: 'customer.name|customer.email'
                                },
                                {lbl: t('list.head.qty'),},
                                {lbl: t('list.head.state')},
                                {lbl: t('list.head.shipping')},
                                {lbl: t('list.head.hook')},
                                {lbl: t('list.head.locale')},
                                {lbl: ''},
                            ]}
                            emptyList={t('list.errors.no-orders' + (this.props.match.params.state ? '-in-state' : ''))}
                            list={orders}
                            loading={loadingOrders}
                            controls={[
                                (p) => <ButtonSmall onClick={() => this.setState({showChangeHook: !this.state.showChangeHook})}
                                                    style={{marginRight: 6}}
                                                    active={this.state.showChangeHook}>{t('list.change-hooks')}</ButtonSmall>
                            ]}
                            reload={() => this.fetch(true)}
                            element={(p) => <OrdersListItem pushStateUpdate={this.pushStateUpdate}
                                                            savingStateUpdate={this.state.savingStateUpdate}
                                                            t={t} {...p}/>}
                        />
                    }
                />
            </React.Fragment>
        )
    };
}

export default withNamespaces('page--shop-order')(cancelPromise(withAuthPermission(withBackend(storeOrder(Orders)))));
