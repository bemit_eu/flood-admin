import React from 'react';
import {withNamespaces} from 'react-i18next';
import {Redirect, withRouter} from 'react-router-dom';
import {ID} from "@formanta/js";
import {withTimeout} from "@formanta/react";

import {withTheme} from '../../lib/theme';
import {i18n} from "../../lib/i18n";

import {withPusher} from "../../component/Pusher/Pusher";
import {storeGroup} from "./Stores/Group";
import {cancelPromise} from "../../component/cancelPromise";
import {ButtonSmall, InputText} from "../../component/Form";
import ButtonInteractive from "../../component/ButtonInteractive";
import {MdDone, MdHelp} from "react-icons/md";
import {withBackend} from "../BackendConnect";
import {LoadingProgress} from "../../component/LoadingProgress";

class GroupCreate extends React.Component {
    state = {
        name: '',
        saving: false,
        newGroup: false,
        loaded: false
    };

    constructor(props) {
        super(props);
        this.id = ID();
    }

    componentDidMount() {
        if(!this.props.create) {
            this.loadGroup();
        }
    }

    componentDidUpdate(prevProps) {
        if(prevProps.group !== this.props.group) {
            if(!this.props.create) {
                this.loadGroup();
            }
        }
    }

    loadGroup = () => {
        const {
            group,
            loadGroup, cancelPromise,
            hook_active, history
        } = this.props;

        this.setState({loading: 'progress'});
        cancelPromise(loadGroup(group))
            .then((data) => {
                if(data.error) {
                    this.setState({loading: 'error'});
                    return;
                }

                if(hook_active.id !== data.hook) {
                    // if group hook is not the same like active hook, force to content overview
                    // happens when a user reloads the page and another hook then the default was active
                    history.push('/' + i18n.languages[0] + '/shop/products');
                }

                this.setState({
                    loading: true,
                    name: data.name
                });
            })
            .catch((err) => {
                if(err && !err.cancelled) {
                    this.setState({loading: 'error'});
                }
            });
    };

    pushNewGroup = () => {
        const {
            createGroup, cancelPromise,
            setTimeout
        } = this.props;

        this.setState({saving: 'progress'});
        cancelPromise(createGroup(this.state.name))
            .then((data) => {
                if(data.error) {
                    this.setState({saving: 'error'});
                    return;
                }

                this.setState({
                    saving: 'success',
                    newGroup: data.success
                });

                setTimeout(() => {
                    this.setState({
                        newGroup: false
                    });
                }, 0);

                setTimeout(() => {
                    this.setState({saving: false});
                }, 4000);
            })
            .catch((err) => {
                if(err && !err.cancelled) {
                    this.setState({saving: 'error'});
                }
            });
    };

    pushUpdateGroup = () => {
        const {
            group,
            updateGroup, cancelPromise,
            setTimeout
        } = this.props;

        this.setState({saving: 'progress'});
        cancelPromise(updateGroup(group, this.state.name))
            .then((data) => {
                if(data.error) {
                    this.setState({saving: 'error'});
                    return;
                }

                this.setState({
                    saving: 'success'
                });

                setTimeout(() => {
                    this.setState({saving: false});
                }, 4000);
            })
            .catch((err) => {
                if(err && !err.cancelled) {
                    this.setState({saving: 'error'});
                }
            });
    };

    render() {
        const {
            t, theme,
            create
        } = this.props;

        return (
            <React.Fragment>
                {
                    (
                        this.state.newGroup
                    ) ? <Redirect to={'/' + i18n.languages[0] + '/shop/products/' + this.state.newGroup}/>
                        : null
                }

                <LoadingProgress progress={this.state.loading}/>

                {create || true === this.state.loading ?
                    <form onSubmit={(e) => {
                        e.preventDefault();
                        e.target.reportValidity()
                    }}>
                        <div style={{marginBottom: '12px'}}>
                            <label htmlFor={this.id + '-name'}>{t('group.edit.name')}</label>

                            <InputText id={this.id + '-name'}
                                       value={this.state.name}
                                       onChange={(e) => this.setState({name: e.target.value})}
                                       ref={(r) => {
                                           this.input_name = r
                                       }}
                                       required
                            />
                        </div>

                        <ButtonInteractive
                            comp={(p) => (
                                <ButtonSmall {...p}>
                                    {t('group.edit.btn-' + (create ? 'create' : 'update'))} {p.children}
                                </ButtonSmall>
                            )}
                            step={create ? false : [
                                null,
                                <MdHelp size={'1em'} color={theme.textColor}/>,
                                <MdDone size={'1em'} color={theme.textColor}/>,
                            ]}
                            onClick={create ? this.pushNewGroup : this.pushUpdateGroup}
                        />

                        <LoadingProgress progress={this.state.saving}/>

                    </form> : null}
            </React.Fragment>
        )
    };
}

export default withNamespaces('page--shop')(withPusher(withTimeout(cancelPromise(withRouter(withBackend(storeGroup(withTheme(GroupCreate))))))));
