import React from 'react';
import {withNamespaces} from 'react-i18next';
import {MdEdit} from 'react-icons/md';
import {Redirect, withRouter} from 'react-router-dom';

import Loading from "../../component/Loading";

import {withTheme, withStyles} from '../../lib/theme';
import {i18n} from "../../lib/i18n";
import ButtonInteractiveDelete from "../../component/ButtonInteractiveDelete";

import ProductDetailHead from './ProductDetailHead';
import ProductDetailLocaleSwitch from './ProductDetailLocaleSwitch';
import ProductDetailBaseEdit from './ProductDetailBaseEdit';
import ProductDetailLocaleBaseData from './ProductDetailLocaleBaseData';
import {storeProduct} from "./Stores/Product";
import {cancelPromise} from "../../component/cancelPromise";
import {storeSchema} from "../Content/Stores/Schema";
import {ContainerStyle, SplitScreen} from "../../component/SplitScreen";
import {DropDown} from "../../lib/DropDown";
import StyleGrid from "../../lib/StyleGrid";
import {ButtonRaw as Button} from "../../component/Form";
import {withBackend} from "../BackendConnect";

const styles = {
    top: {
        display: 'flex',
        alignItems: 'center',
        flexWrap: 'wrap',
        margin: '-12px 0 6px 0',
        [StyleGrid.smUp]: {
            flexWrap: 'no-wrap',
        }
    },
    deleteButton: {
        width: '100%',
        margin: '3px 0',
        fontStyle: 'italic',
        [StyleGrid.smUp]: {
            width: 'auto',
            margin: '0 0 0 auto',
            fontStyle: 'normal',
        }
    },
    subTitle: {
        margin: 0
        //margin: '-12px 0 6px 0',
    }
};

const initialState = {
    productInfo: {},
    hasLocales: false,
    localeActive: false,
    localesPossible: [],
    locales: [],
    productTypesPossible: [],
    loaded: false,
    openBaseEdit: false,
    cacheid: false,
    moveToGroup: false,
    openHead: true,
    saving: {
        new_locale: false,
        delete: false,
        delete_locale: false,
        base_edit: false,
        update_locale: false,
    },
};

class ProductDetail extends React.PureComponent {

    state = initialState;

    componentDidMount() {
        this.fetch();

        if('0' === window.localStorage.getItem('hy-admin-shop-product-openhead') || '1' === window.localStorage.getItem('hy-admin-shop-product-openhead')) {
            this.setState({openHead: '1' === window.localStorage.getItem('hy-admin-shop-product-openhead')});
        }

        const {
            changeStyleInnerWrapper
        } = this.props;
        changeStyleInnerWrapper(ContainerStyle);
    }

    componentWillUnmount() {
        this.setState(initialState);
        this.props.changeStyleInnerWrapper({});
    }

    //
    // Api Fetch and Push

    createUpdatedSaving(name, val) {
        const saving = {...this.state.saving};
        saving[name] = val;
        return saving;
    }

    fetch(locale = false) {
        const {cancelPromise, loadProduct, hook_active, history} = this.props;

        this.setState({loaded: 'progress'});
        cancelPromise(loadProduct(this.props.match.params.product))
            .then((data) => {

                if(hook_active.id !== data.hook) {
                    // if product hook is not the same like active hook, force to content overview
                    // happens when a user reloads the page and another hook then the default was active
                    history.push('/' + i18n.languages[0] + '/shop/products');
                }

                let hasLocales = false;
                let locales = [];
                let localeActive = false;
                let localesPossible = [];
                let productTypesPossible = [];
                if(data.locales) {
                    hasLocales = true;
                    locales = data.locales;
                    localeActive = locale ? locale : data.localeDefault;
                    localesPossible = data.localesPossible;
                    productTypesPossible = data.productTypesPossible;

                    if(-1 === locales.indexOf(localeActive) && locales[0]) {
                        localeActive = locales[0];
                    }
                }

                this.setState({
                    productInfo: data,
                    hasLocales,
                    locales,
                    localeActive,
                    localesPossible,
                    productTypesPossible,
                    loaded: true,
                    cacheid: '#' + this.props.match.params.product + '#'
                });
            })
            .catch((err) => {
                if(err && !err.cancelled) {
                    this.setState({
                        loaded: 'error'
                    });
                }
            });
    }

    pushDelete = () => {
        const {cancelPromise, deleteProduct} = this.props;

        this.setState({saving: this.createUpdatedSaving('delete', 'progress')});
        cancelPromise(deleteProduct(this.props.match.params.group, this.props.match.params.product))
            .then((data) => {
                this.setState({
                    saving: this.createUpdatedSaving('delete', 'success'),
                });

                setTimeout(() => {
                    this.setState({saving: this.createUpdatedSaving('delete', false)});
                }, 4000);
            })
            .catch((err) => {
                if(err && !err.cancelled) {
                    this.setState({saving: this.createUpdatedSaving('delete', 'error')});
                }
            });
    };

    pushNewLocale = () => {
        const {cancelPromise, createProductLocale} = this.props;
        const {localeActive} = this.state;

        this.setState({saving: this.createUpdatedSaving('new_locale', 'progress')});
        cancelPromise(createProductLocale(this.props.match.params.product, localeActive))
            .then((data) => {
                this.setState({
                    saving: this.createUpdatedSaving('new_locale', 'success'),
                });
                this.fetch(localeActive);

                setTimeout(() => {
                    this.setState({saving: this.createUpdatedSaving('new_locale', false)});
                }, 4000);
            })
            .catch((data) => {
                if(!data.cancelled) {
                    this.setState({saving: this.createUpdatedSaving('new_locale', 'error')});
                }
            });
    };

    pushDeleteLocale = () => {
        const {cancelPromise, deleteProductLocale} = this.props;
        const {localeActive, productInfo} = this.state;

        cancelPromise(deleteProductLocale(this.props.match.params.product, productInfo.data[localeActive].id))
            .then((data) => {
                this.setState({
                    saving: this.createUpdatedSaving('delete_locale', 'success'),
                });

                this.fetch(localeActive);

                setTimeout(() => {
                    this.setState({saving: this.createUpdatedSaving('delete_locale', false)});
                }, 4000);
            })
            .catch((data) => {
                if(!data.cancelled) {
                    this.setState({saving: this.createUpdatedSaving('delete_locale', 'error')});
                }
            });
    };

    pushBaseData = (name, productType = false, group = false) => {
        const {localeActive, productInfo} = this.state;
        const {cancelPromise, updateProductBaseData} = this.props;

        const moveToGroup = (group && productInfo.group !== group);

        const data = {
            name,
        };
        if(group) {
            data.group = group
        }
        if(productType) {
            data.product_type = productType;
        }

        this.setState({saving: this.createUpdatedSaving('base_edit', 'progress')});
        cancelPromise(updateProductBaseData((group || productInfo.group), this.props.match.params.product, data))
            .then((data) => {
                this.setState({
                    saving: this.createUpdatedSaving('base_edit', 'success'),
                    openBaseEdit: false
                });
                if(moveToGroup) {
                    this.setState({
                        moveToGroup: group,
                    });
                    setTimeout(() => {
                        this.setState({
                            moveToGroup: false,
                        })
                    }, 0);
                } else {
                    this.fetch(localeActive);
                }

                setTimeout(() => {
                    this.setState({saving: this.createUpdatedSaving('base_edit', false)});
                }, 4000);
            })
            .catch((data) => {
                if(!data.cancelled) {
                    this.setState({saving: this.createUpdatedSaving('base_edit', 'error')});
                }
            });
    };

    pushDataLocale = (productData) => {
        const {productInfo, localeActive} = this.state;
        const {cancelPromise, updateProductLocaleData} = this.props;

        const product_data = {...productData};

        const itmInf = {...productInfo};
        const itmInfData = {...itmInf.data};
        itmInfData[localeActive] = product_data;
        itmInf.data = itmInfData;
        this.setState({
            productInfo: itmInf,
            saving: this.createUpdatedSaving('update_locale', 'progress')
        });

        cancelPromise(updateProductLocaleData(productInfo.data[localeActive].id, product_data))
            .then((data) => {
                this.setState({saving: this.createUpdatedSaving('update_locale', 'success')});

                setTimeout(() => {
                    this.setState({saving: this.createUpdatedSaving('update_locale', false)});
                }, 4000);
            })
            .catch((data) => {
                if(!data.cancelled) {
                    this.setState({saving: this.createUpdatedSaving('update_locale', 'error')});
                }
            });
    };

    //
    // Open Toggles

    openBaseEdit = () => {
        this.setState({
            openBaseEdit: !this.state.openHead ? true : !this.state.openBaseEdit
        });
        if(!this.state.openHead) {
            this.toggleHead();
        }
    };

    //
    // Default Lifecycles

    toggleHead = () =>
        this.setState(
            {openHead: !this.state.openHead},
            () => window.localStorage.setItem('hy-admin-shop-product-openhead', this.state.openHead ? '1' : '0')
        );

    componentDidUpdate(prevProps) {
        if(prevProps.match.params.group !== this.props.match.params.group ||
            prevProps.match.params.product !== this.props.match.params.product) {
            this.fetch();
        }
    }

    localeSwitch = (locale) => {
        if(locale !== this.state.localeActive) {
            this.setState({
                localeActive: locale
            })
        }
    };

    render() {
        const {
            t,
            theme, classes,
            isSchemaLoaded, schemas
        } = this.props;
        const {
            hasLocales, locales, localeActive, localesPossible,
            productTypesPossible,
            productInfo
        } = this.state;

        /**
         * If the locale exists in the locales of the product
         * @var {boolean} activeLocaleExists
         */
        const activeLocaleExists = (hasLocales && locales && -1 !== locales.indexOf(localeActive));
        /**
         * If the locale is valid as a locale (e.g. activeLocale is not allowed from API but somehow was submitted from API)
         * @var {boolean} activeLocalePossible
         */
        const activeLocalePossible = (hasLocales && localesPossible && -1 !== localesPossible.indexOf(localeActive));
        /**
         * Hold only the product information for the active locale
         */
        const productData = activeLocaleExists ? productInfo.data[localeActive] : {};

        const {hasVariation, isBundle} = activeLocaleExists && productData && productInfo.product_type_data &&
        productInfo.product_type_data.schema && true === isSchemaLoaded(productInfo.product_type_data.schema) ?
            {
                hasVariation: !!schemas[productInfo.product_type_data.schema].vary,
                isBundle: !!+schemas[productInfo.product_type_data.schema].bundle
            } : {
                hasVariation: false,
                isBundle: false
            };

        return (
            <React.Fragment>
                {
                    (
                        this.state.moveToGroup &&
                        this.props.match.params.group !== this.state.moveToGroup
                    ) ? <Redirect to={'/' + i18n.languages[0] + '/shop/products/' + this.state.moveToGroup + '/' + this.props.match.params.product}/>
                        : null
                }
                {
                    (
                        'success' === this.state.saving.delete
                    ) ? <Redirect to={'/' + i18n.languages[0] + '/shop/products/' + this.props.match.params.group}/>
                        : null
                }
                {true !== this.state.loaded ?
                    !this.state.openHead ?
                        <h3>{t('title')}</h3>
                        : <h1>{t('title')}</h1> : null}
                {
                    true === this.state.loaded ?
                        (
                            <SplitScreen
                                isTopOpen={this.state.openHead}
                                top={
                                    <React.Fragment>
                                        <DropDown open={this.state.openHead}>
                                            <h1>{t('title' +
                                                (hasVariation ? '-variation' :
                                                    isBundle ? '-bundle' : '')
                                            )}</h1>

                                            {productInfo && productInfo.id ?
                                                <div className={classes.top}>
                                                    <h2 className={classes.subTitle}>{productInfo.name}</h2>

                                                    <Button onClick={this.openBaseEdit}
                                                            active={this.state.openBaseEdit}
                                                            style={{marginLeft: '3px'}}
                                                    ><MdEdit color={theme.textColor} size={'1em'}/></Button>

                                                    <ButtonInteractiveDelete onClick={this.pushDelete}
                                                                             className={classes.deleteButton}
                                                                             style={{
                                                                                 fontSize: '0.875rem',
                                                                                 color: theme.textColor,
                                                                                 display: 'flex',
                                                                                 alignItems: 'center'
                                                                             }}
                                                    ><span style={{marginRight: '3px'}}>{t('btn-delete-product')}</span></ButtonInteractiveDelete>

                                                    {('progress' === this.state.saving.delete || 'error' === this.state.saving.delete ?
                                                        <Loading style={{
                                                            wrapper: {margin: 0},
                                                            item: ('error' === this.state.saving.delete ? {borderColor: theme.errorColor} : {}),
                                                        }}/> :
                                                        null)}
                                                </div>
                                                : <p>unkown error</p>}

                                            <ProductDetailBaseEdit
                                                t={t}
                                                localesPossible={localesPossible}
                                                productInfo={productInfo}
                                                pushBaseData={this.pushBaseData}
                                                open={this.state.openBaseEdit}
                                                saving={this.state.saving.base_edit}
                                            />

                                            <ProductDetailLocaleSwitch
                                                t={t}
                                                localesPossible={localesPossible}
                                                locales={locales}
                                                localeActive={localeActive}
                                                localeSwitch={this.localeSwitch}
                                            />

                                            <ProductDetailHead
                                                t={t}
                                                data={productData}
                                                localesPossible={localesPossible}
                                                localeActive={localeActive}
                                                productTypesPossible={productTypesPossible}
                                                pushNewLocale={this.pushNewLocale}
                                                pushDeleteLocale={this.pushDeleteLocale}
                                                activeLocalePossible={activeLocalePossible}
                                                activeLocaleExists={activeLocaleExists}
                                                savingDelete={this.state.saving.delete_locale}
                                                savingNew={this.state.saving.new_locale}
                                            />
                                        </DropDown>

                                        {/*<DropDown open={!this.state.openHead}>
                                            <h3 style={{margin: 0}}>{productInfo && productInfo.name ? productInfo.name : null}</h3>
                                        </DropDown>*/}
                                        {!this.state.openHead ? <h3 style={{margin: 0}}>
                                            <Button onClick={this.toggleHead}
                                                    active={this.state.openBaseEdit && this.state.openHead}
                                                    style={{color: 'inherit', display: 'flex'}}
                                            >
                                                {productInfo && productInfo.name ? productInfo.name : null}&nbsp;
                                                {localeActive ? localeActive : null}&nbsp;
                                                <MdEdit color={theme.textColor} size={'.85em'}/>
                                            </Button>
                                        </h3> : null}
                                    </React.Fragment>
                                }

                                center={
                                    <React.Fragment>
                                        {activeLocaleExists ?
                                            productData && productInfo.product_type_data ?
                                                <ProductDetailLocaleBaseData
                                                    t={t}
                                                    isBundle={isBundle}
                                                    hasVariation={hasVariation}
                                                    productInfo={productInfo}
                                                    productData={productData}
                                                    localeActive={localeActive}
                                                    schema={productInfo.product_type_data.schema}
                                                    pushDataLocale={this.pushDataLocale}
                                                    saving={this.state.saving.update_locale}
                                                    cacheid={this.state.cacheid}
                                                    toggleHead={this.toggleHead}
                                                    openHead={this.state.openHead}
                                                /> :
                                                <React.Fragment>
                                                    (<p>{t('product-data-error')} {!productData ? t('product-data-error--no-data') : null} {!productInfo.product_type_data ? t('product-data-error--no-product-type-data') : null}</p>)
                                                </React.Fragment>
                                            : null}
                                    </React.Fragment>
                                }
                            />
                        ) :
                        ('progress' === this.state.loaded || 'error' === this.state.loaded ?
                            <Loading style={{
                                wrapper: {margin: 0},
                                item: ('error' === this.state.loaded ? {borderColor: theme.errorColor} : {}),
                            }}
                                     center={true}
                            ><p style={{margin: '2px 0', textAlign: 'center'}}>{t('loading')}</p></Loading> :
                            null)
                }
            </React.Fragment>
        )
    };
}

export default withNamespaces('page--shop-product')(cancelPromise(withRouter(withBackend(storeProduct(storeSchema(withStyles(styles)(withTheme(ProductDetail))))))));
