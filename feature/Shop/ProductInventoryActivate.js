import React from 'react';
import {withNamespaces} from 'react-i18next';
import {ID} from '@formanta/js';
import {withTheme} from '../../lib/theme';

import {storeWarehouse} from "./Stores/Warehouse";
import {storeStock} from "./Stores/Stock";

import {cancelPromise} from "../../component/cancelPromise";
import Loading from "../../component/Loading";
import {InputGroup, ButtonSmall, Label, Select, InputText} from "../../component/Form";
import {storeStockItem} from "./Stores/StockItem";
import {DropDown} from "../../lib/DropDown";
import {MdSave} from "react-icons/md";

class ProductInventoryActivate extends React.Component {
    state = {
        saving: false,
        selectedWarehouse: '-',
        selectedStock: '-',
        selectedName: '',
        selectedSku: '',
        selectedAmountAvailable: '0',
        newProduct: false,
        createManual: false,
        createAutomatic: false,
        openCreate: false,
        openSelect: false,
    };

    constructor(props) {
        super(props);
        this.id = ID();
    }

    componentDidMount() {
        const {
            loadWarehouseList,
            loadStockList
        } = this.props;
        loadWarehouseList();
        loadStockList();
    }

    selectWarehouse = (e) => {
        this.setState({
            selectedStock: '-',
            selectedWarehouse: [...e.target.options].filter(o => o.selected).map(o => o.value)[0]
        });
    };

    selectStock = (e) => {
        const {loadStockItemsList} = this.props;

        const selectedStock = [...e.target.options].filter(o => o.selected).map(o => o.value)[0];

        if('-' !== selectedStock) {
            loadStockItemsList(selectedStock)
        }

        this.setState({
            selectedStock
        });
    };

    pushNewStockItem = () => {
        const {
            id,
            createStockItem,
            cancelPromise
        } = this.props;
        const {
            selectedStock, selectedWarehouse, selectedSku, selectedName, selectedAmountAvailable
        } = this.state;

        if(selectedStock !== '-' && selectedWarehouse !== '-') {
            this.setState({saving: 'progress'});
            cancelPromise(createStockItem(id, selectedStock, selectedSku, selectedName, selectedAmountAvailable))
                .then((data) => {
                    if(data.error) {
                        this.setState({saving: 'error'});
                        return;
                    }

                    this.setState({
                        saving: 'success',
                        newProduct: data.success
                    });

                    setTimeout(() => {
                        this.setState({
                            newProduct: false
                        });
                    }, 0);

                    setTimeout(() => {
                        this.setState({saving: false});
                    }, 4000);
                })
                .catch((data) => {
                    if(!data.cancelled) {
                        this.setState({saving: 'error'});
                    }
                });
        }
    };

    nameManual = () => {
        if(this.state.createAutomatic) {
            this.setState({
                createAutomatic: false,
                createManual: true,
                selectedName: '',
                selectedSku: '',
            });
            return;
        }

        this.setState({
            createManual: !this.state.createManual,
        })
    };

    nameAutomatic = () => {
        let product_no = this.props.product_no;
        if(!product_no) {
            product_no = '';
            if(this.props.product_name) {
                product_no = this.props.product_name.substr(0, 2).trim();
                if(this.props.product_name.length > 6) {
                    product_no += this.props.product_name.substr(4, 2).trim();
                }
            }
        }

        let localeActive = this.props.localeActive || '';
        if(-1 < localeActive.indexOf('-')) {
            if(0 === localeActive.indexOf('-')) {
                localeActive = localeActive.substr(1);
            } else {
                localeActive = localeActive.substr(0, localeActive.indexOf('-'));
            }
        }

        this.setState({
            createManual: false,
            createAutomatic: this.state.createManual ? true : !this.state.createAutomatic,
            selectedName: this.props.product_name + '',
            selectedSku: (localeActive + product_no + '').toUpperCase(),
        })
    };

    render() {
        const {
            t, theme,
            warehouses, loadingWarehouses,
            stocks, loadingStocks,
            getStockItemsLoaded, stockItems
        } = this.props;
        const {saving} = this.state;

        const error = (
            'error' === loadingWarehouses ||
            'error' === loadingStocks ||
            'error' === saving
        );
        const loading = (
            'progress' === loadingWarehouses ||
            'progress' === loadingStocks ||
            'progress' === saving
        );

        return (
            <React.Fragment>
                {!loading && !error ?
                    <div style={{display: 'flex', columnGap: '12px'}}>
                        <div>
                            <Label htmlFor={this.id + 'warehouse'}>{t('inventory.warehouse')}</Label>
                            <Select
                                id={this.id + 'warehouse'}
                                onChange={(e) => this.selectWarehouse(e)}
                                value={this.state.selectedWarehouse}
                                style={{width: 'max-content'}}
                            >
                                <option value={'-'}>-</option>
                                {warehouses.map((warehouse) => (
                                    <option key={warehouse.id} value={warehouse.id}>{warehouse.name}</option>
                                ))}
                            </Select>
                        </div>
                        {stocks.hasOwnProperty(this.state.selectedWarehouse) ?
                            <div>
                                <Label htmlFor={this.id + 'stock'}>{t('inventory.stock')}</Label>
                                <Select
                                    id={this.id + 'stock'}
                                    onChange={(e) => this.selectStock(e)}
                                    value={this.state.selectedStock}
                                    default={'-'}
                                    style={{width: 'max-content'}}
                                >
                                    <option value={'-'}>-</option>
                                    {stocks[this.state.selectedWarehouse].map((stock) => (
                                        <option key={stock.id} value={stock.id}>{stock.name}</option>
                                    ))}
                                </Select>
                            </div> :
                            '-' !== this.state.selectedWarehouse ? <p>{t('inventory.no-stock')}</p> : null}
                    </div> : null}

                {loading || error || 'progress' === getStockItemsLoaded(this.state.selectedStock) || 'error' === getStockItemsLoaded(this.state.selectedStock) ?
                    <div style={{position: 'relative'}}>
                        <Loading
                            center={true}
                            style={{
                                wrapper: {margin: 0},
                            }}
                            errorWarning={error}
                        >
                            <div style={{margin: '4px 0 0 0'}}>
                                {'progress' === loadingWarehouses || 'error' === loadingWarehouses ? <p style={{textAlign: 'center', margin: '2px 0 2px 0'}}>{t('inventory.loading-warehouses')}</p> : null}
                                {'progress' === loadingStocks || 'error' === loadingStocks ? <p style={{textAlign: 'center', margin: '2px 0 2px 0'}}>{t('inventory.loading-stocks')}</p> : null}
                                {'progress' === getStockItemsLoaded(this.state.selectedStock) || 'error' === getStockItemsLoaded(this.state.selectedStock) ?
                                    <p style={{textAlign: 'center', margin: '2px 0 2px 0'}}>{t('inventory.loading-stock-items')}</p> : null}
                            </div>
                        </Loading>
                    </div> : null}

                {'-' !== this.state.selectedWarehouse && '-' !== this.state.selectedStock && 'progress' !== getStockItemsLoaded(this.state.selectedStock) && 'error' !== getStockItemsLoaded(this.state.selectedStock) && stockItems[this.state.selectedStock] ?
                    <React.Fragment>
                        <h4 style={{margin: '18px 0 6px 0'}}>{t('inventory.stock-item')}</h4>
                        <ButtonSmall active={this.state.openSelect} onClick={() => this.setState({openSelect: !this.state.openSelect, openCreate: false})}>{t('inventory.stock-item-select-existing')}</ButtonSmall>
                        &nbsp;{t('common:or')}&nbsp;
                        <ButtonSmall active={this.state.openCreate} onClick={() => this.setState({openCreate: !this.state.openCreate, openSelect: false})}>{t('inventory.stock-item-create-new')}</ButtonSmall>

                        <DropDown open={this.state.openSelect}>
                            {stockItems[this.state.selectedStock].length ?
                                <div>
                                    <ul style={{paddingLeft: '0'}}>
                                        {stockItems[this.state.selectedStock].map(stockItem => (
                                            <li key={stockItem.id} style={{listStyleType: 'none', margin: '3px 0', padding: '3px 0 3px 9px', borderBottom: '1px solid ' + theme.textColorLight}}>
                                                <input type={'radio'}
                                                       id={this.id + '--stockitem--' + stockItem.id}
                                                       name={this.id + '--stockitem'}
                                                       style={{transform: 'translateX(-6px)'}}/>
                                                <label htmlFor={this.id + '--stockitem--' + stockItem.id}>{stockItem.name} {stockItem.sku ? stockItem.sku : null}</label>
                                            </li>
                                        ))}
                                    </ul>
                                    <ButtonSmall>{t('common:selectNoun')}</ButtonSmall>
                                </div>
                                : <p style={{fontStyle: 'italic'}}>{t('inventory.no-stock-items-for-stock')}</p>}
                        </DropDown>

                        <DropDown open={this.state.openCreate}>
                            <h4 style={{margin: '18px 0 6px 0'}}>{t('inventory.stock-item-create')}</h4>
                            <p style={{margin: '6px 0 6px 0'}}>{t('inventory.stock-item-create-desc')}</p>
                            <ButtonSmall style={{marginRight: '6px'}} active={this.state.createManual} onClick={this.nameManual}>{t('inventory.stock-item-name-manual')}</ButtonSmall>
                            <ButtonSmall active={this.state.createAutomatic} onClick={this.nameAutomatic}>{t('inventory.stock-item-name-automatic')}</ButtonSmall>

                            {this.state.createManual || this.state.createAutomatic ?
                                <div>
                                    <div style={{display: 'flex', margin: '12px 0'}}>
                                        <InputGroup style={{wrapper: {marginRight: '12px'}}}>
                                            <Label htmlFor={this.id + 'stockitemsku'}>{t('inventory.sku')}</Label>
                                            <InputText id={this.id + 'stockitemsku'} onChange={(e) => this.setState({selectedSku: e.target.value})} value={this.state.selectedSku}/>
                                        </InputGroup>
                                        <InputGroup style={{wrapper: {marginRight: '12px'}}}>
                                            <Label htmlFor={this.id + 'stockitemname'}>{t('inventory.name')}</Label>
                                            <InputText id={this.id + 'stockitemname'} onChange={(e) => this.setState({selectedName: e.target.value})} value={this.state.selectedName}/>
                                        </InputGroup>
                                        <InputGroup>
                                            <Label htmlFor={this.id + 'stockamount_available'}>{t('inventory.amount_available')}</Label>
                                            <InputText type='number' id={this.id + 'stockamount_available'} min={0} onChange={(e) => this.setState({selectedAmountAvailable: e.target.value})} value={this.state.selectedAmountAvailable}/>
                                        </InputGroup>
                                    </div>
                                    <ButtonSmall style={{marginTop: '6px'}}
                                                 onClick={this.pushNewStockItem}
                                    ><MdSave color={'inherit'} style={{display: 'block', float: 'left', marginRight: '4px'}}/> {t('inventory.create-item')}</ButtonSmall>
                                </div>
                                : null}
                        </DropDown>
                    </React.Fragment>
                    : null}
            </React.Fragment>
        )
    };
}

export default withNamespaces('page--shop')(cancelPromise(storeWarehouse(storeStock(storeStockItem(withTheme(ProductInventoryActivate))))));
