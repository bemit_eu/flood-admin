import React from 'react';
import {MdDone} from 'react-icons/md';

import Loading from "../../component/Loading";

import {useTheme, createUseStyles} from '../../lib/theme';
import {ButtonSmall} from "../../component/Form";
import ButtonInteractiveDelete from "../../component/ButtonInteractiveDelete";
import StyleGrid from "../../lib/StyleGrid";

const useStyles = createUseStyles({
    wrapperLocaleActive: {
        display: 'flex',
        flexWrap: 'wrap',
        margin: '6px 0',
        [StyleGrid.smUp]: {
            flexWrap: 'no-wrap',
        }
    },
    localeDeleteBtn: {
        display: 'flex',
        '& svg': {
            marginLeft: '3px'
        },
        width: '100%',
        margin: '3px 0',
        fontStyle: 'italic',
        [StyleGrid.smUp]: {
            width: 'auto',
            margin: '0 0 0 auto',
            fontStyle: 'normal',
        }
    },
    subTitle: {},
});

const ProductDetailHead = (props) => {
    const {
        t,
        activeLocaleExists, activeLocalePossible, localeActive, pushDeleteLocale, savingDelete, savingNew
    } = props;
    const theme = useTheme();
    const classes = useStyles(theme);


    const pushNewLocale = () => {
        const {pushNewLocale} = props;
        if(pushNewLocale) {
            pushNewLocale();
        } else {
            console.error('pushNewLocale not assigned to ProductDetailHead.');
        }
    };

    return (
        <React.Fragment>
            {activeLocaleExists ?
                // when locale exists, show info and delete button
                <div className={classes.wrapperLocaleActive}>
                    <p style={{margin: 0, fontSize: '0.875rem'}}> {t('locale.active-is')} {'*' === localeActive ? t('locale.universal-locale') : localeActive}</p>
                    <ButtonInteractiveDelete
                        className={classes.localeDeleteBtn}
                        comp={(p) => (
                            <button {...p} style={{fontSize: '0.875rem', color: theme.textColor}}>{t('locale.delete')} {p.children}</button>
                        )}
                        onClick={pushDeleteLocale}
                    />
                    {savingDelete ?
                        ('progress' === savingDelete || 'error' === savingDelete ?
                            <React.Fragment>
                                <Loading style={{
                                    wrapper: {margin: 0},
                                    item: ('error' === savingDelete ? {borderColor: theme.errorColor} : {}),
                                }}/>

                            </React.Fragment> :
                            ('success' === savingDelete ?
                                <MdDone/>
                                : null))
                        : null}
                </div> :
                activeLocalePossible ?
                    // when locale not exists but possible, show info and create form
                    (<React.Fragment>
                        <p>{t('locale.active-not-existing')} <em><strong>{'*' === localeActive ? t('locale.universal-locale') : localeActive}</strong></em></p>
                        <div>
                            <ButtonSmall onClick={pushNewLocale}
                                         style={{marginTop: theme.gutter.base * 8 + 'px'}}
                            >{t('locale.new-locale.btn-create')}</ButtonSmall>

                            {savingNew ?
                                ('progress' === savingNew || 'error' === savingNew ?
                                    <React.Fragment>
                                        <Loading style={{
                                            wrapper: {margin: 0},
                                            item: ('error' === savingNew ? {borderColor: theme.errorColor} : {}),
                                        }}/>

                                    </React.Fragment> :
                                    ('success' === savingNew ?
                                        <MdDone/>
                                        : null))
                                : null}
                        </div>
                    </React.Fragment>)
                    :
                    // when locale not exists and not possible, show info
                    <p>{t('locale.active-not-valid')} <i>{'*' === localeActive ? t('locale.universal-locale') : localeActive}</i></p>}
        </React.Fragment>
    )
};

export default ProductDetailHead;
