import React from 'react';
import {withNamespaces} from "react-i18next";
import {MdList, MdAutorenew, MdPayment, MdLocalShipping, MdBeenhere, MdRotate90DegreesCcw, MdCheckCircle} from "react-icons/md";

import RouteCascaded from '../../lib/RouteCascaded';

import Nav from "../../component/Nav/Nav";
import Sidebar from "../../layout/ControlPanel/Sidebar";

class NavShop extends React.Component {

    render() {
        const {t,} = this.props;

        const navContent = (match) => {
            let content = [];

            content.push({
                path: match.url + '/all',
                lbl: {icon: (p) => <MdList className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.orders-nav.all')},
            });

            content.push({
                path: match.url + '/open',
                lbl: {icon: (p) => <MdAutorenew className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.orders-nav.open')},
            });

            content.push({
                path: match.url + '/payment-pending',
                lbl: {icon: (p) => <MdPayment className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.orders-nav.payment-pending')},
            });

            content.push({
                path: match.url + '/shipped',
                lbl: {icon: (p) => <MdLocalShipping className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.orders-nav.shipped')},
            });

            content.push({
                path: match.url + '/delivered',
                lbl: {icon: (p) => <MdBeenhere className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.orders-nav.delivered')},
            });

            content.push({
                path: match.url + '/retoure',
                lbl: {icon: (p) => <MdRotate90DegreesCcw className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.orders-nav.returned')},
            });

            content.push({
                path: match.url + '/finished',
                lbl: {icon: (p) => <MdCheckCircle className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.orders-nav.finished')},
            });

            return content;
        };

        return (
            <React.Fragment>
                <Sidebar
                    center={(props => <Nav path='/shop/orders' content={navContent} notxt={props.notxt}/>)}
                    textToggle={true}
                />

                {this.props.routes ? this.props.routes.map((route, i) => (
                    route.sidebar ?
                        <RouteCascaded key={i} {...route} component={route.sidebar}/> :
                        null
                )) : null}
            </React.Fragment>
        )
    };
}

export default withNamespaces('page--shop')(NavShop);
