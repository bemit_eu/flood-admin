import React from 'react';
import {sortString, isUndefined,} from "@formanta/js";
import {withContextConsumer} from "@formanta/react";
import {ApiRequest, GET} from "../../../lib/ApiRequest";
import {cancelPromise} from "../../../component/cancelPromise";
import {withApiControl} from "../../../component/ApiControl";
import {withBackend} from "../../BackendConnect";

const StoreStockContext = React.createContext({});

class StoreStockProviderBase extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            stocks: {},
            stocks2Warehouse: {},
            loadingStocks: false,
            loadStockList: this.loadStockList,
            stockItems: {},
            loadingStockItems: {},
            getStockItemsLoaded: this.getStockItemsLoaded,
            loadStockItemsList: this.loadStockItemsList
        };
    }

    componentDidUpdate(prevProps) {
        if(prevProps.hook_active !== this.props.hook_active) {
            this.setState({
                stocks: {},
                loadingStocks: false,
                stockItems: {},
            });
        }
    }

    loadStockList = (forceLoad = false) => {
        if(false === this.state.loadingStocks || forceLoad) {

            this.setState({loadingStocks: 'progress'});

            (new ApiRequest('api', GET, 'inventory/stocks'))
                .debug(true)
                .exec()
                .then((res) => {
                    if(res && res.data) {
                        const stocks = {};
                        const stock2Warehouse = {};
                        if(Array.isArray(res.data)) {
                            res.data.forEach((stock => {
                                if(stock.hasOwnProperty('warehouse')) {
                                    if(!stocks.hasOwnProperty(stock.warehouse)) {
                                        stocks[stock.warehouse] = [];
                                    }
                                    stocks[stock.warehouse].push(stock);
                                    stock2Warehouse[stock.id] = stock.warehouse;
                                }
                            }));
                            Object.keys(stocks).forEach(warehouse => {
                                stocks[warehouse] = sortString(stocks[warehouse], 'name');
                            });
                        }

                        this.setState({
                            stocks,
                            stock2Warehouse,
                            loadingStocks: true
                        });
                    } else {
                        this.setState({
                            loadingStocks: 'error'
                        });
                    }
                })
                .catch((err) => {
                    this.props.handleApiFail(err, 'listing Stocks');
                    if(err && !err.cancelled) {
                        this.setState({
                            loadingStocks: 'error'
                        });
                    }
                });
        }
    };

    getStockItemsLoaded = (stock) => {
        if(isUndefined(this.state.loadingStockItems[stock])) {
            return false;
        }
        return this.state.loadingStockItems[stock];
    };

    updateStockItemsLoaded = (stock, status) => {
        const loadingStockItems = {...this.state.loadingStockItems};
        loadingStockItems[stock] = status;
        this.setState({loadingStockItems});
    };

    loadStockItemsList = (stock, forceLoad = false) => {
        if(isUndefined(this.state.stockItems[stock]) || forceLoad) {

            this.updateStockItemsLoaded(stock, 'progress');

            (new ApiRequest('api', GET, 'inventory/stock/' + stock + '/items'))
                .debug(true)
                .exec()
                .then((res) => {
                    if(res && res.data) {

                        const stockItems = {...this.state.stockItems};
                        stockItems[stock] = sortString(res.data, 'name');

                        this.setState({
                            stockItems,
                        }, () => this.updateStockItemsLoaded(stock, true));
                    } else {
                        this.updateStockItemsLoaded(stock, 'error');
                    }
                })
                .catch((err) => {
                    this.props.handleApiFail(err, 'listing StockItems');
                    if(err && !err.cancelled) {
                        this.updateStockItemsLoaded(stock, 'error');
                    }
                });
        }
    };

    render() {
        return (
            <StoreStockContext.Provider value={this.state}>
                {this.props.children}
            </StoreStockContext.Provider>
        );
    }
}

const StoreStockProvider = cancelPromise(withBackend(withApiControl(StoreStockProviderBase)));

const storeStock = withContextConsumer(StoreStockContext.Consumer);

export {StoreStockProvider, storeStock};