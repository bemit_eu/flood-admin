import React from 'react';
import {sortString} from "@formanta/js";
import {withContextConsumer} from "@formanta/react";
import {ApiRequest, POST} from "../../../lib/ApiRequest";
import {cancelPromise} from "../../../component/cancelPromise";
import {withApiControl} from "../../../component/ApiControl";
import {withPusher} from "../../../component/Pusher/Pusher";
import {doApiRequest} from "../../../component/Pusher/Pushing";

const StoreOrderContext = React.createContext({});

class StoreOrderProviderBase extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            orders: [],
            loadingOrders: false,
            loadOrderList: this.loadOrderList,
            updateOrderState: this.updateOrderState
        };
    }

    /**
     * @todo make loadingOrders using state also, then remove force loading where needed
     * @param {[]} selectedHooks
     * @param state
     * @param forceLoad
     */
    loadOrderList = (selectedHooks, state, forceLoad = false) => {
        if(false === this.state.loadingOrders || forceLoad) {

            this.setState({loadingOrders: 'progress'});

            (new ApiRequest('api', POST, 'shop/order/list' + (state ? '/' + state : ''), {hooks: selectedHooks}))
                .debug(true)
                .exec()
                .then((res) => {
                    if(res && res.data) {
                        this.setState({
                            orders: sortString(res.data, 'date').reverse(),
                            loadingOrders: true
                        })
                    } else {
                        this.setState({
                            loadingOrders: 'error'
                        });
                    }
                })
                .catch((err) => {
                    this.props.handleApiFail(err, 'listing Orders', true, () => this.loadOrderList(state, forceLoad));
                    if(err && !err.cancelled) {
                        this.setState({
                            loadingOrders: 'error'
                        });
                    }
                });
        }
    };

    updateOrderState = (index, order_id, old_state, new_state) => {
        const {execPushing} = this.props;
        const endpoint = 'shop/order/update/' + order_id;

        const orders = [...this.state.orders];
        if(old_state && 'all' !== old_state && old_state !== new_state) {
            orders.splice(index, 1);
        } else {
            let order = {...orders[index]};
            order.state = new_state;
            orders[index] = order;
        }
        this.setState({orders: orders});
        return execPushing(
            doApiRequest(this.props.handleApiFail, POST, endpoint, 'updateOrderState', {state: new_state})
        );
    };

    render() {
        return (
            <StoreOrderContext.Provider value={this.state}>
                {this.props.children}
            </StoreOrderContext.Provider>
        );
    }
}

const StoreOrderProvider = cancelPromise(withPusher(withApiControl(StoreOrderProviderBase)));

const storeOrder = withContextConsumer(StoreOrderContext.Consumer);

export {StoreOrderProvider, storeOrder};