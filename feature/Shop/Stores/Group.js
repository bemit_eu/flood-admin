import React from 'react';
import {sortString} from "@formanta/js";
import {withContextConsumer} from "@formanta/react";
import {ApiRequest, DELETE, GET, POST, PUT} from "../../../lib/ApiRequest";
import {cancelPromise} from "../../../component/cancelPromise";
import {withApiControl} from "../../../component/ApiControl";
import {Pushing, withPusher} from "../../../component/Pusher/Pusher";

const StoreGroupContext = React.createContext({});

class StoreGroupProviderBase extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            groups: [],
            loadingGroups: false,
            createGroup: this.createGroup,
            deleteGroup: this.deleteGroup,
            loadGroup: this.loadGroup,
            updateGroup: this.updateGroup,
            loadGroupList: this.loadGroupList
        };
    }

    createGroup = (name) => {
        const endpoint = 'shop/group';

        const {execPushing} = this.props;

        return execPushing(
            new Pushing(
                'createShopGroup',
                endpoint,
                (resolve, reject) => {
                    const data = {
                        name
                    };
                    (new ApiRequest('api', POST, endpoint, data))
                        .debug(true)
                        .header({})
                        .exec()
                        .then((res) => {
                            if(res && res.data) {
                                resolve(res.data);
                            } else {
                                reject();
                            }
                        })
                        .catch((err) => {
                            this.props.handleApiFail(err, 'createShopGroup');
                            reject();
                        });
                }
            )
        ).then((data) => {
            if(data.error) {
            } else {
                this.loadGroupList(true);
            }
            return data;
        });
    };

    updateGroup = (group, name) => {
        const {execPushing} = this.props;

        const endpoint = 'shop/group/' + group;

        return execPushing(
            new Pushing(
                'updateShopGroup',
                endpoint,
                (resolve, reject) => {
                    (new ApiRequest('api', PUT, endpoint, {name}))
                        .debug(true)
                        .header({})
                        .exec()
                        .then((res) => {
                            if(res && res.data) {
                                resolve(res.data);
                                this.loadGroupList(true);
                            } else {
                                reject();
                            }
                        })
                        .catch((err) => {
                            this.props.handleApiFail(err, 'updateShopGroup');
                            reject();
                        });
                }
            )
        );
    };

    deleteGroup = (group) => {
        const endpoint = 'shop/group/' + group;

        const {execPushing} = this.props;

        return execPushing(
            new Pushing(
                'deleteShopGroup',
                endpoint,
                (resolve, reject) => {
                    (new ApiRequest('api', DELETE, endpoint))
                        .debug(true)
                        .header({})
                        .exec()
                        .then((res) => {
                            if(res && res.data) {
                                resolve(res.data);
                            } else {
                                reject();
                            }
                        })
                        .catch((err) => {
                            this.props.handleApiFail(err, 'deleteShopGroup');
                            reject();
                        });
                }
            )
        ).then((data) => {
            if(data.error) {
            } else {
                this.loadGroupList(true);
            }
            return data;
        });
    };

    loadGroup = (group) => {
        return (new ApiRequest('api', GET, 'shop/group/' + group))
            .debug(true)
            .header({})
            .exec()
            .then((res) => {
                if(res && res.data) {
                    if(res.data.error) {
                        return Promise.reject(res);
                    }
                    return res.data;
                } else {
                    return Promise.reject(res);
                }
            })
            .catch((err) => {
                this.props.handleApiFail(err, 'loadShopGroup');
                return Promise.reject(err);
            });
    };

    loadGroupList = (forceLoad = false) => {
        if(false === this.state.loadingGroups || forceLoad) {

            this.setState({loadingGroups: 'progress'});
            (new ApiRequest('api', GET, 'shop/group'))
                .debug(true)
                .exec()
                .then((res) => {
                    if(res && res.data) {
                        this.setState({
                            groups: sortString(res.data, 'name'),
                            loadingGroups: true
                        });
                    } else {
                        this.setState({
                            loadingGroups: 'error'
                        });
                    }
                })
                .catch((err) => {
                    this.props.handleApiFail(err, 'listing Groups', true, () => this.loadGroupList(forceLoad));
                    if(err && !err.cancelled) {
                        this.setState({
                            loadingGroups: 'error'
                        });
                    }
                });
        }
    };

    render() {
        return (
            <StoreGroupContext.Provider value={this.state}>
                {this.props.children}
            </StoreGroupContext.Provider>
        );
    }
}

const StoreGroupProvider = cancelPromise(withPusher(withApiControl(StoreGroupProviderBase)));

const storeGroup = withContextConsumer(StoreGroupContext.Consumer);

export {StoreGroupProvider, storeGroup};