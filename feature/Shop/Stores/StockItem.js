import React from 'react';
import {withContextConsumer} from "@formanta/react";
import {isUndefined} from "@formanta/js";
import {ApiRequest, GET, POST} from "../../../lib/ApiRequest";
import {cancelPromise} from "../../../component/cancelPromise";
import {withApiControl} from "../../../component/ApiControl";
import {withBackend} from "../../BackendConnect";
import {withPusher} from "../../../component/Pusher/Pusher";
import {doApiRequest} from "../../../component/Pusher/Pushing";

const StoreStockItemContext = React.createContext({});

class StoreStockItemProviderBase extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            stockItemsDetails: {},
            stockBundleDetails: {},
            stockProduct2Items: {},
            loadingStockItem: {},
            loadingStockItemForProduct: {},
            createStockItem: this.createStockItem,
            isStockItemLoaded: this.isStockItemLoaded,
            updateStockItem: this.updateStockItem,
            updateStockItemQuantity: this.updateStockItemQuantity,
            isStockItemLoadedForProduct: this.isStockItemLoadedForProduct,
            loadStockItem: this.loadStockItem,
            loadStockItems: this.loadStockItems,
            loadStockItemForProduct: this.loadStockItemForProduct
        };
    }

    componentDidUpdate(prevProps) {
        if(prevProps.hook_active !== this.props.hook_active) {
            this.setState({
                stockItemsDetails: {},
                stockProduct2Items: {},
                loadingStockItem: {},
                loadingStockItemForProduct: {},
            });
        }
    }

    createStockItem = (id, stock, sku, name, amount_available) => {
        const endpoint = 'inventory/stock-item';

        const {
            execPushing
        } = this.props;
        if(!Array.isArray(id)) {
            id = [id];
        }

        const data = {
            stock,
            sku,
            name,
            amount_available,
            products: id
        };

        return execPushing(
            doApiRequest(this.props.handleApiFail, POST, endpoint, 'createStockItem', data)
        ).then((data) => {
            id.forEach(i => {
                this.loadStockItemForProduct(i, true);
            });
            // todo: do with store `stock` reload stock items of stock
            //this.loadProductList(group, true);
            return data;
        });
    };

    updateStockItem = (id, data) => {
        const {execPushing} = this.props;
        const endpoint = 'inventory/stock-item/' + id;

        return execPushing(
            doApiRequest(this.props.handleApiFail, POST, endpoint, 'updateStockItem', data)
        ).then((data) => {
            //this.loadUserList(true);
            return data;
        });
    };

    updateStockItemQuantity = (id, data) => {
        const {execPushing} = this.props;
        const endpoint = 'inventory/stock-item-quantity/' + id;

        return execPushing(
            doApiRequest(this.props.handleApiFail, POST, endpoint, 'updateStockItemQuantity', data)
        ).then((data) => {

            if(
                data.hasOwnProperty('stock_item') && this.state.stockItemsDetails.hasOwnProperty(id) &&
                (data.stock_item.hasOwnProperty('amount_available') || data.stock_item.hasOwnProperty('amount_reserved'))
            ) {
                const stockItemsDetails = {...this.state.stockItemsDetails};
                const stockItem = {...stockItemsDetails[id]};
                if(data.stock_item.hasOwnProperty('amount_available')) {
                    stockItem.amount_available = data.stock_item.amount_available;
                }
                if(data.stock_item.hasOwnProperty('amount_reserved')) {
                    stockItem.amount_reserved = data.stock_item.amount_reserved;
                }
                stockItemsDetails[id] = stockItem;

                this.setState({
                    stockItemsDetails,
                });
            }

            return data;
        });
    };

    isStockItemLoaded = (item) => {
        if(isUndefined(this.state.loadingStockItem[item])) {
            return false;
        }
        return this.state.loadingStockItem[item];
    };

    updateStockItemsLoaded = (item, status) => {
        const loadingStockItem = {...this.state.loadingStockItem};
        loadingStockItem[item] = status;
        // eslint-disable-next-line
        this.state.loadingStockItem[item] = status;
        this.setState({loadingStockItem});
    };

    isStockItemLoadedForProduct = (item) => {
        if(isUndefined(this.state.loadingStockItemForProduct[item])) {
            return false;
        }
        return this.state.loadingStockItemForProduct[item];
    };


    updateStockItemsLoadedForProduct = (product, status) => {
        const loadingStockItemForProduct = {...this.state.loadingStockItemForProduct};
        loadingStockItemForProduct[product] = status;
        // eslint-disable-next-line
        this.state.loadingStockItem[product] = status;
        this.setState({loadingStockItemForProduct});
    };

    loadStockItem = (item, forceLoad = false) => {
        if(false === this.isStockItemLoaded(item) || forceLoad) {

            this.updateStockItemsLoaded(item, 'progress');

            (new ApiRequest('api', GET, 'inventory/stock-item/single/' + item))
                .debug(true)
                .exec()
                .then((res) => {
                    if(res && res.data) {

                        const stockItemsDetails = {...this.state.stockItemsDetails};
                        stockItemsDetails[item] = res.data;

                        this.setState({
                            stockItemsDetails,
                        }, () => this.updateStockItemsLoaded(item, true));
                    } else {
                        this.updateStockItemsLoaded(item, 'error');
                    }
                })
                .catch((err) => {
                    this.props.handleApiFail(err, 'listing StockItemsDetails');
                    if(err && !err.cancelled) {
                        this.updateStockItemsLoaded(item, 'error');
                    }
                });
        }
    };

    loadStockItems = () => {
        return (new ApiRequest('api', GET, 'inventory/stock-items'))
            .debug(true)
            .exec()
            .then((res) => {
                if(res && res.data) {
                    return res.data;
                } else {
                    return Promise.reject(res);
                }
            })
            .catch((err) => {
                return Promise.reject(err);
            });
    };

    loadStockItemForProduct = (product, forceLoad = false) => {
        if(!this.isStockItemLoadedForProduct(product) || forceLoad) {
            this.updateStockItemsLoadedForProduct(product, 'progress');

            (new ApiRequest('api', GET, 'inventory/stock-item/of-product/' + product))
                .debug(true)
                .exec()
                .then((res) => {
                    if(res) {
                        if(res.data) {
                            if(res.data.id) {
                                const stockItemsDetails = {...this.state.stockItemsDetails};
                                stockItemsDetails[res.data.id] = res.data;
                                const stockProduct2Items = {...this.state.stockProduct2Items};
                                stockProduct2Items[product] = res.data.id;

                                this.setState({
                                    stockItemsDetails,
                                    stockProduct2Items,
                                }, () => {
                                    this.updateStockItemsLoadedForProduct(product, true);
                                    this.updateStockItemsLoaded(res.data.id, true);
                                });
                            } else if(res.data.bundle_info && res.data.bundled && res.data.products) {
                                const stockBundleDetails = {...this.state.stockBundleDetails};
                                stockBundleDetails[product] = {
                                    products: res.data.products,
                                    bundled: res.data.bundled,
                                    info: res.data.bundle_info
                                };
                                this.setState({
                                    stockBundleDetails
                                });
                                this.updateStockItemsLoadedForProduct(product, true);
                            } else {
                                this.updateStockItemsLoadedForProduct(product, 0);
                            }
                        } else {
                            this.updateStockItemsLoadedForProduct(product, 0);
                        }
                    } else {
                        this.updateStockItemsLoadedForProduct(product, 'error');
                    }
                })
                .catch((err) => {
                    this.props.handleApiFail(err, 'listing StockItemsForProduct');
                    if(err && !err.cancelled) {
                        this.updateStockItemsLoadedForProduct(product, 'error');
                    }
                });
        }
    };

    render() {
        return (
            <StoreStockItemContext.Provider value={this.state}>
                {this.props.children}
            </StoreStockItemContext.Provider>
        );
    }
}

const StoreStockItemProvider = cancelPromise(withBackend(withPusher(withApiControl(StoreStockItemProviderBase))));

const storeStockItem = withContextConsumer(StoreStockItemContext.Consumer);

export {StoreStockItemProvider, storeStockItem};
