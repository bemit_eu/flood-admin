import React from 'react';
import {withContextConsumer} from "@formanta/react";
import {ApiRequest, GET} from "../../../lib/ApiRequest";
import {cancelPromise} from "../../../component/cancelPromise";
import {withApiControl} from "../../../component/ApiControl";
import {withBackend} from "../../BackendConnect";

const StoreWarehouseContext = React.createContext({});

class StoreWarehouseProviderBase extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            warehouses: [],
            loadingWarehouses: false,
            loadWarehouseList: this.loadWarehouseList
        };
    }

    componentDidUpdate(prevProps) {
        if(prevProps.hook_active !== this.props.hook_active) {
            this.setState({
                warehouses: [],
                loadingWarehouses: false,
            });
        }
    }

    loadWarehouseList = (forceLoad = false) => {
        if(false === this.state.loadingWarehouses || forceLoad) {

            this.setState({loadingWarehouses: 'progress'});

            (new ApiRequest('api', GET, 'inventory/warehouses'))
                .debug(true)
                .exec()
                .then((res) => {
                    if(res && res.data) {
                        this.setState({
                            warehouses: res.data,
                            loadingWarehouses: true
                        });
                    } else {
                        this.setState({
                            loadingWarehouses: 'error'
                        });
                    }
                })
                .catch((err) => {
                    this.props.handleApiFail(err, 'listing Warehouses');
                    if(err && !err.cancelled) {
                        this.setState({
                            loadingWarehouses: 'error'
                        });
                    }
                });
        }
    };

    render() {
        return (
            <StoreWarehouseContext.Provider value={this.state}>
                {this.props.children}
            </StoreWarehouseContext.Provider>
        );
    }
}

const StoreWarehouseProvider = cancelPromise(withBackend(withApiControl(StoreWarehouseProviderBase)));

const storeWarehouse = withContextConsumer(StoreWarehouseContext.Consumer);

export {StoreWarehouseProvider, storeWarehouse};