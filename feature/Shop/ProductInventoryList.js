import React from 'react';
import {withNamespaces} from 'react-i18next';
import {withTimeout} from '@formanta/react';
import {withTheme} from '../../lib/theme';

import Loading from "../../component/Loading";

import {storeWarehouse} from "./Stores/Warehouse";
import {storeStock} from "./Stores/Stock";

import {cancelPromise} from "../../component/cancelPromise";
import {storeStockItem} from "./Stores/StockItem";
import ProductInventory from "./ProductInventory";
import {storeProduct} from "./Stores/Product";
import {MdSync} from "react-icons/md";

class ProductInventoryList extends React.PureComponent {

    state = {
        loadingBundle: false,
        bundledProducts: {},
    };

    componentDidMount() {
        this.handleBundle();
    }

    componentDidUpdate(prevProps) {
        if(prevProps.data !== this.props.data) {
            this.handleBundle();
        }
    }

    handleBundle() {
        const {
            data, loadProductByProductNo
        } = this.props;

        if(data && data.data && data.data.products) {
            let product_nos = [];
            data.data.products.forEach((product) => {
                if(product.product_no) {
                    product_nos.push(product.product_no);
                }
            });

            this.setState({loadingBundle: 'progress'});
            loadProductByProductNo(data.locale, product_nos)
                .then((data) => {
                    this.setState({
                        bundledProducts: data,
                        loadingBundle: 'success'
                    });
                })
                .catch((err) => {
                    this.setState({loadingBundle: 'error'});
                });
        }
    }

    forceUpdateStockItem = (product) => {
        const {
            loadStockItemForProduct,
        } = this.props;
        loadStockItemForProduct(product, true);
    };

    render() {
        const {
            t, theme,
            data,
            localeActive,
            product_name,
            hasVariation, isBundle,
        } = this.props;

        return (
            <React.Fragment>
                <h2>{hasVariation ? 'Master' : null}{!isBundle ? ' Product' : null}{isBundle ? 'Bundle' : null}</h2>
                <ProductInventory
                    data={data}
                    product_name={product_name}
                    localeActive={localeActive}
                    displayCompact={true}
                    isBundle={isBundle}
                />

                {hasVariation ?
                    <React.Fragment>
                        <h2 style={{marginTop: '2rem'}}>Variations</h2>
                    </React.Fragment>
                    : null}

                {isBundle ?
                    <React.Fragment>
                        <h2 style={{marginTop: '2rem'}}>{t('inventory.titles.bundled-products')}</h2>
                        {this.state.loadingBundle ?
                            ('progress' === this.state.loadingBundle || 'error' === this.state.loadingBundle ?
                                <React.Fragment>
                                    <Loading style={{
                                        wrapper: {margin: 0},
                                        item: ('error' === this.state.loadingBundle ? {borderColor: theme.errorColor} : {}),
                                    }}>Loading Bundled Products</Loading>
                                </React.Fragment> : null)
                            : null}
                        {'success' === this.state.loadingBundle ?
                            data.data.products.map((product, i) => (
                                <div key={i} style={{margin: '0 0 30px 0'}}>
                                    {this.state.bundledProducts[product.product_no] ?
                                        <React.Fragment>
                                            <h3 style={{marginBottom: '6px', display: 'flex'}}>{this.state.bundledProducts[product.product_no].product_name}&nbsp;
                                                <i>x{product.qty}</i>
                                                <button style={{marginLeft: 4}} onClick={() => this.forceUpdateStockItem(this.state.bundledProducts[product.product_no].id)}><MdSync color={theme.textColor} size={'1.25em'}/></button>
                                            </h3>
                                            <div style={{marginLeft: '9px'}}>
                                                <ProductInventory
                                                    data={this.state.bundledProducts[product.product_no]}
                                                    product_name={this.state.bundledProducts[product.product_no].product_name}
                                                    localeActive={localeActive}
                                                    displayCompact={true}
                                                />
                                            </div>
                                        </React.Fragment>
                                        : <h3>{product.product_no} <i>x{product.qty}</i> error</h3>}
                                </div>
                            ))
                            : null}
                    </React.Fragment>
                    : null}
            </React.Fragment>
        )
    };
}

export default withNamespaces('page--shop')(cancelPromise(withTimeout(storeProduct(storeWarehouse(storeStock(storeStockItem(withTheme(ProductInventoryList))))))));
