import React from 'react';
import {withNamespaces} from 'react-i18next';
import {withStyles} from '../../lib/theme';

import {storeWarehouse} from "./Stores/Warehouse";
import {storeStock} from "./Stores/Stock";

import {cancelPromise} from "../../component/cancelPromise";
import Loading from "../../component/Loading";
import {ButtonSmall as Button} from "../../component/Form";
import {storeStockItem} from "./Stores/StockItem";
import {DropDown} from "../../lib/DropDown";
import ProductInventoryDetails from "./ProductInventoryDetails";
import ProductInventoryActivate from "./ProductInventoryActivate";

const styles = {
    table: {
        borderCollapse: 'collapse',
        marginLeft: '-4px',
        '& th': {
            padding: '0 4px'
        },
        '& td': {
            padding: '0 4px'
        },
        '& td.num': {
            textAlign: 'right'
        },
    }
};

class ProductInventory extends React.PureComponent {
    state = {
        activateInventory: false,
    };

    componentDidMount() {
        this.loadStockItem();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(prevProps.data !== this.props.data) {
            this.loadStockItem();
        }
    }

    loadStockItem() {
        const {
            data,
            loadStockItemForProduct,
            loadWarehouseList, loadStockList,
        } = this.props;

        // todo: check loading again for bundled products
        /*if(data && data.data && data.products) {
            if(data && data.id) {
                loadStockItemForProduct(data.id, false);
            }
            loadWarehouseList();
            loadStockList();
        }*/
        if(data) {
            if(data.id) {
                loadStockItemForProduct(data.id, false);
            }
            loadWarehouseList();
            loadStockList();
        }
    }

    render() {
        const {
            t, classes,
            loadingStocks,
            loadingWarehouses, product_name,
            stockBundleDetails, stockItemsDetails, isStockItemLoadedForProduct, stockProduct2Items,
            displayCompact,
            data, localeActive,
            isBundle
        } = this.props;

        return (
            <React.Fragment>
                {data && data.id ? <div>
                    {('progress' === isStockItemLoadedForProduct(data.id) || 'error' === isStockItemLoadedForProduct(data.id) ||
                    true !== loadingStocks || true !== loadingWarehouses ?
                        <div style={{position: 'relative'}}>
                            <Loading
                                center={true}
                                style={{
                                    wrapper: {margin: 0},
                                }}
                                errorWarning={'error' === isStockItemLoadedForProduct(data.id)}
                            ><p style={{textAlign: 'center', margin: '6px 0 2px 0'}}>{t('editbasedata.checking-inventory')}</p></Loading>
                        </div> :
                        null)}

                    {(true === isStockItemLoadedForProduct(data.id) || 0 === isStockItemLoadedForProduct(data.id)) &&
                    (true === loadingStocks && true === loadingWarehouses) ?
                        <React.Fragment>
                            {isBundle && stockBundleDetails[data.id] ?
                                <div style={{marginBottom: 24}}>
                                    <h4 style={{margin: displayCompact ? '9px 0' : '28px 0 3px 0'}}>{t('page--shop:inventory.titles.bundled-stat')}</h4>

                                    {Object.keys(stockBundleDetails[data.id].info.products).length ?
                                        <React.Fragment>
                                            {stockBundleDetails[data.id].info && false !== stockBundleDetails[data.id].info.total ?
                                                <p style={{margin: displayCompact ? '6px 0' : '12px 0'}}>
                                                    {t('page--shop:inventory.bundled-stat.total')}: {stockBundleDetails[data.id].info.total}
                                                </p> : null}

                                            {stockBundleDetails[data.id].bundled && stockBundleDetails[data.id].products &&
                                            stockBundleDetails[data.id].info && stockBundleDetails[data.id].info.products ? <React.Fragment>
                                                <h4 style={{margin: displayCompact ? '6px 0' : '12px 0'}}>
                                                    {t('page--shop:inventory.bundled-stat.product_possible')}
                                                </h4>

                                                <table className={classes.table}>
                                                    <thead>
                                                    <tr>
                                                        <th>{t('editbasedata.product_no')}</th>
                                                        <th>{t('page--shop:inventory.available')}</th>
                                                        <th>{t('page--shop:inventory.reserved')}</th>
                                                        <th>{t('page--shop:inventory.bundled-stat.per_bundle')}</th>
                                                        <th>{t('page--shop:inventory.bundled-stat.max_bundle')}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    {stockBundleDetails[data.id].products.map((product) => (
                                                        <tr key={product.product_no}>
                                                            <td>{product.product_no}</td>
                                                            {stockBundleDetails[data.id].info.products[product.product_no] ?
                                                                <React.Fragment>
                                                                    <td className={'num'}>{stockBundleDetails[data.id].bundled[product.product_no].amount_available}</td>
                                                                    <td className={'num'}>{stockBundleDetails[data.id].bundled[product.product_no].amount_reserved}</td>
                                                                    <td className={'num'}>{product.qty}</td>
                                                                    <td className={'num'}>{stockBundleDetails[data.id].info.products[product.product_no].toFixed(1)}</td>
                                                                </React.Fragment>
                                                                : <td colSpan={4} style={{textAlign: 'centercon'}}>{t('page--shop:inventory.not-activated')}</td>}
                                                        </tr>
                                                    ))}
                                                    </tbody>
                                                </table>

                                            </React.Fragment> : null}
                                        </React.Fragment>
                                        : <p>{t('page--shop:inventory.not-activated-in-bundle')}</p>}
                                </div>
                                : null}

                            {stockItemsDetails && stockProduct2Items && stockProduct2Items[data.id] && stockItemsDetails[stockProduct2Items[data.id]] ?
                                <React.Fragment>
                                    <ProductInventoryDetails stockItemDetails={stockItemsDetails[stockProduct2Items[data.id]]} product={data.id}
                                                             product_no={data.product_no}
                                                             product_name={product_name}
                                                             localeActive={localeActive}
                                                             displayCompact={displayCompact}
                                    />
                                </React.Fragment>
                                : <React.Fragment>
                                    <Button
                                        active={this.state.activateInventory}
                                        style={{margin: '0 auto 12px auto'}}
                                        onClick={() => this.setState({activateInventory: !this.state.activateInventory})}>{t('btn-inventory-activate')}</Button>
                                    <DropDown open={this.state.activateInventory}>
                                        <ProductInventoryActivate id={data.id}
                                                                  product_no={data.product_no}
                                                                  product_name={product_name}
                                                                  localeActive={localeActive}
                                        />
                                    </DropDown>
                                </React.Fragment>}
                        </React.Fragment> : null}
                </div> : null}
            </React.Fragment>
        )
    };
}

export default withNamespaces('page--shop-product')(cancelPromise(storeWarehouse(storeStock(storeStockItem(withStyles(styles)(ProductInventory))))));
