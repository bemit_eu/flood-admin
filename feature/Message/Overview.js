import React from 'react';
import {withNamespaces} from 'react-i18next';
import {MdAdd} from 'react-icons/md';
import posed from 'react-pose';

import {cancelPromise} from "../../component/cancelPromise";
import {DataGrid} from "../../component/DataGrid/DataGrid";
import MessagesListItem from "./MessagesListItem";
import {ButtonSmall} from "../../component/Form";
import {withBackend} from "../BackendConnect";
import {withAuthPermission} from "../AuthPermission";
//import {FEATURE_MESSAGE} from "../../permissions";
import {ContainerStyle, SplitScreen} from "../../component/SplitScreen";
import MessageDetails from './MessageDetails';
import MessageEdit from './MessageEdit';
import MessageReply from './MessageReply';
import {storeMessage} from "./Stores/Message";

const DropDown = posed.div({
    hidden: {
        height: 0,
        marginBottom: 0,
    },
    visible: {
        height: 'min-content',
        marginBottom: '24px',
    }
});

const initialState = {
    showCreate: false,
};

class Messages extends React.PureComponent {

    state = initialState;

    componentDidMount() {
        const {
            changeStyleInnerWrapper
        } = this.props;

        changeStyleInnerWrapper(ContainerStyle);
        this.fetch();
    }

    componentWillUnmount() {
        this.setState(initialState);
        this.props.changeStyleInnerWrapper({});
    }

    fetch(force = false) {
        this.props.loadMessageList(force);
    }

    showCreate = (p) => <ButtonSmall onClick={() => this.setState({showCreate: !this.state.showCreate})}
                                     style={{marginRight: 6}}
                                     active={this.state.showCreate}><MdAdd style={{verticalAlign: 'bottom'}}/> {this.props.t('create-message-btn')}</ButtonSmall>;

    element = (p) => <MessagesListItem t={this.props.t} {...p}/>;

    top = (p) => {
        const {
            t, match,
        } = this.props;

        return <React.Fragment>
            {match.params.message ? <MessageDetails message={match.params.message}/> : null}

            {/*match.params.action && 'forward' === match.params.action ? <MessageReply message={match.params.message}/> : null*/}

            <DropDown withParent={false} pose={match.params.action && 'reply' === match.params.action ? 'visible' : 'hidden'} style={{
                overflow: 'hidden', display: 'flex', flexDirection: 'column'
            }}>
                {match.params.action && 'reply' === match.params.action ? <MessageReply message={match.params.message}/> : null}
            </DropDown>

            <DropDown withParent={false} pose={this.state.showCreate ? 'visible' : 'hidden'} style={{
                overflow: 'hidden', display: 'flex', flexDirection: 'column'
            }}>
                <h2 style={{margin: '0 0 3px 0'}}>{t('create-message')}</h2>
                {this.state.showCreate ?
                    <MessageEdit create={true}/> :
                    null}
            </DropDown>
        </React.Fragment>
    };

    render() {
        const {
            t, match,
            messages, loadingMessages,
        } = this.props;

        return (
            <React.Fragment>
                <h1>{t('title' + (match.params.message ? '-details' : ''))}</h1>

                <SplitScreen

                    isTopOpen={this.state.showCreate || match.params.action || match.params.message}
                    topRender={this.top}

                    center={(
                        <DataGrid

                            active={this.props.match.params.message}
                            activeFilter={'id'}

                            headers={[
                                {
                                    lbl: t('list.head.time'),
                                    search: 'time'
                                },
                                {
                                    lbl: t('list.head.id'),
                                    search: 'id'
                                },
                                {
                                    lbl: t('list.head.subject'),
                                    search: 'subject'
                                },
                                {
                                    lbl: t('list.head.sender'),
                                    search: 'sender.name|sender.email'
                                },
                                {
                                    lbl: t('list.head.recipient'),
                                    search: 'recipient.name|recipient.email'
                                },
                                {
                                    lbl: t('list.head.type'),
                                    search: 'type'
                                },
                                {
                                    lbl: t('list.head.hook'),
                                    search: 'hook'
                                },
                                {lbl: ''},
                            ]}
                            emptyList={t('list.errors.no-messages')}
                            loadingLabel={t('api.loading-message-list')}
                            list={messages}
                            loading={loadingMessages}
                            controls={[this.showCreate]}
                            reload={() => this.fetch(true)}
                            element={this.element}
                        />
                    )}
                />
            </React.Fragment>
        )
    };
}

export default withNamespaces('page--message')(cancelPromise(withAuthPermission(withBackend(storeMessage(Messages)))));
