import React from 'react';
import {withNamespaces} from 'react-i18next';
import {withRouter} from 'react-router-dom';
import {i18n} from "../../lib/i18n";
import {withTimeout} from "@formanta/react";

import {useTheme} from '../../lib/theme';
import {Button} from '../../component/Form';

import {withPusher} from "../../component/Pusher/Pusher";
import {storeMessage} from "./Stores/Message";
import {cancelPromise} from "../../component/cancelPromise";
import Loading from "../../component/Loading";
import ButtonInteractiveDelete from "../../component/ButtonInteractiveDelete";

class MessageDelete extends React.Component {
    state = {
        deleting: false,
    };

    pushDeleteMessage = () => {
        const {
            message,
            deleteMessage, cancelPromise, setTimeout,
            history
        } = this.props;

        cancelPromise(deleteMessage(message))
            .then((data) => {
                if(data.error) {
                    this.setState({deleting: 'error'});
                    return;
                }

                this.setState({
                    deleting: 'success'
                });

                history.push('/' + i18n.languages[0] + '/messages');

                setTimeout(() => {
                    this.setState({deleting: false});
                }, 4000);
            })
            .catch((err) => {
                if(err && !err.cancelled) {
                    this.setState({deleting: 'error'});
                }
            });
    };

    button = (p) => {
        const {
            t,
        } = this.props;
        const theme = useTheme();

        return <Button {...p} style={{color: theme.textColor}} type={'button'}>{t('edit.btn-delete')} {p.children} {('progress' === this.state.deleting || 'error' ===
        this.state.deleting ?
            <Loading style={{
                wrapper: {margin: 0},
                item: ('error' === this.state.deleting ? {borderColor: theme.errorColor} : {}),
            }}/> :
            null)}</Button>;
    };

    render() {

        return (
            <React.Fragment>
                <ButtonInteractiveDelete
                    onClick={this.pushDeleteMessage}
                    comp={this.button}/>
            </React.Fragment>
        )
    };
}

export default withNamespaces('page--message')(withPusher(withTimeout(withRouter(cancelPromise(storeMessage(MessageDelete))))));
