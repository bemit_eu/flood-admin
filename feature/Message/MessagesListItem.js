import React from 'react';
import {MdReply, MdForward, MdChevronRight} from 'react-icons/md';
import {Link} from 'react-router-dom';

import {i18n} from '../../lib/i18n';
import {withTheme, withStyles} from '../../lib/theme';
import {withBackend} from "../BackendConnect";

const styles = theme => ({
    item: {
        '& td:first-child': {
            borderLeft: '4px solid transparent',
            transition: 'border-left ' + theme.transition
        },
        '&.active td:first-child': {
            borderLeft: '4px solid ' + theme.accent.blue
        }
    },
    cell: {
        padding: '2px 6px',
        '&:first-child': {
            paddingLeft: '6px',
        },
        '&:last-child': {
            paddingRight: 0
        }
    }
});

class MessagesListItem extends React.Component {

    state = {
        showDetails: false
    };

    render() {
        const {
            t, theme, classes,
            active: act, item,
        } = this.props;

        let active = false;
        if(Array.isArray(act) && act.includes(item.id)) {
            active = true;
        } else if(act && act === item.id) {
            active = true;
        }

        return (
            <React.Fragment>
                <tr className={classes.item + (active ? ' active' : '')}>
                    <td className={classes.cell} style={{borderBottom: '1px solid ' + theme.textColorLight}}>
                        <span style={{fontSize: '0.875rem'}}>{item.time}</span>
                    </td>
                    <td className={classes.cell} style={{borderBottom: '1px solid ' + theme.textColorLight}}>
                        <span style={{fontSize: '0.875rem'}}>{item.id}</span>
                    </td>
                    <td className={classes.cell} style={{borderBottom: '1px solid ' + theme.textColorLight}}>
                        {item.subject}
                    </td>
                    <td className={classes.cell} style={{borderBottom: '1px solid ' + theme.textColorLight}}>
                        {item.sender ?
                            item.sender.name ?
                                item.sender.name + (item.sender.email ? ' | ' + item.sender.email : '') :
                                item.sender.email ? item.sender.email : ''
                            : t('no-sender')}
                    </td>
                    <td className={classes.cell} style={{borderBottom: '1px solid ' + theme.textColorLight}}>
                        {item.recipient ?
                            item.recipient.name ?
                                item.recipient.name + (item.recipient.email ? ' | ' + item.recipient.email : '') :
                                item.recipient.email ? item.recipient.email : ''
                            : t('no-recipient')}
                    </td>
                    <td className={classes.cell} style={{borderBottom: '1px solid ' + theme.textColorLight}}>
                        {item.type}
                    </td>
                    <td className={classes.cell} style={{borderBottom: '1px solid ' + theme.textColorLight}}>
                        {item.hook ? item.hook : '-'}
                    </td>
                    <td className={classes.cell} style={{borderBottom: '1px solid ' + theme.textColorLight}}>
                        <div style={{display: 'flex'}}>
                            <Link to={'/' + i18n.languages[0] + '/messages/' + item.id + '/reply'} style={{padding: '3px'}}><MdReply color={theme.textColor} size={'1.25em'}/></Link>
                            <Link to={'/' + i18n.languages[0] + '/messages/' + item.id + '/forward'} style={{padding: '3px'}}><MdForward color={theme.textColor} size={'1.25em'}/></Link>
                            <Link to={'/' + i18n.languages[0] + '/messages/' + item.id} style={{padding: '3px'}}><MdChevronRight color={theme.textColor} size={'1.25em'}/></Link>
                        </div>
                    </td>
                </tr>
            </React.Fragment>
        )
    };
}

export default withBackend(withStyles(styles)(withTheme(MessagesListItem)));
