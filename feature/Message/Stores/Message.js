import React from 'react';
import {sortString} from "@formanta/js";
import {withContextConsumer} from "@formanta/react";
import {ApiRequest, DELETE, GET, POST, PUT} from "../../../lib/ApiRequest";
import {withPusher} from '../../../component/Pusher/Pusher';
import {cancelPromise} from "../../../component/cancelPromise";
import {withApiControl} from "../../../component/ApiControl";
import {doApiRequest} from "../../../component/Pusher/Pushing";

const StoreMessageContext = React.createContext({});

class StoreMessageProviderBase extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            messages: [],
            loadingMessages: false,
            roles: [],
            loadingMessageRoles: false,
            createMessage: this.createMessage,
            deleteMessage: this.deleteMessage,
            loadMessage: this.loadMessage,
            loadMessageList: this.loadMessageList,
            loadMessageRoleList: this.loadMessageRoleList,
            updateMessage: this.updateMessage,
        };
    }

    loadMessage = (message) => {
        return (new ApiRequest('api', GET, 'message/' + message))
            .debug(true)
            .header({})
            .exec()
            .then((res) => {
                if(res && res.data) {
                    if(res.data.error) {
                        return Promise.reject(res);
                    }
                    return res.data;
                } else {
                    return Promise.reject(res);
                }
            })
            .catch((err) => {
                this.props.handleApiFail(err, 'loadMessage');
                return Promise.reject(err);
            });
    };

    loadMessageList = (forceLoad = false) => {
        const {cancelPromise} = this.props;
        if(false === this.state.loadingMessages || forceLoad) {

            this.setState({
                loadingMessages: 'progress'
            });
            cancelPromise(new ApiRequest('api', POST, 'message/list')
                .debug(true)
                .exec())
                .then((res) => {
                    if(res && res.data && !res.data.error) {

                        this.setState({
                            messages: sortString(res.data, 'time').reverse(),
                        });
                        this.setState({
                            loadingMessages: true
                        });
                    } else {
                        this.setState({
                            loadingMessages: 'error'
                        });
                    }
                })
                .catch((err) => {
                    this.props.handleApiFail(err, 'loadMessageList', true, () => this.loadMessageList(forceLoad));
                    if(err && !err.cancelled) {
                        this.setState({
                            loadingMessages: 'error'
                        });
                    }
                });
        }
    };

    createMessage = (data) => {
        const endpoint = 'message/create';

        const {execPushing} = this.props;

        return execPushing(
            doApiRequest(this.props.handleApiFail, POST, endpoint, 'createMessage', data)
        ).then((data) => {
            this.loadMessageList(true);
            return data;
        });
    };

    deleteMessage = (message) => {
        const endpoint = 'message/' + message;

        const {execPushing} = this.props;

        return execPushing(
            doApiRequest(this.props.handleApiFail, DELETE, endpoint, 'deleteMessage')
        ).then((data) => {
            this.loadMessageList(true);
            return data;
        });
    };

    updateMessage = (message, data) => {
        const {execPushing} = this.props;
        const endpoint = 'message/' + message;

        return execPushing(
            doApiRequest(this.props.handleApiFail, PUT, endpoint, 'updateMessage', data)
        ).then((data) => {
            this.loadMessageList(true);
            return data;
        });
    };

    loadMessageRoleList = (hooks, forceLoad = false) => {
        const {cancelPromise} = this.props;
        if(false === this.state.loadingMessageRoles || forceLoad) {

            this.setState({
                loadingMessageRoles: 'progress'
            });
            cancelPromise(new ApiRequest('api', GET, 'message/roles', {hooks})
                .debug(true)
                .exec())
                .then((res) => {
                    if(res && res.data && !res.data.error) {

                        this.setState({
                            roles: res.data,
                            loadingMessageRoles: true
                        });
                    } else {
                        this.setState({
                            loadingMessageRoles: 'error'
                        });
                    }
                })
                .catch((err) => {
                    this.props.handleApiFail(err, 'loadMessageRoleList', true, () => this.loadMessageRoleList(forceLoad));
                    if(err && !err.cancelled) {
                        this.setState({
                            loadingMessageRoles: 'error'
                        });
                    }
                });
        }
    };

    render() {
        return (
            <StoreMessageContext.Provider value={this.state}>
                {this.props.children}
            </StoreMessageContext.Provider>
        );
    }
}

const storeMessage = withContextConsumer(StoreMessageContext.Consumer);

const StoreMessageProvider = cancelPromise(withApiControl(withPusher(StoreMessageProviderBase)));

export {StoreMessageProvider, storeMessage};