import React from 'react';
import {withNamespaces} from 'react-i18next';
import {withRouter} from 'react-router-dom';
import {ID} from "@formanta/js";
import {withTimeout} from "@formanta/react";
import {i18n} from "../../lib/i18n";

import {withPusher} from "../../component/Pusher/Pusher";
import {storeMessage} from "./Stores/Message";
import {cancelPromise} from "../../component/cancelPromise";
import {Button, InputTextarea} from "../../component/Form";
import {LoadingProgress} from '../../component/LoadingProgress';

class MessageDetails extends React.Component {
    state = {
        message: '',
        saving: false,
        loaded: false
    };

    constructor(props) {
        super(props);
        this.id = ID();
    }

    componentDidMount() {
        //this.loadMessage();
    }

    componentDidUpdate(prevProps) {
        if(prevProps.message !== this.props.message) {
            //this.loadMessage();
        }
    }

    selectRoles = (e) => {
        this.setState({
            roles: [...e.target.options].filter(o => o.selected).map(o => o.value)
        });
    };

    pushNewMessage = () => {
        const {
            history,
            createMessage, cancelPromise,
            setTimeout
        } = this.props;

        this.setState({saving: 'progress'});
        let roles = this.state.roles.filter((r) => '-' !== r);
        cancelPromise(createMessage({
            name: this.state.name,
            password: this.state.password,
            roles: roles,
            active: this.state.active
        }))
            .then((data) => {
                if(data.error) {
                    this.setState({saving: 'error'});
                    return;
                }

                this.setState({
                    saving: 'success',
                    name: '',
                    password: '',
                    roles: ['-'],
                    active: false,
                });

                history.push('/' + i18n.languages[0] + '/messages/' + data.success);

                setTimeout(() => {
                    this.setState({saving: false});
                }, 4000);
            })
            .catch((err) => {
                if(err && !err.cancelled) {
                    this.setState({saving: 'error'});
                }
            });
    };

    render() {
        const {
            t,
            //message,
        } = this.props;

        return (
            <React.Fragment>
                <LoadingProgress
                    progress={this.state.loading}
                    label={t('api.loading-message')}
                />

                {true === this.state.loading || true ?
                    <div style={{display: 'flex', flexDirection: 'column'}}>
                        <div>
                            <h2>{t('reply-title')}</h2>

                            <InputTextarea/>
                        </div>
                        <Button>{t('send-reply')}</Button>
                    </div> : null}
            </React.Fragment>
        )
    };
}

export default withNamespaces('page--message')(withPusher(withTimeout(withRouter(cancelPromise(storeMessage(MessageDetails))))));
