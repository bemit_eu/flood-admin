import React from 'react';
import {withNamespaces} from 'react-i18next';
import {withRouter} from 'react-router-dom';
import {ID} from "@formanta/js";
import {withTimeout} from "@formanta/react";

import {withTheme} from '../../lib/theme';
import {i18n} from "../../lib/i18n";

import {withPusher} from "../../component/Pusher/Pusher";
import {storeMessage} from "./Stores/Message";
import {cancelPromise} from "../../component/cancelPromise";
import {Button, InputCheckbox, InputText, InputPassword, Select} from "../../component/Form";
import ButtonInteractive from "../../component/ButtonInteractive";
import {MdDone, MdHelp} from "react-icons/md";
import Loading from "../../component/Loading";
import MessageDelete from './MessageDelete';

class MessageCreate extends React.Component {
    state = {
        name: '',
        password: '',
        roles: ['-'],
        active: false,
        saving: false,
        loaded: false
    };

    constructor(props) {
        super(props);
        this.id = ID();
    }

    componentDidMount() {
        this.props.loadMessageRoleList();
        if(!this.props.create) {
            this.loadMessage();
        } else {
            this.props.loadMessageRoleList();
        }
    }

    componentDidUpdate(prevProps) {
        if(prevProps.message !== this.props.message) {
            if(!this.props.create) {
                this.loadMessage();
            }
        }
    }

    selectRoles = (e) => {
        this.setState({
            roles: [...e.target.options].filter(o => o.selected).map(o => o.value)
        });
    };

    loadMessage = () => {
        const {
            message,
            loadMessage, cancelPromise
        } = this.props;

        this.setState({loading: 'progress'});
        cancelPromise(loadMessage(message))
            .then((data) => {
                if(data.error) {
                    this.setState({loading: 'error'});
                    return;
                }

                this.setState({
                    loading: true,
                    name: data.name,
                    password: '',
                    has_password: data.password,
                    roles: data.roles,
                    active: data.active,
                });
            })
            .catch((err) => {
                if(err && !err.cancelled) {
                    this.setState({loading: 'error'});
                }
            });
    };

    pushNewMessage = () => {
        const {
            history,
            createMessage, cancelPromise,
            setTimeout
        } = this.props;

        this.setState({saving: 'progress'});
        let roles = this.state.roles.filter((r) => '-' !== r);
        cancelPromise(createMessage({
            name: this.state.name,
            password: this.state.password,
            roles: roles,
            active: this.state.active
        }))
            .then((data) => {
                if(data.error) {
                    this.setState({saving: 'error'});
                    return;
                }

                this.setState({
                    saving: 'success',
                    name: '',
                    password: '',
                    roles: ['-'],
                    active: false,
                });

                history.push('/' + i18n.languages[0] + '/messages/' + data.success);

                setTimeout(() => {
                    this.setState({saving: false});
                }, 4000);
            })
            .catch((err) => {
                if(err && !err.cancelled) {
                    this.setState({saving: 'error'});
                }
            });
    };

    pushUpdateMessage = () => {
        const {
            message,
            updateMessage, cancelPromise,
            setTimeout
        } = this.props;

        let roles = this.state.roles.filter((r) => '-' !== r);
        this.setState({saving: 'progress'});
        const data = {
            name: this.state.name,
            roles: roles,
            active: this.state.active
        };

        if(this.state.password.trim().length > 0) {
            data.password = this.state.password;
        }

        cancelPromise(updateMessage(message, data))
            .then((data) => {
                if(data.error) {
                    this.setState({saving: 'error'});
                    return;
                }

                this.setState({
                    saving: 'success'
                });

                setTimeout(() => {
                    this.setState({saving: false});
                }, 4000);
            })
            .catch((err) => {
                if(err && !err.cancelled) {
                    this.setState({saving: 'error'});
                }
            });
    };

    render() {
        const {
            t, theme,
            message,
            loadingMessageRoles, roles,
            create
        } = this.props;

        return (
            <React.Fragment>

                {(('progress' === this.state.loading || 'error' === this.state.loading) || ('progress' === loadingMessageRoles || 'error' === loadingMessageRoles) ?
                    <Loading style={{
                        wrapper: {margin: 0},
                        item: ('error' === this.state.loading ? {borderColor: theme.errorColor} : {}),
                    }}>{
                        ('progress' === this.state.loading || 'error' === this.state.loading) ?
                            t('api.loading-message') :
                            ('progress' === loadingMessageRoles || 'error' === loadingMessageRoles) ?
                                t('api.loading-roles-list') :
                                ''
                    }</Loading> :
                    null)}

                {(create || true === this.state.loading) && true === loadingMessageRoles ?
                    <form onSubmit={(e) => {
                        e.preventDefault();
                        e.target.reportValidity()
                    }}>
                        <div style={{marginBottom: '12px'}}>
                            <label htmlFor={this.id + '-name'}>{t('edit.name')}</label>

                            <InputText id={this.id + '-name'}
                                       value={this.state.name}
                                       onChange={(e) => this.setState({name: e.target.value})}
                                       ref={(r) => {
                                           this.input_name = r
                                       }}
                                       required
                            />
                        </div>
                        <div style={{marginBottom: '12px'}}>
                            <label htmlFor={this.id + '-password'}>{t('edit.password')}</label>

                            <InputPassword id={this.id + '-password'}
                                           value={this.state.password}
                                           placeholder={!create && this.state.has_password ? '*****' : ''}
                                           onChange={(e) => this.setState({password: e.target.value})}
                            />
                            {!create ?
                                <p style={{fontStyle: 'italic', marginTop: 0, fontSize: '0.875rem'}}>
                                    {this.state.has_password ?
                                        t('edit.password-has-already') :
                                        t('edit.password-not-existing')}
                                </p> :
                                null}
                        </div>
                        <div style={{marginBottom: '12px'}}>
                            <InputCheckbox id={this.id + '-active'}
                                           checked={this.state.active}
                                           onChange={(e) => this.setState({active: e.target.checked})}
                            />
                            <label htmlFor={this.id + '-active'} style={{marginLeft: '4px'}}>{t('edit.active')}</label>
                        </div>

                        <div style={{marginBottom: '12px'}}>
                            <label htmlFor={this.id + '-roles'}>{t('edit.roles')}</label>
                            <Select id={this.id + '-roles'}
                                    multiple
                                    value={this.state.roles} onChange={this.selectRoles} style={{maxHeight: '5rem'}}>
                                <option value={'-'}>{t('edit.no-role')}</option>
                                {roles.map((role) => (
                                    <option key={role}
                                            value={role}>{role}</option>
                                ))}
                            </Select>
                        </div>

                        <ButtonInteractive
                            comp={(p) => (
                                <Button {...p} style={{marginRight: create ? 0 : '6px'}}>
                                    {t('edit.btn-' + (create ? 'create' : 'update'))} {p.children}
                                </Button>
                            )}
                            step={create ? false : [
                                null,
                                <MdHelp size={'1em'} color={theme.textColor}/>,
                                <MdDone size={'1em'} color={theme.textColor}/>,
                            ]}
                            onClick={create ? this.pushNewMessage : this.pushUpdateMessage}
                        />

                        {create ? null : <MessageDelete message={message}/>}

                        {('progress' === this.state.saving || 'error' === this.state.saving ?
                            <Loading style={{
                                wrapper: {margin: 0},
                                item: ('error' === this.state.saving ? {borderColor: theme.errorColor} : {}),
                            }}>{t('api.saving-message')}</Loading> :
                            null)}
                    </form> : null}
            </React.Fragment>
        )
    };
}

export default withNamespaces('page--message')(withPusher(withTimeout(withRouter(cancelPromise(storeMessage(withTheme(MessageCreate)))))));
