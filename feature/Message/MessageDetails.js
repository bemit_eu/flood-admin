import React from 'react';
import {withNamespaces} from 'react-i18next';
import {Link, withRouter} from 'react-router-dom';
import {ID} from "@formanta/js";
import {withTimeout} from "@formanta/react";

import {withTheme} from '../../lib/theme';
import {i18n} from "../../lib/i18n";

import {withPusher} from "../../component/Pusher/Pusher";
import {storeMessage} from "./Stores/Message";
import {cancelPromise} from "../../component/cancelPromise";
import {MdForward, MdDelete, MdReply} from "react-icons/md";
import Loading from "../../component/Loading";

//import MessageDelete from './MessageDelete';

class MessageDetails extends React.Component {
    state = {
        message: '',
        saving: false,
        loaded: false
    };

    constructor(props) {
        super(props);
        this.id = ID();
    }

    componentDidMount() {
        this.loadMessage();
    }

    componentDidUpdate(prevProps) {
        if(prevProps.message !== this.props.message) {
            this.loadMessage();
        }
    }

    selectRoles = (e) => {
        this.setState({
            roles: [...e.target.options].filter(o => o.selected).map(o => o.value)
        });
    };

    loadMessage = () => {
        const {
            message,
            loadMessage, cancelPromise
        } = this.props;

        this.setState({loading: 'progress'});
        cancelPromise(loadMessage(message))
            .then((data) => {
                if(data.error) {
                    this.setState({loading: 'error'});
                    return;
                }

                this.setState({
                    loading: true,
                    message: data
                });
            })
            .catch((err) => {
                if(err && !err.cancelled) {
                    this.setState({loading: 'error'});
                }
            });
    };

    pushNewMessage = () => {
        const {
            history,
            createMessage, cancelPromise,
            setTimeout
        } = this.props;

        this.setState({saving: 'progress'});
        let roles = this.state.roles.filter((r) => '-' !== r);
        cancelPromise(createMessage({
            name: this.state.name,
            password: this.state.password,
            roles: roles,
            active: this.state.active
        }))
            .then((data) => {
                if(data.error) {
                    this.setState({saving: 'error'});
                    return;
                }

                this.setState({
                    saving: 'success',
                    name: '',
                    password: '',
                    roles: ['-'],
                    active: false,
                });

                history.push('/' + i18n.languages[0] + '/messages/' + data.success);

                setTimeout(() => {
                    this.setState({saving: false});
                }, 4000);
            })
            .catch((err) => {
                if(err && !err.cancelled) {
                    this.setState({saving: 'error'});
                }
            });
    };

    render() {
        const {
            t, theme,
            message,
        } = this.props;

        return (
            <React.Fragment>

                {(('progress' === this.state.loading || 'error' === this.state.loading) ?
                    <Loading style={{
                        wrapper: {margin: 0},
                        item: ('error' === this.state.loading ? {borderColor: theme.errorColor} : {}),
                    }}>{
                        ('progress' === this.state.loading || 'error' === this.state.loading) ?
                            t('api.loading-message') :
                            ''
                    }</Loading> :
                    null)}

                {true === this.state.loading ?
                    <React.Fragment>
                        <div style={{display: 'flex', flexGrow: 3}}>
                            <div>
                                <h2 style={{marginBottom: '6px'}}>{this.state.message && this.state.message.subject ? this.state.message.subject : ''}</h2>
                                <p style={{margin: '6px 0', fontSize: '0.875rem'}}>ID: {this.state.message && this.state.message.id ? this.state.message.id : ''}</p>
                                <table style={{margin: '9px 0'}}>
                                    <tbody>
                                    <tr>
                                        <td style={{paddingRight: '5px', fontWeight: 'bold', fontStyle: 'italic'}}>{t('from')}</td>
                                        <td>
                                            {this.state.message && this.state.message.sender ?
                                                this.state.message.sender.name ?
                                                    this.state.message.sender.name + (this.state.message.sender.email ? ' | ' + this.state.message.sender.email : '') :
                                                    this.state.message.sender.email ? this.state.message.sender.email : ''
                                                : t('no-sender')}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style={{paddingRight: '5px', fontWeight: 'bold', fontStyle: 'italic'}}>{t('to')}</td>
                                        <td>
                                            {this.state.message && this.state.message.recipient ?
                                                this.state.message.recipient.name ?
                                                    this.state.message.recipient.name + (this.state.message.recipient.email ? ' | ' + this.state.message.recipient.email : '') :
                                                    this.state.message.recipient.email ? this.state.message.recipient.email : ''
                                                : t('no-recipient')}
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div style={{marginLeft: 'auto'}}>
                                <p style={{marginTop: 0, marginBottom: '6px', textAlign: 'right'}}>{this.state.message && this.state.message.time ? this.state.message.time : ''}</p>
                                <p style={{marginTop: 0, marginBottom: '6px', textAlign: 'right'}}>{this.state.message && this.state.message.type ? this.state.message.type : '-'} <span style={{fontWeight: 'bold'}}>Type</span></p>
                                <p style={{marginTop: 0, marginBottom: '6px', textAlign: 'right'}}>{this.state.message && this.state.message.hook ? this.state.message.hook : '-'} <span style={{fontWeight: 'bold'}}>Hook</span></p>
                            </div>
                        </div>
                        <div style={{display: 'flex', padding: '6px 0'}}>
                            <Link to={'/' + i18n.languages[0] + '/messages/' + message + '/reply'} style={{padding: '3px'}}><MdReply color={theme.textColor} size={'1.25em'}/></Link>
                            <Link to={'/' + i18n.languages[0] + '/messages/' + message + '/forward'} style={{padding: '3px'}}><MdForward color={theme.textColor} size={'1.25em'}/></Link>
                            <Link to={'/' + i18n.languages[0] + '/messages/' + message + '/trash'} style={{padding: '3px'}}><MdDelete color={theme.textColor} size={'1.25em'}/></Link>
                        </div>
                        <div style={{display: 'flex'}}>
                            {this.state.message && this.state.message.text ?
                                <p style={{width: '100%', margin: '0 0 15px 0', padding: '6px 0', borderTop: '1px solid ' + theme.textColorLight, borderBottom: '1px solid ' + theme.textColorLight}}
                                   dangerouslySetInnerHTML={{
                                       __html: this.state.message.text.replace(/[\u00A0-\u9999<>&]/gim, function(i) {
                                           return '&#' + i.charCodeAt(0) + ';';
                                       }).replace(/(?:\r\n|\r|\n)/g, '<br>')
                                   }}/>
                                : 'no-text'}
                        </div>
                    </React.Fragment>
                    : null}
            </React.Fragment>
        )
    };
}

export default withNamespaces('page--message')(withPusher(withTimeout(withRouter(cancelPromise(storeMessage(withTheme(MessageDetails)))))));
