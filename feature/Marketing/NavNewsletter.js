import React from 'react';
import {withNamespaces} from "react-i18next";
import {MdAccountBox, MdCreate, MdHistory} from "react-icons/md";

import RouteCascaded from '../../lib/RouteCascaded';

import Nav from "../../component/Nav/Nav";
import Sidebar from "../../layout/ControlPanel/Sidebar";

class NavNewsletter extends React.Component {

    render() {
        const {t} = this.props;

        const navContent = (match) => {
            let content = [];

            content.push({
                path: match.url + '/user',
                lbl: {icon: (p) => <MdAccountBox className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.nav-newsletter.user')},
            });

            content.push({
                path: match.url + '/create',
                lbl: {icon: (p) => <MdCreate className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.nav-newsletter.create')},
            });

            content.push({
                path: match.url + '/history',
                lbl: {icon: (p) => <MdHistory className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.nav-newsletter.history')},
            });


            return content;
        };

        return (
            <React.Fragment>
                <Sidebar
                    center={(props => <Nav path='/marketing/newsletter' content={navContent} notxt={props.notxt}/>)}
                    textToggle={true}
                />

                {this.props.routes ? this.props.routes.map((route, i) => (
                    route.sidebar ?
                        <RouteCascaded key={i} {...route} component={route.sidebar}/> :
                        null
                )) : null}
            </React.Fragment>
        )
    };
}

export default withNamespaces('page--marketing')(NavNewsletter);
