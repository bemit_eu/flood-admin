import React from 'react';
import {withNamespaces} from "react-i18next";
import {MdMail, MdEvent} from "react-icons/md";

import RouteCascaded from '../../lib/RouteCascaded';

import Nav from "../../component/Nav/Nav";
import Sidebar from "../../layout/ControlPanel/Sidebar";
import {withFeatureManager} from "../../component/FeatureManager";

class NavShop extends React.Component {

    render() {
        const {
            t,
            isEnabled
        } = this.props;

        const navContent = (match) => {
            let content = [];

            content.push({
                path: match.url + '/newsletter',
                lbl: {icon: (p) => <MdMail className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.newsletter')},
            });

            if(isEnabled('marketingCampaign')) {
                content.push({
                    path: match.url + '/campaign',
                    lbl: {icon: (p) => <MdEvent className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.campaign')},
                });
            }

            return content;
        };

        return (
            <React.Fragment>
                <Sidebar
                    center={(props => <Nav path='/marketing' content={navContent} notxt={props.notxt}/>)}
                    textToggle={true}
                />

                {this.props.routes ? this.props.routes.map((route, i) => (
                    route.sidebar ?
                        <RouteCascaded key={i} {...route} component={route.sidebar}/> :
                        null
                )) : null}
            </React.Fragment>
        )
    };
}

export default withNamespaces('page--marketing')(withFeatureManager(NavShop));
