import React from 'react';
import {withNamespaces} from 'react-i18next';

import NewsletterUserAdd from './NewsletterUserAdd';
import {storeNewsletterUser} from "./Stores/User";
import {DataGrid} from "../../component/DataGrid/DataGrid";
import NewsletterUserListItem from "./NewsletterUserListItem";
import {ButtonSmall} from "../../component/Form";
import {ContainerStyle, SplitScreen} from "../../component/SplitScreen";

const initialState = {
    loaded: false,
    showForm: false
};

class NewsletterUser extends React.Component {

    state = initialState;

    componentDidMount() {
        this.props.loadNewsletterUserList();
        this.props.changeStyleInnerWrapper(ContainerStyle);
    }

    componentWillUnmount() {
        this.setState(initialState);
        this.props.changeStyleInnerWrapper({});
    }

    render() {
        const {
            t,
            newsletterUsers, loadingNewsletterUsers
        } = this.props;

        return (
            <React.Fragment>
                <h1>{t('title')}</h1>

                <SplitScreen
                    isTopOpen={this.state.showForm}
                    top={<NewsletterUserAdd showForm={this.state.showForm}/>}

                    center={
                        <DataGrid
                            headers={[
                                {lbl: t('list.head.state')},
                                {
                                    lbl: t('list.head.email'),
                                    search: 'email',
                                    sort: 'email',
                                },
                                {
                                    lbl: t('list.head.subscribing-time'),
                                    search: 'subscribing_time',
                                    sort: 'subscribing_time',
                                },
                                {
                                    lbl: t('list.head.optin-time'),
                                    search: 'optin_time',
                                    sort: 'optin_time',
                                },
                                {
                                    lbl: t('list.head.unsubscribing-time'),
                                    search: 'unsubscribe_time',
                                    sort: 'unsubscribe_time',
                                },
                            ]}
                            controls={[
                                (p) => <ButtonSmall onClick={() => this.setState({showForm: !this.state.showForm})}
                                                    style={{width: 'max-content', marginRight: 6}}>{this.state.showForm ? t('add.close-form') : t('add.show-form')}</ButtonSmall>
                            ]}
                            loading={loadingNewsletterUsers}
                            list={newsletterUsers}
                            element={(p) => <NewsletterUserListItem t={t} {...p}/>}
                        />
                    }
                />
            </React.Fragment>
        )
    };
}

export default withNamespaces('page--marketing-newsletter-user')(storeNewsletterUser(NewsletterUser));
