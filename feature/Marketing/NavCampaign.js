import React from 'react';
import {withNamespaces} from "react-i18next";
import {MdEvent, MdBlurOn, MdHistory, MdLocalPlay} from "react-icons/md";

import RouteCascaded from '../../lib/RouteCascaded';

import Nav from "../../component/Nav/Nav";
import Sidebar from "../../layout/ControlPanel/Sidebar";

class NavCampaign extends React.Component {

    render() {
        const {t} = this.props;

        const navContent = (match) => {
            let content = [];

            content.push({
                path: match.url + '/planner',
                lbl: {icon: (p) => <MdBlurOn className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.nav-campaign.planner')},
            });

            content.push({
                path: match.url + '/calendar',
                lbl: {icon: (p) => <MdEvent className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.nav-campaign.calendar')},
            });

            content.push({
                path: match.url + '/hot',
                lbl: {icon: (p) => <MdLocalPlay className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.nav-campaign.hot')},
            });

            content.push({
                path: match.url + '/history',
                lbl: {icon: (p) => <MdHistory className={p.className} size={p.notxt ? '1.75em' : '1.125em'}/>, txt: t('nav.nav-campaign.history')},
            });


            return content;
        };

        return (
            <React.Fragment>
                <Sidebar
                    center={(props => <Nav path='/marketing/campaign' content={navContent} notxt={props.notxt}/>)}
                    textToggle={true}
                />

                {this.props.routes ? this.props.routes.map((route, i) => (
                    route.sidebar ?
                        <RouteCascaded key={i} {...route} component={route.sidebar}/> :
                        null
                )) : null}
            </React.Fragment>
        )
    };
}

export default withNamespaces('page--marketing')(NavCampaign);
