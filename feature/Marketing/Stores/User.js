import React from 'react';
import {withContextConsumer} from "@formanta/react";
import {ApiRequest, POST} from "../../../lib/ApiRequest";
import {cancelPromise} from "../../../component/cancelPromise";
import {withApiControl} from "../../../component/ApiControl";
import {Pushing, withPusher} from "../../../component/Pusher/Pusher";

const StoreNewsletterUserContext = React.createContext({});

class StoreNewsletterUserProviderBase extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            newsletterUsers: [],
            loadingNewsletterUsers: false,
            createNewsletterUser: this.createNewsletterUser,
            loadNewsletterUserList: this.loadNewsletterUserList
        };
    }

    createNewsletterUser = (data) => {
        const endpoint = 'marketing/newsletter/user';

        const {execPushing} = this.props;

        return execPushing(
            new Pushing(
                'createNewsletterUser',
                endpoint,
                (resolve, reject) => {
                    (new ApiRequest('api', POST, endpoint, data))
                        .debug(true)
                        .header({})
                        .exec()
                        .then((res) => {
                            if(res && res.data) {
                                resolve(res.data);
                            } else {
                                reject();
                            }
                        })
                        .catch((err) => {
                            this.props.handleApiFail(err, 'createNewsletterUser');
                            reject();
                        });
                }
            )
        ).then((data) => {
            if(data.error) {
            } else {
                this.loadNewsletterUserList(true);
            }
            return data;
        });
    };

    loadNewsletterUserList = (forceLoad = false) => {
        if(false === this.state.loadingNewsletterUsers || forceLoad) {

            this.setState({loadingNewsletterUsers: 'progress'});

            (new ApiRequest('api', POST, 'marketing/newsletter/user/list'))
                .debug(true)
                .exec()
                .then((res) => {
                    if(res && res.data) {
                        this.setState({
                            newsletterUsers: res.data,
                            loadingNewsletterUsers: true
                        });
                    } else {
                        this.setState({
                            loadingNewsletterUsers: 'error'
                        });
                    }
                })
                .catch((err) => {
                    this.props.handleApiFail(err, 'listing NewsletterUsers', true, () => this.loadNewsletterUserList(forceLoad));
                    if(err && !err.cancelled) {
                        this.setState({
                            loadingNewsletterUsers: 'error'
                        });
                    }
                });
        }
    };

    render() {
        return (
            <StoreNewsletterUserContext.Provider value={this.state}>
                {this.props.children}
            </StoreNewsletterUserContext.Provider>
        );
    }
}

const StoreNewsletterUserProvider = cancelPromise(withPusher(withApiControl(StoreNewsletterUserProviderBase)));

const storeNewsletterUser = withContextConsumer(StoreNewsletterUserContext.Consumer);

export {StoreNewsletterUserProvider, storeNewsletterUser};