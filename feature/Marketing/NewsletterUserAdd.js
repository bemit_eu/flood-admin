import React from 'react';
import {withNamespaces} from "react-i18next";
import posed from 'react-pose';

import {ID} from '@formanta/js';
import {withTimeout} from '@formanta/react';

import {withTheme} from '../../lib/theme';
import {Label, Button, ButtonSmall, InputText, InputTextarea, InputCheckbox, InputGroup} from "../../component/Form";
import {storeNewsletterUser} from "./Stores/User";
import {cancelPromise} from "../../component/cancelPromise";
import Loading from "../../component/Loading";
import {MdDone} from "react-icons/md";

const AddForm = posed.div({
    hidden: {
        height: 0,
        marginBottom: 0,
    },
    visible: {
        height: 'auto',
        marginBottom: '24px',
    }
});

class NewsletterUserAdd extends React.Component {

    state = {
        saving: false,
        singleMail: true,
        optinAssured: false,
        mailSingle: '',
        mailSingleOptin: '',
        mailMultiple: '',
    };

    constructor(props) {
        super(props);

        this.id = ID();
    }

    pushNewsletterUser = () => {
        const {cancelPromise, createNewsletterUser} = this.props;
        const data = {};
        if(this.state.singleMail) {
            data.email = this.state.mailSingle;
            if(this.state.mailSingleOptin) {
                data.optin = this.state.mailSingleOptin;
            }
        } else {
            data.emails = this.state.mailMultiple.split(/[\r\n]+/);
        }

        this.setState({saving: 'progress'});
        cancelPromise(createNewsletterUser(data))
            .then(res => {
                if(res.failed) {
                    // todo maybe only some and not all fail
                    this.setState({saving: 'error'});
                } else if(res.success) {
                    this.setState({
                        saving: 'success',
                        mailSingle: '',
                        mailSingleOptin: '',
                        mailMultiple: '',
                        optinAssured: false,
                    });
                    setTimeout(() => {
                        this.setState({saving: false});
                    }, 650);
                }
            })
            .catch(err => {
                // todo
                this.setState({saving: 'error'});
            });
    };

    render() {
        const {
            t, theme, showForm,
        } = this.props;

        return (
            <AddForm withParent={false} pose={showForm ? 'visible' : 'hidden'} style={{overflow: 'hidden'}}>
                <form onSubmit={(e) => {
                    e.preventDefault();
                    e.target.reportValidity();
                    if(e.target.checkValidity()) {
                        this.pushNewsletterUser();
                    }
                }}>
                    <ButtonSmall onClick={() => this.setState({singleMail: !this.state.singleMail})} style={{marginBottom: '6px'}} type='button'>
                        {this.state.singleMail ? t('add.switch-to-multiple') : t('add.switch-to-single')}
                    </ButtonSmall>

                    {this.state.singleMail ?
                        <React.Fragment>
                            <Label htmlFor={this.id + 'mail-single'}>{t('add.mail-single')}</Label>
                            <InputText id={this.id + 'mail-single'}
                                       value={this.state.mailSingle}
                                       onChange={(e) => this.setState({mailSingle: e.target.value})}
                                       type='email'
                                       required={true}/>
                            <Label htmlFor={this.id + 'mail-single-optin'}>{t('add.mail-single-optin')}</Label>
                            <InputText id={this.id + 'mail-single-optin'} value={this.state.mailSingleOptin} maxLength={150} onChange={(e) => this.setState({mailSingleOptin: e.target.value})}/>
                        </React.Fragment>
                        : null}

                    {!this.state.singleMail ?
                        <React.Fragment>
                            <Label htmlFor={this.id + 'mail-multiple'}>{t('add.mail-multiple')}</Label>
                            <p style={{marginTop: '3px', fontSize: '0.85rem'}}>{t('add.mail-multiple-optin-format')}</p>
                            <InputTextarea id={this.id + 'mail-multiple'} style={{minHeight: '120px'}} value={this.state.mailMultiple} onChange={(e) => this.setState({mailMultiple: e.target.value})}/>
                        </React.Fragment>
                        : null}

                    <InputGroup style={{wrapper: {display: 'flex', alignItems: 'center', marginTop: '18px', position: 'relative'}}}>
                        <InputCheckbox id={this.id + 'mail-optin-assure'} checked={this.state.optinAssured} onChange={() => this.setState({optinAssured: !this.state.optinAssured})}
                                       size='1.2em'
                                       required={true}
                        />
                        <Label htmlFor={this.id + 'mail-optin-assure'} style={{marginLeft: '3px', cursor: 'pointer'}}>{t('add.optin-assure')}</Label>
                    </InputGroup>

                    <Button style={{marginTop: '9px'}}>{t('add.save-and-add')} {'success' === this.state.saving ? <MdDone/> : null}</Button>

                    {this.state.saving ?
                        ('progress' === this.state.saving || 'error' === this.state.saving ?
                            <React.Fragment>
                                <Loading style={{
                                    wrapper: {margin: 0},
                                    item: ('error' === this.state.saving ? {borderColor: theme.errorColor} : {}),
                                }}/>

                            </React.Fragment> : null)
                        : null}
                </form>
            </AddForm>
        )
    };
}

export default withNamespaces('page--marketing-newsletter-user')(cancelPromise(withTimeout(storeNewsletterUser(withTheme(NewsletterUserAdd)))));
