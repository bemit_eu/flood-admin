import React from 'react';
import {MdAutorenew, MdCheck, MdNotInterested} from "react-icons/md";

import {withStyles} from '../../lib/theme';

const styles = {
    item: {},
    cell: {
        padding: '6px 6px ',
    }
};

class NewsletterUserListItem extends React.Component {

    state = {
        showDetails: false
    };

    componentDidUpdate(prevProps) {
        if(prevProps.item !== this.props.item) {
            this.setState({
                showDetails: false
            });
        }
    }

    componentDidMount() {
    }

    render() {
        const {
            t, classes,
            item
        } = this.props;

        return (
            <tr className={classes.item}>
                <td className={classes.cell} style={{textAlign: 'center'}}>
                    {item.state ?
                        'subscribing' === item.state ?
                            <MdAutorenew/> :
                            'subscribed' === item.state ?
                                <MdCheck/> :
                                'unsubscribed' === item.state ?
                                    <MdNotInterested/> :
                                    t('list.cell.state-unkown')
                        : t('list.cell.state-error')}
                </td>
                <td className={classes.cell}>
                    {item.email}
                </td>
                <td className={classes.cell}>
                    {item.subscribing_time ?
                        item.subscribing_time
                        : t('list.cell.not-done')}
                </td>
                <td className={classes.cell}>
                    {item.optin_time ?
                        item.optin_time
                        : t('list.cell.not-done')}
                </td>
                <td className={classes.cell}>
                    {item.unsubscribe_time ?
                        item.unsubscribe_time
                        : t('list.cell.not-done')}
                </td>
            </tr>
        )
    };
}

export default withStyles(styles)(NewsletterUserListItem);
