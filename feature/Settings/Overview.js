import React from 'react';
import {MdDeleteSweep, MdCheck, MdCheckBox, MdCheckBoxOutlineBlank} from 'react-icons/md';
import {withNamespaces} from 'react-i18next';

import {withTheme, withStyles} from '../../lib/theme';
import {HookSwitch} from "../../component/Hook/HookSwitch";
import {withFeatureManager} from "../../component/FeatureManager";
import {JsonViewer} from "../../component/JsonViewer";

const styles = {
    wrapper: {
        display: 'block',
    },
    row: {
        display: 'flex',
        flexDirection: 'column'
    },
    item: {
        display: 'table-cell',
        verticalAlign: 'middle',
        lineHeight: '1.45rem',
        padding: '0 6px',
        textAlign: 'left',
        color: (theme) => theme.textColor,
        '&:first-child': {
            paddingLeft: 0
        },
        '&:last-child': {
            paddingRight: 0
        }
    },
    icon: {
        verticalAlign: 'middle'
    },
};

class Overview extends React.Component {
    evts = [];

    state = {
        clientCacheCleared: false,
        showLocalStorage: false,
    };

    componentWillUnmount() {
        this.evts.forEach((id) => {
            clearTimeout(id);
        });
    }

    clearClientCache = () => {
        let tmp_jwt = window.localStorage.getItem('jwt');
        window.localStorage.clear();
        window.localStorage.setItem('jwt', tmp_jwt);
        this.setState({
            clientCacheCleared: true
        });

        let id = setTimeout(() => {
            this.setState({
                clientCacheCleared: false
            })
        }, 800);
        this.evts.push(id);

        window.location.reload(true);
    };

    activateDevMode = () => {
        if(window.localStorage.getItem('flood-dev') * 1) {
            window.localStorage.setItem('flood-dev', '0');
        } else {
            window.localStorage.setItem('flood-dev', '1');
        }
        this.setState({});
    };

    render() {
        const {
            t,
            theme, classes,
            isEnabled
        } = this.props;

        return (
            <div>
                <h1>{t('title')}</h1>
                <div className={classes.wrapper}>
                    {isEnabled('backendConnect') ?
                        <div className={classes.row} style={{marginBottom: ''}}>
                            <HookSwitch/>
                        </div> : null}
                    <div className={classes.row}>
                        <div style={{display: 'flex'}}>
                            <p className={classes.item}>
                                {t('clear-client-cache.lbl')}
                            </p>
                            <button className={classes.item} onClick={this.clearClientCache}>
                                <MdDeleteSweep size={'1.25em'} color={theme.textColor} className={classes.icon}/>
                            </button>
                            {this.state.clientCacheCleared ? <p className={classes.item}><MdCheck size={'1.25em'} color={theme.textColor} className={classes.icon}/></p> : null}
                        </div>
                    </div>
                    <div className={classes.row} style={{marginBottom: 3}}>
                        <div style={{display: 'flex'}}>
                            <button className={classes.item} onClick={this.activateDevMode}>
                                {t('dev-mode')}
                            </button>
                            <button className={classes.item} onClick={this.activateDevMode}>
                                {window.localStorage.getItem('flood-dev') * 1 ?
                                    <MdCheckBox size={'1.25em'} color={theme.textColor} className={classes.icon}/> :
                                    <MdCheckBoxOutlineBlank size={'1.25em'} color={theme.textColor} className={classes.icon}/>}
                            </button>
                        </div>
                    </div>
                    {window.localStorage.getItem('flood-dev') * 1 ? <div className={classes.row}>
                        <button className={classes.item} onClick={() => this.setState({showLocalStorage: !this.state.showLocalStorage})}>
                            window.localStorage
                        </button>
                        {this.state.showLocalStorage ? <div style={{display: 'flex', flexDirection: 'column'}}>
                            {Object.keys(window.localStorage).map(key => (
                                <JsonViewer key={key}
                                            name={key}
                                            src={window.localStorage.getItem(key)}/>
                            ))}
                        </div> : null}
                    </div> : null}
                </div>
            </div>
        );
    }
}

export default withFeatureManager(withNamespaces('page--settings')(withStyles(styles)(withTheme(Overview))));
