import React from 'react';
import {withContextConsumer} from "@formanta/react";
import {ApiRequest, DELETE, GET, POST, PUT} from "../../../lib/ApiRequest";
import {withPusher} from '../../../component/Pusher/Pusher';
import {cancelPromise} from "../../../component/cancelPromise";
import {withApiControl} from "../../../component/ApiControl";
import {doApiRequest} from "../../../component/Pusher/Pushing";

const StoreUserContext = React.createContext({});

class StoreUserProviderBase extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            users: [],
            loadingUsers: false,
            roles: [],
            loadingUserRoles: false,
            createUser: this.createUser,
            deleteUser: this.deleteUser,
            loadUser: this.loadUser,
            loadUserList: this.loadUserList,
            loadUserRoleList: this.loadUserRoleList,
            updateUser: this.updateUser,
        };
    }

    loadUser = (user) => {
        return (new ApiRequest('api', GET, 'user/' + user))
            .debug(true)
            .header({})
            .exec()
            .then((res) => {
                if(res && res.data) {
                    if(res.data.error) {
                        return Promise.reject(res);
                    }
                    return res.data;
                } else {
                    return Promise.reject(res);
                }
            })
            .catch((err) => {
                this.props.handleApiFail(err, 'loadUser');
                return Promise.reject(err);
            });
    };

    loadUserList = (forceLoad = false) => {
        const {cancelPromise} = this.props;
        if(false === this.state.loadingUsers || forceLoad) {

            this.setState({
                loadingUsers: 'progress'
            });
            cancelPromise(new ApiRequest('api', POST, 'user/list')
                .debug(true)
                .exec())
                .then((res) => {
                    if(res && res.data && !res.data.error) {

                        this.setState({
                            users: res.data,
                        });
                        this.setState({
                            loadingUsers: true
                        });
                    } else {
                        this.setState({
                            loadingUsers: 'error'
                        });
                    }
                })
                .catch((err) => {
                    this.props.handleApiFail(err, 'loadUserList', true, () => this.loadUserList(forceLoad));
                    if(err && !err.cancelled) {
                        this.setState({
                            loadingUsers: 'error'
                        });
                    }
                });
        }
    };

    createUser = (data) => {
        const endpoint = 'user/create';

        const {execPushing} = this.props;

        return execPushing(
            doApiRequest(this.props.handleApiFail, POST, endpoint, 'createUser', data)
        ).then((data) => {
            this.loadUserList(true);
            return data;
        });
    };

    deleteUser = (user) => {
        const endpoint = 'user/' + user;

        const {execPushing} = this.props;

        return execPushing(
            doApiRequest(this.props.handleApiFail, DELETE, endpoint, 'deleteUser')
        ).then((data) => {
            this.loadUserList(true);
            return data;
        });
    };

    updateUser = (user, data) => {
        const {execPushing} = this.props;
        const endpoint = 'user/' + user;

        return execPushing(
            doApiRequest(this.props.handleApiFail, PUT, endpoint, 'updateUser', data)
        ).then((data) => {
            this.loadUserList(true);
            return data;
        });
    };

    loadUserRoleList = (hooks, forceLoad = false) => {
        const {cancelPromise} = this.props;
        if(false === this.state.loadingUserRoles || forceLoad) {

            this.setState({
                loadingUserRoles: 'progress'
            });
            cancelPromise(new ApiRequest('api', GET, 'user/roles', {hooks})
                .debug(true)
                .exec())
                .then((res) => {
                    if(res && res.data && !res.data.error) {

                        this.setState({
                            roles: res.data,
                            loadingUserRoles: true
                        });
                    } else {
                        this.setState({
                            loadingUserRoles: 'error'
                        });
                    }
                })
                .catch((err) => {
                    this.props.handleApiFail(err, 'loadUserRoleList', true, () => this.loadUserRoleList(forceLoad));
                    if(err && !err.cancelled) {
                        this.setState({
                            loadingUserRoles: 'error'
                        });
                    }
                });
        }
    };

    render() {
        return (
            <StoreUserContext.Provider value={this.state}>
                {this.props.children}
            </StoreUserContext.Provider>
        );
    }
}

const storeUser = withContextConsumer(StoreUserContext.Consumer);

const StoreUserProvider = cancelPromise(withApiControl(withPusher(StoreUserProviderBase)));

export {StoreUserProvider, storeUser};