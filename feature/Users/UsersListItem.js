import React from 'react';
import posed from 'react-pose';
import {MdCreate} from "react-icons/md";

import {useTheme, createUseStyles} from '../../lib/theme';
import {withBackend} from "../BackendConnect";
import UserEdit from "./UserEdit";

const useStyles = createUseStyles(theme => ({
    item: {
        '& td:first-child': {
            borderLeft: '4px solid transparent',
            transition: 'border-left ' + theme.transition
        },
        '&.active td:first-child': {
            borderLeft: '4px solid ' + theme.accent.blue
        }
    },
    cell: {
        padding: '2px 6px',
        '&:first-child': {
            paddingLeft: '6px',
        },
        '&:last-child': {
            paddingRight: 0
        }
    },
    itemDetail: {
        '& td': {
            borderTop: '1px solid transparent',
            paddingTop: '6px',
            transition: 'border-left ' + theme.transition + ', border-bottom ' + theme.transition
        },
        '&.active td': {
            borderTop: '1px solid ' + theme.textColorLight
        },
    },
    itemDetailCell: {
        overflow: 'hidden',
        '&:first-child': {
            paddingLeft: '6px',
        },
        '&:last-child': {
            paddingRight: '6px',
        },
    },
    detailTop: {
        display: 'flex',
    },
    detailCustomer: {
        display: 'flex',
        flexDirection: 'column',
        '& p': {
            margin: '0 0 3px 0'
        }
    },
    detailShipping: {
        display: 'flex',
    },
    detailProducts: {},
    detailProductsItem: {
        display: 'flex'
    },
    detailProductsItemQty: {
        fontWeight: 'bold',
        margin: '0 6px 0 0'
    },
    detailProductsItemDetail: {
        '& p': {
            margin: '0 0 6px 0'
        }
    },
    detailStateLog: {},
    detailStateLogTransition: {
        textAlign: 'center',
        margin: '3px 0',
        padding: '6px 0',
    },
    detailStateLogTransitionTime: {
        margin: '0 0 3px 0',
    },
    detailStateLogTransitionTrans: {
        margin: 0
    }
}));


const ItemDetail = posed.div({
    hidden: {
        height: 0,
        marginBottom: 0,
    },
    visible: {
        height: 'auto',
        marginBottom: '24px',
    }
});

const UsersListItemBase = (props) => {
    const {
        t, showDetails, toggleShowDetails,
        active: act, colLength, item,
    } = props;
    const theme = useTheme();
    const classes = useStyles(theme);

    let active = false;
    if(Array.isArray(act) && act.includes(item.id)) {
        active = true;
    } else if(act && act === item.id) {
        active = true;
    }

    return (
        <React.Fragment>
            <tr className={classes.item + (showDetails || active ? ' active' : '')}>
                <td className={classes.cell}>
                    {item.name}
                </td>
                <td className={classes.cell}>
                    <ul style={{display: 'flex', flexWrap: 'wrap', margin: 0}}>
                        {item.roles ? item.roles.map((role, i) => (
                            <li key={role}
                                style={{
                                    paddingRight: '3px',
                                    listStyleType: 'none'
                                }}
                            >{role}{item.roles.length > (i + 1) ? ',' : ''}</li>
                        )) : '-'}
                    </ul>
                </td>
                <td className={classes.cell}>
                    {item.password ? t('list.item.yes') : t('list.item.no')}
                </td>
                <td className={classes.cell}>
                    {
                        // active is transmitted as string, so '0' equals to true and not false
                        // eslint-disable-next-line
                        item.active == 1 ? t('list.item.yes') : t('list.item.no')
                    }
                </td>
                <td className={classes.cell} style={{width: '1.5em'}}>
                    <button onClick={toggleShowDetails} style={{padding: 3}}><MdCreate size={'1.25em'} color={theme.textColor}/></button>
                </td>
            </tr>
            <tr className={classes.item + ' ' + classes.itemDetail + (showDetails || active ? ' active' : '')}>
                <td colSpan={colLength} style={{borderBottom: (showDetails || active ? '4px solid ' + theme.bgPage : '0')}}>
                    <ItemDetail className={classes.itemDetailCell} withParent={false} pose={active || showDetails ? 'visible' : 'hidden'}>
                        {active || showDetails ?
                            <UserEdit user={item.id}/> :
                            null}
                    </ItemDetail>
                </td>
            </tr>
        </React.Fragment>
    )
};

class UsersListItem extends React.PureComponent {

    state = {
        showDetails: false
    };

    render() {

        return <UsersListItemBase
            {...this.props}
            showDetails={this.state.showDetails}
            toggleShowDetails={() => this.setState({showDetails: !this.state.showDetails})}/>
    };
}

export default withBackend(UsersListItem);
