import React from 'react';
import {withNamespaces} from 'react-i18next';
import {MdAdd} from 'react-icons/md';
import posed from 'react-pose';
import {cancelPromise} from "../../component/cancelPromise";
import {DataGrid} from "../../component/DataGrid/DataGrid";
import UsersListItem from "./UsersListItem";
import {ButtonSmall} from "../../component/Form";
import {withBackend} from "../BackendConnect";
import {withAuthPermission} from "../AuthPermission";
//import {FEATURE_USER} from "../../permissions";
import {ContainerStyle, SplitScreen} from "../../component/SplitScreen";
import UserEdit from './UserEdit';
import {storeUser} from "./Stores/User";

const AddUser = posed.div({
    hidden: {
        height: 0,
        marginBottom: 0,
    },
    visible: {
        height: 'min-content',
        marginBottom: '24px',
    }
});

const initialState = {
    showCreate: false,
};

class Users extends React.PureComponent {

    state = initialState;

    componentDidMount() {
        const {
            changeStyleInnerWrapper
        } = this.props;

        changeStyleInnerWrapper(ContainerStyle);
        this.fetch();
    }

    componentWillUnmount() {
        this.setState(initialState);
        this.props.changeStyleInnerWrapper({});
    }

    fetch(force = false) {
        this.props.loadUserList(force);
    }

    showCreate = (p) => <ButtonSmall onClick={() => this.setState({showCreate: !this.state.showCreate})}
                                     style={{marginRight:6}}
                                     active={this.state.showCreate}><MdAdd style={{verticalAlign: 'bottom'}}/> {this.props.t('create-user-btn')}</ButtonSmall>;

    element = (p) => <UsersListItem t={this.props.t} {...p}/>;

    render() {
        const {
            t,
            users, loadingUsers,
        } = this.props;

        return (
            <React.Fragment>
                <h1>{t('title')}</h1>

                <SplitScreen

                    isTopOpen={this.state.showCreate}
                    top={(
                        <AddUser withParent={false} pose={this.state.showCreate ? 'visible' : 'hidden'} style={{
                            overflow: this.state.showCreate ? null : 'hidden', display: 'flex', flexDirection: 'column'
                        }}>
                            <h2 style={{margin: '0 0 3px 0'}}>{t('create-user')}</h2>
                            {this.state.showCreate ?
                                <UserEdit create={true}/> :
                                null}
                        </AddUser>
                    )}

                    center={(
                        <DataGrid

                            active={this.props.match.params.user}
                            activeFilter={'id'}

                            headers={[
                                {
                                    lbl: t('list.head.username'),
                                    search: 'name'
                                },
                                {
                                    lbl: t('list.head.roles')
                                },
                                {
                                    lbl: t('list.head.has-password')
                                },
                                {
                                    lbl: t('list.head.active')
                                },
                                {lbl: ''},
                            ]}
                            colStyle={[
                                {width: '50%'},
                                {width: '50%'}
                            ]}
                            emptyList={t('list.errors.no-users')}
                            loadingLabel={t('api.loading-user-list')}
                            list={users}
                            loading={loadingUsers}
                            controls={[this.showCreate]}
                            reload={() => this.fetch(true)}
                            element={this.element}
                        />
                    )}
                />
            </React.Fragment>
        )
    };
}

export default withNamespaces('page--user')(cancelPromise(withAuthPermission(withBackend(storeUser(Users)))));
