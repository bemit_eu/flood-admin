import React from 'react';
import {withNamespaces} from 'react-i18next';
import {withRouter} from 'react-router-dom';
import {i18n} from "../../lib/i18n";
import {withTimeout} from "@formanta/react";

import {withTheme} from '../../lib/theme';
import {Button} from '../../component/Form';

import {withPusher} from "../../component/Pusher/Pusher";
import {storeUser} from "./Stores/User";
import {cancelPromise} from "../../component/cancelPromise";
import Loading from "../../component/Loading";
import ButtonInteractiveDelete from "../../component/ButtonInteractiveDelete";

class UserDelete extends React.Component {
    state = {
        deleting: false,
    };

    pushDeleteUser = () => {
        const {
            user,
            deleteUser, cancelPromise, setTimeout,
            history
        } = this.props;

        cancelPromise(deleteUser(user))
            .then((data) => {
                if(data.error) {
                    this.setState({deleting: 'error'});
                    return;
                }

                this.setState({
                    deleting: 'success'
                });

                history.push('/' + i18n.languages[0] + '/users');

                setTimeout(() => {
                    this.setState({deleting: false});
                }, 4000);
            })
            .catch((err) => {
                if(err && !err.cancelled) {
                    this.setState({deleting: 'error'});
                }
            });
    };

    render() {
        const {
            t, theme,
        } = this.props;

        return (
            <React.Fragment>
                <ButtonInteractiveDelete
                    onClick={this.pushDeleteUser}
                    comp={(p) => (
                        <Button {...p} style={{color: theme.textColor}} type={'button'}>{t('edit.btn-delete')} {p.children} {('progress' === this.state.deleting || 'error' === this.state.deleting ?
                            <Loading style={{
                                wrapper: {margin: 0},
                                item: ('error' === this.state.deleting ? {borderColor: theme.errorColor} : {}),
                            }}/> :
                            null)}</Button>
                    )}/>
            </React.Fragment>
        )
    };
}

export default withNamespaces('page--user')(withPusher(withTimeout(withRouter(cancelPromise(storeUser(withTheme(UserDelete)))))));
