import React from 'react';
import StyleGrid from '../lib/StyleGrid';
import {useTheme, createUseStyles} from '../lib/theme';

const useStyles = createUseStyles((theme) => {
    if(!theme) return {};

    return {
        '@global': {
            body: {
                background: theme.bgPage,
                transition: theme.transition + ' background'
            },
            '#root': {
                display: 'flex',
                flexDirection: 'column',
                minHeight: '100vh',
                height: '100vh',
                [StyleGrid.smUp]: {}
            },
            html: {
                color: theme.textColor,
                transition: theme.transition + ' color'
            },
            button: {
                cursor: 'pointer'
            },
            'h1, h2, h3, h4, h5, h6': {
                // margin top must be here and not in scss as it wouldn't overwrite normalize.css from scss settings
                marginTop: 0
            },
            a: {
                color: theme.linkColor,
                textDecoration: 'none',
                transition: '0.4s linear color',
                '&:hover': {
                    textDecoration: 'underline'
                }
            },
            //
            // Editor
            '.DraftEditor-root': {},
            '.DraftEditor-root .public-DraftEditorPlaceholder-root': {
                padding: '6px',
            },
            '.DraftEditor-root .public-DraftEditorPlaceholder-root.public-DraftEditorPlaceholder-hasFocus': {},
            '.DraftEditor-root .DraftEditor-editorContainer': {
                padding: '6px',
                transition: theme.transition + ' background',
                background: theme.input.background,
                //border: '1px solid ' + theme.textColorLight
            },
            // Editor Emoji
            '.draftJsEmojiPlugin__emojiSelectPopover__1J1s0': {
                background: theme.bgPage,
                border: '1px solid ' + theme.textColorLight
            },
            '.draftJsEmojiPlugin__emojiSelectButton__3sPol': {
                color: theme.textColor
            },
            '.draftJsEmojiPlugin__emojiSelectPopoverNavEntry__1OiGB, .draftJsEmojiPlugin__emojiSelectButtonPressed__2Tezu': {
                color: theme.textColor
            },
            '.draftJsEmojiPlugin__emojiSelectButtonPressed__2Tezu': {
                background: theme.btnActive
            },
            '.draftJsEmojiPlugin__emojiSelectPopoverNavEntryActive__2j-Vk': {
                background: theme.btnActive,
                color: theme.textColor
            },
            '.draftJsEmojiPlugin__emojiSelectPopoverScrollbarThumb__jGYdG': {
                background: theme.btnActive
            }
        },
        glob: {}
    }
});

/**
 * Inject Global styles through an hack which uses a prop that renders an empty react fragmet, as fragments can not be styled but also global styles need one component to attach to
 * @return {*}
 * @constructor
 */
const GlobalStyle = () => {
    const theme = useTheme();
    const classes = useStyles(theme);

    const Styler = (() => (
        <React.Fragment/>
    ));
    return (
        <Styler className={classes.glob}/>
    );
};

export default GlobalStyle;
