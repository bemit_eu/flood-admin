import React from 'react';
import Loadable from 'react-loadable';
import LoadingAnimation from './component/Loading';
import {withNamespaces} from './lib/i18n';
import {FAIL_DISPLAY, needsPermission} from "./feature/AuthPermission";
import {
    FEATURE_CONTENT,
    FEATURE_MARKETING,
    FEATURE_MARKETING_NEWSLETTER,
    FEATURE_MEDIA,
    FEATURE_MESSAGE,
    FEATURE_SHOP,
    FEATURE_SHOP_GROUP,
    FEATURE_SHOP_ORDER,
    FEATURE_SHOP_PRODUCT,
    FEATURE_SHOP_SETTING,
    FEATURE_USER,
    PERM_ADMIN
} from "./permissions";
import {StoreSectionProvider} from "./feature/Content/Stores/Section";
import {StoreArticleProvider} from "./feature/Content/Stores/Article";
import {StoreSchemaProvider} from "./feature/Content/Stores/Schema";
import {StoreBlockProvider} from "./feature/Content/Stores/Block";
import {StorePageTypeProvider} from "./feature/Content/Stores/PageType";
import {StoreGroupProvider} from "./feature/Shop/Stores/Group";
import {StoreProductTypeProvider} from "./feature/Shop/Stores/ProductTypes";
import {StoreProductProvider} from "./feature/Shop/Stores/Product";
import {StoreOrderProvider} from "./feature/Shop/Stores/Order";
import {StoreNewsletterUserProvider} from "./feature/Marketing/Stores/User";
import {StoreUserProvider} from "./feature/Users/Stores/User";
import {StoreMessageProvider} from "./feature/Message/Stores/Message";
import {StoreWarehouseProvider} from "./feature/Shop/Stores/Warehouse";
import {StoreStockProvider} from "./feature/Shop/Stores/Stock";
import {StoreStockItemProvider} from "./feature/Shop/Stores/StockItem";
import {StoreAnalyzeProvider} from "./feature/Analyze/Stores/Analyze";
import {StoreAnalyzeDiagramsProvider} from "./feature/Analyze/Stores/AnalyzeDiagrams";

const Loading = withNamespaces('common')((props) => <LoadingAnimation {...props}>{props.label ? <p style={{textAlign: 'center', marginBottom: 0}}>{props.t('loading.' + props.label)}</p> : null}</LoadingAnimation>);

/**
 * Simple Content Pages with Header, Content and Footer
 */
const routesPage = (pages) => ({
    main: '/:lng/(auth|info)',
    routes: [
        {
            path: '/:lng/auth',
            component: Loadable({
                loader: () => import('./layout/Page/Auth'),
                loading: (props) => <Loading {...props} name='page/Auth' label={'auth'}/>,
            }),
        }, {
            path: '/:lng/info',
            component: pages.info
        },
    ]
});

const routesControlPanel = (pages) => ({
    main: '/:lng/(dashboard|content|users|messages|shop|marketing|media|analyze|blocks|connections|settings)',
    routes: [
        {
            path: '/:lng/dashboard',
            content: needsPermission([PERM_ADMIN])(Loadable({
                loader: () => import('./layout/ControlPanel/Dashboard'),
                loading: (props) => <Loading {...props} name='Dashboard' label={'page'}/>,
            }))
        },
        {
            path: '/:lng/messages/:message?/:action?',
            provider: (props) => (
                <StoreMessageProvider {...props}/>
            ),
            content: needsPermission([PERM_ADMIN, FEATURE_MESSAGE])(Loadable({
                loader: () => import('./feature/Message/Overview'),
                loading: (props) => <Loading {...props} name='Messages/Overview' label={'page'}/>,
            })),
        },
        {
            path: '/:lng/content',
            provider: (props) => (
                <StorePageTypeProvider>
                    <StoreBlockProvider>
                        <StoreSchemaProvider>
                            <StoreArticleProvider>
                                <StoreSectionProvider {...props}/>
                            </StoreArticleProvider>
                        </StoreSchemaProvider>
                    </StoreBlockProvider>
                </StorePageTypeProvider>
            ),
            sidebar: needsPermission([PERM_ADMIN, FEATURE_CONTENT])(Loadable({
                loader: () => import('./feature/Content/SectionList'),
                loading: (props) => <Loading {...props} name='Content/SectionList'/>,
            })),
            content: needsPermission([PERM_ADMIN, FEATURE_CONTENT], FAIL_DISPLAY)(Loadable({
                loader: () => import('./feature/Content/SectionOverview'),
                loading: (props) => <Loading {...props} name='Content/OverviewSection' label={'page'}/>,
            })),
            routes: [
                {
                    path: '/:lng/content/:section',
                    sidebar: needsPermission([PERM_ADMIN, FEATURE_CONTENT])(Loadable({
                        loader: () => import('./feature/Content/SectionArticleList'),
                        loading: (props) => <Loading {...props} name='Content/SectionArticleList'/>,
                    })),
                    content: needsPermission([PERM_ADMIN, FEATURE_CONTENT], FAIL_DISPLAY)(Loadable({
                        loader: () => import('./feature/Content/SectionOverview'),
                        loading: (props) => <Loading {...props} name='Content/OverviewSection' label={'page'}/>,
                    })),
                    routes: [
                        {
                            path: '/:lng/content/:section/:article',
                            content: needsPermission([PERM_ADMIN, FEATURE_CONTENT], FAIL_DISPLAY)(Loadable({
                                loader: () => import('./feature/Content/ArticleDetail'),
                                loading: (props) => <Loading {...props} name='Content/ArticleDetail' label={'page'}/>,
                            })),
                        }
                    ]
                }
            ]
        },
        {
            path: '/:lng/users/:user?',
            provider: (props) => (
                <StoreUserProvider {...props}/>
            ),
            content: needsPermission([PERM_ADMIN, FEATURE_USER])(Loadable({
                loader: () => import('./feature/Users/Overview'),
                loading: (props) => <Loading {...props} name='Users/Overview' label={'page'}/>,
            })),
        },
        {
            path: '/:lng/shop',
            provider: (props) => (
                <StoreStockProvider>
                    <StoreWarehouseProvider>
                        <StoreProductTypeProvider>
                            <StoreSchemaProvider>
                                <StoreOrderProvider>
                                    <StoreGroupProvider>
                                        <StoreStockItemProvider>
                                            <StoreProductProvider {...props}/>
                                        </StoreStockItemProvider>
                                    </StoreGroupProvider>
                                </StoreOrderProvider>
                            </StoreSchemaProvider>
                        </StoreProductTypeProvider>
                    </StoreWarehouseProvider>
                </StoreStockProvider>
            ),
            sidebar: needsPermission([PERM_ADMIN, FEATURE_SHOP])(Loadable({
                loader: () => import('./feature/Shop/NavShop'),
                loading: (props) => <Loading {...props} name='Shop/NavShop'/>,
            })),
            content: needsPermission([PERM_ADMIN, FEATURE_SHOP], FAIL_DISPLAY)(Loadable({
                loader: () => import('./feature/Shop/Overview'),
                loading: (props) => <Loading {...props} name='Shop/Overview' label={'page'}/>,
            })),
            routes: [
                {
                    path: '/:lng/shop/scan',
                    content: needsPermission([PERM_ADMIN, FEATURE_SHOP_GROUP], FAIL_DISPLAY)(Loadable({
                        loader: () => import('./feature/Shop/ShopScan'),
                        loading: (props) => <Loading {...props} name='Shop/ShopScan' label={'page'}/>,
                    }))
                }, {
                    path: '/:lng/shop/settings',
                    sidebar: needsPermission([PERM_ADMIN, FEATURE_SHOP_SETTING])(Loadable({
                        loader: () => import('./feature/Shop/NavShopSettings'),
                        loading: (props) => <Loading {...props} name='Shop/NavShopSettings'/>,
                    })),
                    content: needsPermission([PERM_ADMIN, FEATURE_SHOP_SETTING], FAIL_DISPLAY)(Loadable({
                        loader: () => import('./feature/Shop/Settings'),
                        loading: (props) => <Loading {...props} name='Shop/Settings' label={'page'}/>,
                    }))
                }, {
                    path: '/:lng/shop/orders',
                    sidebar: needsPermission([PERM_ADMIN, FEATURE_SHOP_ORDER])(Loadable({
                        loader: () => import('./feature/Shop/NavShopOrders'),
                        loading: (props) => <Loading {...props} name='Shop/NavShopOrders'/>,
                    })),
                    content: needsPermission([PERM_ADMIN, FEATURE_SHOP_ORDER], FAIL_DISPLAY)(Loadable({
                        loader: () => import('./feature/Shop/Orders'),
                        loading: (props) => <Loading {...props} name='Shop/Orders' label={'page'}/>,
                    })),
                    routes: [
                        {
                            path: '/:lng/shop/orders/:state',
                            content: needsPermission([PERM_ADMIN, FEATURE_SHOP_ORDER], FAIL_DISPLAY)(Loadable({
                                loader: () => import('./feature/Shop/Orders'),
                                loading: (props) => <Loading {...props} name='Shop/Orders' label={'page'}/>,
                            })),
                        }
                    ]
                },
                {
                    path: '/:lng/shop/products',
                    sidebar: needsPermission([PERM_ADMIN, FEATURE_SHOP_GROUP])(Loadable({
                        loader: () => import('./feature/Shop/GroupList'),
                        loading: (props) => <Loading {...props} name='Shop/GroupList'/>,
                    })),
                    content: needsPermission([PERM_ADMIN, FEATURE_SHOP_GROUP], FAIL_DISPLAY)(Loadable({
                        loader: () => import('./feature/Shop/GroupOverview'),
                        loading: (props) => <Loading {...props} name='Shop/GroupDetails' label={'page'}/>,
                    })),
                    routes: [
                        {
                            path: '/:lng/shop/products/:group',
                            sidebar: needsPermission([PERM_ADMIN, FEATURE_SHOP_PRODUCT])(Loadable({
                                loader: () => import('./feature/Shop/ProductList'),
                                loading: (props) => <Loading {...props} name='Shop/ProductList'/>,
                            })),
                            content: needsPermission([PERM_ADMIN, FEATURE_SHOP_GROUP], FAIL_DISPLAY)(Loadable({
                                loader: () => import('./feature/Shop/GroupOverview'),
                                loading: (props) => <Loading {...props} name='Shop/GroupDetails' label={'page'}/>,
                            })),
                            routes: [
                                {
                                    path: '/:lng/shop/products/:group/:product',
                                    content: needsPermission([PERM_ADMIN, FEATURE_SHOP_PRODUCT], FAIL_DISPLAY)(Loadable({
                                        loader: () => import('./feature/Shop/ProductDetail'),
                                        loading: (props) => <Loading {...props} name='Shop/ProductDetail' label={'page'}/>,
                                    })),
                                }
                            ]
                        }
                    ]
                },
                {
                    path: '/:lng/shop/inventory',
                    content: needsPermission([PERM_ADMIN, FEATURE_SHOP_GROUP], FAIL_DISPLAY)(Loadable({
                        loader: () => import('./feature/Shop/Inventory'),
                        loading: (props) => <Loading {...props} name='Shop/Inventory' label={'page'}/>,
                    })),
                    routes: [{
                        path: '/:lng/shop/inventory/warehouse/:warehouse',
                        sidebar: needsPermission([PERM_ADMIN, FEATURE_SHOP_PRODUCT])(Loadable({
                            loader: () => import('./feature/Shop/ProductList'),
                            loading: (props) => <Loading {...props} name='Shop/ProductList'/>,
                        })),
                        content: needsPermission([PERM_ADMIN, FEATURE_SHOP_GROUP], FAIL_DISPLAY)(Loadable({
                            loader: () => import('./feature/Shop/GroupOverview'),
                            loading: (props) => <Loading {...props} name='Shop/GroupDetails' label={'page'}/>,
                        })),
                        routes: [
                            {
                                path: '/:lng/shop/inventory/warehouse/:warehouse/stock/:stock',
                                content: needsPermission([PERM_ADMIN, FEATURE_SHOP_PRODUCT], FAIL_DISPLAY)(Loadable({
                                    loader: () => import('./feature/Shop/ProductDetail'),
                                    loading: (props) => <Loading {...props} name='Shop/ProductDetail' label={'page'}/>,
                                })),
                            }, {
                                path: '/:lng/shop/inventory/products/products',
                                content: needsPermission([PERM_ADMIN, FEATURE_SHOP_PRODUCT], FAIL_DISPLAY)(Loadable({
                                    loader: () => import('./feature/Shop/ProductDetail'),
                                    loading: (props) => <Loading {...props} name='Shop/ProductDetail' label={'page'}/>,
                                })),
                            }
                        ]
                    }
                    ]
                }
            ]
        },
        {
            path: '/:lng/marketing',
            provider: (props) => (
                <StoreNewsletterUserProvider {...props}/>
            ),
            sidebar: needsPermission([PERM_ADMIN, FEATURE_MARKETING])(Loadable({
                loader: () => import('./feature/Marketing/Nav'),
                loading: (props) => <Loading {...props} name='Marketing/Nav'/>,
            })),
            content: needsPermission([PERM_ADMIN, FEATURE_MARKETING], FAIL_DISPLAY)(Loadable({
                loader: () => import('./feature/Marketing/Overview'),
                loading: (props) => <Loading {...props} name='Marketing/Overview' label={'page'}/>,
            })),
            routes: [
                {
                    path: '/:lng/marketing/newsletter',
                    sidebar: needsPermission([PERM_ADMIN, FEATURE_MARKETING_NEWSLETTER])(Loadable({
                        loader: () => import('./feature/Marketing/NavNewsletter'),
                        loading: (props) => <Loading {...props} name='Marketing/NavNewsletter'/>,
                    })),
                    content: needsPermission([PERM_ADMIN, FEATURE_MARKETING_NEWSLETTER], FAIL_DISPLAY)(Loadable({
                        loader: () => import('./feature/Marketing/Overview'),
                        loading: (props) => <Loading {...props} name='Marketing/Overview' label={'page'}/>,
                    })),
                    routes: [
                        {
                            path: '/:lng/marketing/newsletter/user',
                            content: needsPermission([PERM_ADMIN, FEATURE_MARKETING_NEWSLETTER], FAIL_DISPLAY)(Loadable({
                                loader: () => import('./feature/Marketing/NewsletterUser'),
                                loading: (props) => <Loading {...props} name='Marketing/NewsletterUser' label={'page'}/>,
                            }))
                        },
                        {
                            path: '/:lng/marketing/newsletter/create',
                            content: needsPermission([PERM_ADMIN, FEATURE_MARKETING_NEWSLETTER], FAIL_DISPLAY)(Loadable({
                                loader: () => import('./feature/Marketing/NewsletterOverview'),
                                loading: (props) => <Loading {...props} name='Marketing/NewsletterCreate' label={'page'}/>,
                            }))
                        },
                        {
                            path: '/:lng/marketing/newsletter/history',
                            content: needsPermission([PERM_ADMIN, FEATURE_MARKETING_NEWSLETTER], FAIL_DISPLAY)(Loadable({
                                loader: () => import('./feature/Marketing/NewsletterOverview'),
                                loading: (props) => <Loading {...props} name='Marketing/NewsletterHistory' label={'page'}/>,
                            }))
                        },
                    ]
                }, {
                    path: '/:lng/marketing/campaign',
                    sidebar: needsPermission([PERM_ADMIN, FEATURE_MARKETING])(Loadable({
                        loader: () => import('./feature/Marketing/NavCampaign'),
                        loading: (props) => <Loading {...props} name='Marketing/NavCampaign'/>,
                    })),
                    content: needsPermission([PERM_ADMIN, FEATURE_MARKETING_NEWSLETTER], FAIL_DISPLAY)(Loadable({
                        loader: () => import('./feature/Marketing/Overview'),
                        loading: (props) => <Loading {...props} name='Marketing/Overview' label={'page'}/>,
                    }))
                }
            ]
        },
        {
            path: '/:lng/media',
            content: needsPermission([PERM_ADMIN, FEATURE_MEDIA], FAIL_DISPLAY)(Loadable({
                loader: () => import('./feature/Media/Overview'),
                loading: (props) => <Loading {...props} name='Media/Overview' label={'page'}/>,
            }))
        },
        {
            path: '/:lng/analyze/:event?',
            provider: (props) => (
                <StoreAnalyzeProvider>
                    <StoreAnalyzeDiagramsProvider {...props}/>
                </StoreAnalyzeProvider>
            ),
            content: needsPermission([PERM_ADMIN], FAIL_DISPLAY)(Loadable({
                loader: () => import('./feature/Analyze/Overview'),
                loading: (props) => <Loading {...props} name='Analyze/Overview' label={'page'}/>,
            }))
        },
        {
            path: '/:lng/settings',
            content: Loadable({
                loader: () => import('./feature/Settings/Overview'),
                loading: (props) => <Loading {...props} name='Settings' label={'page'}/>,
            })
        },
        {
            path: '/:lng/(blocks|connections)',
            content: needsPermission([PERM_ADMIN], FAIL_DISPLAY)(Loadable({
                loader: () => import('./feature/Demo/Overview'),
                loading: (props) => <Loading {...props} name='Demo/Overview' label={'page'}/>,
            }))
        },
    ]
});

export {routesPage, routesControlPanel};